import { Injectable } from '@angular/core';
import {DocReader} from '../doc-reader/doc-reader.component';

@Injectable()
export class DocReaderCacheService {
  private cache: Map<string, any> = new Map<string, any>();

  add(documentId: string, data: DocReader): void {
    this.cache.set(documentId, data);
  }

  get(documentId: string): DocReader {
    if (this.cache.has(documentId)) {
      return this.cache.get(documentId);
    }
    return null;
  }
}
