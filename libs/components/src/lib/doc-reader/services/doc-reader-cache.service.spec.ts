import { TestBed } from '@angular/core/testing';

import { DocReaderCacheService } from './doc-reader-cache.service';

describe('DocReaderCacheService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [DocReaderCacheService]
  }));

  it('should be created', () => {
    const service: DocReaderCacheService = TestBed.get(DocReaderCacheService);
    expect(service).toBeTruthy();
  });
});
