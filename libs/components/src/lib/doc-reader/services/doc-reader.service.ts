import {ComponentRef, Injectable, Injector} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal, PortalInjector} from '@angular/cdk/portal';
import {DocReaderOverlayRef} from '../doc-reader-overlay-ref';
import {DocReader, DocReaderComponent} from '../doc-reader/doc-reader.component';
import {DOC_READER_DATA} from '../doc-reader-overlay.tokens';
import {Observable} from "rxjs";

export interface DocReaderData {
  dataSource: Observable<DocReader>;
  /**
   * This property should be unique for different docs. It is used for caching responses.
   */
  id?: string;
}

export interface DocReaderConfig {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
  docReader?: DocReaderData;
}

export const DEFAULT_CONFIG: DocReaderConfig = {
  hasBackdrop: true,
  backdropClass: 'dark-backdrop',
  panelClass: 'doc-reader',
  docReader: null
};

@Injectable()
export class DocReaderService {
  constructor(private injector: Injector,
              private overlay: Overlay) {
  }

  open(config: DocReaderConfig) {

    const docReaderConfig = {...DEFAULT_CONFIG, ...config};

    const overlayRef = this.createOverlay(docReaderConfig);

    const docReaderRef = new DocReaderOverlayRef(overlayRef);

    docReaderRef.componentInstance = this.attachDocReaderContainer(overlayRef, docReaderConfig, docReaderRef);

    return docReaderRef;

  }

  private createOverlay(config: DocReaderConfig) {
    return this.overlay.create(this.getOverlayConfig(config));
  }

  private attachDocReaderContainer(overlayRef: OverlayRef, config: DocReaderConfig, docReaderRef: DocReaderOverlayRef) {
    const injector = this.createInjector(config, docReaderRef);

    const containerPortal = new ComponentPortal(DocReaderComponent, null, injector);
    const containerRef: ComponentRef<DocReaderComponent> = overlayRef.attach(containerPortal);

    return containerRef.instance;
  }

  private createInjector(config: DocReaderConfig, docReaderRef: DocReaderOverlayRef): PortalInjector {
    const injectionTokens = new WeakMap();

    injectionTokens.set(DocReaderOverlayRef, docReaderRef);
    injectionTokens.set(DOC_READER_DATA, config.docReader);

    return new PortalInjector(this.injector, injectionTokens);
  }

  private getOverlayConfig(config: DocReaderConfig): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    return new OverlayConfig({
      hasBackdrop: config.hasBackdrop,
      backdropClass: config.backdropClass,
      panelClass: config.panelClass,
      scrollStrategy: this.overlay.scrollStrategies.block(),
      height: '100%',
      width: '100%',
      positionStrategy
    });
  }

}
