import {Overlay} from '@angular/cdk/overlay';
import {Injector} from '@angular/core';
import {TestBed} from '@angular/core/testing';

import {DocReaderService} from './doc-reader.service';
import createSpy = jasmine.createSpy;

describe('DocReaderService', () => {
  let service: DocReaderService;
  const overlayMock = {create: createSpy(), position: createSpy(), scrollStrategies: {block: createSpy()}};

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          DocReaderService,
          {provide: Overlay, useValue: overlayMock},
          {provide: Injector, useValue: {}}
        ]
      }
    );

    service = TestBed.get(DocReaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open doc reader', () => {
    const attachSpy = createSpy().and.returnValue({instance: null});
    const centerVerticallySpy = createSpy();
    overlayMock.create.and.returnValue({attach: attachSpy});
    overlayMock.position.and.returnValue({
      global: () => ({
        centerHorizontally: () => ({
          centerVertically: centerVerticallySpy
        })
      })
    });
    const config = {
      docReader: {
        documentId: 'FAKE_123_DOCUMENT_321_ID'
      }
    };

    service.open(config);

    expect(overlayMock.create).toHaveBeenCalledWith(jasmine.objectContaining({
      hasBackdrop: true,
      backdropClass: 'dark-backdrop',
      panelClass: 'doc-reader',
      height: '100%',
      width: '100%',
    }));
    expect(overlayMock.scrollStrategies.block).toHaveBeenCalled();
    expect(attachSpy).toHaveBeenCalled();
    expect(centerVerticallySpy).toHaveBeenCalled();
  });
});
