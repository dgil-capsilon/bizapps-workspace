import {InjectionToken} from '@angular/core';
import {DocReaderData} from './services/doc-reader.service';

export const DOC_READER_DATA = new InjectionToken<DocReaderData>('DOC_READER_DATA');
