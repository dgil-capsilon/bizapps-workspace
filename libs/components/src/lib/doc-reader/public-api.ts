export * from './doc-reader.module';
export * from './doc-reader/doc-reader.component';
export * from './services/doc-reader.service';
export * from './doc-reader-overlay-ref';
export * from './doc-reader-overlay.tokens'
