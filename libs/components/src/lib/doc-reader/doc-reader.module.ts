import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocReaderComponent} from './doc-reader/doc-reader.component';
import {DocReaderService} from './services/doc-reader.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {DocReaderCacheService} from './services/doc-reader-cache.service';

@NgModule({
  declarations: [DocReaderComponent],
  providers: [DocReaderService, DocReaderCacheService],
  imports: [
    CommonModule,
    OverlayModule
  ],
  entryComponents: [DocReaderComponent]
})
export class DocReaderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DocReaderModule,
      providers: [DocReaderService]
    };
  }
}
