import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By, DomSanitizer} from '@angular/platform-browser';
import {of} from 'rxjs';
import {DataService} from '@app/services/data.service';
import {DocReaderOverlayRef} from '../doc-reader-overlay-ref';
import {DOC_READER_DATA} from '../doc-reader-overlay.tokens';
import {DocReaderCacheService} from '../services/doc-reader-cache.service';
import {DocReader, DocReaderComponent} from './doc-reader.component';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('DocReaderComponent', () => {
  let component: DocReaderComponent;
  let fixture: ComponentFixture<DocReaderComponent>;
  const docReaderOverlayMock: SpyObj<DocReaderOverlayRef> = createSpyObj(['close']);
  const docReaderDataMock = {documentId: null};
  const domSanitizerMock: SpyObj<DomSanitizer> = createSpyObj(['sanitize', 'bypassSecurityTrustResourceUrl']);
  const dataServiceMock: SpyObj<DataService> = createSpyObj(['docReaderData']);
  const cacheServiceMock: SpyObj<DocReaderCacheService> = createSpyObj(['get', 'add']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocReaderComponent],
      providers: [
        {provide: DocReaderOverlayRef, useValue: docReaderOverlayMock},
        {provide: DOC_READER_DATA, useValue: docReaderDataMock},
        {provide: DomSanitizer, useValue: domSanitizerMock},
        {provide: DataService, useValue: dataServiceMock},
        {provide: DocReaderCacheService, useValue: cacheServiceMock}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    spyOn(window, 'atob');
    dataServiceMock.docReaderData.and.returnValue(of({}));

    fixture = TestBed.createComponent(DocReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load doc (no cache)', () => {
    cacheServiceMock.get.and.returnValue(null);
    domSanitizerMock.bypassSecurityTrustResourceUrl.and.returnValue('some url');

    component.ngOnInit();

    expect(cacheServiceMock.add).toHaveBeenCalled();
    expect(domSanitizerMock.bypassSecurityTrustResourceUrl).toHaveBeenCalled();
    expect(component.urlDoc).toBeDefined();
  });

  it('should load doc (cache)', () => {
    cacheServiceMock.get.and.returnValue({} as any);
    domSanitizerMock.bypassSecurityTrustResourceUrl.and.returnValue('some url');
    dataServiceMock.docReaderData.calls.reset();
    cacheServiceMock.add.calls.reset();

    component.ngOnInit();

    expect(component.urlDoc).toBeDefined();
    expect(domSanitizerMock.bypassSecurityTrustResourceUrl).toHaveBeenCalled();
    expect(cacheServiceMock.add).not.toHaveBeenCalled();
    expect(dataServiceMock.docReaderData).not.toHaveBeenCalled();
  });

  it('should send post message to doc reader', () => {
    const postMessageSpy = createSpy();
    component.iframe = {nativeElement: {contentWindow: {postMessage: postMessageSpy}}} as any;
    fixture.debugElement.query(By.css('#doc-reader')).triggerEventHandler('load', null);

    window.dispatchEvent(new MessageEvent('message', {
      data: '{"command":"getSomething","event":"DOC_READER_READY"}'
    }));

    expect(postMessageSpy).toHaveBeenCalled();
  });

  it('should close doc reader overlay', () => {
    component.iframe = {nativeElement: {contentWindow: null}} as any;
    fixture.debugElement.query(By.css('#doc-reader')).triggerEventHandler('load', null);

    window.dispatchEvent(new MessageEvent('message', {
      data: '{"command":"getSomething","event":"CLOSE_DOC_READER"}'
    }));

    expect(docReaderOverlayMock.close).toHaveBeenCalled();
  });

});
