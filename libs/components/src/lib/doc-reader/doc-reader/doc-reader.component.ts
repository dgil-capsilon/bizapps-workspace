import {Component, ElementRef, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {fromEvent, Subject} from 'rxjs';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DocReaderOverlayRef} from '../doc-reader-overlay-ref';
import {DOC_READER_DATA} from '../doc-reader-overlay.tokens';
import {DocReaderData} from '../services/doc-reader.service';
import {DocReaderCacheService} from '../services/doc-reader-cache.service';
import {takeUntil} from 'rxjs/operators';

export interface DocReader {
  getValidateApplicationAndSiteResult: any;
  getDocument: any;
  getApplicationTickets: any;
  getDirectLaunchConfiguration: any;
  documentReaderURL?: string;
}

const CLOSE = 'CLOSE_DOC_READER';
const EVENT = 'message';
const ESCAPE = 27;

@Component({
  selector: 'app-doc-reader',
  templateUrl: './doc-reader.component.html',
  styleUrls: ['./doc-reader.component.scss']
})
export class DocReaderComponent implements OnInit {
  @ViewChild('iframe', {static: false}) iframe: ElementRef<HTMLIFrameElement>;
  urlDoc: SafeResourceUrl;
  private response: DocReader;

  constructor(private docReaderOverlay: DocReaderOverlayRef,
              @Inject(DOC_READER_DATA) private docReaderData: DocReaderData,
              private domSanitizer: DomSanitizer,
              private cacheService: DocReaderCacheService) {
  }

  ngOnInit() {
    this.loadDoc();
  }

  @HostListener('window:keydown', ['$event'])
  public onKeyDown(event: KeyboardEvent): void {
    if (event.keyCode === ESCAPE) {
      this.docReaderOverlay.close();
    }
  }

  private loadDoc(): void {
    if (this.docReaderData.id) {
      this.loadFromCache(this.docReaderData.id);
    }

    this.docReaderData.dataSource.subscribe((response: DocReader) => {
      this.response = {
        documentReaderURL: response.documentReaderURL,
        getValidateApplicationAndSiteResult: atob(response.getValidateApplicationAndSiteResult),
        getDirectLaunchConfiguration: atob(response.getDirectLaunchConfiguration),
        getApplicationTickets: atob(response.getApplicationTickets),
        getDocument: atob(response.getDocument)
      };
      if (this.docReaderData.id) {
        this.cacheService.add(this.docReaderData.id, this.response);
      }
      this.urlDoc = this.domSanitizer.bypassSecurityTrustResourceUrl(this.response.documentReaderURL);
    });
  }

  getDocReader(): void {
    const destroyEvent$ = new Subject();
    fromEvent(window, EVENT).pipe(takeUntil(destroyEvent$)).subscribe((event: any) => {
      const parsedData = JSON.parse(event.data);
      const data = parsedData.command;
      if (data && this.iframe.nativeElement.contentWindow) {
        this.iframe.nativeElement.contentWindow.postMessage(this.response[data], '*');
      }
      if (parsedData.event === CLOSE) {
        destroyEvent$.next();
        destroyEvent$.complete();
        this.docReaderOverlay.close();
      }
    });
  }

  private loadFromCache(id: string): void {
    const data = this.cacheService.get(id);
    if (data) {
      this.response = data;
      this.urlDoc = this.domSanitizer.bypassSecurityTrustResourceUrl(this.response.documentReaderURL);
    }
  }

}
