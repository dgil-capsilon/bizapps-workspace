import {DocReaderComponent} from './doc-reader/doc-reader.component';
import {OverlayRef} from '@angular/cdk/overlay';

export class DocReaderOverlayRef {
  componentInstance: DocReaderComponent;

  constructor(private overlayRef: OverlayRef) {
  }

  close(): void {
    this.overlayRef.dispose();
  }
}
