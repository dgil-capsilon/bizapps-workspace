export interface ConfirmDialogData {
    title: string;
    contentText: string;
    cancelBtnText: string;
    confirmBtnText: string;
    confirmBtnId?: string;
    cancelBtnId?: string;
}
