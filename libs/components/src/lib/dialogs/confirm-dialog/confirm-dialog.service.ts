import { EventEmitter, Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogData } from './confirm-dialog-data.interface';
import { ConfirmDialogComponent } from './confirm-dialog.component';

@Injectable({
    providedIn: 'root'
})
export class ConfirmDialogService {
    confirm: EventEmitter<void>;
    cancel: EventEmitter<void>;

    constructor(private dialog: MatDialog) {
    }

    open(config: MatDialogConfig<ConfirmDialogData>): MatDialogRef<ConfirmDialogComponent> {
        const reasonDialogCompRef = this.dialog.open(ConfirmDialogComponent, {
            maxWidth: '520px',
            panelClass: 'confirm-dialog-panel',
            backdropClass: 'backdrop-dialog',
            ...config
        });

        this.confirm = reasonDialogCompRef.componentInstance.confirm;
        this.cancel = reasonDialogCompRef.componentInstance.cancel;

        return reasonDialogCompRef;
    }
}
