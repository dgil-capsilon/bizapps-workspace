import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogService} from './confirm-dialog.service';

describe('ConfirmDialogService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: MatDialog, useValue: {}}
            ]
        });
    });

    it('should be created', () => {
        const service: ConfirmDialogService = TestBed.get(ConfirmDialogService);
        expect(service).toBeTruthy();
    });
});
