import {Component, ElementRef, EventEmitter, Inject, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ConfirmDialogData} from './confirm-dialog-data.interface';

@Component({
    selector: 'iu-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
    // Text properties
    title: string;
    contentText: string;
    cancelBtnText: string;
    confirmBtnText: string;
    confirm = new EventEmitter<void>();
    cancel = new EventEmitter<void>();
    @ViewChild('confirmBtn', {static: true, read: ElementRef}) confirmBtnElRef: ElementRef<HTMLButtonElement>;
    @ViewChild('cancelBtn', {static: true, read: ElementRef}) cancelBtnElRef: ElementRef<HTMLButtonElement>;

    constructor(@Inject(MAT_DIALOG_DATA) private data: ConfirmDialogData,
                private renderer: Renderer2) {
        this.title = this.data.title;
        this.contentText = this.data.contentText;
        this.cancelBtnText = this.data.cancelBtnText;
        this.confirmBtnText = this.data.confirmBtnText;
    }

    private setElementIds() {
        const {confirmBtnId, cancelBtnId} = this.data;

        if (confirmBtnId) {
            this.renderer.setAttribute(this.confirmBtnElRef.nativeElement, 'id', confirmBtnId);
        }

        if (cancelBtnId) {
            this.renderer.setAttribute(this.cancelBtnElRef.nativeElement, 'id', cancelBtnId);
        }
    }

    ngOnInit() {
        this.setElementIds();
    }

    confirmForm() {
        this.confirm.emit();
    }

    cancelForm() {
        this.cancel.emit();
    }
}
