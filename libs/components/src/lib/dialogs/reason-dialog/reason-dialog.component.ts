import {Component, ElementRef, EventEmitter, Inject, OnInit, Renderer2, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ReasonDialogData} from './reason-dialog-data.interface';

@Component({
    selector: 'iu-reason-dialog',
    templateUrl: './reason-dialog.component.html',
    styleUrls: ['./reason-dialog.component.scss']
})
export class ReasonDialogComponent implements OnInit {
    form: FormGroup;
    // Text properties
    title: string;
    contentText: string;
    cancelBtnText: string;
    confirmBtnText: string;
    reasonLabelText: string;
    // Other properties
    maxReasonLength: number;
    showReasonLabel: boolean;
    reasonValue: string;
    confirm = new EventEmitter<string>();
    cancel = new EventEmitter<void>();
    @ViewChild('confirmBtn', {static: true, read: ElementRef}) confirmBtnElRef: ElementRef<HTMLButtonElement>;
    @ViewChild('cancelBtn', {static: true, read: ElementRef}) cancelBtnElRef: ElementRef<HTMLButtonElement>;

    constructor(@Inject(MAT_DIALOG_DATA) private data: ReasonDialogData,
                private renderer: Renderer2) {
        this.title = this.data.title;
        this.contentText = this.data.contentText;
        this.cancelBtnText = this.data.cancelBtnText;
        this.confirmBtnText = this.data.confirmBtnText;
        this.reasonLabelText = this.data.reasonLabelText;
        this.maxReasonLength = this.data.maxReasonLength;
        this.showReasonLabel = this.data.showReasonLabel;
        this.reasonValue = this.data.reasonValue;
    }

    get reasonLength() {
        return this.form.controls.reason.value.length;
    }

    ngOnInit() {
        this.createForm();

        if (this.reasonValue) {
            this.form.controls.reason.setValue(this.reasonValue);
        }

        this.setElementIds();
    }

    private createForm() {
        this.form = new FormGroup({
            reason: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(this.maxReasonLength),
                this.noOnlyWhiteSpaces
            ])
        });
    }

    private noOnlyWhiteSpaces(control: FormControl) {
        const trimmedValue = control.value.trim();

        return trimmedValue.length ? null : {noOnlyWhiteSpaces: true};
    }

    private setElementIds() {
        const {confirmBtnId, cancelBtnId} = this.data;

        if (confirmBtnId) {
            this.renderer.setAttribute(this.confirmBtnElRef.nativeElement, 'id', confirmBtnId);
        }

        if (cancelBtnId) {
            this.renderer.setAttribute(this.cancelBtnElRef.nativeElement, 'id', cancelBtnId);
        }
    }

    confirmForm() {
        const reasonText = this.form.controls.reason.value;
        this.confirm.emit(reasonText);
    }

    cancelForm() {
        this.cancel.emit();
    }
}
