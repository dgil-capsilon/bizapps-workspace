import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ReasonDialogData} from './reason-dialog-data.interface';
import {ReasonDialogComponent} from './reason-dialog.component';

describe('ReasonDialogComponent', () => {
    let component: ReasonDialogComponent;
    let fixture: ComponentFixture<ReasonDialogComponent>;
    let dataMock: ReasonDialogData;
    let reasonLabelEl: HTMLLabelElement;
    let reasonEl: HTMLTextAreaElement;
    let confirmEl: HTMLButtonElement;
    let cancelEl: HTMLButtonElement;
    let counterEl: HTMLDivElement;
    let titleEl: HTMLSpanElement;
    let contentEl: HTMLDivElement;

    async function setUp() {
        await TestBed.configureTestingModule({
            declarations: [ReasonDialogComponent],
            imports: [ReactiveFormsModule],
            providers: [
                {provide: MAT_DIALOG_DATA, useValue: dataMock}
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(ReasonDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        reasonLabelEl = fixture.nativeElement.querySelector('.reason-label');
        reasonEl = fixture.nativeElement.querySelector('.reason-field');
        confirmEl = fixture.nativeElement.querySelector('.confirm-btn');
        cancelEl = fixture.nativeElement.querySelector('.cancel-btn');
        counterEl = fixture.nativeElement.querySelector('.waive-length-counter');
        titleEl = fixture.nativeElement.querySelector('.title');
        contentEl = fixture.nativeElement.querySelector('.content-text');
    }

    it('should show reason label', async () => {
        dataMock = {showReasonLabel: true} as any;

        await setUp();

        expect(reasonLabelEl).toBeTruthy();
    });

    it('should not show reason label', async () => {
        dataMock = {showReasonLabel: false} as any;

        await setUp();

        expect(reasonLabelEl).toBeFalsy();
    });

    it('should set reason value', async () => {
        dataMock = {reasonValue: 'some fake reason value'} as any;

        await setUp();

        expect(reasonEl.value).toEqual('some fake reason value');
    });

    it('should not set reason value', async () => {
        dataMock = {reasonValue: null} as any;

        await setUp();

        expect(reasonEl.value).toEqual('');
    });

    it('should not be able to confirm form with error (max reason length)', async () => {
        dataMock = {maxReasonLength: 5} as any;
        await setUp();
        spyOn(component, 'confirmForm');

        reasonEl.value = 'abcdef';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        confirmEl.click();

        expect(component.confirmForm).not.toHaveBeenCalled();
    });

    it('should not be able to confirm form with error (min reason length)', async () => {
        dataMock = {} as any;
        await setUp();
        spyOn(component, 'confirmForm');

        reasonEl.value = '';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        confirmEl.click();

        expect(component.confirmForm).not.toHaveBeenCalled();
    });

    it('should not be able to confirm form with error (only whitespaces)', async () => {
        dataMock = {} as any;
        await setUp();
        spyOn(component, 'confirmForm');

        reasonEl.value = ' ';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        confirmEl.click();

        expect(component.confirmForm).not.toHaveBeenCalled();
    });

    it('should be able to confirm form', async () => {
        dataMock = {maxReasonLength: 15} as any;
        await setUp();
        spyOn(component.confirm, 'emit');

        reasonEl.value = 'valid reason';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        confirmEl.click();

        expect(component.confirm.emit).toHaveBeenCalledWith('valid reason');
    });

    it('should be able to cancel dialog', async () => {
        dataMock = {} as any;
        await setUp();
        spyOn(component.cancel, 'emit');

        cancelEl.click();

        expect(component.cancel.emit).toHaveBeenCalled();
    });

    it('should update reason character counter', async () => {
        dataMock = {maxReasonLength: 16} as any;
        await setUp();
        spyOn(component.confirm, 'emit');

        reasonEl.value = 'some reason';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        expect(counterEl.textContent).toEqual('11/16');

        reasonEl.value = 'some reason 123';
        reasonEl.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        expect(counterEl.textContent).toEqual('15/16');
    });

    it('should set title text', async () => {
        dataMock = {title: 'some interesting title'} as any;

        await setUp();

        expect(titleEl.textContent).toEqual('some interesting title');
    });

    it('should set content text', async () => {
        dataMock = {contentText: 'some awful content text'} as any;

        await setUp();

        expect(contentEl.textContent.trim()).toEqual('some awful content text');
    });

    it('should set cancel button text', async () => {
        dataMock = {cancelBtnText: 'DESTROY MODAL'} as any;

        await setUp();

        expect(cancelEl.textContent).toEqual('DESTROY MODAL');
    });

    it('should set confirm button text', async () => {
        dataMock = {confirmBtnText: 'YEAH, GET ME IPHONE 11 PRO'} as any;

        await setUp();

        expect(confirmEl.textContent).toEqual('YEAH, GET ME IPHONE 11 PRO');
    });

    it('should set label text', async () => {
        dataMock = {reasonLabelText: 'Provide here all your sensitive and personal information, trust us', showReasonLabel: true} as any;

        await setUp();

        expect(reasonLabelEl.textContent).toEqual('Provide here all your sensitive and personal information, trust us');
    });

    it('should set confirm button id', async () => {
        dataMock = {confirmBtnId: 'unusual-confirm-btn-id-9000'} as any;

        await setUp();

        expect(confirmEl.id).toEqual('unusual-confirm-btn-id-9000');
    });

    it('should set cancel button id', async () => {
        dataMock = {cancelBtnId: 'evil-cancel-btn-id-666'} as any;

        await setUp();

        expect(cancelEl.id).toEqual('evil-cancel-btn-id-666');
    });

    it('should not set confirm button id', async () => {
        dataMock = {confirmBtnId: null} as any;

        await setUp();

        expect(confirmEl.id).toEqual('');
    });

    it('should not set cancel button id', async () => {
        dataMock = {cancelBtnId: null} as any;

        await setUp();

        expect(cancelEl.id).toEqual('');
    });
});
