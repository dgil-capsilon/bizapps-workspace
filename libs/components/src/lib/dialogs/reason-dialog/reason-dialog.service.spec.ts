import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {ReasonDialogService} from './reason-dialog.service';

describe('ReasonDialogService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: MatDialog, useValue: {}}
            ]
        });
    });

    it('should be created', () => {
        const service: ReasonDialogService = TestBed.get(ReasonDialogService);
        expect(service).toBeTruthy();
    });
});
