import { EventEmitter, Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ReasonDialogData } from './reason-dialog-data.interface';
import { ReasonDialogComponent } from './reason-dialog.component';

@Injectable({
    providedIn: 'root'
})
export class ReasonDialogService {
    confirm: EventEmitter<string>;
    cancel: EventEmitter<void>;

    constructor(private dialog: MatDialog) {
    }

    open(config: MatDialogConfig<ReasonDialogData>): MatDialogRef<ReasonDialogComponent> {
        const reasonDialogCompRef = this.dialog.open(ReasonDialogComponent, {
            maxWidth: '520px',
            panelClass: 'reason-dialog-panel',
            backdropClass: 'backdrop-dialog',
            ...config
        });

        this.confirm = reasonDialogCompRef.componentInstance.confirm;
        this.cancel = reasonDialogCompRef.componentInstance.cancel;

        return reasonDialogCompRef;
    }
}
