export interface ReasonDialogData {
    title: string;
    contentText: string;
    cancelBtnText: string;
    confirmBtnText: string;
    reasonLabelText: string;
    maxReasonLength: number;
    showReasonLabel: boolean;
    reasonValue?: string;
    confirmBtnId?: string;
    cancelBtnId?: string;
}
