export * from './dialogs.module';
export * from './reason-dialog/reason-dialog.component';
export * from './reason-dialog/reason-dialog.service';
export * from './reason-dialog/reason-dialog-data.interface';

export * from './confirm-dialog/confirm-dialog.component';
export * from './confirm-dialog/confirm-dialog.service';
export * from './confirm-dialog/confirm-dialog-data.interface';
