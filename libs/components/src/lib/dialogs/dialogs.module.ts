import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {ReasonDialogComponent} from './reason-dialog/reason-dialog.component';

@NgModule({
    declarations: [
        ReasonDialogComponent,
        ConfirmDialogComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatButtonModule
    ]
})
export class DialogsModule {
}
