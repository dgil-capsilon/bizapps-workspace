import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-edit-late-payments-list',
  templateUrl: './edit-late-payments-list.component.html',
  styleUrls: ['./edit-late-payments-list.component.scss']
})
export class EditLatePaymentsListComponent implements OnInit {

  @Input() latePayments: Array<string>;
  @Output() newLatePaymentArray = new EventEmitter<Array<string>>();

  constructor() { }

  ngOnInit() {
  }

  removeLatePayment(index: number){
    this.latePayments.splice(index,1 );
    this.newLatePaymentArray.emit(this.latePayments);
  }

  addLatePaymentField() {
    this.latePayments.push(null);
  }

  updateLate($event){
    this.latePayments[$event.index] = $event.date;
    this.newLatePaymentArray.emit(this.latePayments);
  }
}
