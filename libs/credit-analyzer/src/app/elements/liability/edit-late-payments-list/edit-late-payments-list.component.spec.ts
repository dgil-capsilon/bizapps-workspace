import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLatePaymentsListComponent } from './edit-late-payments-list.component';

xdescribe('EditLatePaymentsListComponent', () => {
  let component: EditLatePaymentsListComponent;
  let fixture: ComponentFixture<EditLatePaymentsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLatePaymentsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLatePaymentsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
