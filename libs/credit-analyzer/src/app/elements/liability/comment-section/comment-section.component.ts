import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-comment-section',
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentSectionComponent {
  @Input() comments: string[];
}
