import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input() keys;
  @Input() enumKey;
  @Input() value;
  @Output() valueChange = new EventEmitter<any>();
  @HostBinding('class') override = 'detail-line';
  constructor() { }

  ngOnInit() {
  }

  change($event){
   this.valueChange.emit($event);
  }
}
