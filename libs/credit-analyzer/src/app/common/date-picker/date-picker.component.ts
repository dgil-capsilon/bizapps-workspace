import {Component, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepicker} from '@angular/material';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {Moment} from 'moment';
import * as _moment from 'moment';
import {FormControl} from '@angular/forms';

const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YY',
  },
  display: {
    dateInput: 'MM/YY',
    monthYearLabel: 'MMM YY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YY',
  },
};

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class DatePickerComponent implements OnInit, OnChanges {
  @Input() date: string;
  @Input() index: number;
  @Output() newDate = new EventEmitter<Object>();
  @HostBinding('class') override = 'detail-line';
  form: FormControl;
  maxDate = moment();
  isValid: boolean = true;

  constructor() {
  }

  chosenYearHandler(normalizedYear: Moment) {
    if (this.form.value == null) {
      this.form.setValue(moment().day(0));
    }
    const ctrlValue = this.form.value;
    ctrlValue.year(normalizedYear.year());
    this.form.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.form.value;
    ctrlValue.month(normalizedMonth.month());
    this.form.setValue(ctrlValue);
    datepicker.close();

    if (this.isValid) {
      this.newDate.emit({date: this.form.value.format('YYYY-MM-DD'), index: this.index});
    }
  }

  ngOnInit() {
    this.updateForm();
  }

  ngOnChanges() {
    this.updateForm();
  }

  updateForm() {
    this.form = new FormControl(this.toDate(this.date));
  }

  toDate(date: string) {
    const dateObj = moment(date);
    return date != null ? moment(dateObj) : null;
  }

  dateChange(e) {
    this.isValid = moment(e.value).isValid() === true;

    if (!this.isValid) {
      this.form.setValue(null);
      this.isValid = true;
    }

    if (this.isValid) {
      const date = this.form.value !== null ? this.form.value.format('YYYY-MM-DD') : null;
      this.newDate.emit({date, index: this.index});
    }
  }
}
