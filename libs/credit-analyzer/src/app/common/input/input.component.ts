import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  @Input() value;
  @Input() prefix: boolean = false;
  @Input() mask;
  @Input() disabled: boolean = false;
  @HostBinding('class') override = 'detail-line';

  @Output() changeValue = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  change($event){
    this.changeValue.emit($event);
  }

}
