import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Summary, TabHeader} from './modules/credit-analyzer/models/credit-analyzer-models';
import {CreditAnalyzer, State} from './root-store/root-state';
import {StoreService} from './root-store/store.service';
import {UpdateNavigationComponent} from './root-store/root-actions';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {DialogService} from '../services/dialog.service';

@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'CreditAnalyzer';

  @Input() spinnerEnabled = true;
  summary: CreditAnalyzer;
  tabHeader: Array<TabHeader> = [];
  dataSubscription$: Subscription;

  constructor(private storeService: StoreService, private store: Store<State>, private dialog: DialogService) {
  }

  ngOnInit() {
    this.dataSubscription$ = this.storeService.getAllState().subscribe(summary => {
      this.tabHeader = [];
      this.tabHeader.push({firstName: 'Summary', lastName: '', id: null, route: 'summary'});
      this.summary = summary;
      if (this.summary && this.summary.folderId) {
        this.spinnerEnabled = this.summary.isLoading;
      }
      if (this.summary.applicants) {
        summary.applicants.forEach(applicant => {
          this.tabHeader.push({
            firstName: applicant.firstName,
            lastName: applicant.lastName,
            id: applicant.id,
            route: '/applicant/' + applicant.id
          });
        });
      }
      if (summary.folderReady === false) {
        this.dialog.openDialog({
          message: `This folder has no credit data.\n Please check back later.`
        });
      }
    });
  }

  trackByApplicant(i, item) {
    return item.id;
  }

  resetComponentNavigation() {
    this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
  }

  ngOnDestroy() {
    this.dataSubscription$.unsubscribe();
  }
}
