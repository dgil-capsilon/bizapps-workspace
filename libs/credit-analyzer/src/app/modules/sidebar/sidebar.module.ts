import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ApplicantComponent } from './components/applicant/applicant.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ScoreBadgeComponent} from './components/score-badge/score-badge.component';
import {DashPipe} from '@pipes/dash-pipe';
import {CreditRankComponent} from '@modules/sidebar/components/credit-rank/credit-rank.component';
import {RulesComponent} from '@modules/sidebar/components/rules/rules.component';
import {MatExpansionModule, MatIconModule} from '@angular/material';
import {RuleComponent} from '@modules/sidebar/components/rule/rule.component';
import {StatusIconComponent} from '@modules/sidebar/components/status-icon/status-icon.component';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {HyphenPipe} from '@pipes/hyphen.pipe';
import {EmptyRuleComponent} from '@modules/sidebar/components/empty-rule/empty-rule.component';
import {ClampDirective} from '@app/directives/clamp.directive';

@NgModule({
  imports: [
    CommonModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatIconModule,
    AngularSvgIconModule
  ],
  declarations: [
    SidebarComponent,
    ApplicantComponent,
    ScoreBadgeComponent,
    DashPipe,
    CreditRankComponent,
    RulesComponent,
    RuleComponent,
    StatusIconComponent,
    HyphenPipe,
    EmptyRuleComponent,
    ClampDirective
  ],
  exports: [
    SidebarComponent,
    ScoreBadgeComponent
  ]
})
export class SidebarModule {
}
