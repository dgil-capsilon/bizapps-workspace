import {Component, Input, ChangeDetectionStrategy} from '@angular/core';
import {ApplicantRules} from '@app/root-store/root-state';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RulesComponent {
  @Input() applicants: Array<ApplicantRules>;
  noRules: boolean;

  getFirstName(applicant: ApplicantRules): string {
    return applicant.fullName ? applicant.fullName.split(' ')[0].toLowerCase() : null;
  }

  trackByApplicantId(index, applicant) {
    return applicant.applicantId;
  }

  trackByRuleId(index, rule) {
    return rule.id;
  }
}
