import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-empty-rule',
  templateUrl: './empty-rule.component.html',
  styleUrls: ['./empty-rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyRuleComponent {
}
