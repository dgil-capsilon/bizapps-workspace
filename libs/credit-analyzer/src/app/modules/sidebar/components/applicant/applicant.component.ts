import {Component, Input, OnInit} from '@angular/core';
import {Applicant} from '../../../credit-analyzer/models/credit-analyzer-models';
import {Store} from '@ngrx/store';
import {State} from '../../../../root-store/root-state';
import {
  ExcludeApplicantAction,
  IncludeApplicantAction,
  UpdateNavigationComponent
} from '../../../../root-store/root-actions';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-applicant',
  templateUrl: './applicant.component.html',
  styleUrls: ['./applicant.component.scss']
})
export class ApplicantComponent implements OnInit {

  @Input() applicant: Applicant;
  @Input() folderId: string;

  constructor(private store: Store<State>, private router: Router, private route: ActivatedRoute) {
  }

  onChange($event) {
    if ($event.checked) {
      this.store.dispatch(new IncludeApplicantAction(this.folderId, this.applicant.id));
    } else {
      this.store.dispatch(new ExcludeApplicantAction(this.folderId, this.applicant.id));
    }
  }

  ngOnInit() {
  }

  goToApplicantTab(id, name) {
    this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
    this.store.dispatch(new UpdateNavigationComponent({openedComponent: name}));
    this.router.navigate(['/applicant', id], {relativeTo: this.route, queryParamsHandling: 'merge'});
  }
}
