import {Component, HostBinding, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-score-badge',
  templateUrl: './score-badge.component.html',
  styleUrls: ['./score-badge.component.scss']
})
export class ScoreBadgeComponent implements OnInit, OnChanges {
  @Input() score: number;
  @Input() rank: string;
  @HostBinding('class') rankColor = 'rank';

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.score = this.score || 0;
    this.rankColor = 'rank ' + CreditRank[this.rank];
  }
}

enum CreditRank {
  'EXCELLENT' = 'dark-green',
  'GOOD' = 'light-green',
  'FAIR' = 'orange',
  'POOR' = 'red'
}
