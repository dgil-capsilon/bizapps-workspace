import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScoreBadgeComponent} from './score-badge.component';
import {DashPipe} from '../../../../pipes/dash-pipe';

describe('ScoreBadgeComponent', () => {
  let component: ScoreBadgeComponent;
  let fixture: ComponentFixture<ScoreBadgeComponent>;
  let componentEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScoreBadgeComponent, DashPipe]
    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreBadgeComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create Score Badge component', () => {
    expect(component).toBeTruthy();
  });

  it('should display \'POOR\' rank with score = 579', () => {
    setRank(component, componentEl, fixture, 'poor', 579, 'red');
  });

  it('should display \'FAIR\' rank with score = 699', () => {
    setRank(component, componentEl, fixture, 'fair', 699, 'orange');
  });

  it('should display \'GOOD\' rank with score = 749', () => {
    setRank(component, componentEl, fixture, 'good', 749, 'light-green');
  });

  it('should display \'EXCELLENT\' rank with score = 750', () => {
    setRank(component, componentEl, fixture, 'excellent', 750, 'dark-green');
  });


});

function setRank(component, componentEl, fixture, rank, score, rankColor) {
  const creditScore = {
    score: score,
    rank: rank
  };
  component.rank = creditScore.rank;
  component.score = creditScore.score;
  component.rankColor = 'rank ' + rankColor;
  fixture.detectChanges();

  expect(componentEl.querySelector('.rank').textContent).toContain(creditScore.rank);
  expect(componentEl.querySelector('.rank').className).toContain(rankColor);
  expect(componentEl.querySelector('.score').textContent).toContain(creditScore.score.toString());
}
