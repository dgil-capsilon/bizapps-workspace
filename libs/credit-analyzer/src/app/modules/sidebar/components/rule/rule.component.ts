import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Rule} from '@app/root-store/root-state';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RuleComponent {
  @Input() rule: Rule;
  @Input() showRuleCodes: boolean;
}
