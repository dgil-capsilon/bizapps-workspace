import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleComponent } from './rule.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HyphenPipe} from '@pipes/hyphen.pipe';

describe('CreditRankComponent', () => {
  let component: RuleComponent;
  let fixture: ComponentFixture<RuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuleComponent, HyphenPipe ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
