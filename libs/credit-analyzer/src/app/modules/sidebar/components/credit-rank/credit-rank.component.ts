import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {DtiSummary} from '@app/root-store/root-state';

@Component({
  selector: 'app-credit-rank',
  templateUrl: './credit-rank.component.html',
  styleUrls: ['./credit-rank.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreditRankComponent {
  @Input() qualifyingScore: number = 0;
  @Input() rank;
  @Input() dti: DtiSummary;
}
