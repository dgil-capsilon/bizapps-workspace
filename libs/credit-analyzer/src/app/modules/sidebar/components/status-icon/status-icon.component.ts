import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {RuleStatus} from '@app/app.types';

@Component({
  selector: 'app-status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.scss']
})
export class StatusIconComponent {
  @Input() status: RuleStatus;

  getIconName(status: RuleStatus) {
    if (status) {
      return 'assets/images/svg/' + IconName[status] + '.svg';
    }
  }

  getIconStatus(status: RuleStatus) {
    if (status) {
      return IconStatus[status];
    }
  }
}

export enum IconName {
  FAILED = 'rule-failed',
  PASSED = 'rule-passed',
  NOT_EVALUATED = 'rule-failed',
  NOT_APPLICABLE = 'rule-failed',
  WAIVED = 'rule-waived'
}

export enum IconStatus {
  FAILED = 'icon-failed',
  PASSED = 'icon-passed',
  NOT_EVALUATED = 'icon-failed',
  NOT_APPLICABLE = 'icon-failed',
  WAIVED = 'icon-waived'
}
