import {Component, OnInit} from '@angular/core';
import {Applicant} from '../../../credit-analyzer/models/credit-analyzer-models';
import {ApplicantRules, DtiSummary, State} from '@app/root-store/root-state';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  activeApplicantCount: number = 0;
  applicants: Array<Applicant> = [];
  qualifyingScore: number = 0;
  folderId: number;
  applicantsCount = 0;
  rank;
  dti: DtiSummary;
  showChecklist: boolean;
  applicantsRules: Array<ApplicantRules>;

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
    this.store.select('creditAnalyzer').subscribe(x => {
      this.applicants = x.applicants;
      this.qualifyingScore = x.qualifyingScore;
      this.folderId = x.folderId;
      this.rank = x.rank;
      this.activeApplicantCount = x.applicants ? x.applicants.filter(applicant => applicant.included).length : 0;
      this.dti = x.dtiSummary;
      this.applicantsCount = x.applicants ? x.applicants.length : 0;
      this.showChecklist = x.config.displayChecklist;
      this.applicantsRules = x.rules.applicants;
    });
  }

  trackByApplicant(i, item) {
    return item.id;
  }
}
