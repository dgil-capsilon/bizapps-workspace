import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditComponent} from './components/credit/credit.component';
import {MortgageComponent} from './components/mortgage/mortgage.component';
import {ProviderComponent} from './components/provider/provider.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule, NativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LiabilitiesComponent} from './components/liabilities/liabilities.component';
import {DetailComponent} from './components/liabilities/components/detail/detail.component';
import {FormatDateMMYYPipe} from '../../pipes/format-date-mmyy.pipe';
import {LengthPipe} from '../../pipes/length.pipe';
import {FormatDateDDMMMYYYYPipe} from '../../pipes/format-date-ddmmmyyyy.pipe';
import {FormatDateMMDDYYYYPipe} from '../../pipes/format-date-mmddyyyy.pipe';
import {OtherComponent} from './components/other/other.component';
import {InquiryComponent} from './components/other/components/inquiry/inquiry.component';
import {TagComponent} from './components/tag/tag.component';
import {PublicRecordsComponent} from './components/other/components/public-records/public-records.component';
import {DefaultPipe} from '../../pipes/default.pipe';
import {DateUnknowPipe} from '../../pipes/date-unknow.pipe';
import {EditPublicRecordNameComponent} from './components/other/components/public-records/edit-public-record-name/edit-public-record-name.component';
import {ReviewPublicRecordComponent} from './components/other/components/public-records/review-public-record/review-public-record.component';
import {EditPublicRecordComponent} from './components/other/components/public-records/edit-public-record/edit-public-record.component';
import {EditDetailComponent} from './components/liabilities/components/edit-detail/edit-detail.component';
import {DatePickerComponent} from '../../common/date-picker/date-picker.component';
import {ToggleDetailComponent} from './components/liabilities/components/toggle-detail/toggle-detail.component';
import {EditLatePaymentsListComponent} from '../../elements/liability/edit-late-payments-list/edit-late-payments-list.component';
import {CommentSectionComponent} from '../../elements/liability/comment-section/comment-section.component';
import {SelectComponent} from '../../common/select/select.component';
import {InputComponent} from '../../common/input/input.component';
import {TextMaskModule} from 'angular2-text-mask';
import {AddOtherDialogComponent} from './components/liabilities/components/add-other-dialog/add-other-dialog.component';
import {HousingExpensesComponent} from './components/liabilities/components/housing-expenses/housing-expenses.component';
import {HousingExpensesToggleComponent} from './components/liabilities/components/housing-expenses-toggle/housing-expenses-toggle.component';
import {EditHousingExpensesComponent} from './components/liabilities/components/edit-housing-expenses/edit-housing-expenses.component';
import {CreditScoreDisplayComponent} from './components/credit/credit-score-display/credit-score-display.component';
import {CreditScoreEditComponent} from './components/credit/credit-score-edit/credit-score-edit.component';
import {DisplayModelComponent} from './components/liabilities/components/housing-expenses/display-model/display-model.component';
import {EditModelComponent} from './components/liabilities/components/housing-expenses/edit-model/edit-model.component';
import {ElementComponent} from './components/liabilities/detail-elements/element/element.component';
import {NgxPopperModule} from 'ngx-popper';
import {SnippetComponent} from './components/liabilities/detail-elements/snippet/snippet.component';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {DetailLatePaymentsComponent} from './components/liabilities/components/detail-late-payments/detail-late-payments.component';
import {LiabilitySectionComponent} from './components/liabilities/components/liability-section/liability-section.component';
import { DetailLineComponent } from './components/liabilities/components/detail-line/detail-line.component';
import { HousingApplicationComponent } from './components/liabilities/components/housing-expenses/housing-application/housing-application.component';

@NgModule({
    entryComponents: [
        EditPublicRecordNameComponent
    ],
    imports: [
        MatExpansionModule,
        MatListModule,
        MatCheckboxModule,
        CommonModule,
        MatRadioModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatFormFieldModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        NativeDateModule,
        MatDatepickerModule,
        MatNativeDateModule,
        TextMaskModule,
        MatTabsModule,
        NgxPopperModule,
        AngularSvgIconModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        CreditComponent,
        MortgageComponent,
        ProviderComponent,
        LiabilitiesComponent,
        DetailComponent,
        FormatDateMMYYPipe,
        LengthPipe,
        FormatDateDDMMMYYYYPipe,
        FormatDateMMDDYYYYPipe,
        DefaultPipe,
        DateUnknowPipe,
        OtherComponent,
        InquiryComponent,
        TagComponent,
        PublicRecordsComponent,
        EditPublicRecordNameComponent,
        ReviewPublicRecordComponent,
        EditPublicRecordComponent,
        EditDetailComponent,
        DatePickerComponent,
        ToggleDetailComponent,
        EditLatePaymentsListComponent,
        CommentSectionComponent,
        SelectComponent,
        InputComponent,
        AddOtherDialogComponent,
        HousingExpensesComponent,
        HousingExpensesToggleComponent,
        EditHousingExpensesComponent,
        CreditScoreDisplayComponent,
        CreditScoreEditComponent,
        DisplayModelComponent,
        EditModelComponent,
        ElementComponent,
        SnippetComponent,
        DetailLatePaymentsComponent,
        LiabilitySectionComponent,
        DetailLineComponent,
        HousingApplicationComponent
    ],
    exports: [MortgageComponent]
})
export class MortgageModule {
}
