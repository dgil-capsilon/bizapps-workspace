import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayModelComponent } from './display-model.component';

xdescribe('DisplayModelComponent', () => {
  let component: DisplayModelComponent;
  let fixture: ComponentFixture<DisplayModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
