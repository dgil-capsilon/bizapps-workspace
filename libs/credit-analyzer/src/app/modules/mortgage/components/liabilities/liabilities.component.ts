import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Applicant, ApplicantLiability, LiabilityType} from '../../../credit-analyzer/models/credit-analyzer-models';
import {State} from '../../../../root-store/root-state';
import {Store} from '@ngrx/store';
import {
  ExcludeLiabilityAction,
  ExcludeLiabilityCategoryAction,
  IncludeLiabilityAction,
  IncludeLiabilityCategoryAction, UpdateNavigationComponent,
} from '../../../../root-store/root-actions';
import {MatDialog} from '@angular/material/dialog';
import {AddOtherDialogComponent} from './components/add-other-dialog/add-other-dialog.component';
import {ActivatedRoute} from '@angular/router';
import {ExpenseType} from '../../../../app.types';
import {Subscription} from 'rxjs';
import {ScrollingMethods} from '../../../../utils/scrolling-methods/scrolling-methods';

@Component({
  selector: 'app-liabilities',
  templateUrl: './liabilities.component.html',
  styleUrls: ['./liabilities.component.scss']
})
export class LiabilitiesComponent implements OnInit, OnDestroy {
  @Input() applicant: Applicant;
  open: string;
  expenseType = ExpenseType;
  folderId: string;
  subscription$: Subscription;

  constructor(private store: Store<State>, private route: ActivatedRoute, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.subscription$ = this.store.select('creditAnalyzer').subscribe(ca => {
      this.folderId = ca.folderId.toString();
      this.open = ca.openedComponent !== null ? ca.openedComponent.toLowerCase() : null;
      if (ca.openedComponent === 'collections' || ca.openedComponent === 'foreclosure') {
        ScrollingMethods.scrollWithDelay('activeLiability');
      }
    });
  }

  trackByLiability(i, item) {
    return item.id;
  }

  onChangeDetails($event, id) {
    if ($event.checked) {
      this.store.dispatch(new IncludeLiabilityAction(this.folderId, id));
    } else {
      this.store.dispatch(new ExcludeLiabilityAction(this.folderId, id));
    }
  }

  onChangeCategory($event, type) {
    if ($event.checked) {
      this.store.dispatch(new IncludeLiabilityCategoryAction(this.folderId, this.applicant.id, type));
    } else {
      this.store.dispatch(new ExcludeLiabilityCategoryAction(this.folderId, this.applicant.id, type));
    }
  }


  openModal($event) {
    $event.stopPropagation();
    this.dialog.open(AddOtherDialogComponent, {
      width: '400px',
      data: {

        applicantId: this.applicant.id,
        folderId: this.folderId
      }
    });
  }

  liabilitiesCategoryHaveTag(liabilities: Array<ApplicantLiability>) {
    let allTags = [];
    if (liabilities !== null && liabilities.length > 0) {
      liabilities.forEach(liability => {
        liability.tags.forEach(tag => {
          allTags.push(tag.toLowerCase());
        });
      });
      return allTags.indexOf(this.open) > -1;
    }
  }

  checkParentPanel(liabilities: Array<ApplicantLiability>, panel: any) {
    if(this.liabilitiesCategoryHaveTag(liabilities)) {
      panel.open();
    }
    return true;
  }

  liabilityHaveTag(liability: ApplicantLiability) {
    if (liability !== null && liability.tags !== null && liability.tags.length > 0) {
      return liability.tags.indexOf(this.open) > -1;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.subscription$.unsubscribe();
    this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
  }

  closeAllCategory() {
    if (this.open !== null) {
      this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
    }
  }
}
