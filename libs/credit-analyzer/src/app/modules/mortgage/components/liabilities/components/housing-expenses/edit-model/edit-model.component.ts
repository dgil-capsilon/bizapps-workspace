import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {HousingExpense} from '../../../../../../../root-store/root-state';

@Component({
  selector: 'app-edit-model',
  templateUrl: './edit-model.component.html',
  styleUrls: ['./edit-model.component.scss']
})
export class EditModelComponent implements OnInit {

  @Input() he: HousingExpense;
  @Input() applicantName;
  @Output() changeValue = new EventEmitter<HousingExpense>();
  mask;
  constructor() { }


  ngOnInit() {
    this.mask = createNumberMask({
      integerLimit: 8,
      prefix: '',
      allowDecimal: true
    });
  }
  change(){
    this.changeValue.emit(this.he);
  }
}
