import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiabilitySectionComponent } from './liability-section.component';

describe('LiabilitySectionComponent', () => {
  let component: LiabilitySectionComponent;
  let fixture: ComponentFixture<LiabilitySectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiabilitySectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiabilitySectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
