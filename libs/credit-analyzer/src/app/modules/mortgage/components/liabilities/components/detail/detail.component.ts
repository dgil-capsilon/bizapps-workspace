import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LiabilityType} from '../../../../../credit-analyzer/models/credit-analyzer-models';
import {ECOA, ExpenseType, ItemSourceType, LiabilityCategoryType, Rating} from '../../../../../../app.types';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    @Input() category;
    @Input() liability: LiabilityType;
    @Output() editMode = new EventEmitter<boolean>();

    status = Rating;
    ecoa = ECOA;
    expenseType = ExpenseType;
    itemSource = ItemSourceType;
    isOther: boolean = false;
    isCreditReport: boolean = false;
    isSnippetDisplay: boolean = false;

    constructor() {
    }

    ngOnInit() {
        if (this.liability) {
            if (this.liability.type === LiabilityCategoryType.OTHER) {
                this.isOther = true;
                if(this.liability.itemSource === ItemSourceType.CreditReport){
                    this.isCreditReport = true;
                }
                if(this.liability.itemSource === ItemSourceType.LoanApplication){
                    this.isSnippetDisplay = true;
                }
            }
        }
    }

  edit() {
    this.editMode.emit(true);
  }
}
