import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-detail-line',
  templateUrl: './detail-line.component.html',
  styleUrls: ['./detail-line.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailLineComponent {
}
