import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ElementComponent} from './element.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DefaultPipe} from '@pipes/default.pipe';

describe('ElementComponent', () => {
  let component: ElementComponent;
  let fixture: ComponentFixture<ElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ElementComponent, DefaultPipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementComponent);
    component = fixture.componentInstance;
    component.element = {snippetName: 'HousingExpenses_Proposed_Rent'} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
