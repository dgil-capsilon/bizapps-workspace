import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingApplicationComponent } from './housing-application.component';

describe('HousingApplicationComponent', () => {
  let component: HousingApplicationComponent;
  let fixture: ComponentFixture<HousingApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
