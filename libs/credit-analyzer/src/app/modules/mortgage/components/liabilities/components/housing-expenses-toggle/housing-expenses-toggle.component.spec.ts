import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingExpensesToggleComponent } from './housing-expenses-toggle.component';

xdescribe('HousingExpensesToggleComponent', () => {
  let component: HousingExpensesToggleComponent;
  let fixture: ComponentFixture<HousingExpensesToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingExpensesToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingExpensesToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
