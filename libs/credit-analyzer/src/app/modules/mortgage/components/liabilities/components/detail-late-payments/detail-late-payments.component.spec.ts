import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DetailLatePaymentsComponent} from './detail-late-payments.component';
import {FormatDateMMYYPipe} from '@pipes/format-date-mmyy.pipe';
import {DateUnknowPipe} from '@pipes/date-unknow.pipe';

describe('DetailLatePaymentsComponent', () => {
  let component: DetailLatePaymentsComponent;
  let fixture: ComponentFixture<DetailLatePaymentsComponent>;
  let name;
  let lates;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailLatePaymentsComponent, FormatDateMMYYPipe, DateUnknowPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailLatePaymentsComponent);
    component = fixture.componentInstance;
    name = fixture.nativeElement.querySelector('.name');
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should check name and late payment', () => {
    component.name = '30 days';
    component.latePayments = ['2018-03-01'];
    fixture.detectChanges();
    lates = fixture.nativeElement.querySelector('.late');
    expect(name.textContent).toEqual('30 days (1)');
    expect(lates.textContent.trim()).toEqual('03/18');
  });

  it('should check name and multiple late payments', () => {
    component.name = '60 days';
    component.latePayments = ['2018-03-01', '2011-04-04', '2017-01-01'];
    fixture.detectChanges();
    let late: HTMLElement = fixture.nativeElement.querySelectorAll('.late');
    expect(name.textContent).toEqual('60 days (3)');
    expect(late[0].textContent.trim()).toEqual('03/18');
    expect(late[1].textContent.trim()).toEqual('04/11');
    expect(late[2].textContent.trim()).toEqual('01/17');
  });
});
