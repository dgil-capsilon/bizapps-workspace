import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-housing-expenses-toggle',
    templateUrl: './housing-expenses-toggle.component.html',
    styleUrls: ['./housing-expenses-toggle.component.scss']
})
export class HousingExpensesToggleComponent implements OnInit {
    @Input() category;
    @Input() applicant;
    editMode: boolean;

    constructor() {
    }

    ngOnInit() {
    }
}
