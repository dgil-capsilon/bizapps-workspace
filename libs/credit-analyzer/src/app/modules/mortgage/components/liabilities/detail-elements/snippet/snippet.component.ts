import {Component, Input, OnInit} from '@angular/core';
import {SnippetsService} from '../../../../../../../services/snippets/snippets.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Store} from '@ngrx/store';
import {State} from '../../../../../../root-store/root-state';
import {MposHttpService} from '../../../../../../../services/mpos/mpos-http-service';
import {ActivatedRoute} from '@angular/router';
import {config, fromEvent} from 'rxjs';

enum DocReaderEvents {
    CLOSE = 'CLOSE_DOC_READER',
    READY = 'DOC_READER_READY'
}

@Component({
    selector: 'app-snippet',
    templateUrl: './snippet.component.html',
    styleUrls: ['./snippet.component.scss']
})
export class SnippetComponent implements OnInit {

    @Input() snippetName;
    @Input() liabilityId: number;
    applicantId: number;

    urlDoc: SafeResourceUrl;
    snippetURL: SafeResourceUrl;

    response: DocReader;
    url = '';

    sideNav;
    folderId: number;
    expandedMode: boolean = false;
    event;

    constructor(private snippetService: SnippetsService,
                private domSanitizer: DomSanitizer,
                private store: Store<State>,
                private httpService: MposHttpService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.store.select('creditAnalyzer', 'folderId').subscribe(folderId => {
            this.folderId = folderId;
        });
        this.route.params.subscribe(params => {
            this.applicantId = params['id'];
        });
    }

    toggleMode() {
        this.expandedMode = !this.expandedMode;
        this.onShown();
    }

    openDocViewer() {
        this.httpService.getDocReader(this.folderId, this.applicantId).subscribe((response: Response) => {
            let popupWindow;
            popupWindow = window.open('docreader');
            this.urlDoc = null;
            const result = response.result;
            this.loadDoc(result.documentReaderURL);
            popupWindow.document.write(`<iframe [allowFullscreen]="true" style="width: 100%; height: 100%" frameborder="0"  id="dr" src="${response.result.documentReaderURL}"></iframe>`);

            this.response = {
                getValidateApplicationAndSiteResult: atob(result.getValidateApplicationAndSiteResult),
                getDirectLaunchConfiguration: atob(result.getDirectLaunchConfiguration),
                getApplicationTickets: atob(result.getApplicationTickets),
                getDocument: atob(result.getDocument)
            };

            this.loadDoc(result.documentReaderURL);
            this.getDocReader(popupWindow);

        });

    }

    getDocReader(popup) {
        this.event = fromEvent(popup, 'message').subscribe(event => {
            this.listener(event, popup);
        });
    }

    listener(event, popup) {
        let result = this.response;
        let iframe = popup.document.getElementById('dr');
        let data = JSON.parse(event.data).command;
        if (data) {
            (<HTMLIFrameElement>iframe).contentWindow.postMessage(result[data], '*');
        }

        if (JSON.parse(event.data).event === DocReaderEvents.CLOSE) {
            this.event.unsubscribe();
            this.sideNav.classList.remove('hidden');
            this.urlDoc = null;
        }
    }

    onShown() {
        let errorSnippetUrl = 'assets/images/svg/no-snippet.svg';
        this.liabilityId ?
            this.snippetService.getOtherLiabilitySnippet(this.folderId, this.liabilityId, this.expandedMode ? 'true' : 'false')
                .subscribe(
                    blob => {
                        this.blobToUrl(blob);
                    },
                    error1 => this.loadView(errorSnippetUrl))
            : this.snippetService.getSnippet(this.snippetName, this.folderId, this.applicantId, this.expandedMode ? 'true' : 'false')
                .subscribe(
                    blob => {
                        this.blobToUrl(blob);
                    },
                    error1 => this.loadView(errorSnippetUrl));
    }

    kill() {
        this.snippetURL = '';
        this.expandedMode = false;
        this.event.unsubscribe();
    }

    blobToUrl(blob) {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
            const snippetUrl = reader.result;
            this.url = reader.result.toString();
            this.loadView(snippetUrl);
        });
        if (blob) {
            reader.readAsDataURL(blob);
        }
    }

    loadView(snippetUrl) {
        this.snippetURL = this.domSanitizer.bypassSecurityTrustResourceUrl(snippetUrl);
    }

    loadDoc(url) {
        this.urlDoc = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }
}

interface Response {
    result: DocReader,
    httpStatus: number;
    errorCode: string;
    status: string;
}

interface DocReader {
    getValidateApplicationAndSiteResult: any;
    getDocument: any;
    getApplicationTickets: any;
    getDirectLaunchConfiguration: any;
    documentReaderURL?: string;
}
