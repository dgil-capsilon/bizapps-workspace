import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingExpensesComponent } from './housing-expenses.component';

xdescribe('HousingExpensesComponent', () => {
  let component: HousingExpensesComponent;
  let fixture: ComponentFixture<HousingExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
