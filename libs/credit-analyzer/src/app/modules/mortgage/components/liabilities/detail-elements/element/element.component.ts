import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Applicant, DetailModel} from '@modules/credit-analyzer/models/credit-analyzer-models';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ElementComponent {
  @Input() element: DetailModel;

  get isNotProposedRent() {
    return this.element.snippetName !== 'HousingExpenses_Proposed_Rent';
  }
}
