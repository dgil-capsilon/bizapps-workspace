import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {FormControl} from '@angular/forms';
import {Details, LatePayments, LiabilityType} from '../../../../../credit-analyzer/models/credit-analyzer-models';
import {Store} from '@ngrx/store';
import {State} from '../../../../../../root-store/root-state';
import {RemoveLiabilityAction, UpdateLiabilityAction} from '../../../../../../root-store/root-actions';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {ECOA, ExpenseType, Rating} from '../../../../../../app.types';
import {ActivatedRoute, Router} from '@angular/router';


export const DATE_MMYY_FORMAT = {
  parse: {
    dateInput: 'MM/YY'
  },
  display: {
    dateInput: 'MM/YY',
    monthYearLabel: 'MMM YYYY'
  }
};

@Component({
  selector: 'app-edit-detail',
  templateUrl: './edit-detail.component.html',
  styleUrls: ['./edit-detail.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DATE_MMYY_FORMAT}
  ]
})
export class EditDetailComponent implements OnInit, OnChanges {

  @Output() editMode = new EventEmitter<boolean>();
  date = new FormControl(moment());
  @Input() liability: LiabilityType;
  editedLiability: LiabilityType;
  @Input() category;
  folderId;
  keys;
  keysECOA;
  et;
  expenseTypeHelpVariable: any;
  status = Rating;
  statusText;
  ecoa = ECOA;
  expenseType = ExpenseType;
  mask;
  applicantId;

  constructor(private store: Store<State>, private route: ActivatedRoute, private router: Router) {
  }

  ngOnChanges() {
    this.route.params.subscribe(id=> this.applicantId = id.id);
    const emptyDetails: Details = {
      accountIdentifier: null,
      balance: null,
      dateOpened: null,
      dateReported: null,
      ecoa: null,
      highestCredit: null,
      monthlyPayment: null,
      status: null
    };

    const emptyLatePayments: LatePayments = {
      lates30Days: null,
      lates60Days: null,
      lates90Days: null,
      lates120Days: null
    };

    const latePayments: LatePayments = {
      lates30Days: this.fillLates(this.liability.latePayments.lates30Days),
      lates60Days: this.fillLates(this.liability.latePayments.lates60Days),
      lates90Days: this.fillLates(this.liability.latePayments.lates90Days),
      lates120Days: this.fillLates(this.liability.latePayments.lates120Days)
    };

    this.editedLiability = {
      included: this.liability.included,
      name: this.liability.name,
      totalMonthlyPayment: this.liability.totalMonthlyPayment,
      type: this.liability.type,
      liabilities: [],
      details: this.liability.details ? Object.assign({}, this.liability.details) : emptyDetails,
      latePayments: this.liability.latePayments ? latePayments : emptyLatePayments,
      comments: [],
      id: this.liability.id,
      expenseType: this.liability.expenseType,
      byApplicant: this.liability.byApplicant,
      itemSource: this.liability.itemSource,
      tags: this.liability.tags
    };

    if (this.liability.liabilities && this.liability.liabilities.length > 0) {
      this.liability.liabilities.forEach(liability => {
        this.editedLiability.liabilities.push(liability);
      });
    }

    if (this.liability.comments && this.liability.comments.length > 0) {
      this.liability.comments.forEach(comment => {
        this.editedLiability.comments.push(comment);
      });
    }

    if (this.editedLiability.expenseType) {
      this.expenseTypeHelpVariable = this.editedLiability.expenseType.toString();
      this.statusText = status[this.editedLiability.details.status];
    }
  }

  ngOnInit() {
    this.keys = Object.keys(this.status);
    this.keysECOA = Object.keys(this.ecoa);
    this.et = Object.keys(this.expenseType);
    this.mask = createNumberMask({
      integerLimit: 8,
      prefix: '',
      allowDecimal: true
    });
    this.store.select('creditAnalyzer').subscribe(x => {
      this.folderId = x.folderId;
    });
  }

  cancel() {
    this.editMode.emit(false);
  }

  remove() {
    this.store.dispatch(new RemoveLiabilityAction(this.folderId, this.editedLiability.id));
  }

  save() {
    const {lates60Days, lates90Days, lates120Days, lates30Days} = this.editedLiability.latePayments;
    this.editedLiability.latePayments.lates60Days = lates60Days;
    this.editedLiability.latePayments.lates30Days = lates30Days;
    this.editedLiability.latePayments.lates90Days = lates90Days;
    this.editedLiability.latePayments.lates120Days = lates120Days;
    this.editedLiability.expenseType = this.editedLiability.expenseType ? this.expenseTypeHelpVariable : this.editedLiability.expenseType;
    this.editedLiability.details.highestCredit =
      this.editedLiability.details.highestCredit ? Number(this.editedLiability.details.highestCredit.toString().replace(/[^\d.-]/g, ''))
        : this.editedLiability.details.highestCredit;
    this.editedLiability.details.balance =
      this.editedLiability.details.balance ? Number(this.editedLiability.details.balance.toString().replace(/[^\d.-]/g, ''))
        : this.editedLiability.details.balance;
    this.editedLiability.details.monthlyPayment =
      this.editedLiability.details.monthlyPayment ? Number(this.editedLiability.details.monthlyPayment.toString().replace(/[^\d.-]/g, ''))
        : this.editedLiability.details.monthlyPayment;

    this.store.dispatch(new UpdateLiabilityAction(this.folderId, this.editedLiability));
    this.router.navigate(['/applicant', this.applicantId], {relativeTo: this.route, queryParamsHandling: 'merge'});
    this.editMode.emit(false);
  }

  fillLates(lates: Array<string>) {
    const tmp = [];
    if (lates) {
      lates.forEach(late => {
        tmp.push(late);
      });
    }
    return tmp;
  }
}
