import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-detail-late-payments',
  templateUrl: './detail-late-payments.component.html',
  styleUrls: ['./detail-late-payments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailLatePaymentsComponent {
  @Input() name: string;
  @Input() latePayments: string[];

}
