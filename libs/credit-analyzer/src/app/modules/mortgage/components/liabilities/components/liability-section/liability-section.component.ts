import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-liability-section',
  templateUrl: './liability-section.component.html',
  styleUrls: ['./liability-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LiabilitySectionComponent {
  @Input() title: string

}
