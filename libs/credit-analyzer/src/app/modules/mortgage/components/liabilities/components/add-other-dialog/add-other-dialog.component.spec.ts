import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOtherDialogComponent } from './add-other-dialog.component';

xdescribe('AddOtherDialogComponent', () => {
  let component: AddOtherDialogComponent;
  let fixture: ComponentFixture<AddOtherDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOtherDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOtherDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
