import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-housing-application',
  templateUrl: './housing-application.component.html',
  styleUrls: ['./housing-application.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HousingApplicationComponent {
  @Input() applicantName: string;
  @Input() hideCloseIcon = false;
  @Output() closeIconClick = new EventEmitter<void>();

}
