import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHousingExpensesComponent } from './edit-housing-expenses.component';

xdescribe('EditHousingExpensesComponent', () => {
  let component: EditHousingExpensesComponent;
  let fixture: ComponentFixture<EditHousingExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHousingExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHousingExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
