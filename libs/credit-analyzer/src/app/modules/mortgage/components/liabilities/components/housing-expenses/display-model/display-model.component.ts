import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {HousingExpense, State} from '../../../../../../../root-store/root-state';
import {Store} from '@ngrx/store';
import {Applicant, DetailModel} from '../../../../../../credit-analyzer/models/credit-analyzer-models';

@Component({
    selector: 'app-display-model',
    templateUrl: './display-model.component.html',
    styleUrls: ['./display-model.component.scss']
})
export class DisplayModelComponent implements OnInit, OnChanges {

    @Input() he: HousingExpense;
    @Input() applicant: Applicant;
    applicantName: string;
    housingExpensesArray: Array<DetailModel> = [];
    snippetName = SnippetName;

    constructor(private store: Store<State>) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.he) {
            const he = this.he;
            const snippetName = this.snippetName;
            const id = `HousingExpenses_${this.he.type}_`;
            this.housingExpensesArray = [
                {name: 'Rent', value: he.rent, snippetName: id + SnippetName[SnippetName.Rent]},
                {name: 'First Mortgage (P&I)', value: he.firstMortgage, snippetName: id + SnippetName[snippetName.FirstMortgagePrincipalAndInterest]},
                {name: 'Other Financing (P&I)', value: he.otherFinancing, snippetName: id+ SnippetName[snippetName.OtherMortgageLoanPrincipalAndInterest]},
                {name: 'Hazard Insurance', value: he.hazardInsurance, snippetName: id + SnippetName[snippetName.HomeownersInsurance]},
                {name: 'Real Estate Taxes', value: he.realEstateTaxes, snippetName: id + SnippetName[snippetName.RealEstateTax]},
                {name: 'Mortgage Insurance', value: he.mortgageInsurance, snippetName: id + SnippetName[snippetName.MIPremium]},
                {name: 'Homeowner Assn. Dues', value: he.homeownersAssociationDues, snippetName: id + SnippetName[snippetName.HomeownersAssociationDuesAndCondominiumFees]},
                {name: 'Other', value: he.other, snippetName: id + SnippetName[SnippetName.Other]}
                ];
          this.applicantName = this.applicant.firstName.toLowerCase() + ' ' + this.applicant.lastName.toLowerCase();
        }
    }
}

export enum SnippetName {
    Rent,
    FirstMortgagePrincipalAndInterest,
    OtherMortgageLoanPrincipalAndInterest,
    HomeownersInsurance,
    RealEstateTax,
    MIPremium,
    HomeownersAssociationDuesAndCondominiumFees,
    Other
}
