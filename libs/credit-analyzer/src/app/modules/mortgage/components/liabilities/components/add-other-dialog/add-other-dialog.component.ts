import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import {State} from '../../../../../../root-store/root-state';
import {AddLiabilityAction} from '../../../../../../root-store/root-actions';
import {LiabilityType} from '../../../../../credit-analyzer/models/credit-analyzer-models';
import {ExpenseType} from '../../../../../../app.types';

@Component({
    selector: 'app-add-other-dialog',
    templateUrl: './add-other-dialog.component.html',
    styleUrls: ['./add-other-dialog.component.scss']
})
export class AddOtherDialogComponent implements OnInit {

    keys;
    expense = ExpenseType;
    type: any = null;
    liability: LiabilityType = <LiabilityType>{};
    folderId: string;
    applicantId: number;

    constructor(public store: Store<State>, public dialogRef: MatDialogRef<AddOtherDialogComponent>, @Inject(MAT_DIALOG_DATA) public data) {
    }

    ngOnInit() {
        this.keys = Object.keys(this.expense);
        const data = this.data;
        this.folderId = data.folderId;
        this.applicantId = data.applicantId;

    }

    canSave(): boolean {
        return this.type;
    }

    close() {
        this.dialogRef.close();
    }

    save() {
        this.liability.expenseType = this.type;
        this.dialogRef.close();
        this.store.dispatch(new AddLiabilityAction(this.folderId, this.applicantId, this.liability));

    }

}

