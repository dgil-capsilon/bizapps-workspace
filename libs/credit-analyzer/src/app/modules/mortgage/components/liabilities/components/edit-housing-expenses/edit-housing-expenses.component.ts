import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {Applicant, HousingExpense, HousingExpenses, State} from '../../../../../../root-store/root-state';
import {UpdateHousingExpensesAction} from '../../../../../../root-store/root-actions';
import {HousingExpensesType} from '../../../../../../app.types';

@Component({
  selector: 'app-edit-housing-expenses',
  templateUrl: './edit-housing-expenses.component.html',
  styleUrls: ['./edit-housing-expenses.component.scss']
})
export class EditHousingExpensesComponent implements OnInit, OnChanges {

  @Output() editMode = new EventEmitter<boolean>();
  @Input() category;
  @Input() applicant: Applicant;
  he: HousingExpenses;
  mask;
  applicantId;
  folderId;
  externalId;

  constructor(private store: Store<State>) {
  }

  ngOnChanges() {
    if (this.applicant) {
      this.he = {
        presentHousingExpenses: Object.assign({}, this.applicant.housingExpenses.presentHousingExpenses),
        proposeHousingExpenses: Object.assign({}, this.applicant.housingExpenses.proposeHousingExpenses)
      };
    }
  }

  ngOnInit() {
    this.store.select('creditAnalyzer').subscribe(ca => {
      this.externalId = ca.externalId;
      this.folderId = ca.folderId;
    });
  }

  cancel() {
    this.editMode.emit(false);
  }

  change($event, type) {
    if (HousingExpensesType.Present === type) {
      this.he.presentHousingExpenses = Object.assign({}, $event);
    } else {
      this.he.proposeHousingExpenses = Object.assign({}, $event);
    }
  }

  save() {
    const {proposeHousingExpenses, presentHousingExpenses} = this.he;
    this.changePropertyInObjectToNumberValues(presentHousingExpenses);
    this.changePropertyInObjectToNumberValues(proposeHousingExpenses);
    this.store.dispatch(new UpdateHousingExpensesAction(this.folderId, this.he, this.applicant.id));
    this.editMode.emit(false);
  }

  changePropertyInObjectToNumberValues(object: HousingExpense) {
    object.firstMortgage = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.firstMortgage);
    object.hazardInsurance = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.hazardInsurance);
    object.homeownersAssociationDues = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.homeownersAssociationDues);
    object.mortgageInsurance = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.mortgageInsurance);
    object.other = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.other);
    object.otherFinancing = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.otherFinancing);
    object.realEstateTaxes = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.realEstateTaxes);
    object.rent = this.checkIfValueIsNotNullAndIfNotReturnNumberValue(object.rent);

  }

  checkIfValueIsNotNullAndIfNotReturnNumberValue(value) {
    return value ? this.changeValueToNumber(value) : value;
  }

  changeValueToNumber(value) {
    return Number(value.toString().replace(/[^\d.-]/g, ''));
  }
}
