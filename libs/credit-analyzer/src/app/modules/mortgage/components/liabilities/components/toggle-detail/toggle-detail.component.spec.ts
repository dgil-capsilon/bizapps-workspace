import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleDetailComponent } from './toggle-detail.component';

xdescribe('ToggleDetailComponent', () => {
  let component: ToggleDetailComponent;
  let fixture: ComponentFixture<ToggleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
