import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-toggle-detail',
  templateUrl: './toggle-detail.component.html',
  styleUrls: ['./toggle-detail.component.scss']
})
export class ToggleDetailComponent implements OnInit {

  @Input() detailLiability;
  @Input() category;
  @Input() folderId;
  editMode: boolean;

  constructor() {
  }

  ngOnInit() {
  }
}
