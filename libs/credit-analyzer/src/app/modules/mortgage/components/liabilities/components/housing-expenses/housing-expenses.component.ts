import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {Applicant, HousingExpenses, State} from '../../../../../../root-store/root-state';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-housing-expenses',
    templateUrl: './housing-expenses.component.html',
    styleUrls: ['./housing-expenses.component.scss']
})
export class HousingExpensesComponent implements OnInit {

  @Input() category;
  @Output() editMode = new EventEmitter<boolean>();
    he: HousingExpenses;
    applicantId;
    applicant: Applicant;
    folderId;

    constructor(private store: Store<State>, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.applicantId = params['id'];
            this.store.select('creditAnalyzer', 'applicants').subscribe(app => {
                this.applicant = app.find(x => x.id == this.applicantId);
                this.he = this.applicant.housingExpenses;
            });
        });
        this.store.select('creditAnalyzer', 'folderId').subscribe(id => {
            this.folderId = id;
        });
    }

    edit() {
      this.editMode.emit(true);
    }
}
