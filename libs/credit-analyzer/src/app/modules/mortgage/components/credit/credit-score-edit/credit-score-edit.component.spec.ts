import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditScoreEditComponent } from './credit-score-edit.component';

xdescribe('CreditScoreEditComponent', () => {
  let component: CreditScoreEditComponent;
  let fixture: ComponentFixture<CreditScoreEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditScoreEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoreEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
