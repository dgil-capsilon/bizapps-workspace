import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Applicant} from '../../../credit-analyzer/models/credit-analyzer-models';
import * as moment from 'moment';
import {Store} from '@ngrx/store';
import {State} from '../../../../root-store/root-state';
import {UpdateCreditScoreAction} from '../../../../root-store/root-actions';
import {ProviderType} from '../../../../app.types';


@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit, OnChanges {
  @Input() applicant: Applicant;
  creditScores: Array<CreditScoreDetails>;
  editMode: boolean = false;
  provider: CreditScoreDetails;
  daysToExpiration: number = 0;
  folderId;
  externalId;

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
    this.store.select('creditAnalyzer').subscribe(id => {
      this.folderId = id.folderId;
      this.externalId = id.externalId;
    });

  }

  ngOnChanges() {
    if (this.applicant) {
      const {creditScores, selectedProvider} = this.applicant;
      this.creditScores = [
        {
          name: ProviderType.EQUIFAX,
          img: 'assets/images/svg/equifax.svg',
          score: creditScores.equifax !== null ? creditScores.equifax : 'None',
          active: selectedProvider === ProviderType.EQUIFAX.toString()
        },
        {
          name: ProviderType.TRANSUNION,
          img: 'assets/images/svg/transunion.svg',
          score: creditScores.transunion !== null ? creditScores.transunion : 'None',
          active: selectedProvider === ProviderType.TRANSUNION.toString()
        },
        {
          name: ProviderType.EXPERIAN,
          img: 'assets/images/svg/experian.svg',
          score: creditScores.experian !== null ? creditScores.experian : 'None',
          active: selectedProvider === ProviderType.EXPERIAN.toString()
        }
      ];
      const current = moment().startOf('day');
      const given = moment(this.applicant.creditReportExpirationDate);
      this.daysToExpiration = given.diff(current, 'days');
      this.provider = this.getActiveProvider(this.creditScores, selectedProvider);
    }
  }

  save() {
    this.store.dispatch(new UpdateCreditScoreAction(this.folderId, this.applicant.id, this.applicant.selectedProvider));
  }

  getActiveProvider(creditScoreArray: Array<CreditScoreDetails>, selectedProvider: string) {
    const qualifyingProvider = creditScoreArray.find(x => x.name === selectedProvider);
    return qualifyingProvider ? qualifyingProvider : {name: 'None', score: 0, img: null, active: false};
  }
}

export interface CreditScoreDetails {
  name: string;
  img: string;
  score: any;
  active: boolean;
}



