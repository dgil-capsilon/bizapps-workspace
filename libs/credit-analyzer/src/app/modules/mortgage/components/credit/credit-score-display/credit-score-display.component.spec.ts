import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditScoreDisplayComponent } from './credit-score-display.component';

xdescribe('CreditScoreDisplayComponent', () => {
  let component: CreditScoreDisplayComponent;
  let fixture: ComponentFixture<CreditScoreDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditScoreDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoreDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
