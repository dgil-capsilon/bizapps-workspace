import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {CreditScoreDetails} from '../credit.component';
import {UpdateCreditScoreAction} from '../../../../../root-store/root-actions';
import {Store} from '@ngrx/store';
import {State} from '../../../../../root-store/root-state';
import {ProviderType} from '../../../../../app.types';

@Component({
  selector: 'app-credit-score-edit',
  templateUrl: './credit-score-edit.component.html',
  styleUrls: ['./credit-score-edit.component.scss']
})
export class CreditScoreEditComponent implements OnInit, OnChanges {

  @Input() creditScores: Array<CreditScoreDetails>;
  @Input() providerName: ProviderType;
  @Input() folderId;
  @Input() applicant;
  @Output() editMode = new EventEmitter();
  providerNameEdited;

  constructor(private store: Store<State>) { }

  ngOnInit() {

  }

  ngOnChanges() {
    this.providerNameEdited = this.providerName;
  }

  cancel() {
    this.editMode.emit(false);
  }

  save() {
    this.store.dispatch(new UpdateCreditScoreAction(this.folderId, this.applicant.id, this.providerNameEdited));
    this.editMode.emit(false);
  }
}
