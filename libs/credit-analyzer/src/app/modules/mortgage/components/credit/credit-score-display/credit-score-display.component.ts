import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CreditScoreDetails} from '../credit.component';
import {ProviderType} from '../../../../../app.types';

@Component({
  selector: 'app-credit-score-display',
  templateUrl: './credit-score-display.component.html',
  styleUrls: ['./credit-score-display.component.scss']
})
export class CreditScoreDisplayComponent implements OnInit {

  @Input() creditScores: CreditScoreDetails;
  @Input() providerName: ProviderType;
  @Input() creditReportOrderedDate;
  @Output() editMode = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  edit(){
    this.editMode.emit(true);
  }
}
