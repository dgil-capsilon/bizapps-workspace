import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  @Input() creditScore;
  constructor() { }

  ngOnInit() {
  }

}
