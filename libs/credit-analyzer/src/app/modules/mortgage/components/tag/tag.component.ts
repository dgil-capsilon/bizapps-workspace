import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {TagColor} from '../../../../app.types';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {

  @Input() tag;
  @Input() days: number;
  @HostBinding('class') tagColor = 'tag';

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.days !== null && this.days > 0) {
      let day = this.days === 1 ? ' day' : ' days';
      this.tag = 'Expires ' + this.days + day;
      this.tagColor = 'tag ' + TagColor['Expires'];
    } else if (this.days < 0) {
      this.tag = 'Expired';
      this.tagColor = 'tag ' + TagColor[this.tag];
    } else if (this.days === 0) {
      this.tag = 'Expires today';
      this.tagColor = 'tag ' + TagColor['Expires'];
    } else {
      this.tagColor = 'tag ' + TagColor[this.tag];
    }
  }
}
