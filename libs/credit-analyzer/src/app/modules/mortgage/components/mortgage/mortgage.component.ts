import {Component, OnDestroy, OnInit} from '@angular/core';
import {State} from '../../../../root-store/root-state';
import {Store} from '@ngrx/store';
import {Applicant} from '../../../credit-analyzer/models/credit-analyzer-models';
import {StoreService} from '../../../../root-store/store.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {UpdateNavigationComponent} from '../../../../root-store/root-actions';

@Component({
  selector: 'app-mortgage',
  templateUrl: './mortgage.component.html',
  styleUrls: ['./mortgage.component.scss']
})
export class MortgageComponent implements OnInit, OnDestroy {
  applicant;
  applicants: Array<Applicant>;
  folderId: number;
  inquiries;
  publicRecords;
  applicant$: Subscription;
  subscription$: Subscription;

  constructor(private storeService: StoreService,
              private route: ActivatedRoute,
              private store: Store<State>,
              private router: Router) {
  }

  ngOnInit(): void {
    this.subscription$ = this.store.select('creditAnalyzer', 'folderId').subscribe(folderId => {
      this.folderId = folderId;
    });

    this.subscription$ = this.store.select('creditAnalyzer', 'applicants').subscribe(applicants => {
      this.applicants = applicants;

      this.applicant$ = this.route.params.subscribe(params => {
          const id = params['id'];
          if (this.applicants) {
            this.applicant = this.applicants.find(x => x.id == id) || null;
            if (this.applicant !== null) {
              this.inquiries = this.applicant.inquiries;
              this.publicRecords = this.applicant.publicRecords;
            } else {
              this.router.navigate(['/summary'], {relativeTo: this.route, queryParamsHandling: 'merge'});
            }
          }
        }
      );
    });
  }

  ngOnDestroy(): void {
    this.subscription$.unsubscribe();
    this.applicant$.unsubscribe();
    this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
  }
}
