import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPublicRecordNameComponent } from './edit-public-record-name.component';

xdescribe('EditPublicRecordNameComponent', () => {
  let component: EditPublicRecordNameComponent;
  let fixture: ComponentFixture<EditPublicRecordNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPublicRecordNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPublicRecordNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
