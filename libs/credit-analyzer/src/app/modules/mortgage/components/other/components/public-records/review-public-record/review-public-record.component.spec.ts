import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPublicRecordComponent } from './review-public-record.component';

xdescribe('ReviewPublicRecordComponent', () => {
  let component: ReviewPublicRecordComponent;
  let fixture: ComponentFixture<ReviewPublicRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewPublicRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPublicRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
