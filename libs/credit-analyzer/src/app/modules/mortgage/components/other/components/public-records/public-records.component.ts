import {Component, Input, OnInit} from '@angular/core';
import {PublicRecord} from '../../../../../credit-analyzer/models/credit-analyzer-models';

@Component({
  selector: 'app-public-records',
  templateUrl: './public-records.component.html',
  styleUrls: ['./public-records.component.scss']
})
export class PublicRecordsComponent implements OnInit {
  editMode: boolean;

  @Input() publicRecord: PublicRecord;
  @Input() applicantId: number;

  constructor() {
  }

  ngOnInit() {
  }
}
