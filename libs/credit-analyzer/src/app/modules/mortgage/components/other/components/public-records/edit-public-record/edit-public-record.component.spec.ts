import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPublicRecordComponent } from './edit-public-record.component';

xdescribe('EditPublicRecordComponent', () => {
  let component: EditPublicRecordComponent;
  let fixture: ComponentFixture<EditPublicRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPublicRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPublicRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
