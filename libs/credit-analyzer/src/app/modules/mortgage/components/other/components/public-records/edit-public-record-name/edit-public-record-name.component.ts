import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import {PublicRecord, State} from '../../../../../../../root-store/root-state';
import {UpdatePublicRecordAction} from '../../../../../../../root-store/root-actions';
import {FormControl, Validators} from '@angular/forms';
import {StoreService} from '../../../../../../../root-store/store.service';

@Component({
  selector: 'app-edit-public-record-name',
  templateUrl: './edit-public-record-name.component.html',
  styleUrls: ['./edit-public-record-name.component.scss']
})
export class EditPublicRecordNameComponent implements OnInit {
  name: string;
  folderId: number;
  applicantId: number;
  publicRecordId: number;
  publicRecord: PublicRecord;
  nameControl = new FormControl('', Validators.required);

  constructor(public dialogRef: MatDialogRef<EditPublicRecordNameComponent>, @Inject(MAT_DIALOG_DATA) public data,
              private store: Store<State>, private storeService: StoreService) {
  }

  ngOnInit() {
    this.applicantId = this.data.applicantId;
    this.publicRecord = Object.assign({}, this.data.publicRecord);
    this.publicRecordId = this.data.publicRecord.id;
    this.name = this.data.publicRecord.name;

    this.storeService.getAllState().subscribe(state => {
      this.folderId = state.folderId;
    });
  }

  close() {
    this.dialogRef.close();
  }

  canSave(): boolean {
    return this.nameControl.valid && !this.isWhiteSpace(this.name);
  }

  isWhiteSpace(value: string): boolean {
    return !value.trim();
  }

  save() {
    if (this.nameControl.valid) {
      const publicRecord: PublicRecord = this.publicRecord;

      const body = {
        courtName: publicRecord.courtName,
        creditRepositories: publicRecord.creditRepositories,
        dateFiled: publicRecord.dateFiled,
        dateReported: publicRecord.dateReported,
        dateSatisfied: publicRecord.dateSatisfied,
        dispositionType: publicRecord.dispositionType,
        docketNumber: publicRecord.docketNumber,
        legalObligationAmount: publicRecord.legalObligationAmount,
        name: this.name
      };

      this.store.dispatch(new UpdatePublicRecordAction(this.folderId, this.applicantId, this.publicRecordId, body));
      this.dialogRef.close();
    }
  }
}

