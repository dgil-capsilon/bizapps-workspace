import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Inquiry} from '../../../../../../root-store/root-state';

@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss']
})
export class InquiryComponent implements OnInit, OnChanges {

  @Input() inquiry: Inquiry;
  @Input() index;
  tags: Array<String> = [];

  constructor() {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.inquiry) {
      this.inquiry.creditRepositories
        .forEach(x => {
          if(x.creditRepositorySource == "Other" && x.description == "MergedData"){
            this.tags.push("Equifax");
            this.tags.push("TransUnion");
            this.tags.push("Experian");
          }
          else{
            this.tags.push(x.creditRepositorySource);
          }
        });
    }
  }

}

