import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {EditPublicRecordNameComponent} from './components/public-records/edit-public-record-name/edit-public-record-name.component';
import {Applicant, PublicRecord} from '../../../credit-analyzer/models/credit-analyzer-models';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../../../root-store/root-state';
import {UpdateNavigationComponent} from '../../../../root-store/root-actions';
import {ScrollingMethods} from '../../../../utils/scrolling-methods/scrolling-methods';
import {MatAccordion, MatExpansionPanel} from '@angular/material';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss'],
  viewProviders: [MatExpansionPanel, MatAccordion]
})
export class OtherComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('inquiresPanel', {static: false}) inquiresPanel: MatExpansionPanel;
  @ViewChild('publicRecordsPanel', {static: false}) publicRecordsPanel: MatExpansionPanel;
  @ViewChild('publicRecordsChildPanel', {static: false}) publicRecordsChildPanel: MatAccordion;

  @Input() inquiries;
  @Input() publicRecords: Array<PublicRecord>;
  @Input() applicant: Applicant;
  open: string;
  subscription$: Subscription;

  tags: Array<string> = [];

  constructor(public dialog: MatDialog, private store: Store<State>, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.subscription$ = this.store.select('creditAnalyzer', 'openedComponent').subscribe(openedComponent => {
      this.open = openedComponent;

      if (openedComponent === 'inquiries') {
        if (this.inquiries !== null && this.inquiries.length > 0) {
          this.inquiresPanel.open();
        }
        ScrollingMethods.scrollWithDelay('inquiries');
      }
      if (openedComponent === 'public-records') {
        if (this.publicRecords !== null && this.publicRecords.length > 0) {
          this.publicRecordsPanel.open();
          this.publicRecordsChildPanel.openAll();
        }
        ScrollingMethods.scrollWithDelay('public-records');
      }
    });

    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this.subscription$.unsubscribe();
  }

  getTagsFromPublicRecord(publicRecord) {
    this.tags = [];
    if (publicRecord.creditRepositories) {
      publicRecord.creditRepositories.forEach(x => {
        if (x.creditRepositorySource === 'Other' && x.description === 'MergedData') {
          this.tags.push('Equifax');
          this.tags.push('TransUnion');
          this.tags.push('Experian');
        } else {
          this.tags.push(x.creditRepositorySource);
        }
      });
    }
    return this.tags;
  }

  closeAllCategory() {
    if (this.open !== null) {
      this.store.dispatch(new UpdateNavigationComponent({openedComponent: null}));
    }
  }

  editName(publicRecord: PublicRecord, event: Event) {
    event.stopPropagation();

    const dialogRef = this.dialog.open(EditPublicRecordNameComponent, {
      width: '400px',
      data: {
        publicRecord: publicRecord,
        applicantId: this.applicant.id,
      }
    });
  }

}
