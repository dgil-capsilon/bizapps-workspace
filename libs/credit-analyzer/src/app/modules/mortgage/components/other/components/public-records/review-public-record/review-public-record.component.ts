import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PublicRecord} from '../../../../../../credit-analyzer/models/credit-analyzer-models';
import {DispositionType} from '../../../../../../../app.types';

@Component({
  selector: 'app-review-public-record',
  templateUrl: './review-public-record.component.html',
  styleUrls: ['./review-public-record.component.scss']
})
export class ReviewPublicRecordComponent implements OnInit {

  @Input() publicRecord: PublicRecord;
  @Output() editMode = new EventEmitter<boolean>();
  dispositionType = DispositionType;

  constructor() {
  }

  ngOnInit() {
  }

  edit(){
    this.editMode.emit(true);
  }

}
