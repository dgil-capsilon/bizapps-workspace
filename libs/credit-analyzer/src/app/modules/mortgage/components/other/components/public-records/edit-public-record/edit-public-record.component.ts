import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PublicRecord} from '../../../../../../credit-analyzer/models/credit-analyzer-models';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {UpdatePublicRecordAction} from '../../../../../../../root-store/root-actions';
import {Store} from '@ngrx/store';
import {State} from '../../../../../../../root-store/root-state';
import {DispositionType} from '../../../../../../../app.types';
import {StoreService} from '../../../../../../../root-store/store.service';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {ActivatedRoute, Router} from '@angular/router';

export const DATE_MMYY_FORMAT = {
  parse: {
    dateInput: 'MM/YY'
  },
  display: {
    dateInput: 'MM/YY',
    monthYearLabel: 'MMM YYYY'
  }
};

@Component({
  selector: 'app-edit-public-record',
  templateUrl: './edit-public-record.component.html',
  styleUrls: ['./edit-public-record.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DATE_MMYY_FORMAT}
  ]
})
export class EditPublicRecordComponent implements OnInit {

  @Input() publicRecord: PublicRecord;
  editedPublicRecord: PublicRecord;
  @Input() applicantId: number;
  @Output() editMode = new EventEmitter<boolean>();
  folderId: number;
  keys;
  dispositionType = DispositionType;
  mask;
  externalId: string;

  constructor(private store: Store<State>, private storeService: StoreService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.keys = Object.keys(this.dispositionType);
    this.editedPublicRecord = Object.assign({}, this.publicRecord);

    this.storeService.getAllState().subscribe(state => {
      this.folderId = state.folderId;
    });
    this.mask = createNumberMask({
      integerLimit: 8,
      prefix: '',
      allowDecimal: true
    });
  }

  cancel() {
    this.editMode.emit(false);
  }

  save() {
    const publicRecord: PublicRecord = this.editedPublicRecord;


    publicRecord.legalObligationAmount = this.editedPublicRecord.legalObligationAmount ?
      Number(this.editedPublicRecord.legalObligationAmount.toString().replace(/[^\d.-]/g, ''))
      : this.editedPublicRecord.legalObligationAmount;

    const body = {
      courtName: publicRecord.courtName,
      creditRepositories: publicRecord.creditRepositories,
      dateFiled: publicRecord.dateFiled,
      dateReported: publicRecord.dateReported,
      dateSatisfied: publicRecord.dateSatisfied,
      dispositionType: publicRecord.dispositionType,
      docketNumber: publicRecord.docketNumber,
      legalObligationAmount: publicRecord.legalObligationAmount,
      name: publicRecord.name
    };
    this.store.dispatch(new UpdatePublicRecordAction(this.folderId, this.applicantId, this.editedPublicRecord.id, body));
    this.router.navigate(['/applicant', this.applicantId], {relativeTo: this.route, queryParamsHandling: 'merge'});

    this.editMode.emit(false);
  }
}
