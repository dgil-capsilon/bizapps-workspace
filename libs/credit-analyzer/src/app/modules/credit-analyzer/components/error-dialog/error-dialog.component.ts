import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {
  spinnerEnabled = true;
  message: string;

  constructor(public dialogRef: MatDialogRef<ErrorDialogComponent>, @Inject(MAT_DIALOG_DATA) public data) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    if (this.data && this.data.message) {
      this.message = this.data.message;
    }
  }

  closeSpinner() {
    this.spinnerEnabled = false;
  }

}
