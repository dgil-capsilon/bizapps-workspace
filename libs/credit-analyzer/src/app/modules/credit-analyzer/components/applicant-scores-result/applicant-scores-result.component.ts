import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-applicant-scores-result',
  templateUrl: './applicant-scores-result.component.html',
  styleUrls: ['./applicant-scores-result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicantScoresResultComponent {
  @Input() color: string;
  @Input() scores;
}
