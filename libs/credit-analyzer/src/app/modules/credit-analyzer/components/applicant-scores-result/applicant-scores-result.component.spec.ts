import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantScoresResultComponent } from './applicant-scores-result.component';
import {NonePipe} from '../../../../pipes/none.pipe';

describe('ApplicantScoresResultComponent', () => {
  let component: ApplicantScoresResultComponent;
  let fixture: ComponentFixture<ApplicantScoresResultComponent>;
  let componentEl: HTMLElement;
  const color = 'dark-blue';
  const scores = {
    equifax: 699,
    transUnion: 620,
    experian: 675
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantScoresResultComponent, NonePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantScoresResultComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.scores = scores;
    component.color = color;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have dot with dark blue color', () => {
    expect(componentEl.querySelector('.dot').className).toContain('dark-blue');
  });

  it('should have correct scores in tooltip', () => {
    expect(componentEl.querySelectorAll('.score')[0].textContent).toBe('Equifax: ' + scores.equifax);
    expect(componentEl.querySelectorAll('.score')[1].textContent).toBe('TransUnion: ' + scores.transUnion);
    expect(componentEl.querySelectorAll('.score')[2].textContent).toBe('Experian: ' + scores.experian);
  });
});
