import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtiComponent } from './dti.component';

xdescribe('DtiComponent', () => {
  let component: DtiComponent;
  let fixture: ComponentFixture<DtiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
