import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Summary} from '../../models/credit-analyzer-models';
import {ApplicantDTI, DtiSummary, State} from '../../../../root-store/root-state';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-dti-component',
  templateUrl: './dti.component.html',
  styleUrls: ['./dti.component.scss']
})
export class DtiComponent implements OnInit, OnChanges {
  @Input() application: Summary;
  scores = [];
  maxScore: number;
  dti: number = 0;
  isFrontend: boolean;
  isLoading: boolean;
  dtiSummary: DtiSummary;

  constructor(private store: Store<State>) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    this.isFrontend = false;
    this.store.select('creditAnalyzer', 'dtiSummary').subscribe(dti => {
      this.scores = [];
      this.dtiSummary = dti;
      this.dtiSummary.applicants.forEach(app => this.scores.push(app.overallDti));
      this.maxScore = Math.max(...this.scores);
      this.dti = dti.overallDti;
    });
  }

  calculateProgress(applicant: ApplicantDTI) {
    const value = this.isFrontend ? applicant.frontendDti : applicant.overallDti;

    return value && value > 0 ? value * 100 / this.maxScore : 0;
  }

  loadDtiOverall(isOverall: boolean) {
    this.isFrontend = !isOverall;
    this.scores = [];

    if (this.isFrontend) {
      this.dtiSummary.applicants.forEach(app => {
        this.scores.push(app.frontendDti);
      });
      this.dti = this.dtiSummary.frontendDti;
    } else {
      this.dtiSummary.applicants.forEach(app => {
        this.scores.push(app.overallDti);
      });
      this.dti = this.dtiSummary.overallDti;
    }
    this.maxScore = Math.max(...this.scores);
  }

  get overall() {
    return this.isFrontend ? '' : 'active-btn';
  }

  get frontend() {
    return this.isFrontend ? 'active-btn' : '';
  }

  get tooHigh() {
    return this.dti > 40 ? 'dti-score red' : 'dti-score';
  }
}
