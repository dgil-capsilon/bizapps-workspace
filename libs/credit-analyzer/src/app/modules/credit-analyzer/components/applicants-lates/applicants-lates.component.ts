import {Component, Input, OnInit} from '@angular/core';
import {Late} from '../../models/credit-analyzer-models';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-applicants-lates',
  templateUrl: './applicants-lates.component.html',
  styleUrls: ['./applicants-lates.component.scss'],
})
export class ApplicantsLatesComponent implements OnInit {
  @Input() lates: Array<Late>;
  @Input() type: string;
  latesAmount = 0;
  applicantsCount = 0;
  latesExist = false;

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.lates && this.lates.length > 0) {
      this.applicantsCount = this.lates.length;
      this.latesExist = true;
      let latesAmount = 0;
      this.lates.forEach(x => {
         latesAmount += x.dates.length;
      });
      this.latesAmount = latesAmount;
    }
  }
}
