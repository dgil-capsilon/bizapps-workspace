import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {ApplicantsLatesComponent} from './applicants-lates.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { MatIconModule } from '@angular/material/icon';
import {routes} from '../../../../routes';

xdescribe('ApplicantsLatesComponent', () => {
  let component: ApplicantsLatesComponent;
  let fixture: ComponentFixture<ApplicantsLatesComponent>;
  let componentEl: HTMLElement;

  const lates = [
    {
      applicantId: 1293,
      firstName: 'Andrew',
      lastName: 'Homeowner',
      dates: ['2018-01-01', '2018-03-01']
    },
    {
      applicantId: 1294,
      firstName: 'Mary',
      lastName: 'Homeowner',
      dates: ['2018-01-01', '2018-03-01']
    },
    {
      applicantId: 1295,
      firstName: 'Homer',
      lastName: 'Loanseeker',
      dates: ['2018-01-01', '2018-03-01']
    },
    {
      applicantId: 1296,
      firstName: 'Alice',
      lastName: 'Firstimer',
      dates: ['2018-01-01', '2018-03-01']
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicantsLatesComponent],
      imports: [PerfectScrollbarModule, MatIconModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantsLatesComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.lates = lates;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display horizontal line if there are no lates', () => {
    component.lates = null;
    component.latesExist = false;
    component.type = "";
    component.ngOnInit();
    fixture.detectChanges();
    expect(componentEl.querySelector('.no-lates')).toBeTruthy();
  });

  it('shouldn\'t display tooltip component if there are lates', () => {
    component.lates = null;
    component.latesExist = false;
    component.ngOnInit();
    fixture.detectChanges();
    expect(componentEl.querySelector('.tooltip-lates')).toBeFalsy();
  });

  it('should display lates component if there are lates', () => {
    expect(componentEl.querySelector('.lates-container')).toBeTruthy();
  });

  it('should display correct total lates', () => {
    expect(componentEl.querySelector('.red-circle').textContent).toContain(countLates(lates).toString());
  });

  it('should display correct applicants amount', () => {
    expect(componentEl.querySelector('.applicants-in-late span').textContent).toContain(lates.length.toString());
  });

  it('should display tooltip with correct data', () => {
    expect(componentEl.querySelectorAll('.applicant-name').length).toBe(4);

    for (let i = 0; i < lates.length; i++) {
      expect(componentEl.querySelectorAll('.applicant-name').item(i).textContent)
        .toContain(lates[i].firstName + ' ' + lates[i].lastName);

      for (let j = 0; j < lates[i].dates.length; j++) {
        expect(componentEl.querySelectorAll('.date').item(j).textContent)
          .toContain(convertDate(lates[i].dates[j]));
      }
    }
  });
});

function countLates(lates) {
  let latesAmount = 0;
  lates.forEach(x => {
    latesAmount += x.dates.length;
  });
  return latesAmount;
}

function convertDate(date) {
  const month = date.split('-')[1];
  const year = date.split('-')[0];

  return month + '/' + year;
}
