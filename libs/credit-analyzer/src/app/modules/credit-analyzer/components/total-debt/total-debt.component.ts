import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {LiabilitiesSummary, Applicant, Liability} from '../../models/credit-analyzer-models';

@Component({
  selector: 'app-total-debt',
  templateUrl: './total-debt.component.html',
  styleUrls: ['./total-debt.component.scss']
})
export class TotalDebtComponent implements OnInit, OnChanges {
  @Input() liabilitiesSummary: LiabilitiesSummary;
  @Input() applicants: Applicant[];
  maxSum: number;

  colors: [{
    applicantId: number,
    color: string
  }];

  constructor() {
    this.colors = [{
      applicantId: null,
      color: null
    }];
  }

  ngOnInit() {

  }

  ngOnChanges() {
    const liabilities = this.liabilitiesSummary;
    if (liabilities && liabilities.liabilitiesTypes && liabilities.liabilitiesTypes.length > 0) {
      this.maxSum = Math.max(...liabilities.liabilitiesTypes.map(a => a.totalAmount));
    }

    if (this.applicants && this.applicants.length > 0) {
      this.assignColors();
    }
  }

  assignColors() {
    const colors = ['light-blue', 'dark-blue', 'green', 'orange', 'red', 'purple'];
    const sortedIds = this.applicants.map(a => a.id);
    this.colors = [{
      applicantId: null,
      color: null
    }];
    for (let i = 0; i < sortedIds.length; i++) {
      this.colors.push({
        applicantId: sortedIds[i],
        color: colors[i]
      });
    }
  }

  get totalAmount() {
    return this.liabilitiesSummary && this.liabilitiesSummary.totalAmount ? this.liabilitiesSummary.totalAmount : 0;
  }

}
