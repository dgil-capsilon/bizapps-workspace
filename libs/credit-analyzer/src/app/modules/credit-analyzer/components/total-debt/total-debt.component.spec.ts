import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TotalDebtComponent} from './total-debt.component';
import {ScoreBadgeComponent} from '../../../sidebar/components/score-badge/score-badge.component';
import {HorizontalProgressBarComponent} from '../horizontal-progress-bar/horizontal-progress-bar.component';
import {LiabilityTypeComponent} from '../liability-type/liability-type.component';
import {LiabilitiesSummary} from '../../models/credit-analyzer-models';
import {LiabilityCategoryType} from '../../../../app.types';
import {DashPipe} from '../../../../pipes/dash-pipe';

describe('TotalDebtComponent', () => {
  let component: TotalDebtComponent;
  let fixture: ComponentFixture<TotalDebtComponent>;
  let componentEl: HTMLElement;

  const liabilitiesSummary: LiabilitiesSummary = {
    installment: null,
    mortgage: null,
    other: null,
    revolving: null,
    housingExpenses: null,
    liabilitiesTypes: [
      {
        byApplicants: [
          {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, amount: 500},
          {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, amount: 500},
          {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, amount: 500},
          {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, amount: 500}
        ],
        name: 'Installment',
        totalAmount: 2000,
        type: LiabilityCategoryType.INSTALLMENT,
      },
      {
        byApplicants: [
          {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, amount: 1000},
          {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, amount: 1000},
          {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, amount: 1000},
          {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, amount: 1000}
        ],
        name: 'Mortgage',
        totalAmount: 4000,
        type: LiabilityCategoryType.MORTGAGE,
      },
      {
        byApplicants: [
          {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, amount: 50},
          {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, amount: 50},
          {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, amount: 50},
          {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, amount: 50}
        ],
        name: 'Other',
        totalAmount: 200,
        type: LiabilityCategoryType.OTHER,
      },
      {
        byApplicants: [
          {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, amount: 200},
          {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, amount: 200},
          {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, amount: 200},
          {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, amount: 200}
        ],
        name: 'Revolving',
        totalAmount: 800,
        type: LiabilityCategoryType.REVOLVING,
      },
      {
        byApplicants: [
          {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, amount: 100},
          {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, amount: 200},
          {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, amount: 300},
          {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, amount: 400}
        ],
        name: 'Housing Expenses',
        totalAmount: 1000,
        type: LiabilityCategoryType.HOUSING_EXPENSES,
      }],
    totalAmount: 8000
  };

  const applicants = [
    {
      creditReportExpirationDate: '2018-12-25',
      creditReportOrderedDate: '2018-09-25',
      creditScores: null,
      firstName: 'Andrew',
      id: 1293,
      included: true,
      lastName: 'Homeowner',
      liabilitiesTypes: null,
      score: 735,
      ssn: '123456789',
      totalCollections: null,
      totalInquiries: null,
      totalPublicRecords: null,
      totalForeclosure: null,
      publicRecords: null,
      housingExpenses: null,
      selectedProvider: null
    },
    {
      creditReportExpirationDate: '2018-12-25',
      creditReportOrderedDate: '2018-09-25',
      creditScores: null,
      firstName: 'Mary',
      id: 1294,
      included: true,
      lastName: 'Homeowner',
      liabilitiesTypes: null,
      score: 656,
      ssn: '123456780',
      totalCollections: null,
      totalInquiries: null,
      totalPublicRecords: null,
      totalForeclosure: null,
      publicRecords: null,
      housingExpenses: null,
      selectedProvider: null
    },
    {
      creditReportExpirationDate: '2018-12-25',
      creditReportOrderedDate: '2018-09-25',
      creditScores: null,
      firstName: 'Homer',
      id: 1295,
      included: true,
      lastName: 'Loanseeker',
      liabilitiesTypes: null,
      score: 755,
      ssn: '123456780',
      totalCollections: null,
      totalInquiries: null,
      totalPublicRecords: null,
      totalForeclosure: null,
      publicRecords: null,
      housingExpenses: null,
      selectedProvider: null
    },
    {
      creditReportExpirationDate: '2018-12-25',
      creditReportOrderedDate: '2018-09-25',
      creditScores: null,
      firstName: 'Alice',
      id: 1296,
      included: true,
      lastName: 'Firstimer',
      liabilitiesTypes: null,
      score: 739,
      ssn: '123456785',
      totalCollections: null,
      totalInquiries: null,
      totalPublicRecords: null,
      totalForeclosure: null,
      publicRecords: null,
      housingExpenses: null,
      selectedProvider: null
    }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TotalDebtComponent, ScoreBadgeComponent, HorizontalProgressBarComponent, LiabilityTypeComponent, DashPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalDebtComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.liabilitiesSummary = liabilitiesSummary;
    component.applicants = applicants;
    component.ngOnChanges();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should display correct total debt amount', () => {
    expect(componentEl.querySelector('.score').textContent).toContain(component.liabilitiesSummary.totalAmount.toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD'
    }));
  });

  it('should display 5 liabilities types', () => {
    expect(componentEl.querySelectorAll('app-liability-type').length).toBe(5);
  });
});
