import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-liability-item',
  templateUrl: './liability-item.component.html',
  styleUrls: ['./liability-item.component.scss']
})
export class LiabilityItemComponent implements OnInit {
  @Input()
  headerHeight = '56px';
  @Input() liability;
  @Input() liabilityIndex;

  constructor() { }

  ngOnInit() {
  }

}
