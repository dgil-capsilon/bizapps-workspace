import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiabilityItemComponent } from './liability-item.component';

xdescribe('LiabilitiesListComponent', () => {
  let component: LiabilityItemComponent;
  let fixture: ComponentFixture<LiabilityItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiabilityItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiabilityItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
