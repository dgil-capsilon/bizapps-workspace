import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LiabilityTypeComponent} from './liability-type.component';
import {HorizontalProgressBarComponent} from '../horizontal-progress-bar/horizontal-progress-bar.component';
import {LiabilitiesType} from '../../models/credit-analyzer-models';
import {LiabilityCategoryType} from '../../../../app.types';

describe('LiabilityTypeComponent', () => {
  let component: LiabilityTypeComponent;
  let fixture: ComponentFixture<LiabilityTypeComponent>;
  let componentEl: HTMLElement;

  const liabilities: LiabilitiesType = {
    byApplicants: [
      {
        firstName: 'Andrew',
        lastName: 'Homeowner',
        applicantId: 1293,
        amount: 500
      },
      {
        firstName: 'Mary',
        lastName: 'Homeowner',
        applicantId: 1294,
        amount: 500
      },
      {
        firstName: 'Homer',
        lastName: 'Loanseeker',
        applicantId: 1295,
        amount: 1000
      },
      {
        firstName: 'Alice',
        lastName: 'Firstimer',
        applicantId: 1296,
        amount: 2000
      }],
    totalAmount: 4000,
    type: LiabilityCategoryType.MORTGAGE,
    name: 'Mortgage'
  };

  const colors: any = [
    {
      applicantId: 1293,
      color: 'light-blue'
    },
    {
      applicantId: 1294,
      color: 'dark-blue'
    },
    {
      applicantId: 1295,
      color: 'green'
    },
    {
      applicantId: 1296,
      color: 'orange'
    }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LiabilityTypeComponent, HorizontalProgressBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiabilityTypeComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.liabilities = liabilities;
    component.colors = colors;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display progress bar', () => {
    expect(componentEl.querySelector('app-horizontal-progress-bar')).toBeTruthy();
  });

  it('should display correct amount', () => {
    expect(componentEl.querySelector('.liability-value').textContent).toContain(liabilities.totalAmount.toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD'
    }));
  });
});
