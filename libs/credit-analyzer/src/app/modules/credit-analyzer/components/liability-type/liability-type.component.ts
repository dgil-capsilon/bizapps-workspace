import {Component, Input, OnInit} from '@angular/core';
import {LiabilitiesType} from '../../models/credit-analyzer-models';
import {LiabilityCategoryType} from '../../../../app.types';

@Component({
  selector: 'app-liability-type',
  templateUrl: './liability-type.component.html',
  styleUrls: ['./liability-type.component.scss']
})
export class LiabilityTypeComponent implements OnInit {
  @Input() liabilities: LiabilitiesType;
  @Input() maxSum: number;
  @Input() colors: [{
    applicantId: number,
    color: string
  }];
  liabilityTypes = LiabilityCategoryType;

  constructor() {
  }

  ngOnInit() {
  }

  get totalAmount() {
    let totalAmount = 0;
    if (this.liabilities && this.liabilities.totalAmount) {
      totalAmount = this.liabilities.totalAmount;
    }
    return totalAmount;
  }
}
