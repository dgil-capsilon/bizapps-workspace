import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ApplicantCreditScores} from '../../models/credit-analyzer-models';
import {ProviderType} from '../../../../app.types';

@Component({
  selector: 'app-credit-score',
  templateUrl: './credit-score.component.html',
  styleUrls: ['./credit-score.component.scss']
})
export class CreditScoreComponent implements OnInit, OnChanges {
  @Input() applicants;
  @Input() qualifyingScore;
  @Input() rank;
  applicantsProviders: Array<ApplicantCreditScores> = [];
  qualifiedScores = [];

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.qualifiedScores = [];
    this.applicantsProviders = [];
    if (this.applicants && this.applicants.length > 0) {
      this.applicants.forEach(applicant => {
        this.applicantsProviders.push({
          firstName: applicant.firstName,
          lastName: applicant.lastName,
          scores: {
            equifax: applicant.creditScores.equifax,
            transUnion: applicant.creditScores.transunion,
            experian: applicant.creditScores.experian,
          },
          isQualified: this.calculateQualifyingScore(applicant.id),
          isIncluded: applicant.included,
          selectedProvider: applicant.selectedProvider
        });
      });
    }
  }

  calculateQualifyingScore(applicantId) {
    const qualifiedApplicant = this.applicants.find(x => {
      let selectedScore = 0;

      if (x.selectedProvider === ProviderType.TRANSUNION) {
        selectedScore = x.creditScores.transunion;
      }
      if (x.selectedProvider === ProviderType.EQUIFAX) {
        selectedScore = x.creditScores.equifax;
      }
      if (x.selectedProvider === ProviderType.EXPERIAN) {
        selectedScore = x.creditScores.experian;
      }
      return this.qualifyingScore > 0 && x.included ? selectedScore === this.qualifyingScore : false;
    });
    const qualifiedApplicantId = qualifiedApplicant ? qualifiedApplicant.id : 0;
    return applicantId === qualifiedApplicantId;
  }
}
