import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreditScoreComponent} from './credit-score.component';
import {ScoreBadgeComponent} from '../../../sidebar/components/score-badge/score-badge.component';
import {ApplicantScoresComponent} from '../applicant-scores/applicant-scores.component';
import {MatIconModule} from '@angular/material/icon';
import {FelonizePipe} from '@pipes/felonize.pipe';
import {ProviderType} from '@app/app.types';
import {ApplicantScoresResultComponent} from '../applicant-scores-result/applicant-scores-result.component';
import {NonePipe} from '@pipes/none.pipe';
import {DashPipe} from '@pipes/dash-pipe';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CreditScoreComponent', () => {
  let component: CreditScoreComponent;
  let fixture: ComponentFixture<CreditScoreComponent>;
  let componentEl: HTMLElement;

  const applicants = [
      {
        firstName: 'John',
        lastName: 'Homeowner',
        ssn: '123456780',
        score: 847,
        creditScores: {
          equifax: 840,
          experian: 847,
          transunion: 850
        },
        id: 14,
        included: true,
        creditReportExpirationDate: null,
        creditReportOrderedDate: null,
        liabilitiesTypes: null,
        totalCollections: null,
        totalInquiries: null,
        totalPublicRecords: null,
        totalForeclosure: null,
        publicRecords: null,
        housingExpenses: null,
        selectedProvider: ProviderType.EXPERIAN
      },
      {
        firstName: 'Mary',
        lastName: 'Homeowner',
        ssn: '123456781',
        score: 749,
        creditScores: {
          equifax: 740,
          experian: 749,
          transunion: 760
        },
        id: 15,
        included: true,
        creditReportExpirationDate: null,
        creditReportOrderedDate: null,
        liabilitiesTypes: null,
        totalCollections: null,
        totalInquiries: null,
        totalPublicRecords: null,
        totalForeclosure: null,
        publicRecords: null,
        housingExpenses: null,
        selectedProvider: ProviderType.EXPERIAN
      }
    ];

    const qualifyingScore = 749;
    const rank = 'FAIR';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditScoreComponent, ScoreBadgeComponent, ApplicantScoresComponent, ApplicantScoresResultComponent, FelonizePipe, NonePipe, DashPipe],
      imports: [MatIconModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoreComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;

    component.applicants = applicants;
    component.qualifyingScore = qualifyingScore;
    component.rank = rank;
    component.ngOnChanges();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display score in score badge component', () => {
    expect(componentEl.querySelector('.score').textContent).toContain(qualifyingScore.toString());
  });

  it('should display rank in score badge component', () => {
    expect(componentEl.querySelector('.rank').textContent).toContain('FAIR');
  });

  it('should have orange color in score badge component', () => {
    expect(componentEl.querySelector('.rank').className).toContain('orange');
  });

  it('should have 2 applicants', () => {
    expect(componentEl.querySelectorAll('app-applicant-scores').length).toBe(2);
  });

  it('should have 2 vertical bars', () => {
    expect(componentEl.querySelectorAll('app-applicant-scores-result').length).toBe(2);
  });

  it('should check qualified score', () => {
    expect(componentEl.querySelector('.icon-star').parentElement.innerHTML).toContain(qualifyingScore.toString());
  });
});
