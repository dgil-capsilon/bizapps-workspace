import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {LatesSummary} from '../../models/credit-analyzer-models';
import {Store} from '@ngrx/store';
import {State} from '../../../../root-store/root-state';
import {LoadLatesSummaryAction} from '../../../../root-store/root-actions';

@Component({
  selector: 'app-total-lates',
  templateUrl: './total-lates.component.html',
  styleUrls: ['./total-lates.component.scss']
})
export class TotalLatesComponent implements OnInit, OnChanges {
  @Input() latesSummary: LatesSummary;
  @Input() folderId: string;
  allLates = [];
  isLoading: boolean;

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    const emptyLates = {
      lates30Days: null,
      lates60Days: null,
      lates90Days: null,
      lates120Days: null
    };

      this.allLates = [
        {
          type: 'mortgage',
          lates: this.latesSummary && this.latesSummary.mortgage ? this.latesSummary.mortgage : emptyLates
        },
        {
          type: 'installment',
          lates: this.latesSummary && this.latesSummary.installment ? this.latesSummary.installment : emptyLates
        },
        {
          type: 'revolving',
          lates:  this.latesSummary && this.latesSummary.revolving ? this.latesSummary.revolving : emptyLates
        },
        {
          type: 'other',
          lates:  this.latesSummary && this.latesSummary.other ? this.latesSummary.other : emptyLates
        }];
      this.isLoading = false;
  }

  loadLates(period: number) {
    this.isLoading = true;
    this.store.dispatch(new LoadLatesSummaryAction({externalId: this.folderId, period: period}));
  }

  get totalLatesAmount() {
    return this.latesSummary && this.latesSummary.totalLates ? this.latesSummary.totalLates : 0;
  }

  get months12Btn() {
    return this.latesSummary && this.latesSummary.period === 12 ? 'active-btn' : '';
  }

  get months24Btn() {
    return this.latesSummary && this.latesSummary.period === 24 ? 'active-btn' : '';
  }
}
