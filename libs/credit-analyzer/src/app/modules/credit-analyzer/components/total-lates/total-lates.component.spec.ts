import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TotalLatesComponent} from './total-lates.component';
import {ApplicantsLatesComponent} from '../applicants-lates/applicants-lates.component';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ScoreBadgeComponent} from '../../../sidebar/components/score-badge/score-badge.component';
import {RootStoreModule} from '../../../../root-store/root-store.module';
import {HttpClientModule} from '@angular/common/http';
import {ProgressSpinnerComponent} from '../progress-spinner/progress-spinner.component';

xdescribe('TotalLatesComponent', () => {
  let component: TotalLatesComponent;
  let fixture: ComponentFixture<TotalLatesComponent>;
  let componentEl: HTMLElement;

  const latesSummary = {
    installment: {
      lates30Days: [
        {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, dates: ['2018-01-01', '2018-03-01']}
      ],
      lates60Days: [],
      lates90Days: [],
      lates120Days: []
    },
    mortgage: {
      lates30Days: [],
      lates60Days: [
        {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, dates: ['2018-01-01', '2018-03-01']}
      ],
      lates90Days: [],
      lates120Days: []
    },
    other: {
      lates30Days: [],
      lates60Days: [],
      lates90Days: [],
      lates120Days: []
    },
    revolving: {
      lates30Days: [],
      lates60Days: [],
      lates90Days: [
        {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, dates: ['2018-01-01', '2018-03-01']}
      ],
      lates120Days: [
        {firstName: 'Andrew', lastName: 'Homeowner', applicantId: 1293, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Mary', lastName: 'Homeowner', applicantId: 1294, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Homer', lastName: 'Loanseeker', applicantId: 1295, dates: ['2018-01-01', '2018-03-01']},
        {firstName: 'Alice', lastName: 'Firstimer', applicantId: 1296, dates: ['2018-01-01', '2018-03-01']}
      ]
    },
    totalLates: 32,
    period: 12
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TotalLatesComponent, ApplicantsLatesComponent, ScoreBadgeComponent, ProgressSpinnerComponent],
      imports: [PerfectScrollbarModule, MatIconModule, RootStoreModule, HttpClientModule, MatProgressSpinnerModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalLatesComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.latesSummary = latesSummary;
    component.ngOnChanges();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display correct lates amount', () => {
    expect(componentEl.querySelector('.score').textContent).toContain(latesSummary.totalLates.toString());
  });

  it('should display selected button with correct lates period', () => {
    expect(componentEl.querySelector('.active-btn').textContent).toContain(latesSummary.period.toString() + ' months');
  });

  it('should display correct amount of liabilities', () => {
    expect(componentEl.querySelectorAll('.column.liability-type').length).toBe(5);
  });

  it('should display correct amount of applicants lates component', () => {
    expect(componentEl.querySelectorAll('.lates-container').length).toBe(4);
  });
});
