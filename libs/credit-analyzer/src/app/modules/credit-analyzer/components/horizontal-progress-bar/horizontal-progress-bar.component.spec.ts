import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HorizontalProgressBarComponent} from './horizontal-progress-bar.component';
import {LiabilitiesType} from '../../models/credit-analyzer-models';
import {LiabilityCategoryType} from '../../../../app.types';

describe('HorizontalProgressBarComponent', () => {
  let component: HorizontalProgressBarComponent;
  let fixture: ComponentFixture<HorizontalProgressBarComponent>;
  let componentEl: HTMLElement;

  const liabilities: LiabilitiesType = {
    byApplicants: [
      {
        firstName: 'Andrew',
        lastName: 'Homeowner',
        applicantId: 1293,
        amount: 500,
      },
      {
        firstName: 'Mary',
        lastName: 'Homeowner',
        applicantId: 1294,
        amount: 500
      },
      {
        firstName: 'Homer',
        lastName: 'Loanseeker',
        applicantId: 1295,
        amount: 1000
      },
      {
        firstName: 'Alice',
        lastName: 'Firstimer',
        applicantId: 1296,
        amount: 2000
      }],
    totalAmount: 4000,
    name: 'Revolving',
    type: LiabilityCategoryType.REVOLVING
  };

  const colors = [
    {
      applicantId: 1293,
      color: 'light-blue'
    },
    {
      applicantId: 1294,
      color: 'dark-blue'
    },
    {
      applicantId: 1295,
      color: 'green'
    },
    {
      applicantId: 1296,
      color: 'orange'
    }];

  const maxSum = 2000;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HorizontalProgressBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalProgressBarComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.maxSum = maxSum;
    component.colors = colors;
    component.liabilities = liabilities;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display 4 applicants', () => {
    expect(componentEl.querySelectorAll('.stack:not(.applicant-color)').length).toBe(4);
  });

  it('should display 4 correct colors', () => {
    expect(componentEl.querySelectorAll('div:not(.applicant-color).stack.' + component.colors[0].color).length).toBe(1);
    expect(componentEl.querySelectorAll('div:not(.applicant-color).stack.' + component.colors[1].color).length).toBe(1);
    expect(componentEl.querySelectorAll('div:not(.applicant-color).stack.' + component.colors[2].color).length).toBe(1);
    expect(componentEl.querySelectorAll('div:not(.applicant-color).stack.' + component.colors[3].color).length).toBe(1);
  });
});

function currencyFormat(value) {
  return value.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
}
