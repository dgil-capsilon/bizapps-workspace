import {Component, Input, OnInit} from '@angular/core';
import {LiabilitiesType} from '../../models/credit-analyzer-models';

@Component({
  selector: 'app-horizontal-progress-bar',
  templateUrl: './horizontal-progress-bar.component.html',
  styleUrls: ['./horizontal-progress-bar.component.scss']
})
export class HorizontalProgressBarComponent implements OnInit {

  @Input() maxSum: number;
  @Input() liabilities: LiabilitiesType;
  sum: number;
  @Input() colors: any;

  constructor() {
  }

  ngOnInit() {
    if (this.liabilities && this.liabilities.totalAmount) {
      this.sum = this.liabilities.totalAmount;
    } else {
      this.sum = 0;
    }
  }

  assignColor(applicantId) {
    let assignedColor = '';
    if (this.colors && this.colors.length > 0) {
      const applicantsColors = this.colors.filter(x => x.applicantId === applicantId);
      assignedColor = applicantsColors[0].color;
    }

    return 'stack ' + assignedColor;
  }

  calculateStackProgress(value) {
    return value ? value * 100 / this.sum : 0;
  }

  calculateAllProgress() {
    return this.sum * 100 / this.maxSum;
  }
}
