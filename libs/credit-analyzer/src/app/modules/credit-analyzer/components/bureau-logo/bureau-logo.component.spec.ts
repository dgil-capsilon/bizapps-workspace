import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BureauLogoComponent} from './bureau-logo.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BureauLogoComponent', () => {
  let component: BureauLogoComponent;
  let fixture: ComponentFixture<BureauLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BureauLogoComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BureauLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
