import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-bureau-logo',
  templateUrl: './bureau-logo.component.html',
  styleUrls: ['./bureau-logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BureauLogoComponent {
  @Input() imageSrc: string;

}
