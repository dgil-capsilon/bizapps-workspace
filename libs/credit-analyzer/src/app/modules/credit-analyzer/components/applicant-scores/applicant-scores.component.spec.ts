import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ApplicantScoresComponent} from './applicant-scores.component';
import {MatIconModule} from '@angular/material/icon';
import {FelonizePipe} from '@pipes/felonize.pipe';
import {ApplicantCreditScores} from '../../models/credit-analyzer-models';
import {ProviderType} from '@app/app.types';
import {ApplicantScoresResultComponent} from '../applicant-scores-result/applicant-scores-result.component';
import {NonePipe} from '@pipes/none.pipe';

describe('ApplicantScoresComponent', () => {
  let component: ApplicantScoresComponent;
  let fixture: ComponentFixture<ApplicantScoresComponent>;
  let componentEl: HTMLElement;

  const applicant: ApplicantCreditScores = {
    scores: {
      equifax: 754,
      experian: 725,
      transUnion: 798,
    },
    firstName: 'Karolina',
    isIncluded: true,
    lastName: 'Bird',
    isQualified: false,
    selectedProvider: ProviderType.TRANSUNION
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicantScoresComponent, ApplicantScoresResultComponent, FelonizePipe, NonePipe],
      imports: [MatIconModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantScoresComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.applicant = applicant;
    component.ngOnChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should check if applicant excluded', () => {
    component.applicant.isIncluded = false;
    fixture.detectChanges();
    expect(componentEl.querySelector('.applicant-scores').className).toContain('applicant-disabled');
  });

  it('should check if applicant included', () => {
    component.applicant.isIncluded = true;
    fixture.detectChanges();
    expect(componentEl.querySelector('.applicant-scores').className).not.toContain('applicant-disabled');
  });

  it('should display applicant first name and censored last name', () => {
    fixture.detectChanges();
    const applicantName = applicant.firstName + ' ' + applicant.lastName[0] + '.';
    expect(componentEl.querySelector('.applicant').textContent).toBe(applicantName.toString());
  });

  it('should check if score is not qualified score', () => {
    component.applicant.isQualified = false;
    fixture.detectChanges();
    expect(componentEl.querySelectorAll('.icon-star').length).toBe(0);
  });

  it('should check if score is qualified score', () => {
    component.applicant.isQualified = true;
    fixture.detectChanges();
    expect(componentEl.querySelectorAll('.icon-star').length).toBe(1);
  });
});
