import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit} from '@angular/core';
import {ApplicantCreditScores, ScoreValueType} from '../../models/credit-analyzer-models';
import {ProviderType} from '../../../../app.types';

@Component({
  selector: 'app-applicant-scores',
  templateUrl: './applicant-scores.component.html',
  styleUrls: ['./applicant-scores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicantScoresComponent implements OnInit, OnChanges {
  @Input() applicant: ApplicantCreditScores;
  score: ScoreValueType;

  constructor() {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.applicant && this.applicant.scores) {
      const scores = [
        {
          color: this.assignColor(ProviderType.EQUIFAX),
          value: this.applicant.scores.equifax || 0,
          isQualified: this.applicant.selectedProvider === ProviderType.EQUIFAX
        },
        {
          color: this.assignColor(ProviderType.TRANSUNION),
          value: this.applicant.scores.transUnion || 0,
          isQualified: this.applicant.selectedProvider === ProviderType.TRANSUNION
        },
        {
          color: this.assignColor(ProviderType.EXPERIAN),
          value: this.applicant.scores.experian || 0,
          isQualified: this.applicant.selectedProvider === ProviderType.EXPERIAN
        }];
      this.score = scores.find(x => x.isQualified);
    }
  }

  assignColor(provider: string) {
    let color = '';

    switch (provider) {
      case 'Equifax':
        color = 'brown';
        break;
      case 'TransUnion':
        color = 'light-blue';
        break;
      case 'Experian':
        color = 'dark-blue';
        break;
      default:
        color = 'light-blue';
        break;
    }
    return color;
  }

  calculateBarWidth(score: number) {
    return 100 - this.calculateProgress(score);
  }


  calculateProgress(score: number) {
    const maxScore = 850;
    score = score > maxScore ? maxScore : score;
    const minScore = 300;
    score = score < minScore ? minScore : score;
    const maxHeight = 100;
    return ((score - minScore) * maxHeight) / (maxScore - minScore);
  }
}
