import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {VerticalProgressBarComponent} from './vertical-progress-bar.component';

describe('VerticalProgressBarComponent', () => {
  let component: VerticalProgressBarComponent;
  let fixture: ComponentFixture<VerticalProgressBarComponent>;
  let componentEl: HTMLElement;
  const score = {
    color: 'brown',
    value: 725,
    isQualified: true
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerticalProgressBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalProgressBarComponent);
    component = fixture.componentInstance;
    componentEl = fixture.debugElement.nativeElement;
    component.score = score;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display properly score', () => {
    expect(componentEl.querySelector('.progress-bar-value').textContent).toContain(score.value.toString());
  });

  it('should check if score is qualified', () => {
    component.score.isQualified = true;
    fixture.detectChanges();
    expect(componentEl.querySelector('.progress-bar-element').className).toContain('value-qualified');
  });

  it('should check if score is not qualified', () => {
    component.score.isQualified = false;
    fixture.detectChanges();
    expect(componentEl.querySelector('.progress-bar-element').className).not.toContain('value-qualified');
  });

  it('should check progress bar\'s color', fakeAsync(() => {
    expect(componentEl.querySelector('.progress-bar-element').className).toContain(score.color);
  }));
});
