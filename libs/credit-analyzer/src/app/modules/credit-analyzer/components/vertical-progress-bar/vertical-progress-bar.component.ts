import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-vertical-progress-bar',
  templateUrl: './vertical-progress-bar.component.html',
  styleUrls: ['./vertical-progress-bar.component.scss']
})
export class VerticalProgressBarComponent implements OnInit {
  @Input() score = {
    color: null,
    value: null,
    isQualified: null
  };

  @HostBinding('style.height.%') maxHeight: number;

  constructor() {
  }

  ngOnInit() {
  }

  calculateProgress(value: number) {
    const maxScore = 850;
    value = value > maxScore ? maxScore : value;
    const minScore = 300;
    const maxHeight = 100;
    return ((value - minScore) * maxHeight)  / (maxScore - minScore);
  }

  get barHeight() {
    return this.calculateProgress(this.score.value);
  }
}
