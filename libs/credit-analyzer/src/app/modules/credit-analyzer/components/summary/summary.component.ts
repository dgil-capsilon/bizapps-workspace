import {Component, OnInit} from '@angular/core';
import {StoreService} from '@app/root-store/store.service';
import {CreditAnalyzer} from '@app/root-store/root-state';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  application: CreditAnalyzer;

  constructor(private storeService: StoreService) {
  }

  ngOnInit() {
    this.storeService.getAllState().subscribe(app => {
      this.application = app;
    });
  }
}
