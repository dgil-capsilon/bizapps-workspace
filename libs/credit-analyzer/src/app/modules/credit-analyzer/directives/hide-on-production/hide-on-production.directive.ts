import {Directive, ElementRef} from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Directive({
  selector: '[appHideOnProduction]'
})
export class HideOnProductionDirective {

  constructor(el: ElementRef) {
    if (environment.production) {
      el.nativeElement.style.display = 'none';
    }
  }
}
