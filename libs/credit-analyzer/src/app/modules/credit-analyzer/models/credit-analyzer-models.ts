import {CreditRepositories, HousingExpense} from '../../../root-store/root-state';
import {ExpenseType, HousingExpensesType, ItemSourceType, LiabilityCategoryType, ProviderType} from '../../../app.types';

export interface Applicant {
  id: number;
  firstName: string;
  lastName: string;
  ssn: string;
  score: number;
  included: boolean;
  creditScores: CreditScores;
  creditReportExpirationDate: any;
  creditReportOrderedDate: any;
  liabilitiesTypes: Array<LiabilityType>;
  totalCollections: number;
  totalInquiries: number;
  totalPublicRecords: number;
  totalForeclosure: number;
  publicRecords: Array<PublicRecord>;
  housingExpenses: HousingExpenses;
  selectedProvider: ProviderType;
}

export interface LiabilityType {
  included: boolean;
  name: string;
  totalMonthlyPayment: number;
  type: string;
  liabilities: Array<ApplicantLiability>;
  details: Details;
  latePayments: LatePayments;
  comments: Array<string>;
  id: number;
  expenseType: ExpenseType;
  byApplicant: boolean;
  itemSource: ItemSourceType;
  tags: Array<string>;
}

export interface LatePayments {
  lates30Days: Array<string>;
  lates60Days: Array<string>;
  lates90Days: Array<string>;
  lates120Days: Array<string>;
}

export interface HousingExpenses {
  presentHousingExpenses: HousingExpense;
  proposeHousingExpenses: HousingExpense;
}

export interface HousingExpense {
  firstMortgage: number;
  hazardInsurance: number;
  homeownersAssociationDues: number;
  mortgageInsurance: number;
  other: number;
  otherFinancing: number;
  realEstateTaxes: number;
  rent: number;
  totalAmount: number;
  type: HousingExpensesType;
}

export interface Details {
  accountIdentifier: string;
  balance: number;
  dateOpened: string;
  dateReported: string;
  ecoa: string;
  highestCredit: number;
  monthlyPayment: number;
  status: string;
}

export interface PublicRecord {
  id: number;
  courtName: string;
  creditRepositories: Array<CreditRepositories>;
  dateFiled: string;
  dateReported: string;
  dateSatisfied: string;
  dispositionType: string;
  docketNumber: string;
  legalObligationAmount: number;
  name: string;
}

export interface ApplicantLiability {
  byApplicant: boolean;
  comments: Array<string>;
  id: number;
  monthlyPayment: number;
  name: string;
  tags: Array<string>;
  type: string;
  included: boolean;
}

export interface CreditScores {
  equifax: number;
  experian: number;
  transunion: number;
}

export interface Summary {
  applicants: Array<Applicant>;
  qualifyingScore: number;
  rank: string;
  externalId: string;
  latesSummary: LatesSummary;
  liabilitiesSummary: LiabilitiesSummary;
  folderId: number;
  score: number;
}

export interface LatesSummary {
  mortgage: Lates;
  installment: Lates;
  revolving: Lates;
  other: Lates;
  period: number;
  totalLates: number;
}

export interface Lates {
  lates30Days: Array<Late>;
  lates60Days: Array<Late>;
  lates90Days: Array<Late>;
  lates120Days: Array<Late>;
}

export interface Late {
  applicantId: number;
  firstName: string;
  lastName: string;
  dates: Array<string>;
}

export interface LiabilitiesSummary {
  housingExpenses: LiabilitiesByApplicant;
  mortgage: LiabilitiesByApplicant;
  installment: LiabilitiesByApplicant;
  revolving: LiabilitiesByApplicant;
  other: LiabilitiesByApplicant;
  totalAmount: number;
  liabilitiesTypes: Array<LiabilitiesType>;
}

export interface LiabilitiesType {
  byApplicants: Array<Liability>;
  totalAmount: number;
  name: string;
  type: LiabilityCategoryType;
}

export interface LiabilitiesByApplicant {
  byApplicants: Array<Liability>;
  totalAmount: number;
}

export interface Liability {
  amount: number;
  applicantId: number;
  firstName: string;
  lastName: string;
}

export interface ApplicantCreditScores {
  firstName: string;
  lastName: string;
  scores: {
    experian: number;
    transUnion: number;
    equifax: number;
  };
  isQualified: boolean;
  isIncluded: boolean;
  selectedProvider: ProviderType;
}
export interface ScoreValueType {
  color: string;
  value: number;
  isQualified: boolean;
}

export interface DetailModel {
  name: string;
  value: number;
  snippetName?: string;
}

export interface TabHeader {
  firstName: string;
  lastName: string;
  id: number;
  route: string;
}
