import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      withCredentials: true
    });

    return next.handle(request)
      .pipe(
        catchError((error, caught) => {
          this.handleError(error);
          return of(error);
        }) as any
      );
  }

  /**
   * Handle API errors
   * @param err Error
   * @returns Error
   */
  private handleError(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401) {
      console.log(`[HTTP] ERROR:`, err);
      return of(err.message);
    }
    throw err;
  }

}
