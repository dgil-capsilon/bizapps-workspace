import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {ExtHttpParams} from '@services/ext-http-params';
import {DefaultErrorHandler} from '@services/default-error-handler';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    return next.handle(request).pipe(
      // retry(1),
      map(event => {
        if (event instanceof HttpResponse && event.body) {
          const response = event.body;
          // @TODO: deprecated handling of MPOS responses
          // to be removed after dropping MPOS
          if (response.errorCode === 1 && response.status !== 'Failure') {
            throw new HttpErrorResponse({
              error: { code: 1 },
              headers: event.headers,
              status: 500,
              url: event.url
            });
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        const params = request.params;
        const suppressErrors = params && (params instanceof ExtHttpParams) && params.options.suppressErrors;

        if (!suppressErrors) {
          this.injector.get(DefaultErrorHandler).handleError(error);
        }

        return throwError(error.message);
      })
    );
  }

}
