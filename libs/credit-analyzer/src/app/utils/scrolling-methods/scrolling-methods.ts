export class ScrollingMethods {
  static scrollWithDelay(elementClass: string, delay: number = 500) {
    setTimeout(() => {
      const elements = document.getElementsByClassName(elementClass);
      if (elements !== null && elements.length > 0) {
        let element = elements.item(0);
        element.scrollIntoView({block: 'end', behavior: 'smooth'});
      }
    }, delay);
  }
}
