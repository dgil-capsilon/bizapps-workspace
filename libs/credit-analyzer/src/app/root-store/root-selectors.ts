import {State, CreditAnalyzer} from './root-state';
import {createSelector} from '@ngrx/store';

export const creditAnalyzer = (state: State) => state.creditAnalyzer;

export const applicant = (state: State) => state.applicant;

export const getError = createSelector(creditAnalyzer, (state: CreditAnalyzer) => state.error);

export const getIsLoading = createSelector(creditAnalyzer, (state: CreditAnalyzer) => state.isLoading);
