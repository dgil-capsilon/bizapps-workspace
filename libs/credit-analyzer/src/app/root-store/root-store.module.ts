import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {rootReducer, rootReducerApplicant} from './root-reducer';
import {EffectsModule} from '@ngrx/effects';
import {RootStoreEffects} from './root-effects';
import {environment} from '../../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({creditAnalyzer: rootReducer, applicant: rootReducerApplicant}),
    EffectsModule.forRoot([RootStoreEffects]),
    !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 10 }) : []
  ],
  declarations: []
})
export class RootStoreModule { }
