import {
  Applicant,
  CreditAnalyzer,
  initialState,
  initialStateApplicant
} from './root-state';
import {Actions, ActionTypes} from './root-actions';

export function rootReducer(state = initialState, action: Actions): CreditAnalyzer {
  switch (action.type) {
    case ActionTypes.LOAD_SUMMARY: {
      return {...state, ...action.payload, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_SUMMARY_WITH_CONFIG: {
      return {...state, ...action.payload, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_RULES: {
      return {...state, ...action.payload, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_CONFIG_SUCCESS: {
      return {...state, ...action.payload, isLoading: true, error: null};
    }
    case ActionTypes.INCLUDE_APPLICANT: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.EXCLUDE_APPLICANT: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_LATES_SUMMARY: {
      return {...state, isLoading: false, error: null};
    }
    case ActionTypes.LOAD_LATES_SUMMARY_SUCCESS: {
      return {...state, ...action.payload, isLoading: false, error: null};
    }
    case ActionTypes.EXCLUDE_LIABILITY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.INCLUDE_LIABILITY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.INCLUDE_LIABILITY_SUCCESS: {
      return {...state, ...action.payload, isLoading: false, error: null};
    }
    case ActionTypes.INCLUDE_LIABILITY_CATEGORY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.EXCLUDE_LIABILITY_CATEGORY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_FAILURE: {
      return {...state, isLoading: false, error: action.payload.error};
    }
    case ActionTypes.LOAD_SUCCESS: {
      return {...state, ...action.payload, isLoading: false, error: null};
    }
    case ActionTypes.LOAD_RULES_SUCCESS: {
      //@ToDo: przerobić razem ze storem
      state.rules = action.payload;
      state.isLoading = false;
      state.error = null;
      return state;
    }
    case ActionTypes.LOAD_DATA_TO_EXISTING_FOLDER: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_DATA_TO_NEW_FOLDER: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_CREDIT_REPORT_TO_NEW_FOLDER: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.LOAD_CREDIT_REPORT_TO_EXISTING_FOLDER: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.UPDATE_PUBLIC_RECORD: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.UPDATE_LIABILITY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.ADD_LIABILITY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.REMOVE_LIABILITY: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.UPDATE_HOUSING_EXPENSES: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.UPDATE_CREDIT_SCORE: {
      return {...state, isLoading: true, error: null};
    }
    case ActionTypes.UPDATE_NAVIGATION_COMPONENT: {
      return {...state, ...action.payload};
    }
    default: {
      return state;
    }
  }
}

export function rootReducerApplicant(state = initialStateApplicant, action: Actions): Applicant {
  switch (action.type) {
    case ActionTypes.LOAD_APPLICANT_DATA: {
      return {...state};
    }
    case ActionTypes.LOAD_FAILURE: {
      return {...state};
    }
    case ActionTypes.LOAD_SUCCESS: {
      return {...state, ...action.payload};
    }
    default: {
      return state;
    }
  }
}
