import {
    CreditScores,
    LatesSummary,
    LiabilitiesSummary, LiabilityType
} from '@modules/credit-analyzer/models/credit-analyzer-models';
import {HousingExpensesType, ProviderType} from '../app.types';

export interface Applicant {
    id: number;
    firstName: string;
    lastName: string;
    ssn: string;
    score: number;
    included: boolean;
    creditScores: CreditScores;
    creditReportExpirationDate: any;
    creditReportOrderedDate: any;
    liabilitiesTypes: Array<LiabilityType>;
    totalCollections: number;
    totalInquiries: number;
    totalPublicRecords: number;
    totalForeclosure: number;
    publicRecords: Array<PublicRecord>;
    housingExpenses: HousingExpenses;
    selectedProvider: ProviderType;
}

export interface PublicRecord {
    id: number;
    courtName: string;
    creditRepositories: Array<CreditRepositories>;
    dateFiled: string;
    dateReported: string;
    dateSatisfied: string;
    dispositionType: string;
    docketNumber: string;
    legalObligationAmount: number;
    name: string;
}
export interface HousingExpenses {
    presentHousingExpenses: HousingExpense;
    proposeHousingExpenses: HousingExpense;
}
export interface HousingExpense {
    firstMortgage: number;
    hazardInsurance: number;
    homeownersAssociationDues: number;
    mortgageInsurance: number;
    other: number;
    otherFinancing: number;
    realEstateTaxes: number;
    rent: number;
    totalAmount: number;
    type: HousingExpensesType;
}

export const initialStateApplicant: Applicant = {
    id: null,
    firstName: null,
    lastName: null,
    ssn: null,
    score: null,
    included: null,
    creditScores: null,
    creditReportExpirationDate: null,
    creditReportOrderedDate: null,
    liabilitiesTypes: null,
    totalCollections: null,
    totalInquiries: null,
    totalPublicRecords: null,
    totalForeclosure: null,
    publicRecords: null,
    housingExpenses: null,
    selectedProvider: null
};


export interface CreditAnalyzer {
    id: number;
    applicants: Array<Applicant>;
    qualifyingScore: number;
    rank: string;
    externalId: string;
    folderId: number;
    isLoading: boolean;
    error: any;
    folderReady: boolean;
    latesSummary: LatesSummary;
    liabilitiesSummary: LiabilitiesSummary;
    dtiSummary: DtiSummary;
    config: ConfigData;
    openedComponent: string;
    rules: Rules;
}

export interface State {
    creditAnalyzer: CreditAnalyzer;
    applicant: Applicant;
}

export interface ConfigData {
  displayChecklist: boolean;
  addApplicantsFromCreditReport: boolean;
}

export interface Rules {
  folderId: string;
  loanStatus: string;
  totalQualifyingIncome: number;
  applicants: Array<ApplicantRules>;
}

export interface ApplicantRules {
  applicantId: number;
  extApplicantId: string;
  fullName: string;
  rules: Array<Rule>;
}

export interface Rule {
  ruleId: string;
  description: string;
  status: string;
  id: number;
  waiveable: boolean;
}

export const initialState: CreditAnalyzer = {
    id: null,
    applicants: null,
    qualifyingScore: null,
    rank: null,
    externalId: null,
    folderId: null,
    isLoading: false,
    folderReady: true,
    error: null,
    latesSummary: {
        mortgage: null,
        installment: null,
        revolving: null,
        other: null,
        period: null,
        totalLates: null
    },
    liabilitiesSummary: {
        mortgage: null,
        installment: null,
        revolving: null,
        other: null,
        housingExpenses: null,
        liabilitiesTypes: null,
        totalAmount: null
    },
    dtiSummary: {
        overallDti: null,
        frontendDti: null,
        type: null,
        applicants: []
    },
    config: {
      displayChecklist: false,
      addApplicantsFromCreditReport: false
    },
    openedComponent: null,
    rules: {
      folderId: null,
      loanStatus: null,
      totalQualifyingIncome: null,
      applicants: []
    }
};

export interface Inquiry {
    id: number;
    inquiryDate: string;
    inquiryMadeBy: string;
    creditRepositories: Array<CreditRepositories>;
}

export interface CreditRepositories {
    creditRepositorySource: string;
    description: string;
    subscriberCode: string;
}

export interface DtiSummary {
    applicants: Array<ApplicantDTI>;
    frontendDti: number;
    type: string;
    overallDti: number;
}

export interface ApplicantDTI {
    applicantId: number;
    firstName: string;
    lastName: string;
    frontendDti: number;
    included: boolean;
    overallDti: number;

}
