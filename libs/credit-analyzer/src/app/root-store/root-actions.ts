import {Action} from '@ngrx/store';
import {Applicant, LatesSummary, LiabilityType} from '../modules/credit-analyzer/models/credit-analyzer-models';
import {ConfigData, HousingExpenses, PublicRecord, Rules} from './root-state';
import {ProviderType} from '../app.types';

export const INCLUDE_APPLICANT = 'Inculde_Applicant';
export const EXCLUDE_APPLICANT = 'Exclude_Applicant';

export enum ActionTypes {
    INCLUDE_APPLICANT = 'Inculde Applicant',
    EXCLUDE_APPLICANT = 'Exclude Applicant',
    LOAD_SUMMARY = 'Load Summary',
    UPDATE_NAVIGATION_COMPONENT = 'Update Opened Component In Application',
    LOAD_SUMMARY_WITH_CONFIG = 'Load Summary With Config Data',
    LOAD_RULES = 'Load Rules for folder',
    LOAD_CONFIG_SUCCESS = 'Loaded Successfully Config Data',
    LOAD_APPLICANT_DATA = 'Load Applicant Data',
    LOAD_FAILURE = 'Load Failure',
    LOAD_SUCCESS = 'Load Success',
    LOAD_RULES_SUCCESS = 'Load Rules Success',
    LOAD_LATES_SUMMARY = 'Load Lates Summary',
    LOAD_LATES_SUMMARY_SUCCESS = 'Load Lates Summary Success',
    INCLUDE_LIABILITY = 'Include Liability',
    EXCLUDE_LIABILITY = 'Exclude Liability',
    INCLUDE_LIABILITY_SUCCESS = 'Include Liability Success',
    INCLUDE_LIABILITY_CATEGORY = 'Include Liability Category',
    EXCLUDE_LIABILITY_CATEGORY = 'Exclude Liability Category',
    LOAD_DATA_TO_NEW_FOLDER = 'Load Data To New Folder',
    LOAD_DATA_TO_EXISTING_FOLDER = 'Load Data To Existing Folder',
    LOAD_CREDIT_REPORT_TO_EXISTING_FOLDER = 'Load Credit Report To Existing Folder',
    LOAD_CREDIT_REPORT_TO_NEW_FOLDER = 'Load Credit Report To New Folder',
    UPDATE_PUBLIC_RECORD = 'Update Public Record',
    UPDATE_LIABILITY = 'Update Liability',
    ADD_LIABILITY = 'Add Liability',
    REMOVE_LIABILITY = 'Remove Liability',
    UPDATE_HOUSING_EXPENSES = 'Update Housing Expenses',
    UPDATE_CREDIT_SCORE = 'Update Credit Score'
}
export class UpdateCreditScoreAction implements Action {
    readonly type = ActionTypes.UPDATE_CREDIT_SCORE;

    constructor(public folderId: string, public applicantId: number, public provider: ProviderType) {
    }
}

export class UpdateHousingExpensesAction implements Action {
    readonly type = ActionTypes.UPDATE_HOUSING_EXPENSES;

    constructor(public folderId: string, public housingExpenses: HousingExpenses, public applicantId: number) {
    }
}

export class AddLiabilityAction implements Action {
    readonly type = ActionTypes.ADD_LIABILITY;

    constructor(public folderId: string, public applicantId: number, public liability: LiabilityType) {
    }
}
export class RemoveLiabilityAction implements Action {
    readonly type = ActionTypes.REMOVE_LIABILITY;

    constructor(public folderId: string, public liabilityId: number) {
    }
}

export class UpdateLiabilityAction implements Action {
    readonly type = ActionTypes.UPDATE_LIABILITY;

    constructor(public folderId: string, public liability: LiabilityType) {
    }
}

export class IncludeLiabilityAction implements Action {
    readonly type = ActionTypes.INCLUDE_LIABILITY;

    constructor(public folderId: string, public liabilityId: number) {
    }
}

export class IncludeLiabilitySuccessAction implements Action {
    readonly type = ActionTypes.INCLUDE_LIABILITY_SUCCESS;

    constructor(public payload: { applicants: Array<Applicant> }) {
    }
}

export class ExcludeLiabilityAction implements Action {
    readonly type = ActionTypes.EXCLUDE_LIABILITY;

    constructor(public folderId: string, public liabilityId: number) {
    }
}

export class IncludeLiabilityCategoryAction implements Action {
    readonly type = ActionTypes.INCLUDE_LIABILITY_CATEGORY;

    constructor(public folderId: string, public applicantId: number, public liabilityName: string) {
    }
}

export class ExcludeLiabilityCategoryAction implements Action {
    readonly type = ActionTypes.EXCLUDE_LIABILITY_CATEGORY;

    constructor(public folderId: string, public applicantId: number, public liabilityName: string) {
    }
}

export class IncludeApplicantAction implements Action {
    readonly type = ActionTypes.INCLUDE_APPLICANT;

    constructor(public folderId: string, public applicantId: number) {
    }
}

export class ExcludeApplicantAction implements Action {
    readonly type = ActionTypes.EXCLUDE_APPLICANT;

    constructor(public folderId: string, public applicantId: number) {
    }
}

export class LoadApplicantDataAction implements Action {
    readonly type = ActionTypes.LOAD_APPLICANT_DATA;

    constructor(public folderId: number, public applicantId: number) {
    }
}

export class LoadSummaryAction implements Action {
    readonly type = ActionTypes.LOAD_SUMMARY;

    constructor(public payload: { externalId: string }) {
    }
}

export class LoadSummaryWithConfigAction implements Action {
  readonly type = ActionTypes.LOAD_SUMMARY_WITH_CONFIG;

  constructor(public payload: { externalId: string }) {
  }
}

export class LoadRulesAction implements Action {
  readonly type = ActionTypes.LOAD_RULES;

  constructor(public payload: { externalId: string }) {
  }
}

export class UpdateNavigationComponent implements Action {
  readonly type = ActionTypes.UPDATE_NAVIGATION_COMPONENT;

  constructor(public payload: { openedComponent: string }) {
  }
}

export class LoadConfigSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_CONFIG_SUCCESS;

  constructor(public payload: { config: ConfigData }) {
  }
}


export class LoadLatesSummaryAction implements Action {
    readonly type = ActionTypes.LOAD_LATES_SUMMARY;

    constructor(public payload: { externalId: string, period: number }) {
    }
}

export class LoadDataToNewFolderAction implements Action {
    readonly type = ActionTypes.LOAD_DATA_TO_NEW_FOLDER;

    constructor(public payload: { body: {} }) {
    }
}

export class LoadDataToExistingFolderAction implements Action {
    readonly type = ActionTypes.LOAD_DATA_TO_EXISTING_FOLDER;

    constructor(public payload: { folderId: string, body: {} }) {
    }
}

export class LoadCreditReportToNewFolderAction implements Action {
    readonly type = ActionTypes.LOAD_CREDIT_REPORT_TO_NEW_FOLDER;

    constructor(public payload: { body: {} }) {
    }
}

export class UpdatePublicRecordAction implements Action {
    readonly type = ActionTypes.UPDATE_PUBLIC_RECORD;

    constructor(public folderId: number, public applicantId: number, public publicRecordId: number, public body) {
    }
}

export class LoadCreditReportToExistingFolderAction implements Action {
    readonly type = ActionTypes.LOAD_CREDIT_REPORT_TO_EXISTING_FOLDER;

    constructor(public payload: { folderId: string, body: {} }) {
    }
}

export class LoadLatesSummarySuccessAction implements Action {
    readonly type = ActionTypes.LOAD_LATES_SUMMARY_SUCCESS;

    constructor(public payload: { latesSummary: LatesSummary }) {
    }
}

export class LoadFailureAction implements Action {
    readonly type = ActionTypes.LOAD_FAILURE;

    constructor(public payload: { error: string }) {
    }
}


export class LoadSuccessAction implements Action {
    readonly type = ActionTypes.LOAD_SUCCESS;

    constructor(public payload) {
    }
}

export class LoadRulesSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_RULES_SUCCESS;

  constructor(public payload) {
  }
}

export type Actions =
    IncludeApplicantAction
    | ExcludeApplicantAction
    | LoadFailureAction
    | LoadSuccessAction
    | LoadSummaryAction
    | LoadApplicantDataAction
    | LoadLatesSummaryAction
    | LoadLatesSummarySuccessAction
    | ExcludeLiabilityAction
    | ExcludeLiabilityCategoryAction
    | IncludeLiabilityAction
    | IncludeLiabilityCategoryAction
    | IncludeLiabilitySuccessAction
    | LoadDataToExistingFolderAction
    | LoadDataToNewFolderAction
    | LoadCreditReportToNewFolderAction
    | LoadCreditReportToExistingFolderAction
    | UpdatePublicRecordAction
    | UpdateLiabilityAction
    | AddLiabilityAction
    | RemoveLiabilityAction
    | UpdateHousingExpensesAction
    | UpdateCreditScoreAction
    | LoadSummaryWithConfigAction
    | LoadConfigSuccessAction
    | LoadRulesAction
    | LoadRulesSuccessAction
    | UpdateNavigationComponent;
