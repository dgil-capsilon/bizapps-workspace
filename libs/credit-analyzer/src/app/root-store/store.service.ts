import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {CreditAnalyzer, State} from './root-state';
import {Observable} from 'rxjs';

@Injectable()
export class StoreService {

  constructor(private store: Store<State>) {
  }

  getAllState(): Observable<CreditAnalyzer>{
    return this.store.select('creditAnalyzer');
  }
}

