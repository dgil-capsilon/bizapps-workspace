import {Injectable} from '@angular/core';

import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';

import {Action} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';

import {DataService} from '../../services/data.service';
import * as RootActions from './root-actions';
import {LatesSummary} from '../modules/credit-analyzer/models/credit-analyzer-models';
import {ConfigData} from './root-state';


@Injectable()
export class RootStoreEffects {
  constructor(private  dataService: DataService, private actions$: Actions) {
  }

  @Effect()
  GetData$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadSummaryAction>(RootActions.ActionTypes.LOAD_SUMMARY),
      switchMap(action => this.dataService.postSummaryData(action.payload.externalId)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  GetRules$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadRulesAction>(RootActions.ActionTypes.LOAD_RULES),
      switchMap(action => this.dataService.getRules(action.payload.externalId)
        .pipe(
          mergeMap(res => of(new RootActions.LoadRulesSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  GetConfig$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadSummaryWithConfigAction>(RootActions.ActionTypes.LOAD_SUMMARY_WITH_CONFIG),
      switchMap(
        (action) => this.dataService.getConfigData().pipe(
          mergeMap((config: any) => of(
            new RootActions.LoadConfigSuccessAction({config}),
            new RootActions.LoadRulesAction({externalId: action.payload.externalId}),
            new RootActions.LoadSummaryAction({externalId: action.payload.externalId}),
          )),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  GetNavigationComponent$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.UpdateNavigationComponent>(RootActions.ActionTypes.UPDATE_NAVIGATION_COMPONENT),
      switchMap(action => of(action.payload)
        .pipe(
          map(res => new RootActions.LoadSuccessAction(res)),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        ))
    );

  @Effect()
  GetApplicantData$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadApplicantDataAction>(RootActions.ActionTypes.LOAD_APPLICANT_DATA),
      switchMap(action => this.dataService.getApplicantData(action.folderId, action.applicantId)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  IncludeApplicant$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.IncludeApplicantAction>(RootActions.ActionTypes.INCLUDE_APPLICANT),
      switchMap(action => this.dataService.postIncludeApplicant(action.folderId, action.applicantId)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  ExcludeApplicant$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.ExcludeApplicantAction>(RootActions.ActionTypes.EXCLUDE_APPLICANT),
      switchMap(action => this.dataService.postExcludeApplicant(action.folderId, action.applicantId)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  GetLatesSummary$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadLatesSummaryAction>(RootActions.ActionTypes.LOAD_LATES_SUMMARY),
      switchMap(payload => this.dataService.getLatesSummary(payload.payload.externalId, payload.payload.period)
        .pipe(
          mergeMap((latesSummary: LatesSummary) => of(new RootActions.LoadLatesSummarySuccessAction({latesSummary}))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  PostDataToNewFolder$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadDataToNewFolderAction>(RootActions.ActionTypes.LOAD_DATA_TO_NEW_FOLDER),
      switchMap(payload => this.dataService.postDataToNewFolder(payload.payload.body)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  PostDataToExistingFolder$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadDataToExistingFolderAction>(RootActions.ActionTypes.LOAD_DATA_TO_EXISTING_FOLDER),
      switchMap(payload => this.dataService.postDataToExistingFolder(payload.payload.folderId, payload.payload.body)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  PostCreditReportToNewFolder$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadCreditReportToNewFolderAction>(RootActions.ActionTypes.LOAD_CREDIT_REPORT_TO_NEW_FOLDER),
      switchMap(payload => this.dataService.postCreditReportToNewFolder(payload.payload.body)
        .pipe(
          mergeMap(res => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  PostCreditReportToExistingFolder$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.LoadCreditReportToExistingFolderAction>(RootActions.ActionTypes.LOAD_CREDIT_REPORT_TO_EXISTING_FOLDER),
      switchMap(payload => this.dataService.postCreditReportToExistingFolder(payload.payload.folderId, payload.payload.body)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  ExcludeLiability$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.ExcludeLiabilityAction>(RootActions.ActionTypes.EXCLUDE_LIABILITY),
      switchMap(payload => this.dataService.postExcludeLiability(payload.folderId, payload.liabilityId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.IncludeLiabilitySuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  IncludeLiability$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.IncludeLiabilityAction>(RootActions.ActionTypes.INCLUDE_LIABILITY),
      switchMap(payload => this.dataService.postIncludeLiability(payload.folderId, payload.liabilityId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.IncludeLiabilitySuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  ExcludeLiabilityCategory$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.ExcludeLiabilityCategoryAction>(RootActions.ActionTypes.EXCLUDE_LIABILITY_CATEGORY),
      switchMap(payload => this.dataService.postExcludeLiabilityCategory(payload.folderId, payload.liabilityName, payload.applicantId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.IncludeLiabilitySuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  IncludeLiabilityCategory$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.IncludeLiabilityCategoryAction>(RootActions.ActionTypes.INCLUDE_LIABILITY_CATEGORY),
      switchMap(payload => this.dataService.postIncludeLiabilityCategory(payload.folderId, payload.liabilityName, payload.applicantId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.IncludeLiabilitySuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  UpdatePublicRecord$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.UpdatePublicRecordAction>(RootActions.ActionTypes.UPDATE_PUBLIC_RECORD),
      switchMap(payload => this.dataService.updatePublicRecord(payload.folderId, payload.applicantId,
        payload.publicRecordId, payload.body)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  UpdateLiability$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.UpdateLiabilityAction>(RootActions.ActionTypes.UPDATE_LIABILITY),
      switchMap(payload => this.dataService.putUpdateLiability(payload.folderId, payload.liability)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  AddLiability$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.AddLiabilityAction>(RootActions.ActionTypes.ADD_LIABILITY),
      switchMap(payload => this.dataService.postAddByFolderIdAndApplicantId(payload.folderId, payload.applicantId, payload.liability)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );
  @Effect()
  RemoveLiability$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.RemoveLiabilityAction>(RootActions.ActionTypes.REMOVE_LIABILITY),
      switchMap(payload => this.dataService.removeLiability(payload.folderId, payload.liabilityId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );
  @Effect()
  UpdateHousingExpenses$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.UpdateHousingExpensesAction>(RootActions.ActionTypes.UPDATE_HOUSING_EXPENSES),
      switchMap(payload => this.dataService.updateHousingExpenses(payload.folderId, payload.housingExpenses, payload.applicantId)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );

  @Effect()
  UpdateCreditScore$: Observable<Action> = this.actions$
    .pipe(
      ofType<RootActions.UpdateCreditScoreAction>(RootActions.ActionTypes.UPDATE_CREDIT_SCORE),
      switchMap(payload => this.dataService.updateCreditScore(payload.folderId, payload.applicantId, payload.provider)
        .pipe(
          mergeMap((res: any) => of(new RootActions.LoadSuccessAction(res))),
          catchError(err => of(new RootActions.LoadFailureAction(err)))
        )
      )
    );
}
