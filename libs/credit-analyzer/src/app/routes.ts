import {SummaryComponent} from './modules/credit-analyzer/components/summary/summary.component';
import {MortgageComponent} from './modules/mortgage/components/mortgage/mortgage.component';
import {Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: "summary",
    component: SummaryComponent
  },
  {
    path: "applicant/:id",
    component: MortgageComponent
  },
  {
    path: "applicant/:id/:open",
    component: MortgageComponent
  },
  {
    path: "**",
    redirectTo: 'summary',
  }
];
