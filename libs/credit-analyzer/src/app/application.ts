import {Component, OnDestroy, OnInit} from '@angular/core';
import {State} from './root-store/root-state';
import {Store} from '@ngrx/store';
import {LoadSummaryWithConfigAction} from './root-store/root-actions';
import {ActivatedRoute} from '@angular/router';
import {map, filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  template: '<app-component [spinnerEnabled]="spinnerEnabled"></app-component>',
})
export class Application implements OnInit, OnDestroy {
  spinnerEnabled = true;
  routerSubscription$: Subscription;
  dataSubscription$: Subscription;

  constructor(private store: Store<State>,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.routerSubscription$ = this.activatedRoute.queryParamMap.pipe(
      map(p => p.get('folder')),
      filter(folder => !!folder)
    ).subscribe(externalId => this.store.dispatch(new LoadSummaryWithConfigAction({externalId})));
  }

  ngOnDestroy() {
    this.routerSubscription$.unsubscribe();
    this.dataSubscription$.unsubscribe();
  }
}
