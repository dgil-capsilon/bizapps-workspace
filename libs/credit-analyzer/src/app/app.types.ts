export enum DispositionType {
  Adjudicated = 'Adjudicated',
  Appealed = 'Appealed',
  Canceled = 'Canceled',
  Completed = 'Completed',
  Converted = 'Converted',
  Discharged = 'Discharged',
  Dismissed = 'Dismissed',
  Distributed = 'Distributed',
  Filed = 'Filed',
  Granted = 'Granted',
  InvoluntarilyDischarged = 'Involuntarily Discharged',
  Nonadjudicated = 'Nonadjudicated',
  Other = 'Other',
  Paid = 'Paid',
  PaidNotSatisfied = 'Paid Not Satisfied',
  Pending = 'Pending',
  RealEstateSold = 'Real Estate Sold',
  Released = 'Released',
  Rescinded = 'Rescinded',
  Satisfied = 'Satisfied',
  Settled = 'Settled',
  Unknown = 'Unknown',
  Unreleased = 'Unreleased',
  Unsatisfied = 'Unsatisfied',
  Vacated = 'Vacated',
  VoluntarilyDischarged = 'Voluntarily Discharged',
  Withdrawn = 'Withdrawn'
}

export enum RuleStatus {
  Failed = 'FAILED',
  Passed = 'PASSED',
  NotEvaluated = 'NOT_EVALUATED',
  NotApplicable = 'NOT_APPLICABLE',
  Waived = 'WAIVED'
}

export enum Status {
  CURRENT = 'Current',
  UNRATED = 'Unrated',
  DELINQUENT = 'Delinquent',
  COLLECTIONS = 'Collections',
  BANKRUPTCY = 'Bankruptcy',
  FORECLOSURE = 'Foreclosure'
}

export enum Rating {
  CLOSED = 'Closed',
  FROZEN = 'Frozen',
  OPEN = 'Open',
  PAID = 'Paid',
  REFINANCED = 'Refinanced',
  TRANSFERRED = 'Transferred'
}

export enum ECOA {
  AUTHORISED_USER = 'Authorised user',
  COMAKER = 'Comaker',
  DECEASED = 'Deceased',
  INDIVIDUAL = 'Individual',
  JOINT = 'Joint',
  JOINT_PARTICIPATING = 'Joint Participating',
  MAKER = 'Maker',
  ON_BEHALF_OF = 'On behalf of',
  TERMINATED = 'Terminated',
  UNDESIGNATED = 'Undesignated'
}

export enum TagColor {
  'Equifax' = 'equifax',
  'TransUnion' = 'transUnion',
  'Experian' = 'experian',
  'open' = 'open',
  'late payments' = 'latePayments',
  'current' = 'open',
  'delinquent' = 'delinquent',
  'collections' = 'collections',
  'bankruptcy' = 'bankruptcy',
  'foreclosure' = 'foreclosure',
  'Old 1003' = 'old-housing-expenses',
  'Expires' = 'expires',
  'Expired' = 'expired'
}

export enum ExpenseType {
  Alimony = 'Alimony',
  ChildSupport = 'Child Support',
  JobRelatedExpenses = 'Job Related Expenses',
  Other = 'Other',
  SeparateMaintenanceExpense = 'Separate Maintenance Expense'
}

export enum HousingExpensesType {
  Present = 'Present',
  Proposed = 'Proposed'
}

export enum LiabilityCategoryType {
  MORTGAGE = 'Mortgage',
  INSTALLMENT = 'Installment',
  REVOLVING = 'Revolving',
  OTHER = 'Other',
  HOUSING_EXPENSES = 'Housing Expenses'
}

export enum ProviderType {
  EXPERIAN = 'Experian',
  TRANSUNION = 'TransUnion',
  EQUIFAX = 'Equifax'
}

export enum ItemSourceType {
  LoanApplication = 'LoanApplication',
  CreditReport = 'CreditReport',
  Manual = 'Manual'
}
