import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDateDDMMMYYYY'
})
export class FormatDateDDMMMYYYYPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? moment(value, "YYYY-MM-DD").format("DD MMM, YYYY") : "-"
  }

}
