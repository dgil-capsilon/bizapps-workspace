import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDateMMDDYYYY'
})
export class FormatDateMMDDYYYYPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? moment(value, "YYYY-MM-DD").format("MM/DD/YYYY") : "-"
  }

}
