import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'none'
})
export class NonePipe implements PipeTransform {

  transform(value: any, noneValue: any = 'None'): any {
    return value && value > 0 ? value : noneValue;
  }

}
