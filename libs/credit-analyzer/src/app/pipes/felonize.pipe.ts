import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'felonize'
})
export class FelonizePipe implements PipeTransform {

  transform(name: string, args?: any): any {
    if (name != '') {
      const _name = name || '';
      return _name.slice(0, /[cC][hH]/.test(_name) ? 2 : 1) + '.';

    }
  }

}
