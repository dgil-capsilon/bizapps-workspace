import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dash'
})
export class DashPipe implements PipeTransform {

  transform(value: any, noneValue: any = '-'): any {
    return value && value > 0 ? value : noneValue;
  }

}
