import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDateMMYY'
})
export class FormatDateMMYYPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? moment(value, 'YYYY-MM-DD').format('MM/YY') : '--';
  }

}
