import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateUnknow'
})
export class DateUnknowPipe implements PipeTransform {

  transform(value: any, defaultValue: any = 'date unknow'): any {
    return value || defaultValue;
  }


}
