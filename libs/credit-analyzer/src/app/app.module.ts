import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import {SummaryComponent} from './modules/credit-analyzer/components/summary/summary.component';
import {LiabilityItemComponent} from './modules/credit-analyzer/components/liability-item/liability-item.component';
import {CreditScoreComponent} from './modules/credit-analyzer/components/credit-score/credit-score.component';
import {MposHttpService} from '../services/mpos/mpos-http-service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FelonizePipe} from './pipes/felonize.pipe';
import {SidebarModule} from './modules/sidebar/sidebar.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Application} from './application';
import {RootStoreModule} from './root-store/root-store.module';
import {ApplicantScoresComponent} from './modules/credit-analyzer/components/applicant-scores/applicant-scores.component';
import {VerticalProgressBarComponent} from './modules/credit-analyzer/components/vertical-progress-bar/vertical-progress-bar.component';
import {TotalDebtComponent} from './modules/credit-analyzer/components/total-debt/total-debt.component';
import {HorizontalProgressBarComponent} from './modules/credit-analyzer/components/horizontal-progress-bar/horizontal-progress-bar.component';
import {LiabilityTypeComponent} from './modules/credit-analyzer/components/liability-type/liability-type.component';
import {RouterModule} from '@angular/router';
import {TotalLatesComponent} from './modules/credit-analyzer/components/total-lates/total-lates.component';
import {ApplicantsLatesComponent} from './modules/credit-analyzer/components/applicants-lates/applicants-lates.component';
import {MortgageModule} from './modules/mortgage/mortgage.module';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './interceptors/auth-interceptor';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ProgressSpinnerComponent} from './modules/credit-analyzer/components/progress-spinner/progress-spinner.component';
import {ErrorDialogComponent} from './modules/credit-analyzer/components/error-dialog/error-dialog.component';
import {StoreService} from './root-store/store.service';
import {routes} from './routes';
import {HideOnProductionDirective} from './modules/credit-analyzer/directives/hide-on-production/hide-on-production.directive';
import {AddOtherDialogComponent} from './modules/mortgage/components/liabilities/components/add-other-dialog/add-other-dialog.component';
import { DtiComponent } from './modules/credit-analyzer/components/dti/dti.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ApplicantScoresResultComponent} from './modules/credit-analyzer/components/applicant-scores-result/applicant-scores-result.component';
import {NonePipe} from './pipes/none.pipe';
import {LoggerService} from '../services/logger.service';
import {environment} from '../environments/environment';
import {DialogService} from '../services/dialog.service';
import {SentryProvider} from '../services/sentry/sentry-provider';
import {SentryErrorHandler} from '../services/sentry/sentry-error-handler';
import {DefaultErrorHandler} from '../services/default-error-handler';
import {HttpErrorInterceptor} from './interceptors/http-error.interceptor';
import { BureauLogoComponent } from './modules/credit-analyzer/components/bureau-logo/bureau-logo.component';


export function provideErrorHandler(injector: Injector) {
  const dialog = injector.get(DialogService);
  const logger = injector.get(LoggerService);

  if (environment.production) {
    const sentry = new SentryProvider(injector.get(HttpClient));
    const sentryErrorHandler = new SentryErrorHandler(dialog, logger, sentry);

    sentry.init();

    return sentryErrorHandler;
  } else {
    return new DefaultErrorHandler(dialog, logger);
  }
}

@NgModule({
    entryComponents: [
        ErrorDialogComponent,
        AddOtherDialogComponent
    ],
    declarations: [
        Application,
        AppComponent,
        SummaryComponent,
        LiabilityItemComponent,
        CreditScoreComponent,
        FelonizePipe,
        ApplicantScoresComponent,
        VerticalProgressBarComponent,
        TotalDebtComponent,
        HorizontalProgressBarComponent,
        LiabilityTypeComponent,
        TotalLatesComponent,
        ApplicantsLatesComponent,
        ProgressSpinnerComponent,
        ErrorDialogComponent,
        HideOnProductionDirective,
        DtiComponent,
        ApplicantScoresResultComponent,
        NonePipe,
        BureauLogoComponent
    ],
    imports: [
        MatCheckboxModule,
        MatRadioModule,
        MatCardModule,
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatSidenavModule,
        MatTabsModule,
        SidebarModule,
        ReactiveFormsModule,
        RootStoreModule,
        RouterModule.forRoot(routes),
        MatButtonModule,
        MortgageModule,
        MatGridListModule,
        PerfectScrollbarModule,
        MatProgressSpinnerModule,
        MatInputModule,
        FormsModule,
        MatIconModule,
        MatDialogModule,
        MatTooltipModule
    ],
    exports: [
        MatSidenavModule,
        MatTabsModule,
    ],
    providers: [
        MposHttpService,
        StoreService,
        LoggerService,
        DialogService,
        SentryProvider,
      { provide: ErrorHandler, useFactory: provideErrorHandler, deps: [Injector] },
      { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }
    ],
    bootstrap: [Application]
})
export class AppModule {
}
