const hosts = {
  integration: 'integration-amswmtg.capsilondev.net',
  staging: 'staging-amswmtg.capsilondev.net'
};
let protocol = 'https:';
const host = window.location.hostname;

const apiUrl = (/^(localhost|([0-9]{1,3}\.){3}[0-9]{1,3})/.test(host))
  ? `${protocol}//${hosts.integration}/bizapps/creditanalyzer`
  : `/bizapps/creditanalyzer`;

const dmnUrl = (/^(localhost|([0-9]{1,3}\.){3}[0-9]{1,3})/.test(host))
  ? `${protocol}//${hosts.integration}/bizapps/dmnresults`
  : `/bizapps/dmnresults`;

export const mposServer = {
  url: apiUrl,
  methodMock: {
    summary: '/mock/summary',
    exclude: '/mock/applicants/:id/exclude',
    include: '/mock/applicants/:id/include'
  },
  method: {
    load: '/folders/load',
    config: '/config',
    excludeApplicant: '/folders/:folderId/applicants/:applicantId/exclude',
    includeApplicant: '/folders/:folderId/applicants/:applicantId/include',
    getApplicantData: '/folders/:folderId/applicants/:applicantId',
    getLatesSummary: '/folders/:folderId/latesSummary/',
    excludeLiability: '/folders/:folderId/liabilities/:liabilityId/exclude',
    includeLiability: '/folders/:folderId/liabilities/:liabilityId/include',
    excludeLiabilityCategory: '/folders/:folderId/applicants/:applicantId/liabilities/:liabilityId/exclude',
    includeLiabilityCategory: '/folders/:folderId/applicants/:applicantId/liabilities/:liabilityId/include',
    postDataToNewFolder: '/folders/init',
    postDataToExistingFolder: '/folders/:folderId/init',
    postCreditReportToExistingFolder: '/folders/:folderId/importCreditReport',
    postCreditReportToNewFolder: '/folders/importCreditReport',
    updatePublicRecord: '/folders/:folderId/applicants/:applicantId/public-records/:publicRecordId',
    updateLiability: '/folders/:folderId/liabilities/:liabilityId',
    postAddLiability: '/folders/:folderId/applicants/:applicantId/liabilities-other',
    deleteLiabilityOther: '/folders/:folderId/liabilities-other/:liabilityId',
    putHousingExpensesProposed: '/folders/:folderId/applicants/:applicantId/housing-expenses',
    putUpdateCreditScore: '/folders/:folderId/applicants/:applicantId/credit-score/:provider',
    getSnippet: '/folders/:folderId/applicants/:applicantId/snippet',
    getOtherLiabilitySnippet: '/folders/:folderId/liabilities/:liabilityId/snippet',
    getDocReader: '/folders/:folderId/applicants/:applicantId/docReader/loanApplication'
  }
};

export const DMN_SERVER = {
  url: dmnUrl,
  method: {
    getRules: '/folders/:folderId/checklist/credit'
  }
};
