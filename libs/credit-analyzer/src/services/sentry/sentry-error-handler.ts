import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { SentryProvider } from './sentry-provider';
import { LoggerService } from '../logger.service';
import {DialogService} from '../dialog.service';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {

  constructor(
    public dialog: DialogService,
    public logger: LoggerService,
    public sentry: SentryProvider) {
  }

  handleError(error: any) {
    this.logger.error(error);
    this.sentry.captureException(error);
    this.openDialog(error);
  }

  openDialog(err: Error | HttpErrorResponse) {
    let errorMessage;

    if (err instanceof HttpErrorResponse && err.status === 404) {
      errorMessage = `This folder has no asset data.\n Please check back later.`;
    } else {
      errorMessage = `Unexpected error has occurred.`;
    }

    if (errorMessage) {
      this.dialog.openDialog({
        message: errorMessage
      });
    }
  }
}
