import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import * as Sentry from '@sentry/browser';

@Injectable()
export class SentryProvider {
  private configPath = './assets/config/sentry.json';

  constructor(private httpClient: HttpClient) { }

  async init() {
    try {
      const config = await this.httpClient.get(this.configPath).toPromise();
      this.initializeSentry(config);
    } catch (error) {
      if (error instanceof HttpErrorResponse) {
        console.log('[SENTRY] error loading config');
      } else {
        console.log('[SENTRY] error on initializing Sentry');
      }
    }
  }

  initializeSentry(config: any) {
    Sentry.init(config);
  }

  captureException(exception: any) {
    return Sentry.captureException(exception);
  }

  captureMessage(message: string, level?: any) {
    return Sentry.captureMessage(message, level);
  }

}
