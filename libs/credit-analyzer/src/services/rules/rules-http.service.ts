import {Injectable} from '@angular/core';
import {DMN_SERVER} from '../../const';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RulesHttpService {

  private readonly options = {
    headers: new HttpHeaders({
      'content-type': 'application/json',
      'accept': 'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  getRules(endpoint, folderId) {
    return this.http.get(DMN_SERVER.url + endpoint
      .replace(':folderId', folderId));
  }
}
