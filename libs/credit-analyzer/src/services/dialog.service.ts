import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import {ErrorDialogComponent} from '../app/modules/credit-analyzer/components/error-dialog/error-dialog.component';

interface UserError {
  title?: string;
  message: string;
}

@Injectable()
export class DialogService {

  private dialogRef: MatDialogRef<ErrorDialogComponent>;

  constructor(public dialog: MatDialog) { }

  openDialog(data: UserError, dialogOptions?: any) {

    if (this.dialogRef) {
      this.dialogRef.close();
    }

    const options = {
      data,
      width: '500px',
      disableClose: true,
      panelClass: 'empty-app',
      ...dialogOptions,
    };

    this.dialogRef = this.dialog.open(ErrorDialogComponent, options);

    this.dialogRef.afterClosed().subscribe(result => { });

    return this.dialogRef;
  }

}
