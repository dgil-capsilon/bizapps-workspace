import {Injectable} from '@angular/core';
import {mposServer} from '../../const';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {LiabilityType} from '../../app/modules/credit-analyzer/models/credit-analyzer-models';
import {HousingExpenses} from '../../app/root-store/root-state';
import {ProviderType} from '../../app/app.types';

@Injectable({
    providedIn: 'root'
})
export class MposHttpService {

    source = mposServer.url;
    options: Object;

    constructor(private http: HttpClient) {
        this.options = {
            headers: new HttpHeaders({
                'content-type': 'application/json',
                'accept': 'application/json'
            })
        };
    }

    postById(endpoint, id, body) {
        return this.http.post(this.source + endpoint.replace(':id', id), body, this.options);
    }

    getConfig(endpoint) {
      return this.http.get(this.source + endpoint, this.options);
    }

    postIncludeOrExcludeApplicant(endpoint, folderId, applicantId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':applicantId', applicantId),
            body, this.options);
    }

    postIncludeOrExcludeLiability(endpoint, folderId, liabilityId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':liabilityId', liabilityId),
            body, this.options);
    }

    postIncludeOrExcludeLiabilityCategory(endpoint, folderId, applicantId, liabilityId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':applicantId', applicantId)
            .replace(':liabilityId', liabilityId),
            body, this.options);
    }

    getWithFolderIdAndApplicantId(endpoint, folderId, applicantId) {
        return this.http.get(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':applicantId', applicantId), this.options);
    }

    postLiability(endpoint, folderId, applicantId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':applicantId', applicantId), body, this.options);
    }

    getLatesSummary(endpoint, folderId, period) {
        return this.http.get(this.source + endpoint
            .replace(':folderId', folderId),
            {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    'accept': 'application/json'
                }),
                params: new HttpParams().set('period', period)
            });
    }

    postDataToNewFolder(endpoint, body) {
        return this.http.post(this.source + endpoint,
            body, this.options);
    }

    postDataToExistingFolder(endpoint, folderId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId),
            body, this.options);
    }

    postCreditReportToNewFolder(endpoint, body) {
        return this.http.post(this.source + endpoint,
            body, this.options);
    }

    postCreditReportToExistingFolder(endpoint, folderId, body) {
        return this.http.post(this.source + endpoint
            .replace(':folderId', folderId),
            body, this.options);
    }

    updatePublicRecord(endpoint: string, folderId: number, applicantId: number, publicRecordId: number, body) {
        return this.http.put(this.source + endpoint
            .replace(':folderId', folderId.toString())
            .replace(':applicantId', applicantId.toString())
            .replace(':publicRecordId', publicRecordId.toString()),
          body, this.options);
    }

    putUpdateLiability(endpoint: string, folderId: any, liability: LiabilityType) {
        return this.http.put(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':liabilityId', liability.id.toString()), liability, this.options);
    }

    putUpdateHousingExpenses(endpoint: string, folderId: any, applicantId: number, he: HousingExpenses) {
        return this.http.put(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':applicantId', applicantId.toString()), he, this.options);
    }
    putUpdateCreditScore(endpoint: string, folderId: any, applicantId: number, provider: ProviderType) {
        return this.http.put(this.source + endpoint
            .replace(':folderId', folderId.toString())
            .replace(':applicantId', applicantId.toString())
            .replace(':provider', provider.toString()), null, this.options);
    }

    getSnippet(endpoint: string, folderId: any, applicantId: number, id: string, expanded: string) {
        let params = new HttpParams().set('dataPoint', id).set('expanded', expanded);
        return this.http.get(this.source + endpoint
            .replace(':folderId', folderId.toString())
            .replace(':applicantId', applicantId.toString()), {params, responseType: 'blob'});
    }

    getOtherLiabilitySnippet(endpoint: string, folderId: any, liabilityId: number, expanded: string) {
        let params = new HttpParams().set('expanded', expanded);
        return this.http.get(this.source + endpoint
            .replace(':folderId', folderId.toString())
            .replace(':liabilityId', liabilityId.toString()), {params, responseType: 'blob'});
    }

    deleteLiability(endpoint: string, folderId: any, liabilityId: number) {
        return this.http.delete(this.source + endpoint
            .replace(':folderId', folderId)
            .replace(':liabilityId', liabilityId.toString()), this.options);
    }
    getDocReader(folderId, applicantId){
        return this.http.get(this.source + mposServer.method.getDocReader
            .replace(':folderId', folderId.toString())
            .replace(':applicantId', applicantId.toString()));
    }
}
