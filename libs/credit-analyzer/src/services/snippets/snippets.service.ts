import { Injectable } from '@angular/core';
import {DataService} from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class SnippetsService {

  constructor(private dataService: DataService) { }

  getSnippet(snippetId, folderId, applicantId, expanded){
    return this.dataService.getSnippet(folderId, applicantId, snippetId, expanded);
  }

  getOtherLiabilitySnippet(folderId, liabilityId, expanded){
    return this.dataService.getOtherLiabilitySnippet(folderId, liabilityId, expanded);
  }
}
