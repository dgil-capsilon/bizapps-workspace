import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { LoggerService } from './logger.service';
import {DialogService} from './dialog.service';

@Injectable({ providedIn: 'root' })
export class DefaultErrorHandler implements ErrorHandler {

  constructor(public dialog: DialogService, private logger: LoggerService) { }

  handleError(error: Error | HttpErrorResponse) {

    this.logger.error(error);
    this.openDialog(error);

  }

  openDialog(err: Error | HttpErrorResponse) {
    let errorMessage;

    if (err instanceof HttpErrorResponse && err.status === 404) {
      errorMessage = `This folder has no credit data.<br> Please check back later.`;
    } else {
      errorMessage = `Unexpected error has occurred.`;
    }

    if (errorMessage) {
      this.dialog.openDialog({
        message: errorMessage
      });
    }
  }
}
