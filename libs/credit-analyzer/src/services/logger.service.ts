import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {

  constructor() { }

  log(message: any) {
    console.log(message);
  }

  error(message: any) {
    console.error(message);
  }

}
