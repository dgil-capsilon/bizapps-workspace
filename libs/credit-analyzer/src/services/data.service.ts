import {Injectable} from '@angular/core';
import {MposHttpService} from './mpos/mpos-http-service';
import {mposServer, DMN_SERVER} from '../const';
import {LiabilityType} from '@modules/credit-analyzer/models/credit-analyzer-models';
import {HousingExpenses} from '@app/root-store/root-state';
import {ProviderType} from '@app/app.types';
import {RulesHttpService} from '@services/rules/rules-http.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private httpService: MposHttpService, private checklistService: RulesHttpService) {
    }

    removeLiability(folderId, liabilityId) {
        return this.httpService.deleteLiability(mposServer.method.deleteLiabilityOther, folderId, liabilityId);
    }

    postSummaryData(id) {
        const externalId = {'externalId': id};
        return this.httpService.postById(mposServer.method.load, undefined, externalId);
    }

    getConfigData() {
      return this.httpService.getConfig(mposServer.method.config);
    }

    getRules(folderId) {
      return this.checklistService.getRules(DMN_SERVER.method.getRules, folderId);
    }

    postIncludeApplicant(folderId, applicantId) {
        return this.httpService.postIncludeOrExcludeApplicant(mposServer.method.includeApplicant, folderId, applicantId, undefined);
    }

    postExcludeApplicant(folderId, applicantId) {
        return this.httpService.postIncludeOrExcludeApplicant(mposServer.method.excludeApplicant, folderId, applicantId, undefined);
    }

    postIncludeLiability(folderId, liabilityId) {
        return this.httpService.postIncludeOrExcludeLiability(mposServer.method.includeLiability, folderId, liabilityId, undefined);
    }

    putUpdateLiability(folderId, liability: LiabilityType) {
        return this.httpService.putUpdateLiability(mposServer.method.updateLiability, folderId, liability);
    }

    postExcludeLiabilityCategory(folderId, liabilityName, applicantId) {
        return this.httpService.postIncludeOrExcludeLiabilityCategory(mposServer.method.excludeLiabilityCategory, folderId, applicantId,
            liabilityName, undefined);
    }

    postIncludeLiabilityCategory(folderId, liabilityName, applicantId) {
        return this.httpService.postIncludeOrExcludeLiabilityCategory(mposServer.method.includeLiabilityCategory, folderId, applicantId,
            liabilityName, undefined);
    }

    postExcludeLiability(folderId, liabilityId) {
        return this.httpService.postIncludeOrExcludeLiability(mposServer.method.excludeLiability, folderId, liabilityId, undefined);
    }

    getApplicantData(folderId, applicantId) {
        return this.httpService.getWithFolderIdAndApplicantId(mposServer.method.getApplicantData, folderId, applicantId);
    }

    postAddByFolderIdAndApplicantId(folderId, applicantId, body) {
        return this.httpService.postLiability(mposServer.method.postAddLiability, folderId, applicantId, body);
    }

    getLatesSummary(folderId, period) {
        return this.httpService.getLatesSummary(mposServer.method.getLatesSummary, folderId, period);
    }

    postDataToNewFolder(body) {
        return this.httpService.postDataToNewFolder(mposServer.method.postDataToNewFolder, body);
    }

    postDataToExistingFolder(folderId, body) {
        return this.httpService.postDataToExistingFolder(mposServer.method.postDataToExistingFolder, folderId, body);
    }

    postCreditReportToNewFolder(body) {
        return this.httpService.postCreditReportToNewFolder(mposServer.method.postCreditReportToNewFolder, body);
    }

    postCreditReportToExistingFolder(folderId, body) {
        return this.httpService.postCreditReportToExistingFolder(mposServer.method.postCreditReportToExistingFolder, folderId, body);
    }

    updatePublicRecord(folderId, applicantId, publicRecordId, body) {
        return this.httpService.updatePublicRecord(mposServer.method.updatePublicRecord, folderId, applicantId, publicRecordId, body);
    }

    updateHousingExpenses(folderId: string, housingExpenses: HousingExpenses, applicantId: number) {
      return this.httpService.putUpdateHousingExpenses(mposServer.method.putHousingExpensesProposed, folderId, applicantId,
        housingExpenses);
    }

    updateCreditScore(folderId: string, applicantId: number, provider: ProviderType) {
        return this.httpService.putUpdateCreditScore(mposServer.method.putUpdateCreditScore, folderId, applicantId, provider);
    }
    getSnippet(folderId: string, applicantId: number, id: string, expanded: string) {
        return this.httpService.getSnippet(mposServer.method.getSnippet, folderId, applicantId, id, expanded);
    }
    getOtherLiabilitySnippet(folderId: string, liabilityId: number, expanded: string) {
        return this.httpService.getOtherLiabilitySnippet(mposServer.method.getOtherLiabilitySnippet, folderId, liabilityId, expanded);
    }
}
