import { HttpParams } from '@angular/common/http';

export class ExtHttpParams extends HttpParams {

  constructor(
    public options: { suppressErrors?: boolean, ignoredErrorCodes?: number[] },
    params?: { [param: string]: string | string[] }
  ) {
    super({ fromObject: params });
  }

}
