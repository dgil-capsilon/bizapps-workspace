import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';
import shave from 'shave';

@Directive({
  selector: '[chkClamp]'
})
export class ClampDirective implements AfterViewInit {
  @Input() clampHeight: number;

  constructor(private elRef: ElementRef<HTMLElement>) {
  }

  ngAfterViewInit(): void {
    shave(this.elRef.nativeElement, this.clampHeight);

    // Workaround since font-weight increase after selecting rule
    this.elRef.nativeElement.classList.remove('temporary-font-extend');
  }

}
