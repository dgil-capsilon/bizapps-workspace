import {Component} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {ClampDirective} from './clamp.directive';

@Component({
  template: '<div style="width:256px;font-size:14px;line-height:20px" [clampHeight]="height" chkClamp>{{text}}</div>'
})
class MockComponent {
  height: number;
  text: string;
}

describe('ClampDirective', () => {
  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let directive: ClampDirective;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent,
        ClampDirective
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    const directiveEl = fixture.debugElement.query(By.directive(ClampDirective));
    directive = directiveEl.injector.get(ClampDirective);
  });

  it('should cut off text', () => {
    component.height = 40;
    component.text = 'If commissions are used to qualify income, is there a history of 2 years of commission income?';
    fixture.detectChanges();
    const textDivEl = fixture.nativeElement.querySelector('div');

    expect(textDivEl.childNodes.item(0).textContent)
      .toEqual('If commissions are used to qualify income, is there a history of 2 years of commission');
    expect(textDivEl.childNodes.item(1).textContent).toEqual('…');
  });

  it('should do not cut off text', () => {
    component.height = 40;
    component.text = 'Are there any employment gaps >= 60 days';
    fixture.detectChanges();
    const textDivEl = fixture.nativeElement.querySelector('div');

    expect(textDivEl.childNodes.item(0).textContent).toEqual('Are there any employment gaps >= 60 days');
  });
});
