import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromFolder from '@redux/folder/folder.selectors';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';
import {Subject} from 'rxjs';
import {StatusFilter} from '@models/data.enum';

@Component({
  selector: 'chk-no-rules',
  templateUrl: './no-rules.component.html',
  styleUrls: ['./no-rules.component.scss']
})
export class NoRulesComponent implements OnInit, OnDestroy {
  StatusFilter = StatusFilter;
  statusFilter$ = this.store$.select(fromFolder.getStatusFilter);
  private destroy$ = new Subject();

  constructor(private store$: Store<AppState>,
              private router: Router) {
  }

  ngOnInit(): void {
    this.store$.select(fromRuleDetails.getRuleDetails).subscribe(rule => {
      if (rule) {
        this.router.navigate([rule.id], {queryParamsHandling: 'merge'});
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
