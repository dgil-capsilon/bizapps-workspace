import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {NoRulesComponent} from './no-rules.component';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('NoRulesComponent', () => {
  let component: NoRulesComponent;
  let fixture: ComponentFixture<NoRulesComponent>;
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select']);
  const routerSpy: SpyObj<Router> = createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [NoRulesComponent],
      providers: [
        {provide: Store, useValue: storeSpy},
        {provide: Router, useValue: routerSpy}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
