import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatInput} from '@angular/material/input';
import {FolderStatus} from '@models/enums/folder-status';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromFolder from '@redux/folder/folder.selectors';
import {SetSearchTerm} from '@redux/rules/rules.actions';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'chk-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit {
  @ViewChild(MatInput) searchInput: MatInput;
  isSearchControlEnabled: boolean;
  isStatusFine$ = this.store$.select(fromFolder.getSpecificStatus, {status: FolderStatus.Fine});
  contextSelected$ = this.store$.select(fromFolder.getFolderContext);
  searchControl: FormControl;

  constructor(private store$: Store<AppState>,
              private cdRef: ChangeDetectorRef,
              private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.renderer.listen('document', 'keyup', e => {
      if (e.ctrlKey && e.shiftKey && e.key === 'S') { // Ctrl + Shift + S
        this.isSearchControlEnabled = !this.isSearchControlEnabled;
        this.cdRef.detectChanges();
        this.searchInput.focus();
      }
    });

    this.searchControl = new FormControl();
    this.searchControl.valueChanges.pipe(debounceTime(200), distinctUntilChanged()).subscribe(value => {
      this.store$.dispatch(SetSearchTerm({searchTerm: value}));
      this.cdRef.detectChanges();
    });
  }
}
