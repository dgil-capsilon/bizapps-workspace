import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {SelectNextRule, SelectPreviousRule} from '@redux/rules/rules.actions';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-arrows-nav',
  templateUrl: './arrows-nav.component.html',
  styleUrls: ['./arrows-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrowsNavComponent {
  previousRuleAvailable$ = this.store$.select(fromApplicants.getPreviousRuleId).pipe(map(id => !Boolean(id)));
  nextRuleAvailable$ = this.store$.select(fromApplicants.getNextRuleId).pipe(map(id => !Boolean(id)));

  constructor(private store$: Store<AppState>) {
  }

  nextRule() {
    this.store$.dispatch(SelectNextRule());
  }

  previousRule() {
    this.store$.dispatch(SelectPreviousRule());
  }
}
