import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {ArrowsNavComponent} from './arrows-nav.component';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('ArrowsNavComponent', () => {
  let component: ArrowsNavComponent;
  let fixture: ComponentFixture<ArrowsNavComponent>;
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);

  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [ArrowsNavComponent],
      providers: [
        {provide: Store, useValue: storeSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrowsNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
