import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {RuleStatusFilterComponent} from './rule-status-filter.component';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('RuleStatusFilterComponent', () => {
  let component: RuleStatusFilterComponent;
  let fixture: ComponentFixture<RuleStatusFilterComponent>;
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);

  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [RuleStatusFilterComponent],
      providers: [
        {provide: Store, useValue: storeSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleStatusFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
