import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {SetStatusFilter} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import * as fromRules from '@redux/rules/rules.selectors';
import {RuleStatus, StatusFilter} from '@models/data.enum';

@Component({
  selector: 'chk-rule-status-filter',
  templateUrl: './rule-status-filter.component.html',
  styleUrls: ['./rule-status-filter.component.scss']
})
export class RuleStatusFilterComponent implements OnInit {
  StatusFilter = StatusFilter;
  statusControl = new FormControl();
  waivedRules$ = this.store$.select(fromRules.getRulesWithStatus, {status: RuleStatus.Waived});
  failedRules$ = this.store$.select(fromRules.getRulesWithStatus, {status: RuleStatus.Failed});
  allRules$ = this.store$.select(fromApplicants.getTotalRules);

  constructor(private store$: Store<AppState>) {
  }

  ngOnInit() {
    this.store$.select(fromFolder.getStatusFilter).subscribe(statusFilter => {
      this.statusControl.setValue(statusFilter, {emitEvent: false});
    });

    this.statusControl.valueChanges.subscribe(statusFilter => {
      this.store$.dispatch(SetStatusFilter({statusFilter}));
    });
  }

}
