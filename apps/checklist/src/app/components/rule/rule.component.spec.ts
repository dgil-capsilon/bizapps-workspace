import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RuleStatus} from '@models/enums';
import {HyphenPipeMock} from '@app/test/hyphen.pipe';
import {RuleComponent} from './rule.component';

describe('RuleComponent', () => {
  let component: RuleComponent;
  let fixture: ComponentFixture<RuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RuleComponent, HyphenPipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleComponent);
    component = fixture.componentInstance;
    component.rule = {status: RuleStatus.Passed} as any;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should create status-icon component', () => {
    component.rule = {status: RuleStatus.Passed} as any;
    fixture.detectChanges();
    const status = fixture.nativeElement.querySelector('app-status-icon');
    expect(status).toBeTruthy();
  });

  it('should create div instead status-icon component', () => {
    component.rule = {status: RuleStatus.NotEvaluated} as any;
    fixture.detectChanges();
    const status = fixture.nativeElement.querySelector('.not-accessible');
    expect(status).toBeTruthy();
  });
});
