import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';
import {IsNotDefined} from '@decorators/is-not-defined.decorator';
import {BaseStatuses} from '@models/base-statuses';
import {ApplicantRule} from '@models/data';

@IsNotDefined()
@Component({
  selector: 'chk-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RuleComponent extends BaseStatuses {
  @Input() rule: ApplicantRule;
  @Input() showRuleCodes: boolean;

  @HostBinding('class.disabled') get disabled() {
    return this.rule && this.isNotDefined(this.rule.status);
  }
}
