import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Host, OnInit, Renderer2} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {SetApplicant} from '@redux/applicants/applicants.actions';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {LoadRule} from '@redux/rule-details/rule-details.actions';
import {SelectRule} from '@redux/rules/rules.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {Severity} from '@sentry/browser';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {ApplicantData} from '@models/data';
import {RuleStatus} from '@models/enums';
import {ScrollHoverDirective} from '@app/modules/scroll-hover/scroll-hover.directive';
import {LoggerService} from '@app/services/logger.service';
import {ScrollService} from '@app/services/scroll.service';

@Component({
  selector: 'chk-rules-list',
  templateUrl: './rules-list.component.html',
  styleUrls: ['./rules-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RulesListComponent implements OnInit {
  private selectedRuleId: number;
  private useScroll = true;
  applicants$: Observable<ApplicantData[]> = this.store$.select(fromApplicants.getAllApplicants);
  noRules: boolean;
  showRuleCodes = false;
  applicantRules$ = (id) => this.store$.select(fromApplicants.getAllRulesOfApplicant, {id}).pipe(
    withLatestFrom(this.store$.select(fromRules.getSearchTerm)),
    map(([applicants, searchTerm]) => this.appendSearchFilter(applicants, searchTerm))
    // tslint:disable-next-line:semicolon
  );

  constructor(private store$: Store<AppState>,
              private cdRef: ChangeDetectorRef,
              private scrollService: ScrollService,
              private loggerService: LoggerService,
              @Host() private scrollHoverDirective: ScrollHoverDirective,
              private renderer: Renderer2) {
  }

  selectRule(rule) {
    if (rule.id < 0) {
      this.loggerService.captureMessage('Rule without id', rule, Severity.Warning);
      return;
    }

    const ruleNotDefined = rule.status === RuleStatus.NotApplicable || rule.status === RuleStatus.NotEvaluated;
    if (rule.id === this.selectedRuleId || ruleNotDefined) {
      return;
    }
    this.useScroll = false;
    this.store$.dispatch(SelectRule({id: rule.id}));
    this.store$.dispatch(SetApplicant({id: rule.applicant}));
    this.store$.dispatch(LoadRule());
  }

  ngOnInit(): void {
    this.renderer.listen('document', 'keyup', e => {
      if (e.ctrlKey && e.shiftKey && e.key === 'X') { // Ctrl + Shift + X
        this.showRuleCodes = !this.showRuleCodes;
        this.cdRef.detectChanges();
      }
    });

    this.store$.select(fromRules.getSelectedRuleId).subscribe(selectedRuleId => {
      this.selectedRuleId = selectedRuleId;
      this.cdRef.detectChanges();
    });

    this.store$.select(fromApplicants.getAllNavigationRules)
      .pipe(
        switchMap(() => this.store$.select(fromApplicants.getPreviousRuleId)),
        distinctUntilChanged(),
        // without debounceTime scrollTo doesn't work for Waive and merge.
        debounceTime(0))
      .subscribe(() => {
        this.scrollTo();
      });

    this.store$.select(fromApplicants.getAllNavigationRules)
      .pipe(map(rules => rules.length === 0))
      .subscribe(noRules => this.noRules = noRules);
  }

  isRuleSelected(id: number) {
    return this.selectedRuleId === id;
  }

  trackByApplicantId(index, applicant: ApplicantData) {
    return applicant.applicantId;
  }

  trackByRuleId(index, rule) {
    return rule.id;
  }

  getFirstName(applicant: ApplicantData): string {
    return applicant.fullName.split(' ')[0].toLowerCase();
  }

  private scrollTo() {
    if (!this.useScroll) {
      this.useScroll = true;
      return;
    }

    this.scrollService.scrollToElement({
      container: 'rules_container',
      target: 'rule_' + this.selectedRuleId,
      vertical: true,
      duration: 600,
      offset: -250
    });
  }

  /* INTERNAL - for search */
  private appendSearchFilter(rules: any[], searchTerm: string) {
    if (!searchTerm) {
      return rules;
    }

    const filteredRules = rules.filter(rule => {
      if (!rule) {
        return false;
      }

      const descriptionContains = rule.description.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
      const ruleCodeContains = rule.ruleId.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;

      return descriptionContains || ruleCodeContains;
    });

    // None of rules match search term - show all rules
    if (filteredRules.length === 0) {
      return rules;
    }

    return filteredRules;
  }

  applicantPanelExpand() {
    this.scrollHoverDirective.recalculateAndShow();
  }
}
