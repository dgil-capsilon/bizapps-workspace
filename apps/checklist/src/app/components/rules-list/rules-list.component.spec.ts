import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {LoggerService} from '@app/services/logger.service';
import {RulesListComponent} from './rules-list.component';
import {ScrollHoverDirective} from 'src/app/modules/scroll-hover/scroll-hover.directive';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('RulesListComponent', () => {
  let component: RulesListComponent;
  let fixture: ComponentFixture<RulesListComponent>;
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);
  const loggerServiceSpy: SpyObj<LoggerService> = createSpyObj('LoggerService', ['captureMessage']);
  const scrollHoverDirectiveSpy: SpyObj<ScrollHoverDirective> = createSpyObj('ScrollHoverDirective', ['recalculateAndShow']);

  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [RulesListComponent],
      providers: [
        {provide: Store, useValue: storeSpy},
        {provide: LoggerService, useValue: loggerServiceSpy},
        {provide: ScrollHoverDirective, useValue: scrollHoverDirectiveSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get first name', () => {
    const name = {fullName: 'John Jr Homeowner'} as any;
    expect(component.getFirstName(name)).toBe('john');
  });
});
