import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StatusIconComponent} from './status-icon.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RuleStatus} from 'src/app/models/data.enum';
import {By} from '@angular/platform-browser';

describe('StatusIconComponent', () => {
  let component: StatusIconComponent;
  let fixture: ComponentFixture<StatusIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusIconComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusIconComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get icon when status passing', () => {
    component.status = RuleStatus.Passed;
    fixture.detectChanges();
    const matIcon = fixture.nativeElement.querySelector('mat-icon');
    expect(matIcon.classList).toContain('icon-passed');
  });

  it('should get icon when status failing', () => {
    component.status = RuleStatus.Failed;
    fixture.detectChanges();
    const matIcon = fixture.nativeElement.querySelector('mat-icon');
    expect(matIcon.classList).toContain('icon-failed');
  });
});
