import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {RuleStatus} from '@models/enums';

@Component({
  selector: 'chk-status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusIconComponent {
  @Input() status: RuleStatus;

  get isPassed(): boolean {
    return this.status === RuleStatus.Passed;
  }

  get isFailed(): boolean {
    return this.status === RuleStatus.Failed;
  }

  get isWaived(): boolean {
    return this.status === RuleStatus.Waived;
  }
}
