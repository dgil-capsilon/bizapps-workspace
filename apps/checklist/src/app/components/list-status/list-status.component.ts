import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import * as fromFolder from '@redux/folder/folder.selectors';

@Component({
  selector: 'chk-list-status',
  templateUrl: './list-status.component.html',
  styleUrls: ['./list-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListStatusComponent {
  totalFailedRules$ = this.store$.select(fromApplicants.getTotalFailedRules);
  totalRules$ = this.store$.select(fromApplicants.getTotalRules);
  status$ = this.store$.select(fromFolder.getStatus);

  constructor(private store$: Store<AppState>) {
  }
}
