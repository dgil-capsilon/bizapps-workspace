import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '@redux/app.reducer';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {ListStatusComponent} from './list-status.component';

describe('ListStatusComponent', () => {
  let component: ListStatusComponent;
  let fixture: ComponentFixture<ListStatusComponent>;
  let store: MockStore<{ folder: { status: string } }>;
  const initialState = {
    id: null,
    showAll: false,
    status: 'NOT_INITIALIZED'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListStatusComponent],
      providers: [
        provideMockStore({initialState})
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStatusComponent);
    component = fixture.componentInstance;
    store = TestBed.get<Store<AppState>>(Store);
  });

  it('should show "no income available" status', () => {
    store.setState({folder: {status: 'NO_INCOME_AVAILABLE'}});

    fixture.detectChanges();

    const statusLabelEl: HTMLElement = fixture.nativeElement.querySelector('.status-label');
    expect(statusLabelEl).toBeTruthy('Element with status doesn\'t exist ');
    expect(statusLabelEl.textContent).toContain('No Income Available');
  });

  it('should show "no borrower selected" status', () => {
    store.setState({folder: {status: 'NO_BORROWER_SELECTED'}});

    fixture.detectChanges();

    const statusLabelEl: HTMLElement = fixture.nativeElement.querySelector('.status-label');
    expect(statusLabelEl).toBeTruthy('Element with status doesn\'t exist ');
    expect(statusLabelEl.textContent).toContain('No Borrower Selected');
  });

  it('should show "not applicable" status', () => {
    store.setState({folder: {status: 'NOT_APPLICABLE'}});

    fixture.detectChanges();

    const statusLabelEl: HTMLElement = fixture.nativeElement.querySelector('.status-label');
    expect(statusLabelEl).toBeTruthy('Element with status doesn\'t exist ');
    expect(statusLabelEl.textContent).toContain('Not Applicable');
  });

  describe('should set "fine" status', () => {
    it('and show failed rules info', () => {
      store.overrideSelector(fromApplicants.getTotalFailedRules, 1);
      store.overrideSelector(fromApplicants.getTotalRules, 2);
      store.setState({folder: {status: 'FINE'}});

      fixture.detectChanges();

      const statusLabelEl: HTMLElement = fixture.nativeElement.querySelector('.status-label');
      expect(statusLabelEl).toBeTruthy();
      expect(statusLabelEl.textContent).toContain('1 out of 2 Failed');
    });

    it('and show "all rules passed" info', () => {
      store.overrideSelector(fromApplicants.getTotalFailedRules, 0);
      store.setState({folder: {status: 'FINE'}});

      fixture.detectChanges();

      const statusLabelEl: HTMLElement = fixture.nativeElement.querySelector('.status-label');
      expect(statusLabelEl).toBeTruthy();
      expect(statusLabelEl.textContent).toContain('All Rules Passed');
    });
  });
});
