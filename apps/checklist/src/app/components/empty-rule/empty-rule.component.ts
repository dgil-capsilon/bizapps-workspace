import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'chk-empty-rule',
  templateUrl: './empty-rule.component.html',
  styleUrls: ['./empty-rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyRuleComponent {
}
