import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyRuleComponent } from './empty-rule.component';

describe('EmptyRuleComponent', () => {
  let component: EmptyRuleComponent;
  let fixture: ComponentFixture<EmptyRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
