import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {WaiveDataApi} from '@models/data';
import {RuleStatus} from '@models/data.enum';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import * as fromFolder from '@redux/folder/folder.selectors';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';
import {EditWaiveRule, UnWaiveRule, WaiveRule} from '@redux/rules/rules.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {ConfirmDialogService, ReasonDialogService} from '@bizapps-workspace/components';
import {Subject} from 'rxjs';
import {first, takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'chk-evidence-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  fullName$ = this.store$.select(fromApplicants.getSelectedApplicantFullName);
  description$ = this.store$.select(fromRules.getSelectedRuleDescription);
  waiveData: WaiveDataApi;
  isFolderBlocked: boolean;
  canWaive: boolean;
  status$ = this.store$.select(fromRules.getSelectedRuleStatus).pipe(tap((status) => this.status = status));
  private destroy$ = new Subject();
  private status: RuleStatus;

  get canRuleBeWaived() {
    return this.canWaive && this.status === RuleStatus.Failed;
  }

  get waiveNotEdited() {
    return this.waiveData && this.waiveData.reasonDate === this.waiveData.date;
  }

  constructor(private store$: Store<AppState>,
              private dialog: MatDialog,
              private cdRef: ChangeDetectorRef,
              private reasonDialogService: ReasonDialogService,
              private confirmDialogService: ConfirmDialogService) {
  }

  ngOnInit(): void {
    this.selectFromStore(fromRuleDetails.getRuleDetailsWaive, (next) => {
      this.waiveData = next;
      this.cdRef.detectChanges();
    });
    this.selectFromStore(fromRuleDetails.getRuleDetailsCanWaive, (next) => {
      this.canWaive = next;
      this.cdRef.detectChanges();
    });

    this.selectFromStore(fromFolder.getLoanStatusFolderOpen, (next) => {
      this.isFolderBlocked = !next;
      this.dialog.closeAll();
      this.cdRef.detectChanges();
    });
  }

  openUnWaiveDialog() {
    if (this.isFolderBlocked) {
      return;
    }

    const confirmDialogRef = this.confirmDialogService.open({
      data: {
        title: 'Unwaive this rule',
        contentText: 'Unwaived rule will result in return to the current status of this rule.',
        cancelBtnText: 'CANCEL',
        confirmBtnText: 'UNWAIVE'
      }
    });

    this.confirmDialogService.confirm.subscribe(() => this.store$.dispatch(UnWaiveRule()));
    this.confirmDialogService.cancel.subscribe(() => confirmDialogRef.close());
  }

  openWaiveDialog() {
    if (this.isFolderBlocked) {
      return;
    }

    const reasonDialogRef = this.reasonDialogService.open({
      data: {
        title: 'Waive this rule',
        contentText: `
        Waived rule is no longer treated as failed and number of failed rules will change. Waiving reason is required.`,
        cancelBtnText: 'CANCEL',
        confirmBtnText: 'WAIVE',
        reasonLabelText: 'Required field',
        reasonValue: null,
        showReasonLabel: true,
        maxReasonLength: 500
      }
    });

    this.reasonDialogService.confirm.subscribe(reason => this.store$.dispatch(WaiveRule({reason})));
    this.reasonDialogService.cancel.subscribe(() => reasonDialogRef.close());
  }

  openUpdateWaiveDialog() {
    if (this.isFolderBlocked) {
      return;
    }

    let reasonDialogRef;
    this.store$.select(fromRuleDetails.getRuleDetailsWaive).pipe(first()).subscribe(waiveData => {
      reasonDialogRef = this.reasonDialogService.open({
        data: {
          title: 'Edit a reason',
          contentText: 'The current reason will be overwritten and the previous reason will no longer be available.',
          cancelBtnText: 'CANCEL',
          confirmBtnText: 'OVERWRITE',
          reasonLabelText: null,
          reasonValue: waiveData.reason,
          showReasonLabel: false,
          maxReasonLength: 500
        }
      });
    });

    this.reasonDialogService.confirm.subscribe(reason => this.store$.dispatch(EditWaiveRule({reason})));
    this.reasonDialogService.cancel.subscribe(() => reasonDialogRef.close());
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private selectFromStore(selector: any, callback: Function): void {
    this.store$.select(selector)
      .pipe(takeUntil(this.destroy$))
      .subscribe(next => {
        callback(next);
      });
  }
}
