import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {HyphenPipeMock} from '@app/test/hyphen.pipe';
import {HeaderComponent} from './header.component';
import {ToDatePipeMock} from 'src/app/test/to-date.pipe';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  const navigationServiceMock = {rule: createSpy()};
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent, HyphenPipeMock, ToDatePipeMock],
      providers: [
        {provide: Store, useValue: storeSpy},
        {provide: MatDialog, useValue: jasmine.createSpyObj('MatDialog', ['open'])}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    navigationServiceMock.rule.and.returnValue(of(null));
    storeSpy.select.and.returnValue(of());

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
