import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {ViewPartitionComponent} from './view-partition.component';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('ViewPartitionComponent', () => {
  let component: ViewPartitionComponent;
  let fixture: ComponentFixture<ViewPartitionComponent>;
  const dataServiceMock = {getRuleData: createSpy()};
  const activatedRouteMock = {snapshot: {paramMap: {get: createSpy()}}};
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);

  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [ViewPartitionComponent],
      imports: [RouterTestingModule],
      providers: [
        {provide: Store, useValue: storeSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    dataServiceMock.getRuleData.and.returnValue(of());

    fixture = TestBed.createComponent(ViewPartitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
