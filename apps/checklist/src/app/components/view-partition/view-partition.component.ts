import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';
import {ROUTER_CONSTS} from '@models/router-consts';
import {NodeService} from '@app/services/node.service';

@Component({
  selector: 'chk-view-partition',
  templateUrl: './view-partition.component.html',
  styleUrls: ['./view-partition.component.scss']
})
export class ViewPartitionComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  ruleDataLoading: boolean;

  constructor(private nodeService: NodeService,
              private routerService: Router,
              private store$: Store<AppState>,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.store$.select(fromRuleDetails.getLoading).pipe(takeUntil(this.destroy$)).subscribe(loading => {
      this.ruleDataLoading = loading;
      this.cdRef.detectChanges();
    });

    this.store$.select(fromRuleDetails.getRuleDetails).pipe(
      takeUntil(this.destroy$),
      tap(() => this.cdRef.detectChanges())
    ).subscribe((data) => {
      if (data) {
        this.nodeService.addNodeData(data);
        if (data.id && data.viewType) {
          this.routerService.navigate([data.id, data.viewType], {queryParamsHandling: 'merge'});
          this.cdRef.detectChanges();
        }
      } else {
        this.routerService.navigate([ROUTER_CONSTS.noRules], {queryParamsHandling: 'merge'});
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
