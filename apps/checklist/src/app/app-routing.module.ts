import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NoRulesComponent} from './components/no-rules/no-rules.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ViewPartitionComponent} from './components/view-partition/view-partition.component';
import {CheckRuleGuard} from './guards/check-rule.guard';
import {ROUTER_CONSTS, ROUTER_PARAMS} from '@models/router-consts';

const routes: Routes = [
  {
    path: ROUTER_CONSTS.noRules,
    component: NoRulesComponent
  },
  {
    path: `${ROUTER_PARAMS.id}`,
    component: ViewPartitionComponent,
    canActivate: [CheckRuleGuard],
    children: [
      {
        path: `${ROUTER_CONSTS.timeline}`,
        loadChildren: () => import('./views/evidence/evidence.module')
          .then(m => m.EvidenceModule)
      },
      {
        path: `${ROUTER_CONSTS.dataValidation}`,
        loadChildren: () => import('./views/data-validation/data-validation.module')
          .then(m => m.DataValidationModule)
      },
      {
        path: `${ROUTER_CONSTS.trending}`,
        loadChildren: () => import('./views/trending/trending.module')
          .then(m => m.TrendingModule)
      },
      {
        path: `${ROUTER_CONSTS.merging}`,
        loadChildren: () => import('./views/merging/merging.module')
          .then(m => m.MergingModule)
      }
    ]
  },
  {
    path: `${ROUTER_CONSTS.notFound}`,
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: `${ROUTER_CONSTS.notFound}`
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
