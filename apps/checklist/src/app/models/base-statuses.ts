import {DocStatus, RuleStatus, ValidationStatus} from '@models/enums';

export abstract class BaseStatuses {
  isInvalid?(status: ValidationStatus): boolean;
  isNotEvaluated?(status: ValidationStatus | DocStatus): boolean;
  isValid?(status: ValidationStatus): boolean;
  isExpired?(status: ValidationStatus): boolean;
  isMissing?(status: ValidationStatus): boolean;
  isNotDefined?(status: RuleStatus): boolean;
}
