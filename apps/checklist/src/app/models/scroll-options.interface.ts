import {ElementRef, EventEmitter} from '@angular/core';

export interface ScrollOptions {
  container: HTMLElement | ElementRef | string;
  duration: number;
  vertical: boolean;
  target?: HTMLElement | ElementRef | string;
  by?: number;
  to?: number;
  offset?: number;
  scrollFinishedEmitter?: EventEmitter<void>;
}
