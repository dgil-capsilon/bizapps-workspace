import {LoanStatus, StompEvent} from '@models/enums';

export interface StompFolderEventApi {
  id: string;
  date: number;
  folderId: string;
  loanStatus: LoanStatus;
  eventType: StompEvent;
}
