import {LoanStatus} from '@models/enums';
import {ApplicantDataApi} from '@models/data';

// /bizapps/checklist/folders/{folder-id}/checklist
export interface FolderDataApi {
  applicants: ApplicantDataApi[];
  loanStatus: LoanStatus;
  folderId: string;
}
