import {EmploymentTypes} from '@models/enums';

export interface GapApi {
  active: boolean;
  dataValidation: [];
  documents: [];
  duration: number;
  durationInDays: number;
  endDate: string | Date; // TODO: Primary type is string but Date needed to parse
  primary: boolean;
  startDate: string | Date; // TODO: Primary type is string but Date needed to parse
  type: EmploymentTypes;
}
