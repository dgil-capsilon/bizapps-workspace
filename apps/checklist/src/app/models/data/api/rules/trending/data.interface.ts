// Trending
import {TrendingValidationItemsApi} from '@models/data';

export interface TrendingRuleDataApi {
  validationItems: TrendingValidationItemsApi[];

  employmentDetails: undefined; // TODO: Because of "setViewType" in data-transition.service.ts, find better way
}
