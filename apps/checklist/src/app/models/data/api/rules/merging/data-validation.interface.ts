import {EmploymentDataValidationApi} from '@models/data';

export interface MergingDataValidationApi extends EmploymentDataValidationApi {
  manualMatch: boolean;
}
