import {ValidationFieldTypes, ValidationStatus} from '@models/enums';

export interface EmploymentDataValidationApi {
  docType: string; // TODO: Check
  documentId: string;
  fieldName: string;
  fieldType: ValidationFieldTypes;
  fieldValue: string; // TODO: Check
  group: string;
  snippetUrl: string;
  status: ValidationStatus;

  // TODO: "?" or "primary-type | undefined"?
  referenceValue?: string;
  borderValue?: string;
  referenceFieldName?: string;
  employerNames: string[];
}
