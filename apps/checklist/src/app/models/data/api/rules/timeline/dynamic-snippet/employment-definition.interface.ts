import {HighlightColor} from '@models/enums';
import {SnippetDefinitionCoordinatesApi} from '@models/data';

export interface EmploymentSnippetDefinitionApi {
  color: HighlightColor;
  coordinates: SnippetDefinitionCoordinatesApi;
  marked: boolean;
}
