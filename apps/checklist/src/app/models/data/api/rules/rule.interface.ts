// /bizapps/checklist/folders/{folder-id}/checklist/rules/{rule-id}
import {MergingRuleDataApi, TimelineRuleDataApi, TrendingRuleDataApi, ValidationRuleDataApi, WaiveDataApi} from '@models/data';
import {RuleCode, RuleStatus} from '@models/enums';

export interface RuleApi<T = TimelineRuleDataApi | ValidationRuleDataApi | TrendingRuleDataApi | MergingRuleDataApi> {
  applicantId: string;
  data: T;
  folderId: string;
  fullName: string;
  id: number;
  ruleCode: RuleCode;
  status: RuleStatus;
  waive: WaiveDataApi;
  canWaive: boolean;
}
