export interface TimelineMarkerApi {
  label: string;
  markerPosition: string | Date;
}
