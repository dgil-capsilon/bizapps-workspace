import {ValidationEmploymentDetailsApi} from '@models/data';
import {LoanTypeDetails} from '@models/data/app/rules/timeline/loan-type-details.interface';

export interface ValidationRuleDataApi {
  employmentDetails: ValidationEmploymentDetailsApi[];
  folderId: string;
  referenceDate: string;

  validationItems: undefined; // TODO: Because of "setViewType" in data-transition.service.ts, find better way
  loanTypeDetails: LoanTypeDetails;
}
