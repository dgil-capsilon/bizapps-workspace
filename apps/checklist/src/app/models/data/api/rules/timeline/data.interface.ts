import {GapApi, TimelineEmploymentDetailsApi} from '@models/data';
import {LoanTypeDetailsApi} from '@models/data/api/rules/timeline/loan-type-details.interface';

export interface TimelineRuleDataApi {
  employmentDetails: TimelineEmploymentDetailsApi[] | GapApi[];
  folderId: string;
  referenceDate: string | Date; // TODO: Because of parsing from string to Date

  validationItems: undefined; // TODO: Because of "setViewType" in data-transition.service.ts, find better way
  loanTypeDetails: LoanTypeDetailsApi;
}
