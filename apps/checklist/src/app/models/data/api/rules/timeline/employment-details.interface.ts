import {DocApi, EmployerAddressApi} from '@models/data';
import {EmploymentTypes} from '@models/enums';

export interface TimelineEmploymentDetailsApi {
  active: boolean;
  address: EmployerAddressApi;
  dataValidation: [];
  dateOrigin: string; // TODO: Check
  documents: DocApi[];
  duration: number;
  durationInDays: number;
  employerName: string;
  employmentPositionDescription: string;
  endDate: string | Date; // TODO: Primary type is string but Date needed to parse
  primary: boolean;
  startDate: string | Date; // TODO: Primary type is string but Date needed to parse
  statusType: string; // TODO: Check
  timeline: number;
  type: EmploymentTypes;
  employerOtherNames: string[];
}
