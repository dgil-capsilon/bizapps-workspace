import {EmploymentDataValidationApi} from '@models/data';
import {TimelineMarkerApi} from '@models/data/api/rules/timeline/timeline-marker.interface';

export interface LoanTypeDetailsApi {
  docTitle: string;
  dataValidation: EmploymentDataValidationApi[];
  timelineMarkers: TimelineMarkerApi[];
}
