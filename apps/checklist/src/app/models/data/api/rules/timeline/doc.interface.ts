import {EmploymentSnippetDefinitionApi} from '@models/data';
import {DocStatus, DocTypes} from '@models/enums';

export interface DocApi {
  current: boolean;
  dataSourceId: string;
  durationInDays: number;
  employerName: string;
  endDate: string | Date; // TODO: Primary type is string but Date needed to parse
  group: string; // TODO: Check
  previous: boolean;
  snippetDefinition: EmploymentSnippetDefinitionApi[]; // TODO: This exist on every doc?

  /**
   * This property doesn't exist for new folders. However it is needed for old ones.
   */
  snippetUrl: string;
  snippetUrls: string[];
  startDate: string | Date; // TODO: Primary type is string but Date needed to parse
  status: DocStatus;
  type: DocTypes;
}
