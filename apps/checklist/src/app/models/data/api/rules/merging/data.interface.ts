import {LoanAppEmployers} from '@models/data';
import {MergingDataValidationApi} from '@models/data';


export interface MergingRuleDataApi {
  loanAppEmployers: LoanAppEmployers[];
  employmentValidationItems: MergingDataValidationApi[];
}
