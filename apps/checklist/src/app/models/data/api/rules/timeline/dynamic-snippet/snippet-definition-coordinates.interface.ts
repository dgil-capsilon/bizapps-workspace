export interface SnippetDefinitionCoordinatesApi {
  containerId: string;
  dataSourceId: string;
  offset: string;
  snippetKey: string; // TODO: Check
}
