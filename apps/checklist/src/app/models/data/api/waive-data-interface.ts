export interface WaiveDataApi {
  date: string;
  doneBy: string;
  id: number;
  reason: string;
  reasonDate: string;
  reasonEditedBy: string;
  waived: boolean;
}
