import {RuleCode, RuleStatus} from '@models/enums';
import {WaiveDataApi} from '@models/data';

export interface ApplicantRuleApi {
  description: string;
  id: number;
  ruleId: RuleCode;
  status: RuleStatus;
  waive: WaiveDataApi;
}
