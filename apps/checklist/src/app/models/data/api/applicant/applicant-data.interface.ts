import {ApplicantRuleApi} from '@models/data';

export interface ApplicantDataApi {
  applicantId: number;
  fullName: string;
  rules: ApplicantRuleApi[];
}
