import {ApplicantRuleApi} from '@models/data';

export interface ApplicantRule extends ApplicantRuleApi {
  applicant: number;
}
