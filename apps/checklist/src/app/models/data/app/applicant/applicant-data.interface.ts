import {ApplicantDataApi, ApplicantRule} from '@models/data';

export interface ApplicantData extends ApplicantDataApi {
  rules: ApplicantRule[];
}
