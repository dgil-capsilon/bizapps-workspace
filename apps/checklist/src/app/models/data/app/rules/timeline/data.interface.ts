import {Gap, TimelineEmploymentDetails, TimelineRuleDataApi} from '@models/data';
import {LoanTypeDetails} from '@models/data/app/rules/timeline/loan-type-details.interface';

export interface TimelineRuleData extends TimelineRuleDataApi {
  employmentDetails: TimelineEmploymentDetails[] | Gap[];
  referenceDate: Date;
  loanTypeDetails: LoanTypeDetails;
}
