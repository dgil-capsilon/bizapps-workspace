import {DocApi, DynamicSnippetData} from '@models/data';
import {DocOverlay} from '@models/data/app/rules/timeline/doc-overlay.interface';

export interface Doc extends DocApi {
  id: number;
  startDate: Date;
  endDate: Date;
  lineIndex: number;
  spScrollId: string;
  tlScrollId: string;
  dynamicSnippetData: DynamicSnippetData;
  snippetUrls: string[];
  overlay: DocOverlay;
}
