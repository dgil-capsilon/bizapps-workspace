// /bizapps/checklist/folders/{folder-id}/checklist/rules/{rule-id}
import {RuleApi} from '@models/data';
import {ViewType} from '@models/enums';

export interface Rule<T = any> extends RuleApi<T> {
  viewType: ViewType;
}
