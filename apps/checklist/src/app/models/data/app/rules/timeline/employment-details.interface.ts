import {Doc, TimelineEmploymentDetailsApi} from '@models/data';

export interface TimelineEmploymentDetails extends TimelineEmploymentDetailsApi {
  id: number;
  missingDocsAmount: number;
  orId: number;
  startDate: Date;
  endDate: Date;
  additionalRows: number;
  documents: Doc[];
  spScrollId: string;
  tlScrollId: string;
  error: string;
  notValidForAtLeastOneRule: boolean;
  employmentTimeInLineOfWorkYearsCount: number;
}
