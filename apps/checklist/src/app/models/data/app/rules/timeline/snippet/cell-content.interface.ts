import {HighlightColor} from '@models/enums';

export interface CellContent {
  value: string | number;
  highlighted?: boolean;
  highlightColor?: HighlightColor;
  customTitle?: string;
}

export type Cells = { [key in CellType]: CellContent };

type CellType = 'nameAndAddress' | 'firstDate' | 'position' | 'secondDate' | 'phone' | 'yrsEmployedInProfession';
