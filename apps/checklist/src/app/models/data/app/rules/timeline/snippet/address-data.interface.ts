export interface SnippetAddressData {
  addressLineText: string;
  cityName: string;
  id: string;
  plusFourZipCode: string;
  postalCode: string;
  stateCode: string;
}
