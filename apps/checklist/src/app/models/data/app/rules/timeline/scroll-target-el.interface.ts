export interface ScrollTargetEl {
  [key: string]: any;

  spScrollId: string;
  tlScrollId: null | string;
}
