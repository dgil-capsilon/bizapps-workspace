import {DataValidationGroup} from '@models/data';
import {LoanTypeDetailsApi} from '@models/data/api/rules/timeline/loan-type-details.interface';

export interface LoanTypeDetails extends LoanTypeDetailsApi {
  groups: Map<string, DataValidationGroup>;
}
