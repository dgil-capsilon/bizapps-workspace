import {GapApi} from '@models/data';

export interface Gap extends GapApi {
  id: number;
  error: string;
  tlScrollId: string;
  spScrollId: string;
  endDate: Date;
  startDate: Date;
}
