export interface DrawOptions {
  referenceDate: Date;
  monthWidth: number;
  rowHeight: number;
  calendarStartDate: Date;
  months;
}
