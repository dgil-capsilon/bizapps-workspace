export interface DocOverlay {
  startDate: Date;
  endDate: Date;
}
