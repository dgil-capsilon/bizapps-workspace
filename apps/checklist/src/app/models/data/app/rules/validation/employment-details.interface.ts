import {DataValidationGroup, ValidationEmploymentDetailsApi} from '@models/data';

export interface ValidationEmploymentDetails extends ValidationEmploymentDetailsApi {
  id: number;
  groups: Map<string, DataValidationGroup>;
  startDate: Date;
  endDate: Date;
  spScrollId: string;
  tlScrollId: string;
}
