import {TimelineMarkerApi} from '@models/data/api/rules/timeline/timeline-marker.interface';

export interface TimelineMarker extends TimelineMarkerApi {
  markerPosition: Date;
}
