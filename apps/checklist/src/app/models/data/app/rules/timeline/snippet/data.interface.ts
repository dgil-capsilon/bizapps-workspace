import {EmploymentSnippetDefinitionApi, SnippetAddressData} from '@models/data';

export interface DynamicSnippetData {
  monthsOnJob: number;
  yearsOnJob: number;
  positionDescription: string;
  address: SnippetAddressData;
  startDate: Date;
  endDate: Date;
  employerName: string;
  primary: boolean;
  current: boolean;
  phoneNumber: string;
  snippetDefinition: EmploymentSnippetDefinitionApi[];
  employmentTimeInLineOfWorkYearsCount?: number;
}
