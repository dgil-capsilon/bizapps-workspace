import {Rule, TimelineEmploymentDetails, TimelineRuleData} from '@models/data';

export interface TimelineRule extends Rule<TimelineRuleData> {
  employmentDetails: TimelineEmploymentDetails[];
}
