import {DataValidationGroup, MergingRuleDataApi} from '@models/data';

export interface MergingRuleData extends MergingRuleDataApi {
  groups: Map<string, DataValidationGroup>;
}
