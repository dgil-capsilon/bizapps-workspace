export interface YearsData {
  months: string[];
  year: string;
}
