import {FolderDataApi} from '@models/data';

// /bizapps/checklist/folders/{folder-id}/checklist
export type FolderData = FolderDataApi;
