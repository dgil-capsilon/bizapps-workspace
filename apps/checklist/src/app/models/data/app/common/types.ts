import {
  EmploymentDataValidationApi,
  MergingRuleData,
  MergingRuleDataApi,
  Rule,
  TimelineEmploymentDetails,
  TimelineRule,
  TrendingRuleDataApi,
  ValidationEmploymentDetails,
  ValidationRuleDataApi
} from '@models/data';

export type TimelineData = TimelineRule;
export type ValidationData = Rule<ValidationRuleDataApi>;
export type TrendingData = Rule<TrendingRuleDataApi>;
export type Employment = TimelineEmploymentDetails | ValidationEmploymentDetails;
export type EmploymentDataValidation = EmploymentDataValidationApi;
export type MergingDataApi = Rule<MergingRuleDataApi>;
export type MergingData = Rule<MergingRuleData>;
