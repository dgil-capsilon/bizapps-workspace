import {EmploymentDataValidationApi} from '@models/data';

export interface DataValidationGroup {
  data: EmploymentDataValidationApi[];
  first: EmploymentDataValidationApi;
}
