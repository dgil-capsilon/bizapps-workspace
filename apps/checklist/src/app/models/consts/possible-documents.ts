export const POSSIBLE_DOCUMENTS = new Map<string, string[]>()
  .set('INC-220', ['VOE', 'Paystub'])
  .set('INC-800', ['VOE'])
  .set('INC-1020', ['(E)VOE', 'Paystub', 'W2']);
