export const COMPARED_DOCUMENTS = new Map<string, string[]>()
  .set('INC-750', ['VOE', 'W_2'])
  .set('INC-1040', ['VOE', 'Paystub']);
