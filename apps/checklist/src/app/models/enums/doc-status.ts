export enum DocStatus {
  Missing = 'MISSING',
  Valid = 'VALID',
  NotEvaluated = 'NOT_EVALUATED'
}
