export enum ValidationFieldTypes {
  Date = 'DATE',
  String = 'STRING',
  Number = 'NUMBER',
  Boolean = 'BOOLEAN'
}
