export enum StompEvent {
  LoadStatusUpdate = 'LOAN_STATUS_UPDATE',
  ChecklistUpdated = 'CHECKLIST_UPDATED',
  EmployerMerge = 'EMPLOYER_MERGE'
}
