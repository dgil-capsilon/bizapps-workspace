export enum HighlightColor {
  Green = 'GREEN',
  Red = 'RED',
  Gray = 'GRAY'
}
