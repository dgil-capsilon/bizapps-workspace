export enum EmployerType {
  Primary = 'Primary',
  Secondary = 'Secondary',
  Previous = 'Previous'
}
