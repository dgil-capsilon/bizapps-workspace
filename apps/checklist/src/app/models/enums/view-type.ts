export enum ViewType {
  Timeline = 'timeline',
  Validation = 'dataValidation',
  Trending = 'trending',
  Merging = 'merging'
}
