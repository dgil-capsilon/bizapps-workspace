export enum FolderStatus {
  Fine = 'FINE',
  NoBorrowerSelected = 'NO_BORROWER_SELECTED',
  NotApplicable = 'NOT_APPLICABLE',
  NoIncomeAvailable = 'NO_INCOME_AVAILABLE',
  NotInitialized = 'NOT_INITIALIZED'
}
