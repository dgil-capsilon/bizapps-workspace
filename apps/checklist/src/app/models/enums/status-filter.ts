export enum StatusFilter {
  Failed = 'FAILED',
  Waived = 'WAIVED',
  All = 'All'
}
