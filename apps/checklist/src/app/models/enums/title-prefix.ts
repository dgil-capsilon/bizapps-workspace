export enum TitlePrefix {
  Primary = 'PRIMARY',
  Secondary = 'SECONDARY',
  Previous = 'PREVIOUS'
}
