export enum LoanStatus {
  Open = 'OPEN',
  Lock = 'LOCK',
  Remove = 'REMOVE'
}
