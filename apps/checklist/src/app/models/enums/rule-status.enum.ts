export enum RuleStatus {
  Failed = 'FAILED',
  Passed = 'PASSED',
  NotEvaluated = 'NOT_EVALUATED',
  NotApplicable = 'NOT_APPLICABLE',
  Waived = 'WAIVED'
}
