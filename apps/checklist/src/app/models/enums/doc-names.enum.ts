export enum DocNames {
  URLA = 'Loan Application',
  W_2 = 'W-2',
  VOE = 'VOE',
  EVOE = 'EVOE',
  PAYSTUB = 'Paystub',
  TL_PAYSTUB = 'PS',
  W_2_PAYSTUB = 'W-2 or year-end PS',
  MISSING_VOE = '(E)VOE',
  FHA_CASE_NUMBER = 'FHA Case Number Assignment'
}
