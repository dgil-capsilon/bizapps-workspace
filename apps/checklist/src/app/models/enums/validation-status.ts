export enum ValidationStatus {
  Valid = 'VALID',
  Invalid = 'INVALID',
  Expired = 'EXPIRED',
  NotEvaluated = 'NOT_EVALUATED',
  Missing = 'MISSING'
}
