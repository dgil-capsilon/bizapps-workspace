export enum DocTypes {
  URLA = 'URLA',
  PAYSTUB = 'PAYSTUB',
  W_2 = 'W_2',
  VOE = 'VOE',
  EVOE = 'EVOE',
  W_2_PAYSTUB = 'W_2_PAYSTUB'
}
