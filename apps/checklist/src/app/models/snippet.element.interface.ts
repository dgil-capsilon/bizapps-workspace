import {ElementRef} from '@angular/core';
import {Doc, Employment, Gap} from './data.interface';

export interface SnippetElement {
  ref: ElementRef<HTMLElement>;
  hasData: (data: Doc | Employment | Gap) => boolean;
}
