import {ViewType} from './data.enum';

export const ROUTER_PARAMS = {
  id: ':id',
  index: ':index'
};

export const ROUTER_CONSTS = {
  notFound: 'not-found',
  noRules: 'no-rules',
  dataValidation: ViewType.Validation,
  timeline: ViewType.Timeline,
  trending: ViewType.Trending,
  merging: ViewType.Merging,
  creditAnalyzer: 'credit-analyzer'
};
