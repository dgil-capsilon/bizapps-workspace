import {OnDestroy, OnInit} from '@angular/core';
import {NodeService} from '@app/services/node.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BaseStatuses} from '@models/base-statuses';

export abstract class ViewBase extends BaseStatuses implements OnDestroy, OnInit {
  protected componentDestroy$ = new Subject<void>();

  protected constructor(protected nodeService: NodeService) {
    super();
  }

  ngOnInit(): void {
    this.nodeService.nodeData$.pipe(takeUntil(this.componentDestroy$)).subscribe(ruleData => {
      this.handleRuleData(ruleData);
    });
    if (this.onInitRestHandler) {
      this.onInitRestHandler();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  protected abstract handleRuleData(ruleData: unknown);

  protected onInitRestHandler?(): void;

}
