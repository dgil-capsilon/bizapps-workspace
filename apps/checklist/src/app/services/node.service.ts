import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {filter} from 'rxjs/operators';
import {Rule} from '@models/data';

@Injectable({
  providedIn: 'root'
})
export class NodeService {
  private node: BehaviorSubject<Rule> = new BehaviorSubject(null);

  get nodeData$(): Observable<Rule> {
    return this.node.asObservable().pipe(filter(Boolean)) as Observable<Rule>;
  }

  addNodeData(data: Rule) {
    this.node.next(data);
  }
}
