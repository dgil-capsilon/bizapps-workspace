import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as Sentry from '@sentry/browser';
import {Severity} from '@sentry/browser';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  private configPath = './assets/config/sentry.json';

  constructor(private http: HttpClient) {
  }

  async init(): Promise<void> {
    try {
      const sentryConfig = await this.http.get(this.configPath).toPromise();
      Sentry.init(sentryConfig);
    } catch (error) {
      if (error instanceof HttpErrorResponse) {
        console.log('Error during Sentry config load', error);
      } else {
        console.log('Error during Sentry initialization', error);
      }
    }
  }

  captureException(exception: any) {
    Sentry.captureException(exception);
  }

  captureMessage(message: string, extras: any, lvl: Severity) {
    Sentry.setExtras(extras);
    Sentry.captureMessage(message, lvl);
    console.error(message, extras);
  }
}
