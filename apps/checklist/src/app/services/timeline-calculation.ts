import {DateRange, DrawOptions} from '@models/data.interface';

export class TimelineCalculation {

  public static months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  constructor() {
  }

  public static daysInMonth(month, year): number {
    return new Date(year, month, 0).getDate();
  }

  public static getMonthDayWidth(month: number, year: number, monthWidth: number): number {
    return monthWidth / this.daysInMonth(month, year);
  }

  public static calculateWidthForDateRange(sDate: Date, eDate: Date, monthWidth: number): number {
    const startDate = new Date(sDate);
    const endDate = eDate ? new Date(eDate) : new Date();
    let width = 0;

    if (startDate.getFullYear() === endDate.getFullYear() &&
      startDate.getMonth() === endDate.getMonth()) {
      const daysBetween = Math.round((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24)) + 1;
      width = daysBetween * this.getMonthDayWidth(startDate.getMonth() + 1, startDate.getFullYear(), monthWidth);
      return width;
    }

    const loopEndDate = new Date(endDate.getFullYear(), endDate.getMonth(), 1);
    const skipLastMonth = endDate.getDate() < this.daysInMonth(endDate.getMonth() + 1, endDate.getFullYear());
    if (skipLastMonth) {
      loopEndDate.setMonth(loopEndDate.getMonth() - 1);
    }

    let tempDate = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
    const skipFirstMonth = startDate.getDate() > 1;
    if (skipFirstMonth) {
      tempDate.setMonth(tempDate.getMonth() + 1);
    }

    while (tempDate <= loopEndDate) {
      tempDate = new Date(tempDate.getTime());
      tempDate.setMonth(tempDate.getMonth() + 1);

      width += monthWidth;
    }

    const startDateDaysToEnd = this.daysInMonth(startDate.getMonth() + 1, startDate.getFullYear()) - startDate.getDate() + 1;
    const endDateDaysToEnd = endDate.getDate();

    const startDateWidth = (monthWidth * startDateDaysToEnd)
      / this.daysInMonth(startDate.getMonth() + 1, startDate.getFullYear());

    const endDateWidth = (monthWidth * endDateDaysToEnd)
      / this.daysInMonth(endDate.getMonth() + 1, endDate.getFullYear());

    if (skipFirstMonth) {
      width += startDateWidth;
    }

    if (skipLastMonth) {
      width += endDateWidth;
    }

    return width;
  }

  public static getTopPosition(rowHeight: number, index?: number): number {
    return index ? index * rowHeight : 0;
  }

  public static getLeftPosition(item: DateRange,
                                settings: DrawOptions,
                                isReversed: boolean,
                                date?: Date): number {
    debugger;
    const currentDate = new Date();
    let left, yearsDiff, monthsFromCalendarStart, daysFromMonthStart, dayWidth;
    let startDate;
    if (!item && date) {
      startDate = new Date(date);
    } else {
      startDate = isReversed ? new Date(item.startDate) : item.endDate ? new Date(item.endDate) : new Date(settings.referenceDate);
    }

    if (isReversed) {
      yearsDiff = startDate.getFullYear() - settings.calendarStartDate.getFullYear();
      monthsFromCalendarStart = ((yearsDiff) * 12 + startDate.getMonth() - settings.calendarStartDate.getMonth());
      // Remove one day - if start date starts from first day of the month we don't have to include to width
      daysFromMonthStart = startDate.getDate() - 1;
    } else {
      yearsDiff = currentDate.getFullYear() - startDate.getFullYear();
      monthsFromCalendarStart = (currentDate.getMonth() + (yearsDiff - 1) * 12 + (12 - startDate.getMonth()));
      daysFromMonthStart = (this.daysInMonth(startDate.getMonth() + 1, startDate.getFullYear()) - startDate.getDate());
    }
    dayWidth = this.getMonthDayWidth(startDate.getMonth() + 1, startDate.getFullYear(), settings.monthWidth);
    left = monthsFromCalendarStart * settings.monthWidth + daysFromMonthStart * dayWidth;

    if (!isReversed) {
      // month = endDate.getMonth() + 1 as months start from 0; 8 - margin-left
      left += 8;
    }

    return left;
  }
}
