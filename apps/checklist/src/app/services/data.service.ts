import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromFolder from '@redux/folder/folder.selectors';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '@environments/environment';
import {EmployerMerge, FolderDataApi, Rule} from '@models/data';
import {DataTransitionService} from './data-transition.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private folderId: string;

  constructor(private http: HttpClient,
              private dataTransitionService: DataTransitionService,
              private store$: Store<AppState>) {
    this.store$.select(fromFolder.getId).subscribe(id => this.folderId = id);
  }

  loadInitialData(): Observable<FolderDataApi> {
    return this.http.get<FolderDataApi>(this.apiUrl)
      .pipe(map(data => this.dataTransitionService.adjustFolderData(data)));
  }

  getRuleData(id: number): Observable<Rule> {
    return this.http.get<Rule>(`${this.apiUrl}/rules/${id}`)
      .pipe(map(data => this.dataTransitionService.adjustRuleData(data)));
  }

  docReaderData(documentId: string): Observable<any> {
    return this.http.get(`${this.url()}/folders/${this.folderId}/docReaderData/${documentId}`);
  }

  removeEmployerNames(applicantId: number, employerNames: EmployerMerge[]): Observable<any> {
    return this.http.post(`${this.url()}/folders/${this.folderId}/applicants/${applicantId}/employers/removeNames`, employerNames);
  }

  saveEmployerNames(applicantId: number, employerNames: EmployerMerge[]): Observable<any> {
    return this.http.post(`${this.url()}/folders/${this.folderId}/applicants/${applicantId}/employers/saveNames`, employerNames);
  }

  waiveRule(reason: string, folderId: string, applicantId: number, ruleId: number) {
    return this.http.post(`${this.url()}/folders/${folderId}/applicants/${applicantId}/rules/${ruleId}/waive`, {reason});
  }

  unWaiveRule(folderId: string, applicantId: number, ruleId: number) {
    return this.http.post(`${this.url()}/folders/${folderId}/applicants/${applicantId}/rules/${ruleId}/unwaive`, {});
  }

  editWaiveRule(reason: string, folderId: string, applicantId: number, ruleId: number) {
    return this.http.post(`${this.url()}/folders/${folderId}/applicants/${applicantId}/rules/${ruleId}/updateReason`, {reason});
  }

  get apiUrl(): string {
    const checkListFolderUrl = `folders/${this.folderId}/checklist`;
    return `${this.url()}${checkListFolderUrl}`;
  }

  private url(): string {
    return environment.baseUrl;
  }

}
