import {HttpClient} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {of} from 'rxjs';
import {DataService} from './data.service';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('DataService', () => {
  const httpClientMock = {get: createSpy(), post: createSpy()};
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);

  beforeEach(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      providers: [
        DataService,
        {provide: HttpClient, useValue: httpClientMock},
        {provide: Store, useValue: storeSpy}
      ]
    });
  });

  it('should be created', () => {
    const service: DataService = TestBed.get(DataService);
    expect(service).toBeTruthy();
  });
});
