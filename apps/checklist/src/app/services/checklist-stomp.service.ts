import {Injectable, isDevMode} from '@angular/core';
import {RxStompService} from '@stomp/ng2-stompjs';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '@environments/environment';
import {StompFolderEventApi} from '@models/data-api.interface';

@Injectable({
  providedIn: 'root'
})
export class ChecklistStompService {
  constructor(private rxStompService: RxStompService) {
    const brokerHostname = isDevMode() ? environment.wsHost : window.location.hostname;

    this.rxStompService.configure({
      // ws://localhost:61614/mposs/ws/stomp
      brokerURL: `wss://${brokerHostname}/sesame/stomp`,
      heartbeatIncoming: 0,
      heartbeatOutgoing: 20000,
      reconnectDelay: 200,
      debug: (msg: string) => null
    });
  }

  folderEventWatch(id: string): Observable<StompFolderEventApi> {
    return this.rxStompService.watch(`/topic/folder/${id}/checklistUpdate`)
      .pipe(map(stompResponse => JSON.parse(stompResponse.body)));
  }
}
