import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandler, Injectable} from '@angular/core';
import * as Sentry from '@sentry/browser';
import {Severity} from '@sentry/browser';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {LoggerService} from './logger.service';

@Injectable()
export class ChecklistErrorHandler implements ErrorHandler {
  constructor(private loggerService: LoggerService) {
  }

  handleError(error: any): void {
    console.error(error);
    this.loggerService.captureException(error);
  }
}

export function initializeLoggerService(loggerService: LoggerService) {
  return () => loggerService.init();
}

export function catchErrorWithLogging<R>(valueToReturn?: Observable<any>) {
  return input$ => input$.pipe(
    catchError(err => {
      console.error(err);

      if (err instanceof HttpErrorResponse) {
        handleHttpError(err);
      } else {
        Sentry.captureException(err);
      }

      return valueToReturn || of(err);
    })
  );
}

function handleHttpError(errorResponse: HttpErrorResponse) {
  let message = getHttpErrorMessage(errorResponse.status);

  if (!message) {
    Sentry.captureException(errorResponse);
    return;
  }

  if (errorResponse.error) {
    message += ' - Response details';
    Sentry.setExtras({...errorResponse.error, url: errorResponse.url});
  } else {
    message += ' - No response details';
  }

  Sentry.captureMessage(message, Severity.Warning);
}

function getHttpErrorMessage(status: number): string | null {
  switch (status) {
    case 401:
      return '401 Unauthorized';
    case 404:
      return '404 Not Found';
    case 500:
      return '500 Internal Server Error';
    case 504:
      return '504 Gateway Timeout';
    default:
      return null;
  }
}
