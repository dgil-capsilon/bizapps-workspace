import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import {HttpErrorResponse} from '@angular/common/http';
import * as Sentry from '@sentry/browser';
import {of, throwError} from 'rxjs';
import {catchErrorWithLogging, ChecklistErrorHandler, initializeLoggerService} from './logger-utils';

describe('Logger Utils', () => {
  const loggerServiceSpy = createSpyObj('LoggerService', ['captureException', 'init']);

  beforeAll(() => {
    spyOn(console, 'error');
  });

  it('should call logger capture exception', () => {
    const checklistErrorHandler = new ChecklistErrorHandler(loggerServiceSpy);
    const someError = new Error('some error');

    checklistErrorHandler.handleError(someError);

    expect(loggerServiceSpy.captureException).toHaveBeenCalledWith(someError);
  });

  it('should init logger', () => {
    const initializer = initializeLoggerService(loggerServiceSpy);

    initializer();

    expect(loggerServiceSpy.init).toHaveBeenCalled();
  });

  it('should catch error, log it and return provided value', (done) => {
    spyOnProperty(Sentry, 'captureException', 'get').and.returnValue(createSpy());
    const operator = catchErrorWithLogging(of(25));

    throwError(new Error('observable error'))
      .pipe(operator)
      .subscribe(val => {
        expect(val).toEqual(jasmine.anything());
        expect(Sentry.captureException).toHaveBeenCalledWith(jasmine.anything());
        done();
      });
  });

  it('should catch error, log it and return error', (done) => {
    spyOnProperty(Sentry, 'captureException', 'get').and.returnValue(createSpy());
    const operator = catchErrorWithLogging();

    throwError(new Error('observable error'))
      .pipe(operator)
      .subscribe(val => {
        expect(val).toEqual(new Error('observable error'));
        expect(Sentry.captureException).toHaveBeenCalledWith(jasmine.anything());
        done();
      });
  });

  it('should catch error, log it and return error (401 without details)', (done) => {
    spyOnProperty(Sentry, 'captureMessage', 'get').and.returnValue(createSpy());
    const operator = catchErrorWithLogging();

    throwError(new HttpErrorResponse({status: 401}))
      .pipe(operator)
      .subscribe(val => {
        expect(val).toEqual(new HttpErrorResponse({status: 401}));
        expect(Sentry.captureMessage).toHaveBeenCalledWith('401 Unauthorized - No response details', 'warning');
        done();
      });
  });

  it('should catch error, log it and return error (401 with details)', (done) => {
    spyOnProperty(Sentry, 'captureMessage', 'get').and.returnValue(createSpy());
    spyOnProperty(Sentry, 'setExtras', 'get').and.returnValue(createSpy());
    const operator = catchErrorWithLogging();

    throwError(new HttpErrorResponse({status: 401, error: {someProperty: 'some value 123'}, url: '/some/request/url'}))
      .pipe(operator)
      .subscribe(val => {
        expect(val).toEqual(new HttpErrorResponse({status: 401, error: {someProperty: 'some value 123'}, url: '/some/request/url'}));
        expect(Sentry.captureMessage).toHaveBeenCalledWith('401 Unauthorized - Response details', 'warning');
        expect(Sentry.setExtras).toHaveBeenCalledWith({someProperty: 'some value 123', url: '/some/request/url'});
        done();
      });
  });

  it('should catch error, log it and return error (502)', (done) => {
    spyOnProperty(Sentry, 'captureException', 'get').and.returnValue(createSpy());
    const operator = catchErrorWithLogging();

    throwError(new HttpErrorResponse({status: 502}))
      .pipe(operator)
      .subscribe(val => {
        expect(val).toEqual(new HttpErrorResponse({status: 502}));
        expect(Sentry.captureException).toHaveBeenCalledWith(jasmine.anything());
        done();
      });
  });
});
