import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import * as Sentry from '@sentry/browser';
import {of} from 'rxjs';
import {LoggerService} from './logger.service';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('LoggerService', () => {
  let service: LoggerService;
  let consoleLogSpy;
  const httpSpy: SpyObj<HttpClient> = createSpyObj('HttpClient', ['get']);

  beforeEach(() => {
    consoleLogSpy = spyOn(console, 'log');
    spyOnProperty(Sentry, 'init', 'get').and.returnValue(createSpy());

    TestBed.configureTestingModule({
      providers: [
        {provide: HttpClient, useValue: httpSpy}
      ]
    });

    service = TestBed.get(LoggerService);
  });

  it('should initialize properly', async () => {
    httpSpy.get.and.returnValue(of({
        dsn: 'https://some-url',
        environment: 'custom env',
        release: ''
      }
    ));

    await service.init();

    expect(httpSpy.get).toHaveBeenCalled();
    expect(consoleLogSpy).not.toHaveBeenCalled();
  });

  it('should inform about sentry config load fail', async () => {
    spyOn(console, 'error');
    httpSpy.get.and.callFake(() => {
      throw new HttpErrorResponse({});
    });

    await service.init();

    expect(consoleLogSpy).toHaveBeenCalledWith('Error during Sentry config load', jasmine.any(HttpErrorResponse));
  });

  it('should inform about initialization fail', async () => {
    httpSpy.get.and.callFake(() => {
      throw new Error('init fails');
    });

    await service.init();

    expect(consoleLogSpy).toHaveBeenCalledWith('Error during Sentry initialization', jasmine.anything());
  });
});
