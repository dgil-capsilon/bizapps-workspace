import {TestBed} from '@angular/core/testing';
import {RxStompService} from '@stomp/ng2-stompjs';
import {of} from 'rxjs';

import {ChecklistStompService} from './checklist-stomp.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('ChecklistStompServiceService', () => {
  let service: ChecklistStompService;
  const rxStompServiceSpy: SpyObj<RxStompService> = createSpyObj('RxStompService', ['configure', 'watch']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: RxStompService, useValue: rxStompServiceSpy}
      ]
    });

    service = TestBed.get(ChecklistStompService);
  });

  it('should watch for right folder event destination', () => {
    rxStompServiceSpy.watch.and.returnValue(of());

    service.folderEventWatch('166');

    expect(rxStompServiceSpy.watch).toHaveBeenCalledWith('/topic/folder/166/checklistUpdate');
  });
});
