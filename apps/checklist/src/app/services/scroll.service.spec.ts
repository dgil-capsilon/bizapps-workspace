import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {ScrollOptions} from '@models/scroll-options.interface';
import {ScrollService} from './scroll.service';

describe('ScrollService', () => {
  let service: ScrollService;

  beforeEach(() => {
    spyOn(document, 'querySelector').and.returnValue(null);

    TestBed.configureTestingModule({});
    service = TestBed.get(ScrollService);
  });

  it('should call event emitter when scroll finishes', fakeAsync(() => {
    const fakeEventEmitter = {emit: jasmine.createSpy()} as any;
    const fakeContainerEl = {scrollTop: 1000} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1500,
      vertical: true,
      duration: 600,
      scrollFinishedEmitter: fakeEventEmitter
    };

    service.scrollTo(scrollOptions);
    tick(16 * 37);

    expect(fakeEventEmitter.emit).not.toHaveBeenCalled();

    tick(16);

    expect(fakeEventEmitter.emit).toHaveBeenCalled();
  }));

  it('should call event emitter when start and end point are same', fakeAsync(() => {
    const fakeEventEmitter = {emit: jasmine.createSpy()} as any;
    const fakeContainerEl = {scrollTop: 1000} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1000,
      vertical: true,
      duration: 600,
      scrollFinishedEmitter: fakeEventEmitter
    };

    service.scrollTo(scrollOptions);

    expect(fakeEventEmitter.emit).toHaveBeenCalled();
    expect(window.requestAnimationFrame).not.toHaveBeenCalled();
  }));

  it('should scroll with offset', fakeAsync(() => {
    const fakeContainerEl = {scrollTop: 1000} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1500,
      vertical: true,
      duration: 600,
      offset: 23
    };

    service.scrollTo(scrollOptions);
    tick(16 * 38);

    expect(fakeContainerEl.scrollTop).toEqual(1523);
  }));

  it('should log error when can not get container element', fakeAsync(() => {
    spyOn(document, 'getElementById').and.returnValue(null);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    spyOn(console, 'error');
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1500,
      vertical: true,
      duration: 600,
      offset: 23
    };

    service.scrollTo(scrollOptions);
    tick(16 * 25);

    expect(console.error).toHaveBeenCalledWith('Can not get container element!');
    expect(window.requestAnimationFrame).not.toHaveBeenCalled();
  }));

  it('should scroll vertically to certain point (scrollTo)', fakeAsync(() => {
    const fakeContainerEl = {scrollTop: 1000} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1500,
      vertical: true,
      duration: 600
    };

    service.scrollTo(scrollOptions);
    tick(16 * 38); // eg. 1551691487637(start time) - 1551691487621(current time) = 16, 16/600 = 0.026, 0.026 * 38 = ~1 - animation finishes

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollTop).toEqual(1500);
  }));

  it('should scroll horizontally to certain point (scrollTo)', fakeAsync(() => {
    const fakeContainerEl = {scrollLeft: 750} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      to: 1850,
      vertical: false,
      duration: 600
    };

    service.scrollTo(scrollOptions);
    tick(16 * 38);

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollLeft).toEqual(1850);
  }));

  it('should scroll vertically to element (scrollToElement)', fakeAsync(() => {
    const fakeContainerEl = {scrollTop: 0} as any;
    const fakeTargetEl = {offsetTop: 500} as any;
    spyOn(document, 'getElementById').and.callFake(id => id === 'container' ? fakeContainerEl : fakeTargetEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: 'container',
      target: 'target',
      vertical: true,
      duration: 600
    };

    service.scrollToElement(scrollOptions);
    tick(16 * 38);

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollTop).toEqual(500);
  }));

  it('should scroll horizontally to element (scrollToElement)', fakeAsync(() => {
    const fakeContainerEl = {scrollLeft: 750} as any;
    const fakeTargetEl = {offsetLeft: 1000} as any;
    spyOn(document, 'getElementById').and.callFake(id => id === 'container' ? fakeContainerEl : fakeTargetEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: 'container',
      target: 'target',
      vertical: false,
      duration: 600
    };

    service.scrollToElement(scrollOptions);
    tick(16 * 38);

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollLeft).toEqual(1000);
  }));

  it('should scroll vertically by some px (scrollBy)', fakeAsync(() => {
    const fakeContainerEl = {scrollTop: 1000} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      by: 325,
      vertical: true,
      duration: 600
    };

    service.scrollBy(scrollOptions);
    tick(16 * 38);

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollTop).toEqual(1325);
  }));

  it('should scroll horizontally by some px (scrollBy)', fakeAsync(() => {
    const fakeContainerEl = {scrollLeft: 750} as any;
    spyOn(document, 'getElementById').and.returnValue(fakeContainerEl);
    spyOn(window, 'requestAnimationFrame').and.callThrough();
    const scrollOptions: ScrollOptions = {
      container: '',
      by: 475,
      vertical: false,
      duration: 600
    };

    service.scrollBy(scrollOptions);
    tick(16 * 38);

    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(38);
    expect(fakeContainerEl.scrollLeft).toEqual(1225);
  }));
});
