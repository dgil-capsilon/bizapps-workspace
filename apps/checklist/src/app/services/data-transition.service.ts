import {Injectable} from '@angular/core';
import {ApplicantRuleApi, FolderDataApi, TimelineRule} from '@models/data-api.interface';
import {DocStatus, DocTypes, EmployerType, EmploymentTypes, RuleCode, RuleStatus, ViewType} from '@models/data.enum';
import {
  DataValidationGroup,
  Doc,
  Employment,
  EmploymentDataValidation,
  Rule,
  TimelineData,
  TimelineEmploymentDetails,
  TrendingData,
  ValidationData,
  ValidationEmploymentDetails
} from '@models/data.interface';
import {Severity} from '@sentry/types';
import {LoggerService} from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class DataTransitionService {

  constructor(private loggerService: LoggerService) {
  }

  public adjustRuleData(rule: Rule) {
    if (!rule || !rule.data) {
      return;
    }
    this.setViewType(rule);

    this.timelineTransition(rule);
    this.validationTransition(rule);
    this.trendingTransition(rule);
    this.mergingTransition(rule);

    return rule;
  }

  public adjustFolderData(folderData: FolderDataApi) {
    if (!folderData.applicants) {
      this.loggerService.captureMessage('Missing response field - "applicants"', folderData, Severity.Warning);
      return;
    }

    this.sortFolderRules(folderData);
    this.assignNegativeIdForRulesWithoutId(folderData);

    return folderData;
  }

  private sortFolderRules(folderData: FolderDataApi): void {
    folderData.applicants.forEach(applicant => {
      applicant.rules.sort((a: ApplicantRuleApi, b: ApplicantRuleApi) => {
        if (a.status === b.status) {
          return 0;
        }

        if (a.status === RuleStatus.Failed) {
          return -1;
        }

        if (b.status === RuleStatus.Failed) {
          return 1;
        }

        if (a.status === RuleStatus.Waived) {
          return -1;
        }

        if (b.status === RuleStatus.Waived) {
          return 1;
        }

        if (a.status === RuleStatus.Passed) {
          return -1;
        }

        if (b.status === RuleStatus.Passed) {
          return 1;
        }

        return 0;
      });
    });
  }

  private setViewType(ruleData: Rule): void {
    if (ruleData.viewType) {
      return;
    }

    const haveValidationItems = ruleData.data.validationItems && ruleData.data.validationItems.length > 0;
    const isMeringView = ruleData.data.employmentValidationItems && ruleData.data.employmentValidationItems.length > 0;
    if (haveValidationItems) {
      ruleData.viewType = ViewType.Trending;
    } else if (isMeringView) {
      ruleData.viewType = ViewType.Merging;
    } else {
      const employmentDetails = ruleData.data.employmentDetails;
      const haveEmploymentDetails = employmentDetails && employmentDetails.length > 0;
      if (haveEmploymentDetails) {
        for (const employment of employmentDetails) {
          if (ruleData.viewType != null) {
            break;
          }
          const isTimelineView = employment.documents && employment.documents.length > 0 || employment.type === EmploymentTypes.Gap;

          const dataValidation = ruleData.data.loanTypeDetails ? ruleData.data.loanTypeDetails.dataValidation : employment.dataValidation;
          const isDataValidationView = !isTimelineView && dataValidation && dataValidation.length > 0;
          if (isTimelineView) {
            ruleData.viewType = ViewType.Timeline;
          } else if (isDataValidationView) {
            ruleData.viewType = ViewType.Validation;
          }
        }
      }
    }

    if (ruleData.viewType == null) {
      console.error(`Not supported view type for ${ruleData}!`);
    }

  }

  private timelineTransition(ruleData: Rule): void {
    if (this.isTimeline(ruleData)) {
      this.extendGaps(ruleData);
      this.extendEmploymentsAndDocuments(ruleData);
      this.extendEmploymentsWithMissingDocInfo(ruleData);
      this.extendEmploymentsWithScrollIndices(ruleData);
      this.mergeW2AndPSDocuments(ruleData);
      this.parseStringToDates(ruleData);
      this.removeEmployments(ruleData);
      this.removeMissingDocuments(ruleData);
      this.sortEmployments(ruleData);
      this.sortDocuments(ruleData);
      this.extractDynamicSnippetData(ruleData);
      this.addLineIndex(ruleData);
      this.snippetUrlToArray(ruleData);
      this.extendLoanTypeDetailsWithGroups(ruleData);
      this.extendsDocsWithOverlay(ruleData);
    }
  }

  private validationTransition(ruleData: any): void {
    if (this.isValidation(ruleData)) {
      ruleData.data.employmentDetails.forEach((employment: ValidationEmploymentDetails) => {
        if (employment.dataValidation) {
          employment.groups = this.extendEmploymentsWithValidationGroups(employment.dataValidation);
        }
      });
      this.parseStringToDates(ruleData);
      this.removeEmployments(ruleData);
      this.sortEmployments(ruleData);
      // this.sortDocuments(ruleData); // TODO: Probably not needed
      this.extractDynamicSnippetData(ruleData);
      this.extendLoanTypeDetailsWithGroups(ruleData);
    }
  }

  private trendingTransition(ruleData: Rule): void {
    if (this.isTrending(ruleData)) {
    }
  }

  private mergingTransition(ruleData: Rule): void {
    if (this.isMerging(ruleData)) {
      this.sortLoanAppEmployers(ruleData);
      this.groupMergingEmployers(ruleData);
    }
  }

  private isTimeline(rule: Rule): rule is TimelineData {
    return rule.viewType === ViewType.Timeline;
  }

  public isTrending(rule: Rule): rule is TrendingData {
    return rule.viewType === ViewType.Trending;
  }

  private isValidation(rule: Rule): rule is ValidationData {
    return rule.viewType === ViewType.Validation;
  }

  private isMerging(ruleData: Rule): boolean {
    return ruleData.viewType === ViewType.Merging;
  }

  /**
   * This is valid for case when startDate is greater than endDate
   * Eg.: startDate: 2016-05-25, endDate: 2016-05-22
   */
  private removeEmployments(ruleData: Rule) {
    ruleData.data.employmentDetails = ruleData.data.employmentDetails.filter(employment => employment.startDate < employment.endDate);
  }

  /**
   * This is valid for one case:
   * Rule 1 - gaps in first rule are not really gaps but indicators that there are no 2 years employment history so
   * we do need to modify them to get custom header and error message in snippets instead of gap's one
   */
  private extendGaps(ruleData: Rule) {
    if (ruleData.ruleCode === RuleCode.INC010) {
      ruleData.data.employmentDetails.forEach(employment => {
        if (employment.type === EmploymentTypes.Gap) {
          employment.error = 'Missing Complete 2 Year Employment History';
        }
      });
    }
  }

  /**
   * Since we does not receive indices for employments (gaps too) nor documents we do need to add them on our side
   * It's mainly required for HTML elements (id) and afterwards for scrolling
   */
  private extendEmploymentsAndDocuments(ruleData: Rule) {
    let documentIndex = 0;
    ruleData.data.employmentDetails.forEach((employment: TimelineEmploymentDetails, index: number) => {
      employment.id = index;
      if (employment.documents) {
        employment.documents.forEach((document: Doc) => {
          document.id = documentIndex++;
        });
      }
    });
  }

  /**
   * Checks if certain employment have missing documents was used in several places
   * so we decided to specify that in one place and use across app
   */
  private extendEmploymentsWithMissingDocInfo(ruleData: Rule) {
    ruleData.data.employmentDetails.forEach((employment: TimelineEmploymentDetails) => {
      const haveDocs = !!employment.documents;
      employment.missingDocsAmount = haveDocs ? employment.documents.filter(doc => doc.status === DocStatus.Missing).length : 0;
    });
  }

  /**
   * If there is missing W2 and PayStub we will merge them into one document
   * those single W2 and PayStub documents will be removed
   */
  private mergeW2AndPSDocuments(ruleData: TimelineRule) {
    if (ruleData.ruleCode !== RuleCode.INC040) {
      return;
    }
    ruleData.data.employmentDetails.forEach(employment => {
      if (employment.documents) {
        const docArrays = employment.documents.reduce((arr, doc) => {
          if ((doc.type === DocTypes.W_2 || doc.type === DocTypes.PAYSTUB) && doc.status === DocStatus.Missing) {
            arr[0].push(doc);
          } else {
            arr[1].push(doc);
          }
          return arr;
        }, [[], []]);

        const containsPS = docArrays[0].find((item: Doc) => item.type === DocTypes.PAYSTUB);
        const containsW2 = docArrays[0].find((item: Doc) => item.type === DocTypes.W_2);
        if (docArrays[0].length > 1 && containsPS && containsW2) {
          const doc: Partial<Doc> = { // TODO: Check if Partial is fine
            startDate: docArrays[0][0].startDate,
            endDate: docArrays[0][docArrays[0].length - 1].endDate,
            status: DocStatus.Missing,
            type: DocTypes.W_2_PAYSTUB,
            id: docArrays[0][0].id,
            durationInDays: docArrays[0][0].durationInDays + docArrays[0][docArrays[0].length - 1].durationInDays,
            group: docArrays[0][0].group,
            spScrollId: docArrays[0][0].spScrollId,
            tlScrollId: docArrays[0][0].tlScrollId,
            snippetUrls: []
          };

          if (employment.statusType === EmployerType.Previous && doc.endDate > employment.endDate) {
            doc.endDate = employment.endDate;
          }

          docArrays[0].forEach((item) => {
            if (employment.documents.indexOf(item) >= 0) {
              employment.documents.splice(employment.documents.indexOf(item), 1);
            }
          });

          employment.documents.push(doc);
        }
      }
    });
  }

  /**
   * Sorting documents in way Loan App => VOE/EVOE => PS or W2.
   */
  private sortDocuments(ruleData: Rule): void {
    ruleData.data.employmentDetails.forEach((employment: TimelineEmploymentDetails) => {
      const docs = employment.documents;
      docs.sort((a, b) => {
        if (a.status === DocStatus.Valid && b.status === DocStatus.Missing) {
          return -1;
        }
        if (a.status === DocStatus.Missing && b.status === DocStatus.Valid) {
          return 1;
        }

        if (a.status === DocStatus.NotEvaluated && b.status === DocStatus.Missing) {
          return -1;
        }
        if (a.status === DocStatus.Missing && b.status === DocStatus.NotEvaluated) {
          return 1;
        }
        return 0;
      });

      docs.sort((a, b) => {
        if (a.type === DocTypes.EVOE && b.type === DocTypes.VOE) {
          return b.endDate.getTime() - a.endDate.getTime();
        }

        if (a.type === DocTypes.VOE && b.type === DocTypes.EVOE) {
          return b.endDate.getTime() - a.endDate.getTime();
        }

        if (a.type === DocTypes.URLA && b.type !== DocTypes.URLA) {
          return -1;
        }
        if (a.type !== DocTypes.URLA && b.type === DocTypes.URLA) {
          return 1;
        }
        if (a.type === DocTypes.VOE && b.type !== DocTypes.URLA && b.type !== DocTypes.EVOE) {
          return -1;
        }
        if (a.type === DocTypes.EVOE && b.type !== DocTypes.URLA && b.type !== DocTypes.VOE) {
          return -1;
        }

        if (b.type === DocTypes.VOE && a.type !== DocTypes.URLA && a.type !== DocTypes.EVOE) {
          return 1;
        }
        if (b.type === DocTypes.EVOE && a.type !== DocTypes.URLA && a.type !== DocTypes.VOE) {
          return 1;
        }

        if ((a.type !== DocTypes.URLA && a.type !== DocTypes.VOE) && b.type === DocTypes.VOE) {
          return 1;
        }
        return 0;
      });

      employment.orId = this.findOrId(<Doc[]>docs); // TODO: Check why cast is needed
    });

  }

  /**
   * Finds element id where should be displayed 'OR' in timeline.
   */
  private findOrId(docs: Doc[]): number {
    const id = docs.findIndex((doc: Doc) => doc.type === DocTypes.W_2_PAYSTUB);
    if (id > 0) {
      return id;
    }
    const missingVOE = docs.find((doc: Doc) => doc.type === DocTypes.VOE && doc.status === DocStatus.Missing);
    if (!missingVOE) {
      return -1;
    }
    const w2 = docs.filter((doc) => doc.type === DocTypes.W_2);
    const properW2 = w2.reduce((previousValue: Doc, currentValue: Doc) => {
      if (previousValue === null) {
        return currentValue;
      }
      return new Date(previousValue.startDate).getTime() > new Date(currentValue.startDate).getTime() ? previousValue : currentValue;
    }, null as any);
    return docs.indexOf(properW2);
  }

  /**
   * Purpose of scroll id's to make navigation (scrolling) to certain element on timeline or snippets easier,
   * from beginning we already know which snippet is connected to which element on timeline and vice versa
   */
  private extendEmploymentsWithScrollIndices(ruleData: Rule) {
    ruleData.data.employmentDetails.forEach((employment: Employment) => {
      if (employment.type === EmploymentTypes.Gap) {
        employment.tlScrollId = `tl-gap-${employment.id}`;
      } else {
        employment.tlScrollId = `tl-empl-${employment.id}`;
      }

      employment.spScrollId = `sp-header-${employment.id}`;

      if (employment.documents) {
        employment.documents.forEach((document: Doc) => {
          document.tlScrollId = `tl-doc-${document.id}`;

          if (document.status === DocStatus.Missing) {
            document.spScrollId = `sp-missing-doc-${employment.id}`;
          } else {
            document.spScrollId = `sp-doc-${document.id}`;
          }
        });
      }
    });
  }

  /**
   * Changes all strings treated as a Date into Date object.
   */
  private parseStringToDates(ruleData: Rule) {
    ruleData.data.referenceDate = new Date(ruleData.data.referenceDate);
    ruleData.data.employmentDetails.forEach((item: Employment) => {
      this.stringToDate(item);
      item.documents.forEach((element) => {
        this.stringToDate(element);
      });
    });

    const loanTypeDetails = ruleData.data.loanTypeDetails;
    if (loanTypeDetails) {
      ruleData.data.loanTypeDetails.timelineMarkers = loanTypeDetails.timelineMarkers
        .map(timelineMarker => ({...timelineMarker, markerPosition: new Date(timelineMarker.markerPosition)}));
    }
  }

  private stringToDate(item: Employment | Doc): void {
    if (item.startDate) {
      item.startDate = new Date(item.startDate);
    }
    if (item.endDate) {
      item.endDate = new Date(item.endDate);
    }
  }

  private groupMergingEmployers(ruleData: Rule) {
    const getReferenceOrFieldValue = validationItem => validationItem.referenceValue || validationItem.fieldValue;
    const groups = new Map<string, any>(ruleData.data.employmentValidationItems.map(e => [getReferenceOrFieldValue(e), {data: []}]));

    ruleData.data.employmentValidationItems.forEach(validationItem => {
      const keyValue = getReferenceOrFieldValue(validationItem);
      const group = groups.get(keyValue);

      if (!group.first) {
        group.first = validationItem;
      }

      group.data.push(validationItem);
    });

    ruleData.data.groups = groups;
  }

  private extendEmploymentsWithValidationGroups(dataValidation: EmploymentDataValidation[],
                                                groupBy = 'group',
                                                groups = new Map<string, any>()): Map<string, DataValidationGroup> {
    dataValidation.forEach(data => {
      const group = groups.get(data[groupBy]);
      if (group) {
        group.data.push(data);
      } else {
        if (data[groupBy]) {
          groups.set(data[groupBy], {data: [data], first: data});
        } else {
          groups.set(groups.size.toString(), {data: [data], first: data});
        }
      }
    });
    return groups;

  }

  private sortEmployments(ruleData: Rule): void {
    ruleData.data.employmentDetails.sort((a: Employment, b: Employment) => {
      if (a.primary && !b.primary) {
        return -1;
      }
      if (!a.primary && b.primary) {
        return 1;
      }
      if (a.endDate > b.endDate) {
        return -1;
      }
      if (a.endDate < b.endDate) {
        return 1;
      }
      if (a.startDate < b.startDate) {
        return -1;
      }
      if (a.startDate > b.startDate) {
        return 1;
      }

      return 0;
    });
  }

  private extractDynamicSnippetData(ruleData: Rule) {
    ruleData.data.employmentDetails.forEach(employment => {
      employment.documents.forEach((doc: Doc) => {
        if (doc.type === DocTypes.URLA) {
          doc.dynamicSnippetData = {
            monthsOnJob: employment.employmentMonthsOnJobCount,
            yearsOnJob: employment.employmentYearsOnJobCount,
            positionDescription: employment.employmentPositionDescription,
            phoneNumber: employment.phoneNumber,
            address: employment.address,
            startDate: doc.startDate,
            endDate: doc.endDate,
            employerName: doc.employerName,
            primary: employment.primary,
            current: doc.current,
            snippetDefinition: doc.snippetDefinition
          };

          if (ruleData.ruleCode === RuleCode.INC1110) {
            doc.dynamicSnippetData.employmentTimeInLineOfWorkYearsCount = employment.employmentTimeInLineOfWorkYearsCount || '';
          }
        }
      });
    });
  }

  /**
   * Sorts Loan app employers depending on type. INC010 Primary -> Secondary -> Previous -> alphabetic.
   */
  private sortLoanAppEmployers(ruleData: Rule): void {
    ruleData.data.loanAppEmployers.sort((a, b) => {
      if (a.type === EmployerType.Primary) {
        return -1;
      }
      if (b.type === EmployerType.Primary) {
        return 1;
      }
      if (a.type === EmployerType.Secondary && b.type === EmployerType.Previous) {
        return -1;
      }
      if (a.type === EmployerType.Previous && b.type === EmployerType.Secondary) {
        return 1;
      }
      if (a.type === EmployerType.Previous && b.type === EmployerType.Previous) {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      }
      return 0;
    });
  }

  /**
   * Changes string snippet url into array of strings.
   */
  private snippetUrlToArray(ruleData: TimelineData): void {
    ruleData.data.employmentDetails.forEach(employment => employment.documents.forEach(doc => {
      if (doc.snippetUrl) {
        doc.snippetUrls = [doc.snippetUrl];
      }
    }));
  }

  /**
   * Adding line index to documents. Setting also if additional row is needed.
   * Fixes problem with wrong groups sent by backend.
   */
  private addLineIndex(ruleData: Rule): void {
    ruleData.data.employmentDetails.forEach(employment => this.setLinesToDocs(employment));
  }

  private setLinesToDocs(employment: TimelineEmploymentDetails): void {
    // Loan Application - 1 line
    this.setLineToSingleDoc(employment, DocTypes.URLA, 0);

    // VOE - 2 line
    let voe = this.setLineToSingleDoc(employment, DocTypes.VOE, 1);

    // Electronic VOE
    // 2 line - voe not available
    // 3 line - voe available
    const eVoeLineIndex = voe ? 2 : 1;
    let eVoe = this.setLineToSingleDoc(employment, DocTypes.EVOE, eVoeLineIndex);

    // Line index "visually sort" documents on timeline, another check is needed here beside one in sorting
    if (voe && eVoe && voe.endDate < eVoe.endDate) {
      voe = this.setLineToSingleDoc(employment, DocTypes.VOE, 2);
      eVoe = this.setLineToSingleDoc(employment, DocTypes.EVOE, 1);
    }

    // Merged missing W_2 and missing Paystub to one document
    const W2PS: Doc[] = employment.documents.filter(doc => doc.type === DocTypes.W_2_PAYSTUB);

    const PS: Doc[] = employment.documents.filter(doc => doc.type === DocTypes.PAYSTUB);
    const W2: Doc[] = employment.documents.filter(doc => doc.type === DocTypes.W_2);

    const psCollidingWithW2 = this.getCollidingElementsFromFirstParam(W2, PS).length > 0;
    const psCollidingWithMerged = this.getCollidingElementsFromFirstParam(PS, W2PS);
    const isMergedCollidingWithPS = psCollidingWithMerged.length > 0;

    let startIndex = 2; // lineIndex available from 2, VOE or EVOE not available
    if (voe && eVoe) { // lineIndex available from 3
      startIndex = 3;
    } else if (!voe && !eVoe) { // LineIndex available from 1
      startIndex = 1;
    }

    if (W2PS.length && !PS.length) { // W2PS exist, PS not exist
      this.setLineToGroups([W2PS], startIndex);

      if (startIndex === 3) {
        employment.additionalRows = 1;
      }
    } else if (!W2PS.length && PS.length) { // W2PS not exist, PS exist
      this.setLineToGroups([W2, PS], startIndex);

      if (startIndex === 3) {
        employment.additionalRows = 1;
      }

      if (psCollidingWithW2) {
        this.setLineToGroups([PS], ++startIndex);

        if (startIndex === 3) {
          employment.additionalRows = 1;
        }

        if (startIndex === 4) {
          employment.additionalRows = 2;
        }
      }
    } else if (W2PS.length && PS.length) { // W2PS exist, PS exist
      this.setLineToGroups([W2PS], startIndex);
      this.setLineToGroups([PS], startIndex);

      if (startIndex === 3) {
        employment.additionalRows = 1;
      }

      if (isMergedCollidingWithPS) {
        this.setLineToGroups([PS], ++startIndex);

        if (startIndex === 3) {
          employment.additionalRows = 1;
        }
      }
    } else { // W2PS not exist, PS not exists
      this.setLineToGroups([W2], startIndex);
    }
  }

  private setLineToSingleDoc(employment: TimelineEmploymentDetails, type, index): Doc {
    const urla = employment.documents.find(doc => doc.type === type);
    if (urla) {
      urla.lineIndex = index;
    }
    return urla;
  }

  private setLineToGroups(groupOfDocs: Array<Doc[]>, index): void {
    groupOfDocs.forEach(group => this.setLineIndex(group, index));
  }

  private setLineIndex(docs: Doc[], index): void {
    docs.forEach((doc: Doc) => {
        if (doc) {
          doc.lineIndex = index;
        }
      }
    );
  }

  private getCollidingElementsFromFirstParam(docsToGet: Doc[], docsToCompareWith: Doc[]): Doc[] {
    const collidingElements: Doc[] = [];
    for (const doc1 of docsToGet) {
      for (const doc2 of docsToCompareWith) {
        if (this.areColliding(doc1, doc2)) {
          collidingElements.push(doc1);
        }
      }
    }
    return collidingElements;
  }

  private areColliding(prevDoc: Doc, nextDoc: Doc): boolean {
    return (prevDoc.endDate >= nextDoc.startDate && prevDoc.endDate <= nextDoc.endDate)
      || (prevDoc.startDate <= nextDoc.endDate && prevDoc.startDate >= nextDoc.startDate) ||
      (prevDoc.startDate < nextDoc.startDate && prevDoc.endDate > nextDoc.endDate);
  }

  /**
   * TODO: Temporary workaround till backend change
   * There is no need to show both VOE and Electron VOE as missing docs since (E)VOE can represent both
   */
  private removeMissingDocuments(ruleData: TimelineData) {
    ruleData.data.employmentDetails.forEach(employment => {
      employment.documents = employment.documents.filter(doc => !(doc.status === DocStatus.Missing && doc.type === DocTypes.EVOE));
    });
  }

  private assignNegativeIdForRulesWithoutId(folderData: FolderDataApi) {
    let negativeId = -1;

    folderData.applicants.forEach(applicant => {
      applicant.rules.filter(rule => rule.id == null).forEach(rule => rule.id = negativeId--);
    });
  }

  private extendLoanTypeDetailsWithGroups(ruleData: TimelineData | ValidationData): void {
    const loanTypeDetails = ruleData.data.loanTypeDetails;

    if (loanTypeDetails) {
      loanTypeDetails.groups = this.extendEmploymentsWithValidationGroups(loanTypeDetails.dataValidation);
    }
  }

  private extendsDocsWithOverlay(ruleData: Rule) {
    // INC-1100
    if (ruleData.status !== RuleStatus.Failed || !ruleData.data.loanTypeDetails) {
      return;
    }

    ruleData.data.employmentDetails.forEach(employment => {
      if (employment.type === EmploymentTypes.Employment && employment.primary) {
        employment.documents.forEach(doc => {
          if (doc.type === DocTypes.URLA) {

            // Find correct marker by label - start marker (start date)
            const startMarker = ruleData.data.loanTypeDetails.timelineMarkers
              .find(timelineMarker => timelineMarker.label.includes('6 months prior'));
            // If rule fails then overlay should end at gap end
            const gap = ruleData.data.employmentDetails.find(emp => emp.type === EmploymentTypes.Gap);
            const startMarkerDate = startMarker.markerPosition as Date;

            doc.startDate = startMarkerDate;
            doc.overlay = {
              startDate: startMarkerDate,
              endDate: gap.endDate
            };
          }
        });
      }
    });
  }
}
