import {TestBed} from '@angular/core/testing';
import {Severity} from '@sentry/types';
import {DocStatus, DocTypes, EmployerType, EmploymentTypes, LoanStatus, RuleCode, RuleStatus, ViewType} from '../models/enums';
import {DataTransitionService} from './data-transition.service';
import {LoggerService} from './logger.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('DataTransitionService', () => {
  let service: DataTransitionService;
  let ruleData;
  const loggerServiceSpy: SpyObj<LoggerService> = createSpyObj('LoggerService', ['captureMessage']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerServiceSpy}
      ]
    });

    service = TestBed.get(DataTransitionService);
    ruleData = {
      data: {
        employmentDetails: [
          {
            endDate: '2019-01-31',
            startDate: '2015-12-31',
            type: EmploymentTypes.Employment
          }
        ]
      }
    } as any;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make one document from w2 and paystub', () => {
    ruleData.ruleCode = RuleCode.INC040;
    ruleData.data.employmentDetails = [
      {
        type: EmploymentTypes.Employment,
        endDate: '2019-01-31',
        startDate: '2015-12-31',
        documents: [
          {
            durationInDays: 1128,
            employerName: 'Ford Motors2',
            endDate: '2019-01-31',
            group: 'URLA',
            startDate: '2015-12-31',
            status: 'VALID',
            type: 'URLA'
          },
          {
            durationInDays: 365,
            employerName: 'Ford Motors2',
            endDate: '2018-12-31',
            group: 'PAYSTUB-W_2',
            startDate: '2018-01-01',
            status: DocStatus.Missing,
            type: 'W_2'
          },
          {
            durationInDays: 31,
            endDate: '2019-01-31',
            group: 'PAYSTUB-W_2',
            snippetUrls: [],
            startDate: '2019-01-01',
            status: DocStatus.Missing,
            type: 'PAYSTUB'
          }
        ]
      }
    ];

    const adjustedRuleData = service.adjustRuleData(ruleData);

    expect(adjustedRuleData.data.employmentDetails[0].documents.length).toEqual(2);
    expect(adjustedRuleData.data.employmentDetails[0].documents[1]).toEqual(jasmine.objectContaining({
      startDate: new Date('2018-01-01'),
      endDate: new Date('2019-01-31'),
      status: DocStatus.Missing,
      type: DocTypes.W_2_PAYSTUB,
      id: 1,
      durationInDays: 396,
      group: 'PAYSTUB-W_2',
      snippetUrls: [],
      spScrollId: 'sp-missing-doc-0',
      tlScrollId: 'tl-doc-1'
    }));
  });

  it('should align endDate of w2ps doc if its greater than employment (for previous job)', () => {
    ruleData.ruleCode = RuleCode.INC040;
    ruleData.data.employmentDetails = [
      {
        type: EmploymentTypes.Employment,
        endDate: '2018-03-15',
        startDate: '2016-09-01',
        statusType: EmployerType.Previous,
        documents: [
          {
            durationInDays: 1128,
            employerName: 'Ford Motors2',
            endDate: '2018-03-15',
            group: 'URLA',
            startDate: '2016-09-01',
            status: 'VALID',
            type: 'URLA'
          },
          {
            durationInDays: 365,
            employerName: 'Ford Motors2',
            endDate: '2018-03-15',
            group: 'PAYSTUB-W_2',
            startDate: '2018-01-01',
            status: DocStatus.Missing,
            type: 'W_2'
          },
          {
            durationInDays: 31,
            endDate: '2018-12-31',
            group: 'PAYSTUB-W_2',
            snippetUrls: [],
            startDate: '2018-12-18',
            status: DocStatus.Missing,
            type: 'PAYSTUB'
          }
        ]
      }
    ];

    const adjustedRuleData = service.adjustRuleData(ruleData);

    expect(adjustedRuleData.data.employmentDetails[0].documents[1].endDate).toEqual(new Date('2018-03-15'));
  });

  it('should all dates be an instance of Date', () => {
    ruleData = {
      data: {
        referenceDate: '2019-01-31',
        employmentDetails: [
          {
            endDate: '2019-01-31', startDate: '2015-12-31',
            documents: [
              {endDate: '2019-01-31', startDate: '2015-12-31'},
              {endDate: '2018-12-31', startDate: '2018-01-01'}
            ]
          }
        ]
      }
    } as any;

    service.adjustRuleData(ruleData);

    expect(ruleData.data.referenceDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].endDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].startDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].documents[0].startDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].documents[0].endDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].documents[1].startDate instanceof Date).toBeTruthy();
    expect(ruleData.data.employmentDetails[0].documents[1].endDate instanceof Date).toBeTruthy();
  });

  it('should create groups for data with validations', () => {
    ruleData = {
      data: {
        referenceDate: '2019-01-31',
        employmentDetails: [
          {
            endDate: '2019-01-31', startDate: '2015-12-31',
            documents: [],
            dataValidation: [
              {group: 'Paystub'},
              {group: 'VOE'}
            ]
          },
          {
            endDate: '2019-01-31', startDate: '2015-12-31',
            documents: [],
            dataValidation: [
              {group: 'VOE'},
              {group: 'VOE'}
            ]
          }
        ]
      }
    } as any;

    service.adjustRuleData(ruleData);

    expect(ruleData.data.employmentDetails[0].groups.size).toEqual(2);
    expect(ruleData.data.employmentDetails[0].groups.get('Paystub')).toBeDefined();
    expect(ruleData.data.employmentDetails[0].groups.get('Paystub').data.length).toEqual(1);
    expect(ruleData.data.employmentDetails[0].groups.get('VOE')).toBeDefined();
    expect(ruleData.data.employmentDetails[0].groups.get('VOE').data.length).toEqual(1);

    expect(ruleData.data.employmentDetails[1].groups.size).toEqual(1);
    expect(ruleData.data.employmentDetails[1].groups.get('VOE')).toBeDefined();
    expect(ruleData.data.employmentDetails[1].groups.get('VOE').data.length).toEqual(2);
  });

  it('should create groups for merging data with and without referenceValue', () => {
    const rule = {
      data: {
        viewType: ViewType.Merging,
        loanAppEmployers: [],
        employmentValidationItems: [
          {
            fieldValue: 'Awesome Computers'
          },
          {
            fieldValue: 'Awesome Computers'
          },
          {
            fieldValue: 'Awesome Computers'
          },
          {
            fieldValue: 'Baldwin',
            referenceValue: 'Baldwin'
          },
          {
            fieldValue: 'Museum',
            referenceValue: 'Baldwin'
          }
        ]
      }
    } as any;

    const groupedData = service.adjustRuleData(rule);

    expect(groupedData.data.groups.has('Awesome Computers')).toBeTruthy();
    expect(groupedData.data.groups.get('Awesome Computers').data.length).toBe(3);
    expect(groupedData.data.groups.has('Baldwin')).toBeTruthy();
    expect(groupedData.data.groups.get('Baldwin').data.length).toBe(2);
  });

  describe('sorting documents', () => {
    it('should sort documents in proper way', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {type: DocTypes.VOE, status: DocStatus.Valid},
        {type: DocTypes.PAYSTUB, status: DocStatus.Valid},
        {type: DocTypes.W_2, status: DocStatus.Valid},
        {type: DocTypes.URLA, status: DocStatus.Valid}
      ];


      const adjustedRuleData = service.adjustRuleData(ruleData);
      expect(employment.documents[0].type).toBe(DocTypes.URLA);
      expect(employment.documents[1].type).toBe(DocTypes.VOE);
      expect(employment.documents[2].type).toBe(DocTypes.PAYSTUB);
      expect(employment.documents[3].type).toBe(DocTypes.W_2);
    });

    it('should sort properly documents', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {type: DocTypes.URLA, status: DocStatus.Valid},
        {type: DocTypes.W_2, status: DocStatus.Valid},
        {type: DocTypes.W_2, status: DocStatus.Valid},
        {type: DocTypes.VOE, status: DocStatus.Valid},
        {type: DocTypes.PAYSTUB, status: DocStatus.Valid}
      ];
      const adjustedRuleData = service.adjustRuleData(ruleData);
      expect(employment.documents[0].type).toBe(DocTypes.URLA);
      expect(employment.documents[1].type).toBe(DocTypes.VOE);
      expect(employment.documents[2].type).toBe(DocTypes.W_2);
      expect(employment.documents[3].type).toBe(DocTypes.W_2);
      expect(employment.documents[4].type).toBe(DocTypes.PAYSTUB);
    });

    it('should sort documents first Loan valid, VOE valid, PS/W2', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {type: DocTypes.URLA, status: DocStatus.Valid},
        {type: DocTypes.W_2, status: DocStatus.Valid},
        {type: DocTypes.W_2, status: DocStatus.Missing},
        {type: DocTypes.VOE, status: DocStatus.Missing},
        {type: DocTypes.PAYSTUB, status: DocStatus.Missing}
      ];
      const adjustedRuleData = service.adjustRuleData(ruleData);
      expect(employment.documents[0].type).toBe(DocTypes.URLA);
      expect(employment.documents[1].type).toBe(DocTypes.VOE);
      expect(employment.documents[2]).toEqual(jasmine.objectContaining({type: DocTypes.W_2, status: DocStatus.Valid}));
      expect(employment.documents[3]).toEqual(jasmine.objectContaining({type: DocTypes.W_2, status: DocStatus.Missing}));
      expect(employment.documents[4].type).toBe(DocTypes.PAYSTUB);
    });

    it('should sort documents in order URLA -> VOE -> EVOE -> W2 -> PS (fix for CHK-976)', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [{
        'type': DocTypes.URLA,
        'status': 'VALID',
        'startDate': '2014-09-01',
        'endDate': '2019-09-02',
      }, {
        'type': DocTypes.W_2,
        'status': 'VALID',
        'startDate': '2018-10-02',
        'endDate': '2018-12-31',
      }, {
        'type': DocTypes.VOE,
        'status': 'VALID',
        'startDate': '2018-10-02',
        'endDate': '2019-08-28',
      }, {
        'type': DocTypes.EVOE,
        'status': 'VALID',
        'startDate': '2018-11-21',
        'endDate': '2019-08-18',
      }, {
        'type': DocTypes.PAYSTUB,
        'status': 'MISSING',
        'startDate': '2018-12-18',
        'endDate': '2018-12-31',
      }, {
        'type': DocTypes.PAYSTUB,
        'status': 'NOT_EVALUATED',
        'startDate': '2019-05-26',
        'endDate': '2019-06-07',
      }, {
        'type': DocTypes.PAYSTUB,
        'status': 'NOT_EVALUATED',
        'startDate': '2019-01-16',
        'endDate': '2019-01-28',
      }];


      const adjustedRuleData = service.adjustRuleData(ruleData);
      expect(employment.documents[0].type).toBe(DocTypes.URLA);
      expect(employment.documents[1].type).toBe(DocTypes.VOE);
      expect(employment.documents[2].type).toBe(DocTypes.EVOE);
      expect(employment.documents[3]).toEqual(jasmine.objectContaining({type: DocTypes.W_2, status: DocStatus.Valid}));
      expect(employment.documents[4]).toEqual(jasmine.objectContaining({type: DocTypes.PAYSTUB, status: DocStatus.NotEvaluated}));
      expect(employment.documents[5]).toEqual(jasmine.objectContaining({type: DocTypes.PAYSTUB, status: DocStatus.NotEvaluated}));
      expect(employment.documents[6]).toEqual(jasmine.objectContaining({type: DocTypes.PAYSTUB, status: DocStatus.Missing}));
    });
  });

  describe('calculate orId', () => {
    it('missing VOE, W2 and PS for INC-040', () => {
      ruleData.ruleCode = RuleCode.INC040;
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.VOE},
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.W_2},
        {startDate: new Date('2019-01-01'), status: DocStatus.Missing, type: DocTypes.PAYSTUB}
      ];
      service.adjustRuleData(ruleData);

      expect(employment.orId).toBe(1);
    });
    it('missing VOE, W2 and PS', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.VOE},
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.W_2},
        {startDate: new Date('2019-01-01'), status: DocStatus.Missing, type: DocTypes.PAYSTUB}
      ];
      service.adjustRuleData(ruleData);

      expect(employment.orId).toBe(1);
    });

    it('missing VOE, 3 x W2 and PS', () => {
      const employment = ruleData.data.employmentDetails[0];
      employment.documents = [
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.VOE},
        {startDate: new Date('2016-01-01'), status: DocStatus.Missing, type: DocTypes.W_2},
        {startDate: new Date('2017-01-01'), status: DocStatus.Missing, type: DocTypes.W_2},
        {startDate: new Date('2018-01-01'), status: DocStatus.Missing, type: DocTypes.W_2},
        {startDate: new Date('2019-01-01'), status: DocStatus.Missing, type: DocTypes.PAYSTUB}
      ];
      service.adjustRuleData(ruleData);

      expect(employment.orId).toBe(3);
    });
  });

  describe('sorting employments', () => {

    it('should sort employments by looking on primary job', () => {
      ruleData = {
        viewType: ViewType.Timeline,
        data: {
          referenceDate: '2019-01-31',
          employmentDetails: [
            {
              employerName: 'second',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'primary',
              primary: true,
              endDate: '2019-01-30', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'third',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-31',
              documents: []
            },
          ]
        }
      } as any;
      service.adjustRuleData(ruleData);

      expect(ruleData.data.employmentDetails[0].employerName).toBe('primary');
      expect(ruleData.data.employmentDetails[1].employerName).toBe('second');
      expect(ruleData.data.employmentDetails[2].employerName).toBe('third');
    });

    it('should sort employments by looking on end date of a job', () => {
      ruleData = {
        viewType: ViewType.Timeline,
        data: {
          referenceDate: '2019-01-31',
          employmentDetails: [
            {
              employerName: 'third',
              primary: false,
              endDate: '2019-01-29', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'first',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'second',
              primary: false,
              endDate: '2019-01-30', startDate: '2015-12-31',
              documents: []
            }
          ]
        }
      } as any;
      service.adjustRuleData(ruleData);

      expect(ruleData.data.employmentDetails[0].employerName).toBe('first');
      expect(ruleData.data.employmentDetails[1].employerName).toBe('second');
      expect(ruleData.data.employmentDetails[2].employerName).toBe('third');
    });

    it('should sort employments by looking on start date of a job', () => {
      ruleData = {
        viewType: ViewType.Timeline,
        data: {
          referenceDate: '2019-01-31',
          employmentDetails: [
            {
              employerName: 'third',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'first',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-29',
              documents: []
            },
            {
              employerName: 'second',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-30',
              documents: []
            }
          ]
        }
      } as any;
      service.adjustRuleData(ruleData);

      expect(ruleData.data.employmentDetails[0].employerName).toBe('first');
      expect(ruleData.data.employmentDetails[1].employerName).toBe('second');
      expect(ruleData.data.employmentDetails[2].employerName).toBe('third');
    });

    it('should sort employments by looking on every property of a job', () => {
      ruleData = {
        viewType: ViewType.Timeline,
        data: {
          referenceDate: '2019-01-31',
          employmentDetails: [
            {
              employerName: 'third',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-28',
              documents: []
            },
            {
              employerName: 'fourth',
              primary: false,
              endDate: '2019-01-25', startDate: '2015-12-28',
              documents: []
            },
            {
              employerName: 'first',
              primary: true,
              endDate: '2019-01-25', startDate: '2015-12-31',
              documents: []
            },
            {
              employerName: 'fifth',
              primary: false,
              endDate: '2019-01-24', startDate: '2015-12-25',
              documents: []
            },
            {
              employerName: 'second',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-25',
              documents: []
            }
          ]
        }
      } as any;
      service.adjustRuleData(ruleData);

      expect(ruleData.data.employmentDetails[0].employerName).toBe('first');
      expect(ruleData.data.employmentDetails[1].employerName).toBe('second');
      expect(ruleData.data.employmentDetails[2].employerName).toBe('third');
      expect(ruleData.data.employmentDetails[3].employerName).toBe('fourth');
      expect(ruleData.data.employmentDetails[4].employerName).toBe('fifth');
    });

    it('should remove employment with incorrect dates', () => {
      ruleData = {
        viewType: ViewType.Timeline,
        data: {
          referenceDate: '2019-01-31',
          employmentDetails: [
            {
              employerName: 'first',
              primary: true,
              endDate: '2019-01-25', startDate: '2019-01-29',
              documents: []
            },
            {
              employerName: 'second',
              primary: false,
              endDate: '2019-01-31', startDate: '2015-12-25',
              documents: []
            }
          ]
        }
      } as any;
      service.adjustRuleData(ruleData);

      expect(ruleData.data.employmentDetails.length).toBe(1);
      expect(ruleData.data.employmentDetails[0].employerName).toBe('second');
    });

  });

  describe('check setting line index', () => {

    it('and set index for LA', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
    });

    it('and set index for LA and W_2', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'W_2'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, W2] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(W2.lineIndex).toEqual(1);
    });

    it('and set index for LA and VOE', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'VOE'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
    });

    it('and set index for LA and EVOE', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, EVOE] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(EVOE.lineIndex).toEqual(1);
    });

    it('and set index for LA, VOE and PS', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2019-01-15',
                  status: 'VALID',
                  type: 'PAYSTUB'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(PS.lineIndex).toEqual(2);
    });

    it('and set index for LA, VOE and EVOE (VOE and VOE same dates)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1, 'VOE should be first when date range is same compare to EVOE');
      expect(EVOE.lineIndex).toEqual(2, 'EVOE should be second when date range is same compare to VOE');
    });

    it('and set index for LA, VOE and EVOE (VOE first)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2018-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(EVOE.lineIndex).toEqual(2);
    });

    it('and set index for LA, VOE and EVOE (EVOE first)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                },
                {
                  endDate: '2018-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, EVOE, VOE] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(EVOE.lineIndex).toEqual(1);
      expect(VOE.lineIndex).toEqual(2);
    });

    it('and set index for LA, VOE, EVOE and PS', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2019-01-15',
                  status: 'VALID',
                  type: 'PAYSTUB'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE, PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(EVOE.lineIndex).toEqual(2);
      expect(PS.lineIndex).toEqual(3);
    });

    it('and set index for LA, VOE, EVOE and W2PS', () => {
      ruleData = {
        ruleCode: RuleCode.INC040,
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  type: 'URLA',
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID'
                },
                {
                  type: 'VOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'EVOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'W_2',
                  status: 'MISSING',
                  employerName: 'Fusion Tomo',
                  startDate: '2018-01-01',
                  endDate: '2018-10-22',
                },
                {
                  type: 'PAYSTUB',
                  status: 'MISSING',
                  startDate: '2018-12-18',
                  endDate: '2018-12-31',
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE, W2PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(EVOE.lineIndex).toEqual(2);
      expect(W2PS.lineIndex).toEqual(3);
    });

    it('and set index for LA, VOE, EVOE, W_2 and PS (PS colliding with W_2)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'EVOE'
                },
                {
                  endDate: '2019-12-31',
                  startDate: '2019-01-01',
                  status: 'VALID',
                  type: 'W_2'
                },
                {
                  endDate: '2019-12-31',
                  startDate: '2019-12-15',
                  status: 'VALID',
                  type: 'PAYSTUB'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE, W_2, PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(EVOE.lineIndex).toEqual(2);
      expect(W_2.lineIndex).toEqual(3);
      expect(PS.lineIndex).toEqual(4);
    });

    it('and set index for LA, VOE, W_2 and PS (PS colliding with W_2)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID',
                  type: 'URLA'
                },
                {
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID',
                  type: 'VOE'
                },
                {
                  endDate: '2019-12-31',
                  startDate: '2019-01-01',
                  status: 'VALID',
                  type: 'W_2'
                },
                {
                  endDate: '2019-12-31',
                  startDate: '2019-12-15',
                  status: 'VALID',
                  type: 'PAYSTUB'
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, W_2, PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(W_2.lineIndex).toEqual(2);
      expect(PS.lineIndex).toEqual(3);
    });

    it('and set index for LA, VOE, EVOE W2PS, PS', () => {
      ruleData = {
        ruleCode: RuleCode.INC040,
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  type: 'URLA',
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID'
                },
                {
                  type: 'VOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'EVOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'W_2',
                  status: 'MISSING',
                  employerName: 'Fusion Tomo',
                  startDate: '2018-01-01',
                  endDate: '2018-10-22',
                },
                {
                  type: 'PAYSTUB',
                  status: 'MISSING',
                  startDate: '2018-12-18',
                  endDate: '2018-12-31',
                },
                {
                  type: 'PAYSTUB',
                  status: 'VALID',
                  startDate: '2019-01-01',
                  endDate: '2019-01-14',
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, EVOE, PS, W2PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(EVOE.lineIndex).toEqual(2);
      expect(W2PS.lineIndex).toEqual(3);
      expect(PS.lineIndex).toEqual(3);
    });

    it('and set index for LA, VOE W2PS, PS', () => {
      ruleData = {
        ruleCode: RuleCode.INC040,
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  type: 'URLA',
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID'
                },
                {
                  type: 'VOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'W_2',
                  status: 'MISSING',
                  employerName: 'Fusion Tomo',
                  startDate: '2018-01-01',
                  endDate: '2018-10-22',
                },
                {
                  type: 'PAYSTUB',
                  status: 'MISSING',
                  startDate: '2018-12-18',
                  endDate: '2018-12-31',
                },
                {
                  type: 'PAYSTUB',
                  status: 'VALID',
                  startDate: '2019-01-01',
                  endDate: '2019-01-14',
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, PS, W2PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(W2PS.lineIndex).toEqual(2);
      expect(PS.lineIndex).toEqual(2);
    });

    it('and set index for LA, VOE W2PS, PS (PS colliding with W2PS)', () => {
      ruleData = {
        ruleCode: RuleCode.INC040,
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  type: 'URLA',
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID'
                },
                {
                  type: 'VOE',
                  endDate: '2019-01-31',
                  startDate: '2016-12-31',
                  status: 'VALID'
                },
                {
                  type: 'W_2',
                  status: 'MISSING',
                  employerName: 'Fusion Tomo',
                  startDate: '2018-01-01',
                  endDate: '2018-10-22',
                },
                {
                  type: 'PAYSTUB',
                  status: 'MISSING',
                  startDate: '2018-12-18',
                  endDate: '2018-12-31',
                },
                {
                  type: 'PAYSTUB',
                  status: 'VALID',
                  startDate: '2018-05-11',
                  endDate: '2018-05-25',
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, VOE, PS, W2PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(VOE.lineIndex).toEqual(1);
      expect(W2PS.lineIndex).toEqual(2);
      expect(PS.lineIndex).toEqual(3);
    });

    it('and set index for LA, W2PS, PS (PS colliding with W2PS)', () => {
      ruleData = {
        ruleCode: RuleCode.INC040,
        data: {
          employmentDetails: [
            {
              endDate: '2019-01-31',
              startDate: '2015-12-31',
              documents: [
                {
                  type: 'URLA',
                  endDate: '2019-01-31',
                  startDate: '2015-12-31',
                  status: 'VALID'
                },
                {
                  type: 'W_2',
                  status: 'MISSING',
                  employerName: 'Fusion Tomo',
                  startDate: '2018-01-01',
                  endDate: '2018-10-22',
                },
                {
                  type: 'PAYSTUB',
                  status: 'MISSING',
                  startDate: '2018-12-18',
                  endDate: '2018-12-31',
                },
                {
                  type: 'PAYSTUB',
                  status: 'VALID',
                  startDate: '2018-05-11',
                  endDate: '2018-05-25',
                }
              ]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      const [LA, PS, W2PS] = ruleData.data.employmentDetails[0].documents;
      expect(LA.lineIndex).toEqual(0);
      expect(W2PS.lineIndex).toEqual(1);
      expect(PS.lineIndex).toEqual(2);
    });

  });

  it('and set additional rows in case of LA, VOE, W_2, PS (PS colliding with W_2)', () => {
    ruleData = {
      ruleCode: 'INC-1035',
      data: {
        employmentDetails: [
          {
            startDate: '2016-11-26',
            endDate: '2019-04-04',
            documents: [
              {
                type: 'URLA',
                status: 'VALID',
                startDate: '2016-11-26',
                endDate: '2019-04-04'
              },
              {
                type: 'W_2',
                status: 'MISSING',
                startDate: '2017-01-01',
                endDate: '2017-12-31'
              },
              {
                type: 'W_2',
                status: 'MISSING',
                startDate: '2018-01-01',
                endDate: '2018-12-31'
              },
              {
                type: 'VOE',
                status: 'MISSING',
                startDate: '2016-11-26',
                endDate: '2019-04-04'
              },
              {
                type: 'EVOE',
                status: 'MISSING',
                startDate: '2016-11-26',
                endDate: '2019-04-04'
              },
              {
                type: 'PAYSTUB',
                status: 'NOT_EVALUATED',
                startDate: '2019-11-14',
                endDate: '2019-11-26'
              },
              {
                type: 'PAYSTUB',
                status: 'NOT_EVALUATED',
                startDate: '2018-11-28',
                endDate: '2018-12-06'
              },
              {
                type: 'PAYSTUB',
                status: 'NOT_EVALUATED',
                startDate: '2018-02-16',
                endDate: '2018-02-26',
              }
            ]
          }
        ]
      }
    } as any;

    service.adjustRuleData(ruleData);

    expect(ruleData.data.employmentDetails[0].additionalRows).toEqual(1);
  });

  it('Should sort employers name depending on type and name', () => {
    ruleData = {
      viewType: ViewType.Merging,
      data: {
        employmentValidationItems: [],
        loanAppEmployers: [
          {
            name: 'Blue yonder',
            type: EmployerType.Previous
          },
          {
            name: 'Airplane',
            type: EmployerType.Previous
          },
          {
            name: 'Sky Dreamer',
            type: EmployerType.Previous
          },
          {
            name: 'Baldwin musem',
            type: EmployerType.Secondary
          },
          {
            name: 'Awesome Computers',
            type: EmployerType.Primary
          }
        ]
      }
    } as any;
    service.adjustRuleData(ruleData);
    expect(ruleData.data.loanAppEmployers[0]).toEqual(jasmine.objectContaining({name: 'Awesome Computers'}));
    expect(ruleData.data.loanAppEmployers[1]).toEqual(jasmine.objectContaining({name: 'Baldwin musem'}));
    expect(ruleData.data.loanAppEmployers[2]).toEqual(jasmine.objectContaining({name: 'Airplane'}));
    expect(ruleData.data.loanAppEmployers[3]).toEqual(jasmine.objectContaining({name: 'Blue yonder'}));
    expect(ruleData.data.loanAppEmployers[4]).toEqual(jasmine.objectContaining({name: 'Sky Dreamer'}));
  });

  describe('sorting rules', () => {
    const folderData = {
      applicants: [{
        rules: [
          {
            'status': RuleStatus.Passed,
            'id': 1
          },
          {
            'status': RuleStatus.NotApplicable,
            'id': 5
          },
          {
            'status': RuleStatus.Passed,
            'id': 2
          },
          {
            'status': RuleStatus.Failed,
            'id': 3
          },
          {
            'status': RuleStatus.Waived,
            'id': 4,
            'waive': {}
          },
          {
            'status': RuleStatus.Failed,
            'id': 6
          },
          {
            'status': RuleStatus.Waived,
            'id': 7,
            'waive': {}
          },
          {
            'status': RuleStatus.NotEvaluated,
            'id': 9
          },
          {
            'status': RuleStatus.NotApplicable,
            'id': 8
          },
        ]
      }]
    } as any;

    it('sort rules: failed -> waived -> passed -> not applicable', () => {
      const sortedData = service.adjustFolderData(folderData);
      // failed
      expect(getRuleId(sortedData, 0)).toBe(3);
      expect(getRuleId(sortedData, 1)).toBe(6);

      // waived
      expect(getRuleId(sortedData, 2)).toBe(4);
      expect(getRuleId(sortedData, 3)).toBe(7);

      // passed
      expect(getRuleId(sortedData, 4)).toBe(1);
      expect(getRuleId(sortedData, 5)).toBe(2);

      // N/A
      expect(getRuleId(sortedData, 6)).toBe(5);
      expect(getRuleId(sortedData, 8)).toBe(8);

      // Not Evaluated
      expect(getRuleId(sortedData, 7)).toBe(9);
    });
  });

  describe('set view type', () => {
    it('trending', () => {
      ruleData = {
        data: {
          validationItems: [{}]
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Trending);
    });

    it('merging', () => {
      ruleData = {
        data: {
          employmentValidationItems: [{}],
          loanAppEmployers: []
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Merging);
    });

    it('timeline (employment with docs)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              documents: [{}]
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Timeline);
    });

    it('timeline (gap)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              type: EmploymentTypes.Gap,
              documents: []
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Timeline);
    });

    it('validation (datavalidation)', () => {
      ruleData = {
        data: {
          employmentDetails: [
            {
              dataValidation: [{}],
              documents: []
            }
          ]
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Validation);
    });

    it('validation (loantypdetails - datavalidation)', () => {
      ruleData = {
        data: {
          employmentDetails: [{documents: []}],
          loanTypeDetails: {
            dataValidation: [{}],
            timelineMarkers: []
          }
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(ViewType.Validation);
    });

    it('no supported view', () => {
      ruleData = {
        data: {
          employmentDetails: [],
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(undefined);
    });


    it('no supported view (has employment details)', () => {
      ruleData = {
        data: {
          employmentDetails: [{}],
        }
      } as any;

      service.adjustRuleData(ruleData);

      expect(ruleData.viewType).toEqual(undefined);
    });
  });

  it('should log info when there is no applicants', () => {
    service.adjustFolderData({loanStatus: LoanStatus.Lock, folderId: 4242} as any);

    expect(loggerServiceSpy.captureMessage)
      .toHaveBeenCalledWith('Missing response field - "applicants"', {loanStatus: LoanStatus.Lock, folderId: 4242}, Severity.Warning);
  });
});

function getRuleId(folderData, index) {
  return folderData.applicants[0].rules[index].id;
}
