import {TestBed} from '@angular/core/testing';
import {TimelineCalculation} from './timeline-calculation';
import {DateRange, DrawOptions} from '../models/data.interface';

describe('TimelineCalculation', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('test calculateWidthForDateRange', () => {
    it('should get width for whole month', () => {
      const startDate = new Date('2018-02-01');
      const endDate = new Date('2018-02-28');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toEqual(100);
    });

    it('should get width for half month(Apr)', () => {
      const startDate = new Date('2018-04-15');
      const endDate = new Date('2018-04-30');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(53.3333);
    });

    it('should get width for half month(Feb)', () => {
      const startDate = new Date('2018-02-14');
      const endDate = new Date('2018-02-28');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(53.5714);
    });

    it('should get width from 10 days(Jan)', () => {
      const startDate = new Date('2018-01-10');
      const endDate = new Date('2018-01-31');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(70.9677);
    });

    it('should get width from 10 days(Feb)', () => {
      const startDate = new Date('2018-02-10');
      const endDate = new Date('2018-02-28');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(67.8571);
    });

    it('should get width for whole year', () => {
      const startDate = new Date('2018-01-01');
      const endDate = new Date('2018-12-31');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toEqual(1200);
    });

    it('should get width for 1.5 year', () => {
      const startDate = new Date('2018-01-01');
      const endDate = new Date('2019-06-30');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toEqual(1800);
    });

    it('should get width for 1 day', () => {
      const startDate = new Date('2018-01-01');
      const endDate = new Date('2018-01-02');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(6.4516);
    });

    it('should get width for 1 week', () => {
      const startDate = new Date('2018-01-01');
      const endDate = new Date('2018-01-07');
      const fakeMonthWidth = 100;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(22.5806);
    });

    it('should get width date range in same month', () => {
      const startDate = new Date('2019-03-02');
      const endDate = new Date('2019-03-15');
      const fakeMonthWidth = 60.958333333333336;

      const width = TimelineCalculation.calculateWidthForDateRange(startDate, endDate, fakeMonthWidth);

      expect(width).toBeCloseTo(27.529);
    });

    it('should get full month width after sum 2 date ranges of 1 month', () => {
      const startDate1 = new Date('2017-11-01');
      const endDate1 = new Date('2017-11-17');
      const startDate2 = new Date('2017-11-18');
      const endDate2 = new Date('2017-11-30');
      const fakeMonthWidth = 100;

      const width1 = TimelineCalculation.calculateWidthForDateRange(startDate1, endDate1, fakeMonthWidth);
      const width2 = TimelineCalculation.calculateWidthForDateRange(startDate2, endDate2, fakeMonthWidth);

      expect(width1 + width2).toEqual(100);
    });
  });
  describe('test getTopPosition', () => {
    it('should get top position without index', () => {
      const top = TimelineCalculation.getTopPosition(10);
      expect(top).toBe(0);
    });

    it('should get top position with index = 1', () => {
      const top = TimelineCalculation.getTopPosition(10, 1);
      expect(top).toBe(10);
    });

    it('should get top position with index = 3', () => {
      const top = TimelineCalculation.getTopPosition(15, 3);
      expect(top).toBe(45);
    });
  });

  it('should get left position with reversed position', () => {
    const dateRange: DateRange = {startDate: new Date('2018-02-01'), endDate: new Date('2018-03-01')};
    const settings: DrawOptions = {
      referenceDate: new Date('2018-04-01'),
      calendarStartDate: new Date('2018-05-01'),
      monthWidth: 10
    } as any;
    const left = TimelineCalculation.getLeftPosition(dateRange, settings, true);

    expect(left).toBeCloseTo(-30);
  });
});
