import {ElementRef, EventEmitter, Injectable, NgZone} from '@angular/core';

import {ScrollOptions} from '@models/scroll-options.interface';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {
  constructor(private zone: NgZone) {
  }

  scrollTo(options: ScrollOptions): void {
    let {to} = options;
    const {container, duration, vertical, scrollFinishedEmitter, offset} = options;
    const containerEl = this.getHTMLElement(container);

    if (!containerEl) {
      console.error('Can not get container element!');
      return;
    }

    const start = Date.now();
    const from = vertical ? containerEl.scrollTop : containerEl.scrollLeft;

    if (offset) {
      to += offset;
    }

    if (from === to) {
      if (scrollFinishedEmitter) {
        scrollFinishedEmitter.emit();
      }
      return;
    }

    requestAnimationFrame(this.scrollBase(containerEl, start, duration, from, to, offset, vertical, scrollFinishedEmitter));
  }

  scrollToElement(options: ScrollOptions): void {
    const {container, target, duration, vertical, scrollFinishedEmitter, offset} = options;
    const containerEl = this.getHTMLElement(container);
    const targetEl = this.getHTMLElement(target);

    if (!containerEl || !targetEl) {
      console.error('Can not get container or target element!');
      return;
    }

    const start = Date.now();
    const from = vertical ? containerEl.scrollTop : containerEl.scrollLeft;
    let to = vertical ? targetEl.offsetTop : targetEl.offsetLeft;

    if (offset) {
      to += offset;
    }

    if (from === to) {
      if (scrollFinishedEmitter) {
        scrollFinishedEmitter.emit();
      }
      return;
    }
    this.zone.runOutsideAngular(() => {
      requestAnimationFrame(this.scrollBase(containerEl, start, duration, from, to, offset, vertical, scrollFinishedEmitter));
    });

  }

  scrollBy(options: ScrollOptions): void {
    const {container, by, duration, vertical, scrollFinishedEmitter, offset} = options;
    const containerEl = this.getHTMLElement(container);

    if (!containerEl) {
      console.error('Can not get container element!');
      return;
    }

    const start = Date.now();
    const from = vertical ? containerEl.scrollTop : containerEl.scrollLeft;
    let to = from + by;

    if (offset) {
      to += offset;
    }

    if (from === to) {
      if (scrollFinishedEmitter) {
        scrollFinishedEmitter.emit();
      }
      return;
    }
    this.zone.runOutsideAngular(() => {
      requestAnimationFrame(this.scrollBase(containerEl, start, duration, from, to, offset, vertical, scrollFinishedEmitter));
    });
  }

  private scrollBase(containerEl: HTMLElement,
                     start: number,
                     duration: number,
                     from: number,
                     to: number,
                     offset: number,
                     vertical: boolean,
                     scrollFinishedEmitter: EventEmitter<void>): () => void {
    return () => {
      const currentTime = Date.now();
      const time = Math.min(1, (currentTime - start) / duration);
      const easedT = this.easeInOutQuad(time);

      if (vertical) {
        containerEl.scrollTop = (easedT * (to - from)) + from;
      } else {
        containerEl.scrollLeft = (easedT * (to - from)) + from;
      }

      if (time < 1) {
        this.zone.runOutsideAngular(() => {
          requestAnimationFrame(this.scrollBase(containerEl, start, duration, from, to, offset, vertical, scrollFinishedEmitter));
        });
      } else if (scrollFinishedEmitter) {
        scrollFinishedEmitter.emit();
      }
    };
  }

  private getHTMLElement(element: HTMLElement | ElementRef | string): HTMLElement | null {
    if (element instanceof HTMLElement) {
      return element;
    }
    if (element instanceof ElementRef) {
      return element.nativeElement;
    }

    const elementByScrollId = document.querySelector(`[scroll-id=${element}]`);
    return <HTMLElement>elementByScrollId || document.getElementById(element);
  }

  // https://github.com/danro/jquery-easing/blob/master/jquery.easing.js
  // https://gist.github.com/markgoodyear/9496715
  private easeInOutQuad(t): number {
    return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
  }
}
