import {inject, TestBed} from '@angular/core/testing';
import {CheckRuleGuard} from './check-rule.guard';
import {RouterTestingModule} from '@angular/router/testing';
import {Router, Routes} from '@angular/router';

const routes: Routes = [];

describe('CheckRuleGuard', () => {
  let router: Router;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      providers: [CheckRuleGuard]
    });
    router = TestBed.get(Router);
  });

  it('should be created', inject([CheckRuleGuard], (guard: CheckRuleGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should check if selected rule-details have id', inject([CheckRuleGuard], (guard: CheckRuleGuard) => {
    const next: any = {params: {id: 10}};

    expect(guard.canActivate(next)).toBeTruthy();
  }));
});
