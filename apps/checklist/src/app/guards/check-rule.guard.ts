import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ROUTER_CONSTS} from '@models/router-consts';

@Injectable({
  providedIn: 'root'
})
export class CheckRuleGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = next.params['id'];
    if (!id) {
      console.error(`Selected rule does not have id.`);
      this.router.navigate([ROUTER_CONSTS.notFound], {queryParamsHandling: 'merge'});
    }

    return true;
  }
}
