import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DataValidationComponent} from './data-validation.component';

const routes: Routes = [
  {path: '',
  component: DataValidationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataValidationRoutingModule { }
