import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NodeService} from '@app/services/node.service';
import {DocNamePipeMock} from '@app/test/doc-name.pipe';
import {ToDatePipeMock} from '@app/test/to-date.pipe';
import {of} from 'rxjs/internal/observable/of';
import {DataValidationComponent} from './data-validation.component';
import { DocNamePipe } from '@app/modules/doc-name/doc-name.pipe';

describe('DataValidationComponent', () => {
  let component: DataValidationComponent;
  let fixture: ComponentFixture<DataValidationComponent>;
  const nodeServiceMock = {nodeData$: of({data: {}})};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DataValidationComponent,
        ToDatePipeMock,
        DocNamePipeMock
      ],
      providers: [
        {provide: NodeService, useValue: nodeServiceMock},
        {provide: DocNamePipe, useValue: DocNamePipeMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
