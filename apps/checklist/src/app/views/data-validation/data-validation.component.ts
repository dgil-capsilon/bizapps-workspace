import {ChangeDetectionStrategy, Component} from '@angular/core';
import {NodeService} from '@app/services/node.service';
import {IsNotEvaluated} from '@decorators/is-not-evaluated.decorator';
import {EmploymentDataValidationApi, ValidationData} from '@models/data';
import {RuleCode, RuleStatus, ValidationStatus} from '@models/data.enum';
import {ViewBase} from '@models/view-base';
import {DocNamePipe} from '@app/modules/doc-name/doc-name.pipe';

@IsNotEvaluated()
@Component({
  selector: 'chk-data-validation',
  templateUrl: './data-validation.component.html',
  styleUrls: ['./data-validation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataValidationComponent extends ViewBase {
  ruleData: ValidationData;
  isRuleNotPassing: boolean;
  isNewView = false;
  isCNAView = false;

  constructor(private docNamePipe: DocNamePipe,
              nodeService: NodeService) {
    super(nodeService);
  }

  hasMissingDoc(dataValidation: EmploymentDataValidationApi[]) {
    return dataValidation.some(data => data.status === ValidationStatus.Missing);
  }

  getMissingDocTypes(dataValidation: EmploymentDataValidationApi[]) {
    const validationItems = dataValidation
      .filter(data => data.status === ValidationStatus.Missing)
      .map(data => this.docNamePipe.transform(data.docType, data.status));
    return validationItems.length ? validationItems.join(' or ') : null;
  }

  get showNotPresentText() {
    return this.ruleData.ruleCode === RuleCode.INC1130 && this.ruleData.status === RuleStatus.Failed;
  }

  protected handleRuleData(ruleData: ValidationData) {
    this.ruleData = ruleData;
    this.isRuleNotPassing = ruleData.status !== RuleStatus.Passed;
    this.isCNAView = ruleData.data.loanTypeDetails != null;
    this.checkIfShouldUseTable();
  }

  isNewViewNotEvaluated(data: any[]): boolean {
    return this.isNewView && data.every(value => value.status === ValidationStatus.NotEvaluated);
  }

  trackByIndex(index: number): number {
    return index;
  }

  isOldViewNotEvaluated(data: any): boolean {
    return !data.borderValue && this.isNotEvaluated(data.status) && !this.isNewView;
  }

  documentGroupsOrder = () => null;

  hasBorderValueDateExpired(borderValue: string) {
    return new Date() > new Date(borderValue);
  }

  private checkIfShouldUseTable(): void {
    this.isNewView = this.ruleData.ruleCode === RuleCode.INC710 || this.ruleData.ruleCode === RuleCode.INC720;
  }

}
