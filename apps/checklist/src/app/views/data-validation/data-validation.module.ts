import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {DocHeaderModule} from '@app/modules/doc-header/doc-header.module';
import {DocNameModule} from '@app/modules/doc-name/doc-name.module';
import {DocumentCardModule} from '@app/modules/document-card/document-card.module';
import {HyphenModule} from '@app/modules/hyphen/hyphen.module';
import {NoDocsFoundModule} from '@app/modules/no-docs-found/no-docs-found.module';
import {ScrollHoverModule} from '@app/modules/scroll-hover/scroll-hover.module';
import {ToDateModule} from '@app/modules/to-date/to-date.module';
import {ValidationInfoModule} from '@app/modules/validation-info/validation-info.module';
import {DataValidationRoutingModule} from './data-validation-routing.module';
import {DataValidationComponent} from './data-validation.component';

@NgModule({
  declarations: [
    DataValidationComponent,
  ],
  imports: [
    CommonModule,
    DataValidationRoutingModule,
    DocHeaderModule,
    DocumentCardModule,
    ValidationInfoModule,
    ToDateModule,
    HyphenModule,
    ScrollHoverModule,
    NoDocsFoundModule,
    ValidationInfoModule,
    DocNameModule
  ]
})
export class DataValidationModule {
}
