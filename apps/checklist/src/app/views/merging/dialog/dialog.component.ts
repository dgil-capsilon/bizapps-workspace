import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ChecklistStompService} from '@app/services/checklist-stomp.service';
import {DataService} from '@app/services/data.service';
import {MergingDataValidationApi, StompFolderEventApi} from '@models/data';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {getSelectedApplicantId} from '@redux/applicants/applicants.selectors';
import {FolderReloadAction} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import {BehaviorSubject, Observable, ReplaySubject, Subject} from 'rxjs';
import {filter, switchMap, takeUntil, tap} from 'rxjs/operators';

export interface DialogData {
  header: string;
  bodyText: string;
  cancelText: string;
  confirmButton: { name: string, pressed: string };
  employerName: MergingDataValidationApi | MergingDataValidationApi[];
  employerAlias: string;
}

@Component({
  selector: 'chk-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent implements OnInit, OnDestroy {
  /**
   * Information from documents (VOE/W2/PS)
   */
  employerName: string;
  /**
   * Information from loan app
   */
  employerAlias: string;
  isMerging = false;
  mergeText;
  componentData: DialogData;
  componentDestroy$ = new Subject<void>();
  private lastFolderWatchEvent$ = new ReplaySubject<StompFolderEventApi>(3);

  constructor(private dataService: DataService,
              private store$: Store<AppState>,
              private dialogRef: MatDialogRef<DialogComponent>,
              private checklistStompService: ChecklistStompService) {
  }

  ngOnInit() {
    if (this.configData.employerAlias) {
      this.employerName = this.configData.employerName.fieldValue;
      this.employerAlias = this.configData.employerAlias;
      this.componentData = this.configData.componentData;
      this.mergeText = this.componentData.confirmButton.name;
    } else {
      this.componentData = this.configData.componentData;
      this.mergeText = this.componentData.confirmButton.name;
    }
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  close(): void {
    this.dialogRef.close();
  }

  sendData(): void {
    this.isMerging = true;
    this.mergeText = this.componentData.confirmButton.pressed;
    this.listenOnFolderChange().subscribe(); // Watch for WS folder change before http request
    const response = this.employerAlias && this.employerAlias ? this.mergeNames() : this.restoreNames();
    response.pipe(
      switchMap(({eventId}) => this.lastFolderChangeMsg(eventId)),
      takeUntil(this.componentDestroy$)
    ).subscribe(() => {
      this.store$.dispatch(FolderReloadAction());
      this.isMerging = false;
      this.close();
    });
  }

  mergeNames(): Observable<any> {
    return this.store$.select(getSelectedApplicantId).pipe(
      switchMap(applicantId => this.dataService.saveEmployerNames(applicantId, [{
        employerName: this.employerName,
        employerAlias: this.employerAlias
      }])));
  }

  restoreNames(): Observable<any> {
    const employerNames = (this.componentData.employerName as MergingDataValidationApi[])
      .filter(value => value.fieldValue !== value.referenceValue)
      .map(value => ({
        employerName: value.fieldValue,
        employerAlias: value.referenceValue
      }));
    return this.store$.select(getSelectedApplicantId)
      .pipe(switchMap(applicantId => this.dataService.removeEmployerNames(applicantId, employerNames)));
  }

  private listenOnFolderChange() {
    return this.store$.select(fromFolder.getId).pipe(
      filter(Boolean),
      switchMap((id: string) => this.checklistStompService.folderEventWatch(id)),
      tap(folderEvent => this.lastFolderWatchEvent$.next(folderEvent)),
      takeUntil(this.componentDestroy$)
    );
  }

  private lastFolderChangeMsg(eventId: string) {
    return this.lastFolderWatchEvent$.pipe(
      filter(Boolean),
      filter((folderEvent: StompFolderEventApi) => folderEvent.id === eventId)
    );
  }

  get configData() {
    return this.dialogRef._containerInstance._config.data;
  }
}
