import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DialogComponent} from './dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DataService} from '@app/services/data.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import createSpy = jasmine.createSpy;

xdescribe('DialogComponent', () => {
  let component: DialogComponent;
  let fixture: ComponentFixture<DialogComponent>;
  const dataService = {saveEmployerNames: createSpy()};
  const matDialogRefMock = {
    _containerInstance: {
      _config: {
        data: {
          employerName: '',
          employerAlias: '',
          componentData: {
            header: '',
            bodyText: '',
            cancelText: '',
            confirmButton: {name: '', pressed: ''}
          }
        }
      }
    }
  };
  const navigationService = {selectedApplicant: createSpy()};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogComponent],
      providers: [
        {provide: MatDialogRef, useValue: matDialogRefMock},
        {provide: DataService, useValue: dataService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
