import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MergingRoutingModule} from './merging-routing.module';
import {EmploymentMergingComponent} from './employment-merging/employment-merging.component';
import {MergingHeaderComponent} from './merging-header/merging-header.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {OverlayModule} from '@angular/cdk/overlay';
import {EmployerAliasesModule} from '@app/modules/employer-aliases/employer-aliases.module';
import {DialogComponent} from './dialog/dialog.component';
import {DocumentCardModule} from '@app/modules/document-card/document-card.module';
import {ValidationInfoModule} from '@app/modules/validation-info/validation-info.module';
import {MatIconModule} from '@angular/material/icon';
import {ScrollHoverModule} from '@app/modules/scroll-hover/scroll-hover.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    EmploymentMergingComponent,
    MergingHeaderComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    MergingRoutingModule,
    MatTooltipModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    OverlayModule,
    EmployerAliasesModule,
    DocumentCardModule,
    ValidationInfoModule,
    ScrollHoverModule
  ],
  entryComponents: [DialogComponent],
})
export class MergingModule {
}
