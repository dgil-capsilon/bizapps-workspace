import {AfterViewInit, Component, OnDestroy, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {combineLatest} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ScrollHoverDirective} from '@app/modules/scroll-hover/scroll-hover.directive';
import {RuleStatus} from '@models/data.enum';
import {MergingData} from '@models/data.interface';
import {ViewBase} from '@models/view-base';
import {NodeService} from '@app/services/node.service';
import {DocumentCardComponent} from '@app/modules/document-card/document-card.component';
import {IsNotEvaluated} from '@decorators/is-not-evaluated.decorator';
import {IsInvalid} from '@decorators/is-invalid.decorator';

@IsNotEvaluated()
@IsInvalid()
@Component({
  selector: 'chk-employment-merging',
  templateUrl: './employment-merging.component.html',
  styleUrls: ['./employment-merging.component.scss'],
})
export class EmploymentMergingComponent extends ViewBase implements AfterViewInit, OnDestroy {
  @ViewChildren(DocumentCardComponent) documentCards: QueryList<DocumentCardComponent>;
  @ViewChild(ScrollHoverDirective) scrollDirective: ScrollHoverDirective;
  ruleData: MergingData;
  isRuleNotPassing: boolean;

  constructor(nodeService: NodeService) {
    super(nodeService);
  }

  ngAfterViewInit(): void {
    combineLatest(this.documentCards.map(documentCard => documentCard.onSnippetLoad))
      .pipe(takeUntil(this.componentDestroy$))
      .subscribe(() => {
        this.scrollDirective.recalculateAndShow();
      });
  }

  protected handleRuleData(ruleData: MergingData) {
    this.ruleData = ruleData;
    this.isRuleNotPassing = ruleData.status !== RuleStatus.Passed;
  }

  trackByIndex(index: number): number {
    return index;
  }

  mergeGroupsOrder = () => null;
}
