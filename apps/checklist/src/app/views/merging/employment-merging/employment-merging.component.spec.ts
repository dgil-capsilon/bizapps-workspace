import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmploymentMergingComponent} from './employment-merging.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('EmploymentMergingComponent', () => {
  let component: EmploymentMergingComponent;
  let fixture: ComponentFixture<EmploymentMergingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmploymentMergingComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploymentMergingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
