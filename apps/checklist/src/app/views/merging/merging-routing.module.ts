import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmploymentMergingComponent} from './employment-merging/employment-merging.component';

const routes: Routes = [
  {
    path: '',
    component: EmploymentMergingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MergingRoutingModule { }
