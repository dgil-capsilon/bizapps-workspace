import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {AppState} from '@redux/app.reducer';
import {ValidationStatus} from '@models/enums';
import {FakeEmployerAliasesPipe} from '@app/test/employer-aliases.pipe';
import {FakeMatTooltipDirective} from '@app/test/mat-tooltip.directive';
import {MergingHeaderComponent} from './merging-header.component';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('MergingHeaderComponent', () => {
  let component: MergingHeaderComponent;
  let fixture: ComponentFixture<MergingHeaderComponent>;
  const storeSpy: SpyObj<Store<AppState>> = createSpyObj('Store', ['select', 'dispatch']);
  const matDialogMock = {};
  beforeEach(async(() => {
    storeSpy.select.and.returnValue(of());

    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [
        MergingHeaderComponent,
        FakeEmployerAliasesPipe,
        FakeMatTooltipDirective
      ],
      providers: [
        {provide: MatDialog, useValue: matDialogMock},
        {provide: Store, useValue: storeSpy}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergingHeaderComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find any employment with manual match', () => {
    component.employment = {
      data: [
        {manualMatch: false},
        {manualMatch: true}
      ]
    } as any;

    expect(component.ableToRestore).toEqual(true);
  });

  it('should not find any employment with manual match', () => {
    component.employment = {
      data: [
        {manualMatch: false},
        {manualMatch: false}
      ]
    } as any;

    expect(component.ableToRestore).toEqual(false);
  });

  it('should find any employment with status invalid', () => {
    component.employment = {
      data: [
        {status: ValidationStatus.Valid},
        {status: ValidationStatus.Invalid},
        {status: ValidationStatus.Expired},
        {status: ValidationStatus.NotEvaluated}
      ],
      first: {employerNames: [], fieldValue: 'Some Employer Name'}
    } as any;

    fixture.detectChanges();

    expect(component.ableToMerge).toEqual(true);
  });

  it('should not find any employment with status invalid', () => {
    component.employment = {
      data: [
        {status: ValidationStatus.Expired},
        {status: ValidationStatus.NotEvaluated},
        {status: ValidationStatus.Expired},
        {status: ValidationStatus.NotEvaluated}
      ],
      first: {employerNames: [], fieldValue: 'Some Employer Name'}
    } as any;

    fixture.detectChanges();

    expect(component.ableToMerge).toEqual(false);
  });

  it('should enable possibility to open panel', () => {
    component.employment = {
      data: [
        {status: ValidationStatus.Valid, manualMatch: true}
      ],
      first: {fieldValue: 'Some Employer Name', employerNames: []}
    } as any;

    fixture.detectChanges();

    expect(component.canOpenPanel).toEqual(true);
  });

  xit('should disable possibility to open panel (no valid status & manual match)', () => {
    component.employment = {
      data: [
        {status: ValidationStatus.Expired, manualMatch: false},
        {status: ValidationStatus.NotEvaluated, manualMatch: true}
      ],
      first: {fieldValue: 'Some Employer Name', employerNames: []}
    } as any;

    fixture.detectChanges();

    expect(component.canOpenPanel).toEqual(false);
  });
});
