import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import * as fromFolder from '@redux/folder/folder.selectors';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {LoanAppEmployers, MergingDataValidationApi} from '@models/data';
import {EmployerType, ValidationStatus} from '@models/enums';
import {DialogComponent} from '../dialog/dialog.component';

@Component({
  selector: 'chk-merging-header',
  templateUrl: './merging-header.component.html',
  styleUrls: ['./merging-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MergingHeaderComponent implements OnInit, OnDestroy {
  @Input() employersList: LoanAppEmployers[];
  @Input() employment: { data: MergingDataValidationApi[], first: MergingDataValidationApi };
  @Input() applicantId: number;
  private _panelOpen = false;
  employerName: string;
  employerType: string;
  aliases: string[];
  aliasesTooltipText: string;
  isFolderOpen: boolean;
  componentDestroy$ = new Subject<void>();

  constructor(private dialogService: MatDialog,
              private store$: Store<AppState>,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.employerName = this.employment.first.referenceValue || this.employment.first.fieldValue;
    this.employerType = this.getEmployerType();
    this.aliases = this.employment.first.employerNames;
    this.aliasesTooltipText = this.aliases.join('\n');
    this.store$.select(fromFolder.getLoanStatusFolderOpen).pipe(takeUntil(this.componentDestroy$)).subscribe(open => {
      this.isFolderOpen = open;
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  openEmployerList(): void {
    if (!this.isFolderOpen || !this.canOpenPanel) {
      return;
    }

    this._panelOpen = !this._panelOpen;
  }


  get ableToMerge(): boolean {
    return this.employerName && this.employment.data.some(mergingData => mergingData.status === ValidationStatus.Invalid);
  }

  get ableToRestore(): boolean {
    return this.employment.data.some(mergingData => mergingData.manualMatch);
  }

  get canOpenPanel(): boolean {
    return this.employerName && this.ableToMerge || this.ableToRestore;
  }

  get alreadyMerged(): boolean {
    return this.employerName && this.manuallyMerged || this.automaticallyMerged;
  }

  get panelOpen(): boolean {
    return this._panelOpen && this.isFolderOpen;
  }

  isPrimary(type: string): boolean {
    return type === EmployerType.Primary;
  }

  close(): void {
    this._panelOpen = false;
  }

  merge(employerName: MergingDataValidationApi, employerAlias: LoanAppEmployers): void {
    const dialogConfig = this.setDataForDialog();
    dialogConfig.data = {
      employerName: employerName,
      employerAlias: employerAlias.name,
      componentData: {
        header: 'Merge Employer Name',
        bodyText: `Please confirm that you’d like to merge the following employer name.
        Note that this will be applied throughout the loan folder as well as for all new documents added.`,
        cancelText: 'Cancel',
        confirmButton: {name: 'Confirm Merge', pressed: 'Merging'}
      }
    };
    this.dialogService.open(DialogComponent, dialogConfig);
    this.close();
  }

  restore(employerNames: MergingDataValidationApi[]): void {
    const dialogConfig = this.setDataForDialog();
    const employers = employerNames.filter(value => value.manualMatch === true);
    dialogConfig.data = {
      componentData: {
        employerName: employers,
        header: 'Merge Employer Name',
        bodyText: `Please confirm that you’d like to restore to default the employer name on document.
        Note that this will be applied throughout the loan folder.`,
        cancelText: 'Cancel',
        confirmButton: {name: 'Confirm', pressed: 'Restoring'}
      }
    };
    this.dialogService.open(DialogComponent, dialogConfig);
    this.close();
  }

  setDataForDialog(): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxWidth = '500px';
    dialogConfig.maxHeight = '500px';
    dialogConfig.backdropClass = 'cl-backdrop-dialog';
    return dialogConfig;
  }

  private getEmployerType(): string {
    if (!(this.employment && this.employment.data) || !this.employersList) {
      return '';
    }
    const employer: LoanAppEmployers = this.employersList.filter(value => value.name === this.employment.first.referenceValue)[0];
    if (employer) {
      return employer.type;
    }

    return null;
  }

  private get manuallyMerged(): boolean {
    return this.employment.data.some(mergingData => mergingData.referenceValue && mergingData.manualMatch);
  }

  private get automaticallyMerged(): boolean {
    return this.employment.data.some(mergingData => mergingData.referenceValue && mergingData.manualMatch === false);
  }
}
