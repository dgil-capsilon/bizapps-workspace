import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DocHeaderModule} from '@app/modules/doc-header/doc-header.module.ts';
import {DocNameModule} from '@app/modules/doc-name/doc-name.module';
import {DocumentCardModule} from '@app/modules/document-card/document-card.module';
import {DynamicSnippetModule} from '@app/modules/dynamic-snippet/dynamic-snippet.module';
import {ScrollHoverModule} from '@app/modules/scroll-hover/scroll-hover.module';
import {ToDateModule} from '@app/modules/to-date/to-date.module';
import {ValidationInfoModule} from '@app/modules/validation-info/validation-info.module';
import {DocumentsContainerComponent} from './components/documents-container/documents-container.component';
import {GapComponent} from './components/gap/gap.component';
import {MissingDocComponent} from './components/missing-doc/missing-doc.component';
import {TimelineComponent} from './components/timeline/timeline.component';
import {TlDocComponent} from './components/tl-doc/tl-doc.component';
import {TlEmploymentComponent} from './components/tl-employment/tl-employment.component';
import {TlGapComponent} from './components/tl-gap/tl-gap.component';
import {TlMarkLabelComponent} from './components/tl-mark-label/tl-mark-label.component';
import {TlMarkComponent} from './components/tl-mark/tl-mark.component';
import {TlRowHeaderComponent} from './components/tl-row-header/tl-row-header.component';
import {SnippetElementDirective} from './directives/snippet-element.directive';
import {EvidenceRoutingModule} from './evidence-routing.module';
import {EvidenceComponent} from './evidence/evidence.component';
import {TimelineService} from './services/timeline.service';
import { TlAreaTextComponent } from './components/tl-area-text/tl-area-text.component';

@NgModule({
  declarations: [
    EvidenceComponent,
    // Timeline
    TimelineComponent,
    TlDocComponent,
    TlEmploymentComponent,
    TlGapComponent,
    TlRowHeaderComponent,
    TlMarkComponent,

    // Document container
    DocumentsContainerComponent,
    GapComponent,
    SnippetElementDirective,
    MissingDocComponent,
    TlMarkLabelComponent,
    TlAreaTextComponent
  ],
  imports: [
    CommonModule,
    EvidenceRoutingModule,
    MatIconModule,
    MatTooltipModule,
    DocumentCardModule,
    DocHeaderModule,
    DocNameModule,
    ToDateModule,
    DynamicSnippetModule,
    ScrollHoverModule,
    ValidationInfoModule
  ],
  providers: [TimelineService]
})
export class EvidenceModule {
}
