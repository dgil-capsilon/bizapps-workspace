import {ChangeDetectionStrategy, Component, ElementRef, Input} from '@angular/core';
import {DocNamePipe} from '@app/modules/doc-name/doc-name.pipe';
import {DocStatus} from '@models/data.enum';
import {Doc, TimelineEmploymentDetails} from '@models/data.interface';
import {SnippetElement} from '@models/snippet.element.interface';

@Component({
  selector: 'chk-missing-doc',
  templateUrl: './missing-doc.component.html',
  styleUrls: ['./missing-doc.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MissingDocComponent implements SnippetElement {

  @Input() set employerData(data: TimelineEmploymentDetails) {
    this._employerData = data;
    if (!this.employerData.documents) {
      return;
    }
    if (this._employerData.missingDocsAmount) {
      this.missingDocText = this.findDocsText(this.employerData.documents);
    }
  }

  get employerData(): TimelineEmploymentDetails {
    return this._employerData;
  }

  missingDocText: string;
  private _employerData: TimelineEmploymentDetails;

  constructor(private elRef: ElementRef<HTMLElement>,
              private docNamePipe: DocNamePipe) {
  }

  get ref() {
    return this.elRef;
  }

  hasData(data) {
    return this.employerData.documents.some(doc => doc.status === DocStatus.Missing && doc === data);
  }

  get document() {
    if (!this.employerData) {
      return;
    }
    return this.employerData.documents.find(doc => doc.status === DocStatus.Missing);
  }

  private findDocsText(documents: Doc[]): string {
    let text = '';
    let missingDocs = documents
      .filter((doc) => doc.status === DocStatus.Missing)
      .map(doc => doc.type);
    missingDocs = Array.from(new Set(missingDocs));

    if (missingDocs.length === 1) {
      text = this.docNamePipe.transform(missingDocs[0], 'MISSING');
    }
    if (missingDocs.length > 1) {
      missingDocs.forEach((type, index) => {
        const docName = this.docNamePipe.transform(type, 'MISSING');

        if (!text.includes(docName)) {
          text += docName;
          if (index !== missingDocs.length - 1) {
            text += ' or ';
          }
        }
      });
    }
    return text;
  }
}
