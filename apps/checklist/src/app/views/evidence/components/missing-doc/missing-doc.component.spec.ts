import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocStatus, DocTypes} from '../../../../models/data.enum';
import {MissingDocComponent} from './missing-doc.component';
import {DocNamePipe} from 'src/app/modules/doc-name/doc-name.pipe';

describe('MissingDocComponent', () => {
  let component: MissingDocComponent;
  let fixture: ComponentFixture<MissingDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MissingDocComponent
      ],
      providers: [
        DocNamePipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingDocComponent);
    component = fixture.componentInstance;
    component.employerData = {documents: [], status: DocStatus.Missing} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get element ref', () => {
    expect(component.ref).toBeTruthy();
  });

  it('should check if component do not have data', () => {
    const document = {status: DocStatus.Missing};
    const data = {status: DocStatus.Missing} as any;
    expect(component.hasData(document)).toBe(false);
  });

  it('should check if component have data', () => {
    const document = {status: DocStatus.Missing};
    const data = {status: DocStatus.Missing, documents: [document]} as any;
    component.employerData = data;
    expect(component.hasData(document)).toBe(true);
  });

  describe('check missingDocText text', () => {
    it('text for missing VOE document', () => {
      component.employerData = {
        missingDocsAmount: 1,
        documents: [
          {
            status: DocStatus.Missing,
            type: DocTypes.VOE
          }
        ]
      } as any;
      expect(component.missingDocText).toBe('(E)VOE');
    });

    it('text for missing (E)VOE and W-2 documents', () => {
      component.employerData = {
        missingDocsAmount: 1,
        documents: [
          {
            status: DocStatus.Missing,
            type: DocTypes.VOE
          },
          {
            status: DocStatus.Missing,
            type: DocTypes.W_2
          }
        ]
      } as any;
      expect(component.missingDocText).toBe('(E)VOE or W-2');
    });

    it('text for missing VOE, W-2 and PS documents', () => {
      component.employerData = {
        missingDocsAmount: 1,
        documents: [
          {
            status: DocStatus.Missing,
            type: DocTypes.VOE
          },
          {
            status: DocStatus.Missing,
            type: DocTypes.W_2_PAYSTUB
          }
        ]
      } as any;
      expect(component.missingDocText).toBe('(E)VOE or W-2 or year-end PS');
    });
  });

});
