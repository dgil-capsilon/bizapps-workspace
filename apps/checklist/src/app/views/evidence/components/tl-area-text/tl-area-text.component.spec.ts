import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TlAreaTextComponent} from './tl-area-text.component';

describe('TlAreaTextComponent', () => {
  let component: TlAreaTextComponent;
  let fixture: ComponentFixture<TlAreaTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TlAreaTextComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlAreaTextComponent);
    component = fixture.componentInstance;
    component.employment = null;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
