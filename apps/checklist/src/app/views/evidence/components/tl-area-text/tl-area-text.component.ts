import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {DrawOptions, Gap, TimelineEmploymentDetails} from '@models/data';
import {isUndefined} from 'util';

@Component({
  selector: 'chk-tl-area-text',
  templateUrl: './tl-area-text.component.html',
  styleUrls: ['./tl-area-text.component.scss']
})
export class TlAreaTextComponent implements OnInit {
  @HostBinding('style.left.px') left: number;
  @HostBinding('style.width.px') width: number;
  @Input() employment: TimelineEmploymentDetails | Gap;
  @Input() settings: DrawOptions;

  private _isReversed: boolean;
  @Input() set isReversed(value) {
    if (isUndefined(value)) {
      return;
    }

    this._isReversed = value;
    this.calculatePositionAndDimension();
  }

  ngOnInit() {
    this.calculatePositionAndDimension();
  }

  calculatePositionAndDimension() {
    if (!this.employment) {
      return;
    }

    this.width = TimelineCalculation.calculateWidthForDateRange(
      this.employment.startDate,
      this.employment.endDate,
      this.settings.monthWidth);
    this.left = TimelineCalculation.getLeftPosition(this.employment, this.settings, this._isReversed);
  }
}
