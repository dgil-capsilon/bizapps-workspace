import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {of} from 'rxjs';
import {DocStatus, DocTypes, EmploymentTypes} from '../../../../models/data.enum';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {TimelineService} from '../../services/timeline.service';
import {TlEmploymentComponent} from './tl-employment.component';
import createSpy = jasmine.createSpy;

describe('TlEmploymentComponent', () => {
  let component: TlEmploymentComponent;
  let fixture: ComponentFixture<TlEmploymentComponent>;
  let timelineService: any;
  let timelineServiceMock;

  beforeEach(async(() => {
    timelineServiceMock = {
      documentChanges: of(),
      onDocumentFocus: of(),
      onDocumentHover: of(),
      employmentChanges: of(),
      changeEmployment: createSpy(),
      documentFocus: createSpy()
    };

    TestBed.configureTestingModule({
      declarations: [TlEmploymentComponent],
      providers: [
        {provide: TimelineService, useValue: timelineServiceMock},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlEmploymentComponent);
    component = fixture.componentInstance;
    timelineService = fixture.componentRef.injector.get(TimelineService);
    component.settings = {rowHeight: null, monthWidth: null, months: null} as any;
    component.employment = {type: ''} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not highlight missing doc (no selected doc)', () => {
    component.selectedDoc = null;

    expect(component.isMissingDocFocused(null)).toEqual(false);
  });

  it('should not highlight missing doc (no missing doc selected)', () => {
    const doc = {type: 'PS', status: 'VALID'} as any;
    component.selectedDoc = {status: 'VALID'} as any;

    expect(component.isMissingDocFocused(doc)).toEqual(false);
  });

  it('should not highlight missing docs (not belongs to employment)', () => {
    const missingDoc = {type: 'VOE', status: 'MISSING'} as any;
    component.employment = {
      type: '',
      documents: [
        {type: 'PS', status: 'VALID'},
        {type: 'W-2', status: 'MISSING'}
      ]
    } as any;

    component.selectedDoc = missingDoc as any;

    expect(component.isMissingDocFocused(missingDoc)).toEqual(false);
  });

  it('should all highlight missing docs', () => {
    const missingDoc = {type: 'VOE', status: 'MISSING'} as any;
    component.employment = {
      type: '',
      documents: [
        {type: 'PS', status: 'VALID'},
        {type: 'W-2', status: 'MISSING'},
        missingDoc
      ]
    } as any;

    component.selectedDoc = missingDoc as any;

    expect(component.isMissingDocFocused(missingDoc)).toEqual(true);
  });

  it('should not set reversed variable', () => {
    component.isReversed = undefined;

    expect(component.isReversed).toBeUndefined();
  });

  it('should calculate width when timeline being reversed', () => {
    spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);
    spyOn(TimelineCalculation, 'getLeftPosition');

    component.isReversed = true;

    expect(TimelineCalculation.calculateWidthForDateRange).toHaveBeenCalled();
    expect(component.width).toBeDefined();
  });

  it('should calculate left position when timeline being reversed', () => {
    spyOn(TimelineCalculation, 'calculateWidthForDateRange');
    spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(1);

    component.isReversed = true;

    expect(TimelineCalculation.getLeftPosition).toHaveBeenCalled();
    expect(component.left).toBeDefined();
  });

  it('should increase number of rows if employment has additional one', () => {
    component.employment = {additionalRows: 1} as any;
    const rowsBefore = component.rows;

    component.ngOnInit();

    expect(component.rows).toEqual(rowsBefore + 1);
  });

  it('should set selected doc when document changes', () => {
    const doc = {id: 1};
    timelineService.documentChanges = of(doc);

    component.ngOnInit();

    expect(component.selectedDoc).toEqual(jasmine.objectContaining(doc));
  });

  it('should set selected doc when document get focus', () => {
    const doc = {id: 2};
    timelineService.onDocumentFocus = of(doc);

    component.ngOnInit();

    expect(component.selectedDoc).toEqual(jasmine.objectContaining(doc));
  });

  it('should set selected doc when document get hover', () => {
    const doc = {id: 3};
    timelineService.onDocumentHover = of(doc);

    component.ngOnInit();

    expect(component.selectedDoc).toEqual(jasmine.objectContaining(doc));
  });

  it('should set gap as selected doc', () => {
    const employment = {id: 1, type: EmploymentTypes.Gap};
    timelineService.employmentChanges = of(employment);

    component.ngOnInit();

    // TODO: Check if cast is needed
    expect(component.selectedDoc as any).toEqual(jasmine.objectContaining({id: 1, type: EmploymentTypes.Gap}));
  });

  it('should not set employment as selected doc', () => {
    const employment = {id: 2, type: EmploymentTypes.Employment};
    timelineService.employmentChanges = of(employment);

    component.ngOnInit();

    expect(component.selectedDoc).toBeUndefined();
  });

  it('should change employment', () => {
    const employment = {id: 3, type: EmploymentTypes.Employment, documents: []} as any;
    component.employment = employment;
    fixture.detectChanges();

    fixture.nativeElement.querySelector('.employment-area').click();

    expect(timelineServiceMock.changeEmployment).toHaveBeenCalledWith(employment);
    expect(timelineServiceMock.documentFocus).not.toHaveBeenCalled();
  });

  it('should change employment and focus document', () => {
    const doc = {id: 5, type: DocTypes.PAYSTUB};
    const employment = {id: 4, type: EmploymentTypes.Employment, documents: [doc]} as any;
    component.employment = employment;
    fixture.detectChanges();

    fixture.nativeElement.querySelector('.employment-area').click();

    expect(timelineServiceMock.changeEmployment).toHaveBeenCalledWith(employment);
    expect(timelineServiceMock.documentFocus).toHaveBeenCalledWith(doc);
  });

  describe('addLineUnderPSAndW2', () => {
    it('should not set grouped settings without paystub or w_2', () => {
      component.employment = {
        documents: [
          {id: 1, type: DocTypes.URLA}
        ]
      } as any;

      component.ngOnInit();

      expect(component.groupedSettings).toBeUndefined();
    });

    it('should set grouped settings (paystub)', () => {
      spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);
      spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(2);
      spyOn(TimelineCalculation, 'getTopPosition').and.returnValue(3);
      component.employment = {
        documents: [
          {id: 1, type: DocTypes.VOE, status: DocStatus.Missing, lineIndex: 1},
          {id: 2, type: DocTypes.PAYSTUB, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2'},
          {id: 3, type: DocTypes.W_2, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2'}
        ]
      } as any;

      component.ngOnInit();

      expect(component.groupedSettings).toBeDefined();
    });

    it('should use setting reference date', () => {
      spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);
      spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(2);
      spyOn(TimelineCalculation, 'getTopPosition').and.returnValue(3);
      const date = new Date(2019, 3, 24);
      component.employment = {
        documents: [
          {id: 1, type: DocTypes.VOE, status: DocStatus.Missing, lineIndex: 1},
          {id: 2, type: DocTypes.PAYSTUB, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2'},
          {id: 3, type: DocTypes.W_2, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2'}
        ]
      } as any;
      component.settings = {referenceDate: date} as any;

      component.ngOnInit();

      expect(TimelineCalculation.calculateWidthForDateRange).toHaveBeenCalledWith(jasmine.anything(), date, undefined);
    });

    it('should use end date', () => {
      spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);
      spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(2);
      spyOn(TimelineCalculation, 'getTopPosition').and.returnValue(3);
      const date = new Date(2019, 3, 24);
      component.employment = {
        documents: [
          {id: 1, type: DocTypes.VOE, status: DocStatus.Missing, lineIndex: 1},
          {id: 2, type: DocTypes.PAYSTUB, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2', endDate: new Date(2019, 3, 23)},
          {id: 3, type: DocTypes.W_2, status: DocStatus.Missing, lineIndex: 2, group: 'PAYSTUB-W_2', endDate: date}
        ]
      } as any;

      component.ngOnInit();

      expect(TimelineCalculation.calculateWidthForDateRange).toHaveBeenCalledWith(jasmine.anything(), date, null);
      expect(TimelineCalculation.getLeftPosition).toHaveBeenCalledWith({
        startDate: jasmine.anything(),
        endDate: date
      }, jasmine.anything(), undefined);
    });
  });
});
