import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {DocStatus, DocTypes, EmploymentTypes} from '@models/data.enum';
import {Doc, DrawOptions, Employment, Gap, TimelineEmploymentDetails} from '@models/data.interface';
import {TimelineMarkerApi} from '@models/data/api/rules/timeline/timeline-marker.interface';
import {merge, Subject} from 'rxjs';
import {filter} from 'rxjs/internal/operators/filter';
import {takeUntil} from 'rxjs/operators';
import {isUndefined} from 'util';
import {TimelineService} from '../../services/timeline.service';
import {TlDocComponent} from '../tl-doc/tl-doc.component';
import {TlGapComponent} from '../tl-gap/tl-gap.component';

@Component({
  selector: 'chk-tl-employment',
  templateUrl: './tl-employment.component.html',
  styleUrls: ['./tl-employment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TlEmploymentComponent implements OnInit, OnDestroy {
  @Input() set options(options: any) {
    if (isUndefined(options)) {
      return;
    }
    this._isReversed = options.isReversed;
    this.employment = options.employment;
    this.settings = options.settings;
    this.gaps = options.gaps;
    this.currentDate = options.currentDate;
    this.loanDate = options.loanDate;
    this.timelineMarkers = options.timelineMarkers;
    this.width = TimelineCalculation.calculateWidthForDateRange(
      this.employment.startDate,
      this.employment.endDate || this.settings.referenceDate,
      this.settings.monthWidth);
    this.left = TimelineCalculation.getLeftPosition(this.employment, this.settings, this.isReversed);
  }
   employment: TimelineEmploymentDetails;
   gaps: Gap[];
   settings: DrawOptions;
   currentDate: Date;
   loanDate: Date;
   timelineMarkers: TimelineMarkerApi[];

  private _isReversed: boolean;
  private onComponentDestroy$ = new Subject<void>();
  rows = 3;
  documents: Doc[];



  get isReversed(): boolean {
    return this._isReversed;
  }

  width: number;
  left: number;
  selectedDoc: Doc;
  groupedSettings: { width: number, left: number, top: number };
  @ViewChild(TlGapComponent) gapRef: TlGapComponent;
  @ViewChildren(TlDocComponent) docRefs: QueryList<TlDocComponent>;

  constructor(private timelineService: TimelineService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    if (this.employment.additionalRows) {
      this.rows += this.employment.additionalRows;
    }

    if (this.employment.documents) {
      this.addLineUnderPSAndW2();
    }

    merge(
      this.timelineService.documentChanges,
      this.timelineService.onDocumentFocus,
      this.timelineService.onDocumentHover
    ).pipe(takeUntil(this.onComponentDestroy$))
      .subscribe((document: Doc) => {
        this.selectedDoc = document;
        this.cdRef.detectChanges();
      });

    this.timelineService.employmentChanges
      .pipe(
        filter(empl => empl.type === EmploymentTypes.Gap),
        takeUntil(this.onComponentDestroy$)
      ).subscribe(gap => {
      this.selectedDoc = gap as any;
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.onComponentDestroy$.next();
    this.onComponentDestroy$.complete();
  }

  onItemClick(employment: Employment): void {
    this.timelineService.changeEmployment(employment);

    if (employment.documents.length) {
      this.timelineService.documentFocus(employment.documents[0]);
    }
  }

  private addLineUnderPSAndW2(): void {
    const missing = this.employment.documents.filter(value => value.status === DocStatus.Missing &&
      (value.type === DocTypes.PAYSTUB || value.type === DocTypes.W_2));
    if (missing.length === 0) {
      return;
    }
    const missingIds = missing.map(value => value.lineIndex);
    const getUniqueLineIds = new Set(missingIds);
    const startDate = this.getStartDate(missing);
    const endDate = this.getEndDate(missing);

    if (getUniqueLineIds.size === 1) {
      this.groupedSettings = {
        width: TimelineCalculation.calculateWidthForDateRange(startDate, endDate, this.settings.monthWidth) - 4,
        left: TimelineCalculation.getLeftPosition({startDate, endDate}, this.settings, this.isReversed),
        top: TimelineCalculation.getTopPosition(this.settings.rowHeight, Array.from(getUniqueLineIds)[0])
      };
    }
  }

  private getStartDate(missingDocs: Doc[]): Date {
    const startDateCollection = missingDocs.map((doc) => doc.startDate);
    return new Date(Math.min.apply(null, [...startDateCollection]));
  }

  private getEndDate(missingDocs: Doc[]): Date {
    const endDateCollection = missingDocs.map((doc) => {
      if (doc.endDate === undefined) {
        return this.settings.referenceDate;
      }
      return doc.endDate;
    });

    return new Date(Math.max.apply(null, [...endDateCollection]));
  }

  isMissingDocFocused(document: Doc) {
    return this.selectedDoc != null
      && document.status === DocStatus.Missing
      && this.selectedDoc.status === DocStatus.Missing
      && this.employment.documents.includes(this.selectedDoc);
  }

}
