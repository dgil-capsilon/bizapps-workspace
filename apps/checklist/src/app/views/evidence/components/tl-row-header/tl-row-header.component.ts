import {Component, Input} from '@angular/core';
import {TimelineEmploymentDetails} from '@models/data';

@Component({
  selector: 'chk-tl-row-header',
  templateUrl: './tl-row-header.component.html',
  styleUrls: ['./tl-row-header.component.scss']
})
export class TlRowHeaderComponent {
  private defaultRowNumber = 3;
  private _employment: TimelineEmploymentDetails;
  rowHeight = 24;

  @Input() set employment(data: TimelineEmploymentDetails) {
    this._employment = data;
    this.employmentHeight = this.calculateEmploymentHeight(this._employment);
  }

  get employment(): TimelineEmploymentDetails {
    return this._employment;
  }

  @Input() showDates = true;

  employmentHeight: number;

  private calculateEmploymentHeight(employment: TimelineEmploymentDetails): number {
    let height = 0;
    height += employment.additionalRows ?
      (this.defaultRowNumber + employment.additionalRows) * this.rowHeight
      : this.defaultRowNumber * this.rowHeight;
    height += 5; // 1px between rows, 2 + 2 top, bottom paddings
    return height;
  }

}
