import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocNamePipeMock} from '@app/test/doc-name.pipe';
import {TlRowHeaderComponent} from './tl-row-header.component';
import {ToDateModule} from '@app/modules/to-date/to-date.module';

describe('TlRowHeaderComponent', () => {
  let component: TlRowHeaderComponent;
  let fixture: ComponentFixture<TlRowHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TlRowHeaderComponent,
        DocNamePipeMock
      ],
      imports: [ToDateModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlRowHeaderComponent);
    component = fixture.componentInstance;
    component.employment = {employerName: null, startDate: null, endDate: null} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
