import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {TlMarkComponent} from './tl-mark.component';

describe('TlMarkComponent', () => {
  let component: TlMarkComponent;
  let fixture: ComponentFixture<TlMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TlMarkComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not set reversed variable', () => {
    component.isReversed = undefined;

    expect(component.isReversed).toBeUndefined();
  });

  it('should calculate left position when timeline being reversed', () => {
    spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(1);

    component.isReversed = true;

    expect(TimelineCalculation.getLeftPosition).toHaveBeenCalled();
    expect(component.left).toBeDefined();
  });

  it('should calculate left position when date changes', () => {
    spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(1);

    component.date = new Date();

    expect(TimelineCalculation.getLeftPosition).toHaveBeenCalled();
    expect(component.left).toBeDefined();
  });
});
