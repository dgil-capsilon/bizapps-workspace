import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { DrawOptions } from '@models/data';
import { TimelineCalculation } from '@app/services/timeline-calculation';

@Component({
  selector: 'chk-tl-mark',
  templateUrl: './tl-mark.component.html',
  styleUrls: ['./tl-mark.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TlMarkComponent {
  @HostBinding('style.left.px') left: number;
  @Input() set options(options: {settings: DrawOptions, date: Date, isReversed: boolean}) {
    this.left = TimelineCalculation.getLeftPosition(null, options.settings, options.isReversed, options.date);
  }
}
