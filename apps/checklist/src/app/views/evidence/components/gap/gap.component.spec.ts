import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GapComponent} from './gap.component';
import {ToDatePipeMock} from 'src/app/test/to-date.pipe';

describe('GapComponent', () => {
  let component: GapComponent;
  let fixture: ComponentFixture<GapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GapComponent,
        ToDatePipeMock
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GapComponent);
    component = fixture.componentInstance;
    component.data = {durationInDays: null, startDate: null, endDate: null} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get element ref', () => {
    expect(component.ref).toBeTruthy();
  });

  it('should check if component have data', () => {
    const data = {durationInDays: null, startDate: null, endDate: null} as any;
    expect(component.hasData(data)).toBeFalsy();
  });

  it('should get months gap when days are the same and months/years are different', () => {
    const data = {startDate: new Date('2018-01-17'), endDate: new Date('2019-03-17')} as any;
    expect(component.monthGap(data)).toBe('14 MONTH GAP');
  });

  it('should get months gap when days are different and months/years are the same', () => {
    const data = {startDate: new Date('2019-01-17'), endDate: new Date('2019-01-21')} as any;
    expect(component.monthGap(data)).toBe('4 DAY GAP');
  });

  it('should get months gap when all values are different', () => {
    const data = {startDate: new Date('2018-03-20'), endDate: new Date('2019-01-18')} as any;
    expect(component.monthGap(data)).toBe('9 MONTH AND 29 DAY GAP');
  });
});
