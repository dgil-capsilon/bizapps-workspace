import {ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input} from '@angular/core';
import {Gap} from '@models/data';
import {SnippetElement} from '@models/snippet.element.interface';
import {LocalDate, Period} from 'js-joda';

@Component({
  selector: 'chk-gap',
  templateUrl: './gap.component.html',
  styleUrls: ['./gap.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GapComponent implements SnippetElement {

  @Input() data: Gap;
  @Input() months = false;

  @HostBinding('attr.scroll-id')
  @Input()
  scrollId: string;
  @Input() displayMonths = false;

  constructor(private elRef: ElementRef<HTMLElement>) {
  }

  get ref() {
    return this.elRef;
  }

  hasData(data) {
    return this.data === data;
  }

  monthGap(data: Gap): string {
    const start = LocalDate.of(data.startDate.getFullYear(), data.startDate.getMonth() + 1, data.startDate.getDate());
    const end = LocalDate.of(data.endDate.getFullYear(), data.endDate.getMonth() + 1, data.endDate.getDate());
    const period = Period.between(start, end);
    const months = period.months() + period.years() * 12;
    const days = period.days();
    let text = '';
    if (months > 0) {
      text += months + ' MONTH';
    }
    if (text !== '' && days > 0) {
      text += ` AND ${days} DAY`;
    } else if (days > 0) {
      text += `${days} DAY`;
    }
    return text + ' GAP';
  }
}
