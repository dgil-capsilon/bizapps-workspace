import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {TimelineService} from '../../services/timeline.service';
import {DocNamePipeMock} from '@app/test/doc-name.pipe';
import {TlDocComponent} from './tl-doc.component';
import createSpy = jasmine.createSpy;

describe('TlDocComponent', () => {
  let component: TlDocComponent;
  let fixture: ComponentFixture<TlDocComponent>;
  const timelineServiceMock = {
    changeDocument: createSpy(),
    documentHover: createSpy()
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TlDocComponent,
        DocNamePipeMock
      ],
      providers: [
        {provide: TimelineService, useValue: timelineServiceMock},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlDocComponent);
    component = fixture.componentInstance;
    // Component only uses "type" property of the object, the "any" is here to suppress error about interface
    component.document = {type: null, startDate: new Date()} as any;
    component.drawOptions = {rowHeight: null, monthWidth: null, months: null, referenceDate: new Date()} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate left position', () => {
    spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(1);

    component.isReversed = false;

    expect(TimelineCalculation.getLeftPosition).toHaveBeenCalled();
    expect(component.left).toBeDefined();
  });

  it('should calculate top position', () => {
    spyOn(TimelineCalculation, 'getTopPosition').and.returnValue(1);

    component.isReversed = false;

    expect(TimelineCalculation.getTopPosition).toHaveBeenCalled();
    expect(component.top).toBeDefined();
  });

  it('should calculate width', () => {
    spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);

    component.isReversed = false;

    expect(TimelineCalculation.calculateWidthForDateRange).toHaveBeenCalled();
    expect(component.width).toBeDefined();
  });

  it('should not calculate any property', () => {
    component.isReversed = undefined;

    expect(component.width).toBeUndefined();
    expect(component.top).toBeUndefined();
    expect(component.left).toBeUndefined();
  });

  it('should change document when component is clicked', () => {
    const timelineDoc: DebugElement = fixture.debugElement;
    timelineDoc.triggerEventHandler('click', null);
    expect(timelineServiceMock.changeDocument).toHaveBeenCalled();
  });

  it('should change document when component is clicked', () => {
    const timelineDoc: DebugElement = fixture.debugElement;
    timelineDoc.triggerEventHandler('mouseenter', null);
    expect(timelineServiceMock.documentHover).toHaveBeenCalledTimes(1);
    timelineDoc.triggerEventHandler('mouseleave', null);
    expect(timelineServiceMock.documentHover).toHaveBeenCalledTimes(2);
  });
});
