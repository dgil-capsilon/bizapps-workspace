import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  Renderer2,
  ViewChild
} from '@angular/core';
import { MatTooltip } from '@angular/material/tooltip';
import { TimelineCalculation } from '@app/services/timeline-calculation';
import { DocStatus, DocTypes } from '@models/data.enum';
import { Doc, DrawOptions } from '@models/data.interface';
import { isUndefined } from 'util';
import { TimelineService } from '../../services/timeline.service';

@Component({
  selector: 'chk-tl-doc',
  templateUrl: './tl-doc.component.html',
  styleUrls: ['./tl-doc.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TlDocComponent implements AfterViewInit {
  @ViewChild(MatTooltip) matTooltip: MatTooltip;
  @ViewChild('docName') docNameElRef: ElementRef<HTMLElement>;
  @ViewChild('docOverlay') docOverlayElRef: ElementRef<HTMLDivElement>;

  @HostBinding('style.width.px') width: number;
  @HostBinding('style.top.px') top: number;
  @HostBinding('style.left.px') left: number;

  @Input() set options(options: any) {
    if (isUndefined(options)) {
      return;
    }
    this._isReversed = options.isReversed;
    this.document = options.document;
    this.index = options.index;
    this.drawOptions = options.drawOptions;
    this.width = TimelineCalculation.calculateWidthForDateRange(
      this.document.startDate,
      this.document.endDate || this.drawOptions.referenceDate,
      this.drawOptions.monthWidth);
    this.top = TimelineCalculation.getTopPosition(this.drawOptions.rowHeight, this.index);
    this.left = TimelineCalculation.getLeftPosition(this.document, this.drawOptions, this._isReversed);
    this.isPs = this.document.type === DocTypes.PAYSTUB;
    this.isMissing = this.document.status === DocStatus.Missing;

    if (options.document.overlay) {
      // 2px right border
      this.overlayWidth = TimelineCalculation.calculateWidthForDateRange(
        this.document.overlay.startDate,
        this.document.overlay.endDate,
        this.drawOptions.monthWidth) - 2;

      this.docLimitedWidth = this.width - this.overlayWidth;

      this.renderer.setStyle(this.docNameElRef.nativeElement, 'max-width', `${this.docLimitedWidth}px`);
      this.renderer.setStyle(this.docOverlayElRef.nativeElement, this._isReversed ? 'left' : 'right', '2px');
    }
  };

  document: Doc;
  index: number;
  drawOptions: DrawOptions;

  @HostBinding('class.reversed')
  private _isReversed: boolean;

  private docLimitedWidth: number;
  shouldShowTooltip: boolean;
  isPs: boolean;
  isMissing: boolean;
  overlayWidth: number;

  constructor(private timelineService: TimelineService,
              private cdRef: ChangeDetectorRef,
              private renderer: Renderer2) {
  }

  ngAfterViewInit(): void {
    let hostOffsetWidth = Math.round(this.width);

    if (this.document.overlay) {
      hostOffsetWidth = this.docLimitedWidth;
    }

    const docNameScrollWidth = this.docNameElRef.nativeElement.scrollWidth;

    this.shouldShowTooltip = docNameScrollWidth > hostOffsetWidth;

    this.setDocNameTextSize();
    this.cdRef.detectChanges();
  }

  @HostListener('click')
  onClick() {
    this.timelineService.changeDocument(this.document);
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.timelineService.documentHover(this.document);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.timelineService.documentHover(null);
  }

  private setDocNameTextSize() {
    if (this.width < 15) {
      this.renderer.setStyle(this.docNameElRef.nativeElement, 'font-size', `9px`);
    } else if (this.width >= 15 && this.width <= 20) {
      this.renderer.setStyle(this.docNameElRef.nativeElement, 'font-size', `11px`);
    }
  }
}
