import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TlMarkLabelComponent} from './tl-mark-label.component';

describe('TlMarkLabelComponent', () => {
  let component: TlMarkLabelComponent;
  let fixture: ComponentFixture<TlMarkLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TlMarkLabelComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlMarkLabelComponent);
    component = fixture.componentInstance;
    component.timelineMarkers = [];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
