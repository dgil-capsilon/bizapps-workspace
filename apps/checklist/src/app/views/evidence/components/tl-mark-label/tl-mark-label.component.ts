import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {DrawOptions} from '@models/data';
import {TimelineMarker} from '@models/data/app/rules/timeline/timeline-marker.interface';
import {isUndefined} from 'util';

@Component({
  selector: 'chk-tl-mark-label',
  templateUrl: './tl-mark-label.component.html',
  styleUrls: ['./tl-mark-label.component.scss']
})
export class TlMarkLabelComponent implements OnInit {
  @HostBinding('style.left.px') left: number;
  @HostBinding('style.width.px') width: number;
  @Input() timelineMarkers: TimelineMarker[];
  @Input() settings: DrawOptions;

  private _isReversed: boolean;
  @Input() set isReversed(value) {
    if (isUndefined(value)) {
      return;
    }

    this._isReversed = value;
    this.calculatePositionAndDimension();
  }

  ngOnInit() {
    this.calculatePositionAndDimension();
  }

  calculatePositionAndDimension() {
    if (!this.timelineMarkers || !this.timelineMarkers.length) {
      return;
    }

    const [firstDate, secondDate] = this.timelineMarkers
      .map(timelineMarker => timelineMarker.markerPosition)
      .sort((a, b) => b.getTime() - a.getTime());

    const firstDateLeftPos = TimelineCalculation.getLeftPosition(null, this.settings, this._isReversed, firstDate);
    const secondDateLeftPos = TimelineCalculation.getLeftPosition(null, this.settings, this._isReversed, secondDate);

    this.left = this._isReversed ? secondDateLeftPos : firstDateLeftPos;
    this.width = secondDateLeftPos - firstDateLeftPos;
  }

}
