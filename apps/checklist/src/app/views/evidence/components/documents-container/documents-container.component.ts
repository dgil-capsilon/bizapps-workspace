import {Platform} from '@angular/cdk/platform';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChildren
} from '@angular/core';
import {DocumentCardComponent} from '@app/modules/document-card/document-card.component';
import {ScrollService} from '@app/services/scroll.service';
import {IsNotEvaluated} from '@decorators/is-not-evaluated.decorator';
import {BaseStatuses} from '@models/base-statuses';
import {EmploymentDataValidationApi} from '@models/data';
import {Doc, Employment, Gap} from '@models/data/app';
import {LoanTypeDetails} from '@models/data/app/rules/timeline/loan-type-details.interface';
import {DocStatus, DocTypes, EmploymentTypes, RuleCode} from '@models/enums';
import {SnippetElement} from '@models/snippet.element.interface';
import {combineLatest, merge, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TimelineService} from '../../services/timeline.service';

const STICKY_OFFSET = 59; // 51px height of header + 8px top margin snippet

@IsNotEvaluated()
@Component({
  selector: 'chk-documents-container',
  templateUrl: './documents-container.component.html',
  styleUrls: ['./documents-container.component.scss']
})
export class DocumentsContainerComponent extends BaseStatuses implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  private destroy$ = new Subject<void>();
  private snippetsLoaded = new Subject<void>();
  private scrollToUsed = false;
  @Input() rulesData: Employment[];
  @Input() borrowerName: string;
  @Input() ruleCode: RuleCode | string;
  @Input() loanTypeDetails: LoanTypeDetails;

  @ViewChildren('docs') docs: QueryList<ElementRef>;
  @ViewChildren('snippet') snippets: QueryList<SnippetElement>;

  focusedSnippet: SnippetElement;
  stickyIndex: number;
  top: number;
  CNA_DOC_HEADER_ID = 0;
  CNA_DOC_HEADER_Z_INDEX = 10;

  constructor(private elRef: ElementRef<HTMLElement>,
              private renderer: Renderer2,
              private timelineService: TimelineService,
              private scrollService: ScrollService,
              private cdRef: ChangeDetectorRef,
              public platform: Platform) {
    super();
  }

  @HostListener('scroll', ['$event.target.scrollTop'])
  onScroll(scrollTop): void {
    if (this.platform.TRIDENT) {
      this.setStickyHeaderForIE(scrollTop);
    }
  }

  ngOnChanges() {
    if (this.platform.TRIDENT) {
      this.top = 0;
      this.elRef.nativeElement.scrollTop = 0;
    }
  }

  ngOnInit(): void {
    combineLatest([this.timelineService.documentChanges, this.snippetsLoaded])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([document]) => {
        this.handleScrollAndFocusedSnippet(document, document.spScrollId);
      });

    combineLatest([this.timelineService.employmentChanges, this.snippetsLoaded])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([employment]) => {
        const scrollId = employment.documents && employment.documents.length > 0 ?
          employment.documents[0].spScrollId : employment.spScrollId;
        const useDefaultScroll = this.scrollToGapForIE(employment);
        if (useDefaultScroll) {
          this.handleScrollAndFocusedSnippet(employment, scrollId);
        }
      });

    merge(this.timelineService.onDocumentFocus, this.timelineService.onDocumentHover).pipe(takeUntil(this.destroy$)).subscribe(doc => {
      this.updateFocusedSnippet(doc);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  isNotMissing(status: DocStatus): boolean {
    return status !== DocStatus.Missing;
  }

  isGap(type: EmploymentTypes): boolean {
    return type === EmploymentTypes.Gap;
  }

  getDataValidationForDocType(docType: DocTypes, employment: Employment) {
    return employment.dataValidation && employment.dataValidation.filter((data: EmploymentDataValidationApi) => data.docType === docType);
  }

  private scrollToGapForIE(employment: Employment): boolean {
    const isGap = employment.type === EmploymentTypes.Gap;
    if (isGap && this.platform.TRIDENT) {
      const index = this.rulesData.indexOf(employment);
      const isStickyOnActualEmployment = index === this.stickyIndex;
      const partOfStickyVisibleAndScrollToUsed = index <= this.stickyIndex && this.scrollToUsed;
      if (isStickyOnActualEmployment || partOfStickyVisibleAndScrollToUsed) {
        this.scroll(employment.spScrollId, !isGap, -STICKY_OFFSET);
        this.updateFocusedSnippet(employment);
        return false;
      }
    }
    return true;
  }

  private updateFocusedSnippet(item: Doc | Employment | Gap): void {
    if (!this.snippets) {
      return;
    }

    this.focusedSnippet = this.snippets.find(snippet => snippet.hasData(item));
    this.cdRef.detectChanges();
  }

  ngAfterViewInit(): void {
    this.snippets.changes.subscribe(() => {
      this.handleSnippetsLoadEvent();
    });

    if (this.snippets.length) {
      this.handleSnippetsLoadEvent();
    }
  }

  private handleSnippetsLoadEvent() {
    combineLatest(
      this.snippets.filter((document: DocumentCardComponent) => document.onSnippetLoad != null)
        .map((document: DocumentCardComponent) => document.onSnippetLoad)
    ).pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.addLastItemMargin();
      this.snippetsLoaded.next();
    });
  }

  private addLastItemMargin(): void {
    const lastSnippet = this.snippets.last;
    if (!lastSnippet) {
      return;
    }
    const lastItemRef = lastSnippet.ref.nativeElement, containerRef = this.elRef.nativeElement;

    if (containerRef.scrollHeight > containerRef.offsetHeight) {
      let margin = lastItemRef.offsetTop - (containerRef.scrollHeight - containerRef.offsetHeight);
      margin -= STICKY_OFFSET - 8; // 8px of margin top should not be counted here
      this.renderer.setStyle(lastItemRef, 'margin-bottom', `${margin}px`);
    }
  }

  trackByIndex(index: number): number {
    return index;
  }

  private handleScrollAndFocusedSnippet(employment: Doc | Employment | Gap, scrollId: string): void {
    const isGap = employment.type === 'GAP';
    this.scroll(scrollId, !isGap);
    this.updateFocusedSnippet(employment);
  }

  private scroll(target: string, includeOffset = true, additionalOffset = 0) {
    if (!target) {
      return;
    }
    this.scrollToUsed = true;

    const scrollFinishEmitter = new EventEmitter<void>();

    scrollFinishEmitter.subscribe(() => {
      this.timelineService.scrollFinish();
      this.scrollToUsed = false;
    });

    this.scrollService.scrollToElement({
      target: target,
      container: 'evidence-container',
      duration: 600,
      vertical: true,
      scrollFinishedEmitter: scrollFinishEmitter,
      offset: includeOffset ? -STICKY_OFFSET + additionalOffset : additionalOffset
    });
  }

  private stickyHeaderOffset(): number {
    if (this.scrollToUsed && this.platform.TRIDENT) {
      return -STICKY_OFFSET;
    }
    return 0;
  }

  private setStickyHeaderForIE(scrollTop): void {
    this.top = this.docs.toArray()[0].nativeElement.parentElement.offsetTop;

    const scroll = scrollTop;

    let offset = this.stickyHeaderOffset();
    this.docs.forEach((doc, index) => {
      let height = doc['nativeElement'].offsetHeight;

      const top = offset + height;
      const bottom = offset;

      if (this.stickyIndex === index) {
        height += STICKY_OFFSET;
      }

      if (bottom < scroll && scroll <= top && this.stickyIndex !== index) {
        this.stickyIndex = this.loanTypeDetails ? index : index + 1;
      }

      offset += height;
    });
  }

}
