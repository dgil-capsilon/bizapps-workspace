import {Component, ElementRef, NO_ERRORS_SCHEMA, Renderer2, Type} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {of} from 'rxjs';
import {TimelineService} from '../../services/timeline.service';
import {DocumentsContainerComponent} from './documents-container.component';
import createSpy = jasmine.createSpy;
import { FakeMatTooltipDirective } from 'src/app/test/mat-tooltip.directive';
import { FakeEmployerAliasesPipe } from 'src/app/test/employer-aliases.pipe';
import { ScrollService } from 'src/app/services/scroll.service';
import { DocHeaderComponent } from 'src/app/modules/doc-header/doc-header/doc-header.component';
import { EmploymentTypes, DocStatus, DocTypes } from 'src/app/models/enums';

@Component({
  selector: 'chk-document-card',
  template: '<div style="min-height: 150px;background: green;"></div>',
  styles: [':host {min-width: 850px}']
})
class MockDocumentCardComponent {
  constructor(public ref: ElementRef<HTMLElement>) {
  }
}

@Component({
  selector: 'chk-gap',
  template: '<div style="min-height: 150px;background: red"></div>',
  styles: [':host {min-width: 850px}']
})
class MockGapComponent {
  constructor(public ref: ElementRef<HTMLElement>) {
  }
}

describe('DocumentsContainerComponent', () => {
  let component: DocumentsContainerComponent;
  let fixture: ComponentFixture<DocumentsContainerComponent>;
  let timelineService: any;
  const scrollServiceMock = {scrollToElement: createSpy()};
  const timelineServiceMock = {documentChanges: of(), employmentChanges: of(), onDocumentFocus: of(), onDocumentHover: of()};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockDocumentCardComponent,
        MockGapComponent,
        DocumentsContainerComponent,
        DocHeaderComponent,
        FakeMatTooltipDirective,
        FakeEmployerAliasesPipe
      ],
      providers: [
        {provide: ScrollService, useValue: scrollServiceMock},
        {provide: TimelineService, useValue: timelineServiceMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    scrollServiceMock.scrollToElement.calls.reset();

    fixture = TestBed.createComponent(DocumentsContainerComponent);
    component = fixture.componentInstance;
    timelineService = fixture.componentRef.injector.get(TimelineService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate last snippet bottom margin', fakeAsync(() => {
    const renderer = fixture.componentRef.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
    spyOn(renderer, 'setStyle');
    spyOnProperty(fixture.nativeElement, 'scrollHeight', 'get').and.returnValue(1260);
    spyOnProperty(fixture.nativeElement, 'offsetHeight', 'get').and.returnValue(453);

    const lastItem = {
      ref: {
        nativeElement: {offsetTop: 1107, offsetHeight: 145}
      }
    };
    component.snippets = {
      changes: of(null),
      last: lastItem,
      find: () => null,
      filter: () => [{onSnippetLoad: of(null)}],
      map: () => null
    } as any;

    component.ngAfterViewInit();
    tick();

    expect(renderer.setStyle).toHaveBeenCalledWith(
      {offsetTop: 1107, offsetHeight: 145},
      'margin-bottom',
      '249px'
    );
  }));

  it('should scroll after all snippets are loaded (doc)', fakeAsync(() => {
    component.snippets = {
      changes: of(null),
      last: null,
      find: () => null,
      filter: () => [{onSnippetLoad: of(null)}]
    } as any;
    timelineService.documentChanges = of({status: DocStatus.Valid, type: DocTypes.URLA, spScrollId: 'sp-doc-0'});
    component.rulesData = [];

    component.ngOnInit();

    expect(scrollServiceMock.scrollToElement).not.toHaveBeenCalled();

    component.ngAfterViewInit();
    tick();

    expect(scrollServiceMock.scrollToElement).toHaveBeenCalled();
  }));

  it('should scroll after all snippets are loaded (empl/gap)', fakeAsync(() => {
    component.snippets = {
      changes: of(null),
      last: null,
      find: () => null,
      filter: () => [{onSnippetLoad: of(null)}]
    } as any;
    timelineService.employmentChanges = of({type: EmploymentTypes.Employment, spScrollId: 'sp-header-0'});
    component.rulesData = [];

    component.ngOnInit();

    expect(scrollServiceMock.scrollToElement).not.toHaveBeenCalled();

    component.ngAfterViewInit();
    tick();

    expect(scrollServiceMock.scrollToElement).toHaveBeenCalled();
  }));
});
