import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {of} from 'rxjs/internal/observable/of';
import {DocStatus, EmploymentTypes, RuleStatus} from '../../../../models/data.enum';
import {ScrollService} from '@app/services/scroll.service';
import {TimelineService} from '../../services/timeline.service';
import {TimelineComponent} from './timeline.component';
import createSpy = jasmine.createSpy;

describe('TimelineComponent', () => {
  let component: TimelineComponent;
  let fixture: ComponentFixture<TimelineComponent>;
  const timelineServiceMock = {
    changeEmployment: createSpy(),
    changeDocument: createSpy(),
    documentChanges: of({}),
    employmentChanges: of({})
  };
  const scrollServiceMock = {scrollToElement: createSpy(), scrollBy: createSpy()};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineComponent],
      providers: [
        {provide: TimelineService, useValue: timelineServiceMock},
        {provide: ScrollService, useValue: scrollServiceMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    scrollServiceMock.scrollBy.calls.reset();

    fixture = TestBed.createComponent(TimelineComponent);
    component = fixture.componentInstance;
    component.employments = [{type: EmploymentTypes.Employment, documents: []}] as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should scroll horizontally right', fakeAsync(() => {
    const fakeEvent = {deltaY: 125, deltaX: 0};
    const timelineEl: DebugElement = fixture.debugElement.query(By.css('.employments-layer'));
    component.employmentsLayer = {nativeElement: {scrollLeft: 0}};

    timelineEl.triggerEventHandler('wheel', fakeEvent);

    expect(scrollServiceMock.scrollBy).toHaveBeenCalledWith(jasmine.objectContaining({
      container: {nativeElement: {scrollLeft: 0}},
      by: 300,
      vertical: false,
      duration: 600
    }));
  }));

  it('should scroll horizontally left', fakeAsync(() => {
    const fakeEvent = {deltaY: -125, deltaX: 0};
    const timelineEl: DebugElement = fixture.debugElement.query(By.css('.employments-layer'));
    component.employmentsLayer = {nativeElement: {scrollLeft: 500}};

    timelineEl.triggerEventHandler('wheel', fakeEvent);

    expect(scrollServiceMock.scrollBy).toHaveBeenCalledWith(jasmine.objectContaining({
      container: {nativeElement: {scrollLeft: 500}},
      by: -300,
      vertical: false,
      duration: 600
    }));
  }));

  it('should prevent mouse wheel scrolling when jobs > 3', () => {
    spyOn(window, 'setInterval');
    spyOn(window, 'clearInterval');
    component.employments = [
      {type: EmploymentTypes.Employment, documents: []},
      {type: EmploymentTypes.Employment, documents: []},
      {type: EmploymentTypes.Employment, documents: []},
      {type: EmploymentTypes.Employment, documents: []}
    ] as any;
    const timelineEl: DebugElement = fixture.debugElement.query(By.css('.timeline-wrapper'));
    const fakeEvent = {deltaY: 125, deltaX: 0};

    timelineEl.triggerEventHandler('wheel', fakeEvent);

    expect(window.setInterval).not.toHaveBeenCalled();
    expect(window.clearInterval).not.toHaveBeenCalled();
  });

  it('should scroll to first missing doc', fakeAsync(() => {
    component.ruleStatus = RuleStatus.Failed;
    component.employments = [
      {
        type: EmploymentTypes.Employment,
        documents: [{id: 1, status: DocStatus.Valid}]
      },
      {
        type: EmploymentTypes.Employment,
        documents: [
          {id: 2, status: DocStatus.Valid},
          {id: 3, status: DocStatus.Missing},
          {id: 4, status: DocStatus.Valid}
        ]
      }
    ] as any;

    component.ngOnInit();
    tick();

    expect(timelineServiceMock.changeDocument).toHaveBeenCalledWith({id: 3, status: DocStatus.Missing});
  }));

  it('should scroll to gap', fakeAsync(() => {
    component.ruleStatus = RuleStatus.Failed;
    component.employments = [
      {type: EmploymentTypes.Employment, documents: [{id: 1, status: DocStatus.Valid}]},
      {type: EmploymentTypes.Gap, durationInDays: 61}
    ] as any;

    component.ngOnInit();
    tick();

    expect(timelineServiceMock.changeEmployment).toHaveBeenCalledWith({type: EmploymentTypes.Gap, durationInDays: 61});
  }));

  it('should be at least 2 years on timeline when jobs are shorten', () => {
    component.referenceDate = new Date('2019-03-17');
    component.employments = [
      {type: EmploymentTypes.Employment, startDate: new Date('2018-04-03'), documents: []},
      {type: EmploymentTypes.Employment, startDate: new Date('2017-07-03'), documents: []},
    ] as any;
    const length = component.yearsData.length - 1;
    const last = component.yearsData[length];
    expect(last.year).toBe('2017');
    expect(last.months.length).toBe(10);
  });

  it('should be ~3 years in timeline from reference date.', () => {
    component.referenceDate = new Date('2019-03-17');
    component.employments = [
      {type: EmploymentTypes.Employment, startDate: new Date('2016-04-03'), documents: []},
      {type: EmploymentTypes.Employment, startDate: new Date('2017-07-03'), documents: []},
    ] as any;
    const length = component.yearsData.length - 1;
    const last = component.yearsData[length];
    expect(last.year).toBe('2016');
    expect(last.months.length).toBe(9);
  });

  it('should be ~3 years in timeline from reference date calculated from document date.', () => {
    component.referenceDate = new Date('2019-03-17');
    component.employments = [
      {type: EmploymentTypes.Employment, startDate: new Date('2017-04-03'), documents: [{startDate: new Date('2016-04-03')}]},
      {type: EmploymentTypes.Employment, startDate: new Date('2017-07-03'), documents: [{startDate: new Date('2017-03-03')}]},
    ] as any;
    const length = component.yearsData.length - 1;
    const last = component.yearsData[length];
    expect(last.year).toBe('2016');
    expect(last.months.length).toBe(9);
  });
});
