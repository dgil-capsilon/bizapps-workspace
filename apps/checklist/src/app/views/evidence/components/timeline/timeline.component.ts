import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ScrollService} from '@app/services/scroll.service';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {DocStatus, EmploymentTypes, RuleCode, RuleStatus} from '@models/data.enum';
import {Doc, DrawOptions, Gap, TimelineEmploymentDetails, YearsData} from '@models/data.interface';
import {TimelineMarkerApi} from '@models/data/api/rules/timeline/timeline-marker.interface';
import {LocalDate, Period} from 'js-joda';
import {fromEvent, merge, Subject} from 'rxjs';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {TimelineService} from '../../services/timeline.service';

@Component({
  selector: 'chk-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit, OnDestroy, AfterViewInit {

  employmentsLayerHeight = 0;
  @Input()
  timelineMarkers: TimelineMarkerApi[];
  @Input() ruleCode: string;
  private defaultRowNumber = 3;
  private _referenceDate: Date;
  @Input() set referenceDate(v: Date) {
    this._referenceDate = v;
    this.drawOptions.referenceDate = v;
  }

  get referenceDate(): Date {
    return this._referenceDate;
  }

  @Input() set employments(data: Array<TimelineEmploymentDetails>) {
    if (!data || data.length < 0) {
      console.error('Employment details is empty.');
      return;
    }
    if (data.length > 0) {
      this._employments = this.filterEmployments(data, EmploymentTypes.Employment) as TimelineEmploymentDetails[]; // TODO: Cast is needed?
      this._gaps = this.filterEmployments(data, EmploymentTypes.Gap) as Gap[]; // // TODO: Cast is needed?
      this.employmentsLayerHeight = this.calculateEmploymentsLayerHeight(this.employments);
      if (this.months) {
        this.calculateDrawingOptions();
      }
      this.redrawTimeline(data);
    }
  }

  get employments(): TimelineEmploymentDetails[] {
    return this._employments;
  }

  get gaps(): Gap[] {
    return this._gaps;
  }

  get INC1100Gaps() {
    const ruleCodeAndStatusCheck = this.ruleCode === RuleCode.INC1100 && this.ruleStatus === RuleStatus.Failed;

    return ruleCodeAndStatusCheck ? this.gaps.filter(gap => gap.active) : [];
  }

  /**
   * Workaround for jumping timeline when scroll is showed up.
   */
  get employmentsLayerHeightHeader(): number {
    let height = this.employmentsLayerHeight - 8;

    if (this.timelineMarkers) {
      height += 20;
    }

    return height;
  }

  @Input() ruleStatus: RuleStatus;

  @ViewChild('timeline') timeline: ElementRef;
  @ViewChild('employmentsLayer') employmentsLayer: ElementRef;
  @ViewChild('tableHeader') tableHeader: ElementRef;
  @ViewChild('yearsHeader') yearsHeader: ElementRef;

  private _employments: TimelineEmploymentDetails[];
  private _gaps: Gap[];
  private onComponentDestroy$ = new Subject<void>();
  private readonly TIMELINE_SCROLL_OFFSET = -48;
  yearsData: Array<YearsData> = [];
  months: Array<string>;
  rowHeight = 24;
  calendarWidth: number;
  monthWidth: number;
  currentDate: Date;
  isReversed = false;
  drawOptions: DrawOptions = {
    monthWidth: null,
    referenceDate: null,
    rowHeight: null,
    calendarStartDate: null,
    months: []
  };
  scrollWidth: number;

  constructor(private timelineService: TimelineService,
              private renderer: Renderer2,
              private scrollService: ScrollService) {
    this.scrollWidth = this.getScrollbarWidth();
  }

  ngOnInit(): void {
    merge(this.timelineService.documentChanges, this.timelineService.employmentChanges)
      .pipe(takeUntil(this.onComponentDestroy$)).subscribe((item: TimelineEmploymentDetails | Doc) => {
      this.scroll(item);
    });

    this.calculateDrawingOptions();

    setTimeout(() => {
      if (this.ruleStatus === RuleStatus.Failed) {
        this.scrollToFailReasonElement();
      }
    }, 0);
  }

  ngAfterViewInit(): void {
    const isManyEmployments = this.employments.length <= 3;
    if (isManyEmployments) {
      this.employmentsLayerEnters();
      this.employmentsLayerLeaves();
    }
  }

  ngOnDestroy(): void {
    this.onComponentDestroy$.next();
    this.onComponentDestroy$.complete();
  }

  private calculateDrawingOptions(): void {
    this.drawOptions.referenceDate = this.referenceDate;
    this.drawOptions.rowHeight = this.rowHeight;
    this.handleDifferentDevices();
  }

  private employmentsLayerEnters(): void {
    this.layerEventEnter('mouseenter');
    this.layerEventEnter('touchstart');
  }

  private layerEventEnter(event: string): void {
    fromEvent(this.employmentsLayer.nativeElement, event)
      .pipe(takeUntil(this.onComponentDestroy$)).subscribe(() => {
      this.adjustEmploymentsHeightToScroll(1);
    });
  }

  private employmentsLayerLeaves(): void {
    this.layerEventLeave('mouseleave');
    this.layerEventLeave('touchend');
  }

  private layerEventLeave(event: string): void {
    fromEvent(this.employmentsLayer.nativeElement, event)
      .pipe(takeUntil(this.onComponentDestroy$), debounceTime(500)).subscribe(() => {
      this.adjustEmploymentsHeightToScroll(-1);
    });
  }

  private adjustEmploymentsHeightToScroll(multiplier: number): void {
    this.employmentsLayerHeight = this.employmentsLayerHeight + this.scrollWidth * multiplier;
  }

  private filterEmployments(data: TimelineEmploymentDetails[], type: EmploymentTypes): TimelineEmploymentDetails[] | Gap[] {
    return data.filter((item) => {
      return item.type === type;
    });
  }

  private handleDifferentDevices(): void {
    if (window.devicePixelRatio >= 2) {
      this.countStyles(2);
    } else {
      this.countStyles(1);
    }
  }

  private redrawTimeline(employments: TimelineEmploymentDetails[]): void {
    [this.yearsData, this.months] = this.getYearsMonths(employments);
    this.drawOptions.months = this.months;
    this.currentDate = new Date();

    // TODO: check if this is needed.
    // this.timeline.nativeElement.scrollLeft = 0;
    this.isReversed = false;
  }

  private getYearsMonths(employments: TimelineEmploymentDetails[]): [Array<YearsData>, string[]] {
    const calendarStartDate = new Date(Math.min.apply(null, [...this.getAllStartDates(employments), this.minTwoYears()]));
    const calendarEndDate = new Date();
    const years = [];
    const months = [];
    const yearsTemp = {};
    this.drawOptions.calendarStartDate = calendarStartDate;
    for (let y = calendarStartDate.getFullYear(); y <= calendarEndDate.getFullYear(); y++) {
      let startMonth;
      let endMonth;

      startMonth = y === calendarStartDate.getFullYear() ? calendarStartDate.getMonth() : 0;
      endMonth = y === calendarEndDate.getFullYear() ? calendarEndDate.getMonth() : 11;

      for (let m = startMonth; m <= endMonth; m++) {
        if (!yearsTemp[y]) {
          yearsTemp[y] = [];
        }
        yearsTemp[y].push(TimelineCalculation.months[m]);
        months.push(TimelineCalculation.months[m]);
      }
    }
    for (const yearsTempKey of Object.keys(yearsTemp)) {
      years.push({year: yearsTempKey, months: yearsTemp[yearsTempKey].reverse()});
    }
    return [years.reverse(), months.reverse()];
  }

  private countStyles(multiplier: number): void {
    this.monthWidth = ((screen.width - 349 - 188 - 48) / 24) * multiplier;    // 349px left navbar, 188px table header, 48px padding
    this.drawOptions.monthWidth = this.monthWidth;
    this.calendarWidth = (this.months.length * (this.monthWidth) + 24 * multiplier);
  }

  onItemClick(employment: TimelineEmploymentDetails): void {
    this.scroll(employment);
    this.timelineService.changeEmployment(employment);

    if (employment.documents.length) {
      this.timelineService.documentFocus(employment.documents[0]);
    }
  }

  private getAllStartDates(employments: TimelineEmploymentDetails[]): Date[] {
    const employmentDates = employments.map((item) => item.startDate);
    const documentsDates = employments.map((item) => {
      if (item.documents) {
        return item.documents.map(doc => doc.startDate);
      }
      return;
    })
      .reduce((previousValue, currentValue) => [...previousValue, ...currentValue], []);
    return [...employmentDates, ...documentsDates];
  }

  private scroll(item: TimelineEmploymentDetails | Doc): void {
    if (item.tlScrollId === null) {
      return;
    }

    this.scrollService.scrollToElement({
      container: this.employmentsLayer,
      target: item.tlScrollId,
      vertical: false,
      duration: 600,
      offset: this.TIMELINE_SCROLL_OFFSET
    });
  }

  onReverse() {
    this.isReversed = !this.isReversed;
    this.months = this.months.reverse();
    this.yearsData = this.yearsData.reverse();
    this.yearsData.forEach((year) => {
      year['months'].reverse();
    });
    this.employmentsLayer.nativeElement.scrollLeft = this.isReversed ? this.employmentsLayer.nativeElement.scrollWidth : 0;
  }

  onScroll(e): void {
    if (this._employments.length > 3) {
      return;
    }

    if (e.deltaY !== 0 && e.deltaX === 0) {
      const forward = e.deltaY > 0;

      this.scrollService.scrollBy({
        container: this.employmentsLayer,
        by: forward ? 300 : -300,
        vertical: false,
        duration: 600
      });
    }
  }

  onCalendarScroll(e): void {
    this.tableHeader.nativeElement.scrollTop = e.target.scrollTop;
    this.yearsHeader.nativeElement.scrollLeft = e.target.scrollLeft;
  }

  scrollToCNADoc() {
    this.timelineService.changeDocument({
      spScrollId: 'sp-cna-doc',
      tlScrollId: null
    });
  }

  private getScrollbarWidth(): number {
    const outer = this.renderer.createElement('div');
    this.renderer.setStyle(outer, 'visibility', 'hidden');
    this.renderer.setStyle(outer, 'width', '100px');
    this.renderer.setStyle(outer, 'msOverflowStyle', 'scrollbar');

    this.renderer.appendChild(document.body, outer);

    const widthNoScroll = outer.offsetWidth;
    this.renderer.setStyle(outer, 'overflow', 'scroll');

    // add innerdiv
    const inner = this.renderer.createElement('div');
    this.renderer.setStyle(inner, 'width', '100%');
    this.renderer.appendChild(outer, inner);

    const widthWithScroll = inner.offsetWidth;

    // remove divs
    this.renderer.removeChild(document.body, outer);

    return widthNoScroll - widthWithScroll;
  }

  private scrollToFailReasonElement(): void {
    let employmentIndex = null, missingDocIndex = null;
    this._employments.find((employment: TimelineEmploymentDetails, emplIndex: null) => {
      if (employment && employment.documents) {
        return !!employment.documents.find((doc: Doc, docIndex: number) => {
          if (doc.status === DocStatus.Missing) {
            missingDocIndex = docIndex;
            employmentIndex = emplIndex;
            return true;
          }
          return false;
        });
      }
    });

    if (employmentIndex != null && missingDocIndex != null) {
      const doc = this._employments[employmentIndex].documents[missingDocIndex];
      this.timelineService.changeDocument(doc);
    } else if (this._gaps.length) {
      this.timelineService.changeEmployment(this._gaps[0] as any); // TODO: Check if cast is needed
    }
  }

  private minTwoYears(): Date {
    let date;
    if (this.referenceDate) {
      date = new Date(this.referenceDate);
    } else {
      date = new Date();
    }

    date.setFullYear(date.getFullYear() - 2);
    date.setDate(1);
    return date;
  }

  private calculateEmploymentsLayerHeight(employments: TimelineEmploymentDetails[]): number {
    let height = 0;
    const length = employments.length > this.defaultRowNumber ? this.defaultRowNumber : employments.length;
    for (let i = 0; i < length; i++) {
      height += employments[i].additionalRows ? (this.defaultRowNumber + 1) * this.rowHeight : this.defaultRowNumber * this.rowHeight;
      height += 15;
    }

    if (this.timelineMarkers) {
      height += 20;
    }

    return height;
  }

  monthGap(data: Gap): string {
    const start = LocalDate.of(data.startDate.getFullYear(), data.startDate.getMonth() + 1, data.startDate.getDate());
    const end = LocalDate.of(data.endDate.getFullYear(), data.endDate.getMonth() + 1, data.endDate.getDate());
    const period = Period.between(start, end);
    const months = period.months() + period.years() * 12;
    let text = '';
    if (months > 0) {
      text += months + ' MOS';
    }
    return text + ' GAP';
  }
}
