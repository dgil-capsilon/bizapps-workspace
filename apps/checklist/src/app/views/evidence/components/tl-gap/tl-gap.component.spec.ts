import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TlGapComponent} from './tl-gap.component';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {of} from 'rxjs';
import {TimelineService} from '../../services/timeline.service';
import createSpy = jasmine.createSpy;

describe('TlGapComponent', () => {
  let component: TlGapComponent;
  let fixture: ComponentFixture<TlGapComponent>;
  const timelineServiceMock = {
    changeEmployment: createSpy()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TlGapComponent],
      providers: [
        {provide: TimelineService, useValue: timelineServiceMock},
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlGapComponent);
    component = fixture.componentInstance;
    component.settings = {rowHeight: null, monthWidth: null, months: null} as any;
    component.gap = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not set reversed variable', () => {
    component.isReversed = undefined;

    expect(component.isReversed).toBeUndefined();
  });

  it('should calculate width when timeline being reversed', () => {
    spyOn(TimelineCalculation, 'calculateWidthForDateRange').and.returnValue(1);
    spyOn(TimelineCalculation, 'getLeftPosition');

    component.isReversed = true;

    expect(TimelineCalculation.calculateWidthForDateRange).toHaveBeenCalled();
    expect(component.width).toBeDefined();
  });

  it('should calculate left position when timeline being reversed', () => {
    spyOn(TimelineCalculation, 'calculateWidthForDateRange');
    spyOn(TimelineCalculation, 'getLeftPosition').and.returnValue(1);

    component.isReversed = true;

    expect(TimelineCalculation.getLeftPosition).toHaveBeenCalled();
    expect(component.left).toBeDefined();
  });

  it('should trigget click event', () => {
    fixture.debugElement.triggerEventHandler('click', null);
    expect(timelineServiceMock.changeEmployment).toHaveBeenCalled();
  });
});
