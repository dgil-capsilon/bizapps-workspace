import {ChangeDetectionStrategy, Component, HostBinding, HostListener, Input} from '@angular/core';
import {isUndefined} from 'util';
import {DrawOptions, Gap} from '@models/data.interface';
import {TimelineCalculation} from '@app/services/timeline-calculation';
import {TimelineService} from '../../services/timeline.service';

@Component({
  selector: 'chk-tl-gap',
  templateUrl: './tl-gap.component.html',
  styleUrls: ['./tl-gap.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TlGapComponent {
  private gap
  @Input() set options(options: any) {
    this.gap = options.gap;
    this.width = TimelineCalculation.calculateWidthForDateRange(options.gap.startDate, options.gap.endDate, options.settings.monthWidth);
    this.left = TimelineCalculation.getLeftPosition(options.gap, options.settings, options.isReversed);
  }

  @HostBinding('style.width.px') width: number;
  @HostBinding('style.left.px') left: number;

  constructor(private timelineService: TimelineService) {
  }

  @HostListener('click')
  onClick() {
    this.timelineService.changeEmployment(this.gap as any); // TODO: Check if cast is needed
  }
}
