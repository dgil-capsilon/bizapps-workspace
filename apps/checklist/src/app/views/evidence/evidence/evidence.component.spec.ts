import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {of} from 'rxjs';
import {RuleStatus} from '@models/enums';
import {NodeService} from '@app/services/node.service';
import {EvidenceComponent} from './evidence.component';
import {TimelineService} from '../services/timeline.service';
import createSpy = jasmine.createSpy;

describe('EvidenceComponent', () => {
  let component: EvidenceComponent;
  let fixture: ComponentFixture<EvidenceComponent>;
  let nodeService;
  const timelineServiceMock = {changeEmployment: createSpy(), changeDocument: createSpy()};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EvidenceComponent],
      providers: [
        {provide: NodeService, useValue: {nodeData$: of()}},
        {provide: TimelineService, useValue: timelineServiceMock},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvidenceComponent);
    nodeService = TestBed.get(NodeService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change document to one with nearest end date', fakeAsync(() => {
    const expectedDoc = {id: 3, startDate: new Date('2013-02-17'), endDate: new Date('2013-02-24')};
    nodeService.nodeData$ = of({
      status: RuleStatus.Passed,
      data: {
        employmentDetails: [
          {
            documents: [
              {id: 1, startDate: new Date('2013-02-01'), endDate: new Date('2013-02-08')},
              {id: 2, startDate: new Date('2013-02-09'), endDate: new Date('2013-02-16')},
              expectedDoc
            ]
          }
        ]
      }
    });

    component.ngOnInit();
    tick();

    expect(timelineServiceMock.changeDocument).toHaveBeenCalledWith(expectedDoc);
  }));

});
