import {Component} from '@angular/core';
import {NodeService} from '@app/services/node.service';
import {Gap, TimelineEmploymentDetails, TimelineRule} from '@models/data';
import {RuleStatus} from '@models/data.enum';
import {LoanTypeDetails} from '@models/data/app/rules/timeline/loan-type-details.interface';
import {ViewBase} from '@models/view-base';
import {TimelineService} from '../services/timeline.service';

@Component({
  selector: 'chk-evidence',
  templateUrl: './evidence.component.html',
  styleUrls: ['./evidence.component.scss']
})
export class EvidenceComponent extends ViewBase {
  rulesData: TimelineEmploymentDetails[] | Gap[]; // TODO: Check for union type
  borrowerName: string;
  ruleCode: string;
  referenceDate: Date;
  ruleStatus: RuleStatus;
  loanTypeDetails: LoanTypeDetails;

  constructor(private timelineService: TimelineService,
              nodeService: NodeService) {
    super(nodeService);
  }

  protected handleRuleData(rule: TimelineRule) {
    if (rule && rule.data && rule.data.employmentDetails) {
      this.rulesData = rule.data.employmentDetails;

      this.borrowerName = rule.fullName;
      this.ruleCode = rule.ruleCode;
      this.ruleStatus = rule.status;
      this.referenceDate = rule.data.referenceDate;

      if (rule.data.loanTypeDetails) {
        this.loanTypeDetails = rule.data.loanTypeDetails;
      }

      if (this.ruleStatus === RuleStatus.Passed &&
        this.rulesData.length > 0 &&
        this.rulesData[0].documents &&
        this.rulesData[0].documents.length > 0) {
        const doc = this.getNewestDocument();
        setTimeout(() => this.timelineService.changeDocument(doc), 0);
      }
    }
  }

  /**
   * Get document with nearest end date
   */
  private getNewestDocument() {
    const [doc] = (this.rulesData as TimelineEmploymentDetails[]).reduce((documents, ruleData: TimelineEmploymentDetails) => {
      return [...documents, ...ruleData.documents];
    }, []).sort((a, b) => a.endDate > b.endDate ? -1 : a.endDate < b.endDate ? 1 : 0);

    return doc;
  }

}
