import {Directive, HostListener, Input} from '@angular/core';
import {TimelineService} from '../services/timeline.service';

@Directive({
  selector: '[chkSnippetElement]'
})
export class SnippetElementDirective {
  @Input() mouseEventsDoc: any;
  @Input() mouseEvents: string[];

  constructor(private timelineService: TimelineService) {
  }

  @HostListener('click')
  onClick() {
    if (this.mouseEvents.includes('click')) {
      this.timelineService.changeDocument(this.mouseEventsDoc);
    }
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.mouseEvents.includes('mousehover')) {
      this.timelineService.documentHover(this.mouseEventsDoc);
    }
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.mouseEvents.includes('mousehover')) {
      this.timelineService.documentHover(null);
    }
  }
}
