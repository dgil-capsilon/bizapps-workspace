import {SnippetElementDirective} from './snippet-element.directive';
import {TimelineService} from '../services/timeline.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('SnippetElementDirective', () => {
  let directive: SnippetElementDirective;
  let timelineServiceMock: SpyObj<TimelineService>;

  beforeEach(() => {
    timelineServiceMock = createSpyObj(['changeDocument', 'documentHover']);
    directive = new SnippetElementDirective(timelineServiceMock);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call service to change document', () => {
    directive.mouseEvents = ['click'];

    directive.onClick();

    expect(timelineServiceMock.changeDocument).toHaveBeenCalled();
  });

  it('should not call service to change document', () => {
    directive.mouseEvents = ['mousehover'];

    directive.onClick();

    expect(timelineServiceMock.changeDocument).not.toHaveBeenCalled();
  });

  it('should call service to hover document', () => {
    directive.mouseEvents = ['mousehover'];

    directive.onMouseEnter();
    directive.onMouseLeave();

    expect(timelineServiceMock.documentHover).toHaveBeenCalledTimes(2);
  });

  it('should not call service to hover document', () => {
    directive.mouseEvents = ['click'];

    directive.onMouseEnter();
    directive.onMouseLeave();

    expect(timelineServiceMock.documentHover).not.toHaveBeenCalled();
  });
});
