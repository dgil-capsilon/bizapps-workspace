import {TestBed} from '@angular/core/testing';

import {TimelineService} from './timeline.service';

describe('TimelineService', () => {
  let service: TimelineService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TimelineService
      ]
    });
    service = TestBed.get(TimelineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
