import {Injectable} from '@angular/core';
import {Doc, Employment} from '@models/data';
import {ScrollTargetEl} from '@models/data/app/rules/timeline/scroll-target-el.interface';
import {Subject} from 'rxjs';

@Injectable()
export class TimelineService {
  private _employmentChanges$: Subject<Employment> = new Subject<Employment>();
  private _documentChanges$: Subject<Doc> = new Subject<Doc>();
  private _onDocumentFocus$: Subject<Doc> = new Subject<Doc>();
  private _onDocumentHover$: Subject<Doc> = new Subject<Doc>();
  private _onScrollFinish$ = new Subject();
  private lastClickedDoc: Doc;

  get employmentChanges() {
    return this._employmentChanges$.asObservable();
  }

  get documentChanges() {
    return this._documentChanges$.asObservable();
  }

  get onDocumentFocus() {
    return this._onDocumentFocus$.asObservable();
  }

  get onDocumentHover() {
    return this._onDocumentHover$.asObservable();
  }

  changeEmployment(employment: Employment) {
    this._employmentChanges$.next(employment);
  }

  changeDocument(document: ScrollTargetEl) {
    this.lastClickedDoc = document as Doc;
    this._documentChanges$.next(document as Doc);
  }

  documentFocus(document: Doc) {
    this._onDocumentFocus$.next(document);
  }

  documentHover(document: Doc) {
    if (document) {
      this._onDocumentHover$.next(document);
    } else {
      this._onDocumentFocus$.next(this.lastClickedDoc);
    }
  }

  scrollFinish() {
    this._onScrollFinish$.next();
  }
}
