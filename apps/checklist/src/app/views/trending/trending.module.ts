import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrendingComponent} from './trending/trending.component';
import {ValidationInfoModule} from '@app/modules/validation-info/validation-info.module';
import {TrendingRoutingModule} from './trending-routing.module';
import {ScrollHoverModule} from '@app/modules/scroll-hover/scroll-hover.module';

@NgModule({
  declarations: [TrendingComponent],
  imports: [
    CommonModule,
    ValidationInfoModule,
    TrendingRoutingModule,
    ScrollHoverModule
  ]
})
export class TrendingModule {
}
