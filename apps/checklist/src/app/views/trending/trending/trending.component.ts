import {ChangeDetectionStrategy, Component} from '@angular/core';
import {environment} from '@environments/environment';
import {RuleCode} from '@models/data.enum';
import {TrendingData} from '@models/data.interface';
import {ViewBase} from '@models/view-base';
import {NodeService} from '@app/services/node.service';

@Component({
  selector: 'chk-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrendingComponent extends ViewBase {
  ruleData: TrendingData;
  incomeUrl: string;
  linkText: string;

  constructor(nodeService: NodeService) {
    super(nodeService);
  }

  protected handleRuleData(ruleData: TrendingData) {
    if (!ruleData) {
      return;
    }
    this.ruleData = ruleData;

    if (this.ruleData.ruleCode === RuleCode.INC220) {
      this.linkText = 'Click here to see variance details';
    } else {
      this.linkText = 'Click here to see trending details';
    }

    if (ruleData.data
      && ruleData.data.validationItems
      && ruleData.data.validationItems.length > 0
      && ruleData.data.validationItems[0].snippetUrl) {
      this.incomeUrl = environment.url + this.ruleData.data.validationItems[0].snippetUrl;
    }

  }
}
