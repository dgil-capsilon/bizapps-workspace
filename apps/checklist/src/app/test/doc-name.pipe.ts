import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'docName'
})
export class DocNamePipeMock implements PipeTransform {
  transform(value: any, args?: any): any {
    return value;
  }
}
