import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'employerAliases'})
export class FakeEmployerAliasesPipe implements PipeTransform {
  transform = value => value;
}
