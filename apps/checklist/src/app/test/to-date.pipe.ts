import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'toDate'
})
export class ToDatePipeMock implements PipeTransform {
  transform(value: any, args?: any): any {
    return value;
  }
}
