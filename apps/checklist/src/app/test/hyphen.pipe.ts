import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'hyphen'
})
export class HyphenPipeMock implements PipeTransform {
  transform(value: any, args?: any): any {
    return value;
  }
}
