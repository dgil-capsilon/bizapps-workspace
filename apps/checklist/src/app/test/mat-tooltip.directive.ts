import {Directive, Input} from '@angular/core';

// tslint:disable-next-line
@Directive({selector: '[matTooltip]'})
export class FakeMatTooltipDirective {
  @Input() matTooltip;
  @Input() matTooltipDisabled;
}
