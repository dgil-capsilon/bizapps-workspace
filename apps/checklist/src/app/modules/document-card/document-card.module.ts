import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {DocNameModule} from '../doc-name/doc-name.module';
import {DynamicSnippetModule} from '../dynamic-snippet/dynamic-snippet.module';
import {HyphenModule} from '../hyphen/hyphen.module';
import {SnippetComponent} from './components/snippet/snippet.component';
import {DocumentCardComponent} from './document-card.component';

@NgModule({
  declarations: [
    DocumentCardComponent,
    SnippetComponent
  ],
  imports: [
    CommonModule,
    DynamicSnippetModule,
    MatIconModule,
    DocNameModule,
    HyphenModule
  ],
  exports: [DocumentCardComponent]
})
export class DocumentCardModule {
}
