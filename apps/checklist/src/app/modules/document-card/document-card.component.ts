import {ChangeDetectionStrategy, Component, ElementRef, Input} from '@angular/core';
import {DocTypes, TitlePrefix} from '@models/data.enum';
import {ReplaySubject} from 'rxjs';
import {Cells, Doc} from '@models/data.interface';
import {SnippetElement} from '@models/snippet.element.interface';
import {DynamicSnippetHandler} from '../dynamic-snippet/services/dynamic-snippet-handler.service';
import {DataService} from '@app/services/data.service';
import {DocReaderService} from '@bizapps-workspace/components';

@Component({
  selector: 'chk-document-card, chk-document-card[scroll-id]',
  templateUrl: './document-card.component.html',
  styleUrls: ['./document-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentCardComponent implements SnippetElement {
  @Input() set document(doc: Doc) {
    if (!doc) {
      return;
    }

    this.useDynamicSnippet = doc.type === DocTypes.URLA;
    this._document = doc;

    if (this.useDynamicSnippet) {
      this.dynamicSnippetCellsData = this.dynamicSnippetHandler.getData(this._document.dynamicSnippetData);
      this.dynamicSnippetTitlePrefix = this.dynamicSnippetHandler.getPrefixTitle(this._document.dynamicSnippetData);

      this.dynamicSnippetHandler.markHighlightedFields(this.dynamicSnippetCellsData, this._document.dynamicSnippetData);

      this._onSnippetLoad$.next();
    }
  }

  @Input() documentId: string;
  @Input() documentType: string;
  @Input() snippetUrl: string;
  dynamicSnippetCellsData: Cells;
  dynamicSnippetTitlePrefix: TitlePrefix;
  useDynamicSnippet: boolean;
  private _document: Doc;
  private _onSnippetLoad$ = new ReplaySubject(1);

  constructor(private elRef: ElementRef<HTMLElement>,
              private docReaderService: DocReaderService,
              private dataService: DataService,
              private dynamicSnippetHandler: DynamicSnippetHandler) {
  }

  get ref() {
    return this.elRef;
  }

  get onSnippetLoad() {
    return this._onSnippetLoad$.asObservable();
  }

  hasData(data) {
    return this._document === data;
  }

  loadDoc(): void {
    if (this.documentId) {
      this.docReaderService.open({
        docReader: {
          id: this.documentId,
          dataSource: this.dataService.docReaderData(this.documentId)
        }
      });
    }
  }

  snippetLoaded() {
    this._onSnippetLoad$.next();
  }

  isUrla() {
    return this.documentType === DocTypes.URLA;
  }
}
