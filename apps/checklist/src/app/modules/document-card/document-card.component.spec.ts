import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Doc} from 'src/app/models/data.interface';
import {DocumentCardComponent} from './document-card.component';
import {HyphenPipeMock} from 'src/app/test/hyphen.pipe';
import {DocNamePipeMock} from 'src/app/test/doc-name.pipe';
import {DynamicSnippetHandler} from '../dynamic-snippet/services/dynamic-snippet-handler.service';
import {DocStatus} from 'src/app/models/enums';
import {DocReaderService} from 'components';
import {DataService} from '../../services/data.service';

describe('DocumentCardComponent', () => {
  let component: DocumentCardComponent;
  let fixture: ComponentFixture<DocumentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DocumentCardComponent,
        HyphenPipeMock,
        DocNamePipeMock
      ],
      providers: [
        {provide: DataService, useValue: {}},
        {provide: DocReaderService, useValue: {}},
        {provide: DynamicSnippetHandler, useValue: {}}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get element ref', () => {
    expect(component.ref).toBeTruthy();
  });

  it('should check if component have data', () => {
    const data: Doc = {status: DocStatus.Valid} as any;
    component.document = data;
    expect(component.hasData(data)).toEqual(true);
  });
});
