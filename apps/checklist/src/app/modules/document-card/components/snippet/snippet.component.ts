import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {environment} from '@environments/environment';

@Component({
  selector: 'chk-snippet',
  templateUrl: './snippet.component.html',
  styleUrls: ['./snippet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SnippetComponent implements OnInit {
  @Input() sourceExist;
  @Input() snippetUrl: string;
  @Output() loaded = new EventEmitter<void>();
  @HostBinding('class.data-not-available')
  snippetNotFound = false;

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    if (!this.snippetUrl) {
      this.imgLoadFailed();
    }
  }

  snippetUrlBuilder(): string {
    return `${environment.baseUrl}/${this.snippetUrl}`;
  }

  imgLoaded(): void {
    this.loaded.emit();
  }

  imgLoadFailed(): void {
    this.loaded.emit();
    this.snippetNotFound = true;
    this.cdRef.detectChanges();
  }

}
