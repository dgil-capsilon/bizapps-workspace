import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NoDocsFoundComponent} from './no-docs-found.component';
import * as fromRules from '../../../../redux/rules/rules.selectors';
import {EmploymentDataValidationApi, ValidationEmploymentDetailsApi} from '../../../models/data-api.interface';
import {RuleCode} from '@models/enums';
import {Store} from '@ngrx/store';
import {RulesState} from '@redux/rules/rules.reducer';
import {HyphenPipeMock} from '@app/test/hyphen.pipe';
import {NodeService} from '@app/services/node.service';
import {provideMockStore} from '@ngrx/store/testing';
import {AppState} from '@redux/app.reducer';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('NoDocsFoundComponent', () => {
  let component: NoDocsFoundComponent;
  let fixture: ComponentFixture<NoDocsFoundComponent>;
  const initialState: AppState = {
    folder: null,
    applicants: {
      rules: [{ruleId: 1, id: 1, description: ''}]
    },
    ruleDetails: null,
    rules: {
      selectedRuleInfoId: 1,
    },
  } as any;
  let store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoDocsFoundComponent, HyphenPipeMock],
      providers: [
        provideMockStore({initialState})
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoDocsFoundComponent);
    store = TestBed.get<Store<RulesState>>(Store);
    store.overrideSelector(fromRules.getSelectedRuleCode, RuleCode.INC010);
    store.overrideSelector(fromRules.getSelectedRuleDescription, '');
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return text with no VOE', () => {
    store.overrideSelector(fromRules.getSelectedRuleDescription, 'Are the VOEs and voe or voe are less than vOe?');
    component.ngOnInit();
    expect(component.noFileFound).toEqual('No VOE found');
  });

  it('should return text with no Paystub', () => {
    store.overrideSelector(fromRules.getSelectedRuleDescription, 'PAYSTUB paystub PaYStUb');
    component.ngOnInit();
    expect(component.noFileFound).toEqual('No Paystub found');
  });

  it('should return text with no Paystub and W2', () => {
    store.overrideSelector(fromRules.getSelectedRuleDescription, 'PAYSTUB w-2 PaYStUb');
    component.ngOnInit();
    expect(component.noFileFound).toEqual('No Paystub or W-2 found');
  });

  it('should return text with no (E)VOE', () => {
    store.overrideSelector(fromRules.getSelectedRuleDescription, '(E)VOE');
    component.ngOnInit();
    expect(component.noFileFound).toEqual('No (E)VOE found');
  });

  it('should return text with no W2 found', () => {
    store.overrideSelector(fromRules.getSelectedRuleDescription, 'w-2 w-2 w2 w2');
    component.ngOnInit();
    expect(component.noFileFound).toEqual('No W-2 found');
  });

  it('should show text for rule INC-750', () => {
    const data: EmploymentDataValidationApi[] = [
        {docType: 'VOE'},
        {docType: '(E)VOE'}
      ] as any;
    store.overrideSelector(fromRules.getSelectedRuleCode, 'INC-750');
    component.dataValidation = data;
    const show = component.showNoDocs('INC-750' as RuleCode);
    expect(show).toBe(true);
  });

  it('should not show text for rule INC-750', () => {
    const data: EmploymentDataValidationApi[] = [
      {docType: 'VOE'},
      {docType: 'W_2'}
    ] as any;
    store.overrideSelector(fromRules.getSelectedRuleCode, 'INC-750');
    component.dataValidation = data;
    const show = component.showNoDocs('INC-750' as RuleCode);
    expect(show).toBe(false);
  });

  it('should not show text for rule INC-1040', () => {
    const data: EmploymentDataValidationApi[] = [
      {docType: 'VOE'},
      {docType: 'PAYSTUB'}
    ] as any;
    store.overrideSelector(fromRules.getSelectedRuleCode, 'INC-1040');
    component.dataValidation = data;
    const show = component.showNoDocs('INC-1040' as RuleCode);
    expect(show).toBe(false);
  });
});
