import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {EmploymentDataValidationApi} from '@models/data';
import {RuleCode} from '@models/data.enum';
import {POSSIBLE_DOCUMENTS} from '@models/consts/possible-documents';
import {combineLatest, Subject} from 'rxjs';
import * as fromRules from '@redux/rules/rules.selectors';
import {filter, takeUntil} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {COMPARED_DOCUMENTS} from '@models/consts/compared-documents';

@Component({
  selector: 'chk-no-docs-found',
  templateUrl: './no-docs-found.component.html',
  styleUrls: ['./no-docs-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoDocsFoundComponent implements OnInit, OnDestroy {
  @Input() dataValidation: EmploymentDataValidationApi[];
  noFileFound: string;
  showNoDocsFound: boolean;
  private dictionary = ['VOE', 'Paystub', 'W-2', '\\(E\\)VOE'];
  private componentDestroy$ = new Subject<void>();

  constructor(private store$: Store<AppState>,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    combineLatest([
      this.store$.select(fromRules.getSelectedRuleCode),
      this.store$.select(fromRules.getSelectedRuleDescription)
    ]).pipe(
      takeUntil(this.componentDestroy$),
      filter(([ruleCode, ruleDescription]) => ruleCode != null && ruleDescription != null)
    ).subscribe(([ruleCode, ruleDescription]) => {
      this.showNoDocsFound = this.showNoDocs(ruleCode);
      if (COMPARED_DOCUMENTS.has(ruleCode)) {
        const missingDocs = this.getDocumentsNamesForComparison(ruleCode);
        this.noFileFound = `No ${this.generateText(missingDocs)} found`;
      } else {
        this.noFileFound = this.getTextForNotFoundFiles(ruleDescription, ruleCode);
      }
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  showNoDocs(ruleCode: RuleCode) {
    if (COMPARED_DOCUMENTS.has(ruleCode)) {
      const docs = COMPARED_DOCUMENTS.get(ruleCode).map(item => {
        return this.hasDocumentNamesContainsName(item);
      }).every(Boolean);
      return !docs;
    }
    return !(this.dataValidation && this.dataValidation.length);
  }

  private getDocumentsNamesForComparison(ruleCode: string): string[] {
    return COMPARED_DOCUMENTS.get(ruleCode).filter(item => {
      return !this.hasDocumentNamesContainsName(item);
    }).map(value => {
      if (value === 'VOE') {
        return '(E)VOE';
      }
      return value.replace(/_/, '-');
    });
  }

  private hasDocumentNamesContainsName(item: string): boolean {
    return this.dataValidation.map(value => new RegExp(item, 'i').test(value.docType)).some(Boolean);
  }

  private getTextForNotFoundFiles(ruleDescription: string, ruleCode: RuleCode): string {
    const replacedText = ruleDescription.replace(/-/, '');
    let allowedDocuments = this.dictionary.filter((item) => {
      const regex = new RegExp(item.replace(/-/, ''), 'i');
      return regex.test(replacedText);
    }).map(value => value.replace(/\\/g, ''));
    if (allowedDocuments.includes('(E)VOE')) {
      allowedDocuments = allowedDocuments.filter(value => value !== 'VOE');
    }

    if (allowedDocuments.length === 0 && POSSIBLE_DOCUMENTS.has(ruleCode)) {
      return `No ${this.generateText(POSSIBLE_DOCUMENTS.get(ruleCode))} found`;
    }
    return `No ${this.generateText(allowedDocuments)} found`;
  }

  private generateText(allowedNames: string[]): string {
    if (allowedNames.length === 0) {
      return '';
    }
    return allowedNames.join(' or ');
  }
}
