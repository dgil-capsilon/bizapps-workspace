import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NoDocsFoundComponent} from './component/no-docs-found.component';
import {HyphenModule} from '../hyphen/hyphen.module';

@NgModule({
  declarations: [NoDocsFoundComponent],
  imports: [
    CommonModule,
    HyphenModule,
  ],
  exports: [NoDocsFoundComponent]
})
export class NoDocsFoundModule { }
