import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicSnippetComponent} from './dynamic-snippet.component';

describe('DynamicSnippetComponent', () => {
  let component: DynamicSnippetComponent;
  let fixture: ComponentFixture<DynamicSnippetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicSnippetComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicSnippetComponent);
    component = fixture.componentInstance;
    component.cells = {
      nameAndAddress: {value: null},
      firstDate: {value: null},
      position: {value: null},
      secondDate: {value: null},
      phone: {value: null},
      yrsEmployedInProfession: {value: null}
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
