import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DynamicSnippetComponent} from './dynamic-snippet.component';

@NgModule({
  declarations: [
    DynamicSnippetComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DynamicSnippetComponent
  ]
})
export class DynamicSnippetModule {
}
