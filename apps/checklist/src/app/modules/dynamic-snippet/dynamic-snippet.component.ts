import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Cells} from '@models/data';
import {TitlePrefix} from '@models/data.enum';

@Component({
  selector: 'chk-dynamic-snippet',
  templateUrl: './dynamic-snippet.component.html',
  styleUrls: ['./dynamic-snippet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicSnippetComponent {
  @Input() titlePrefix: TitlePrefix;
  @Input() cells: Cells;
}
