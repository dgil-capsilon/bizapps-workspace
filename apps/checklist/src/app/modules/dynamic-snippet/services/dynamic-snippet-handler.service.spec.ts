import {TestBed} from '@angular/core/testing';
import {Cells} from '@models/data';
import {DocTypes} from '@models/enums';
import {ToDateModule} from '../../to-date/to-date.module';
import {DynamicSnippetHandler} from './dynamic-snippet-handler.service';

describe('DynamicSnippetHandler', () => {
  let service: DynamicSnippetHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [ToDateModule],
      providers: [DynamicSnippetHandler]
    });
    service = TestBed.get(DynamicSnippetHandler);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create full address', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          addressLineText: '12367 E Street',
          cityName: 'Washington,',
          plusFourZipCode: '',
          postalCode: '20013',
          stateCode: 'DC'
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;
    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\n12367 E Street, Washington, DC, 20013');
  });

  it('should create full address without city name', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          plusFourZipCode: '',
          postalCode: '20013',
          stateCode: 'DC'
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;
    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\nDC, 20013');
  });

  it('should create address without address line', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          addressLineText: '',
          cityName: 'Washington,',
          plusFourZipCode: '',
          postalCode: '20013',
          stateCode: 'DC'
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\nWashington, DC, 20013');
  });

  it('should create address without city name', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          addressLineText: '12367 E Street',
          cityName: '',
          plusFourZipCode: '',
          postalCode: '20013',
          stateCode: 'DC'
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\n12367 E Street, DC, 20013');
  });

  it('should create address without state code', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          addressLineText: '12367 E Street',
          cityName: 'Washington,',
          plusFourZipCode: '',
          postalCode: '20013',
          stateCode: ''
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\n12367 E Street, Washington, 20013');
  });

  it('should create address without postal code', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {
          addressLineText: '12367 E Street',
          cityName: 'Washington,',
          plusFourZipCode: '',
          postalCode: '',
          stateCode: 'DC'
        },
        employerName: 'Custom Employer Name 1'
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.nameAndAddress.value)
      .toEqual('Custom Employer Name 1\n12367 E Street, Washington, DC');
  });

  it('should create date for current primary employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        current: true,
        yearsOnJob: 2,
        monthsOnJob: 6,
        endDate: new Date(),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.value).toEqual('2 Years, 6 mnths');
  });

  it('should create date for current primary employment without years', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        current: true,
        monthsOnJob: 6,
        endDate: new Date(),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.value).toEqual('6 mnths');
  });

  it('should create date for current primary employment without months', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        current: true,
        yearsOnJob: 2,
        endDate: new Date(),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.value).toEqual('2 Years');
  });

  it('should create date for secondary employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        current: false,
        yearsOnJob: 2,
        endDate: new Date('2019-06-01'),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.value).toEqual('06/01/2019');
  });

  it('should create date for previous employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: false,
        current: false,
        yearsOnJob: 2,
        endDate: new Date('2019-05-01'),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.value).toEqual('05/01/2019');
  });

  it('should set dynamic snippet prefix title for primary employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        current: true,
        address: {cityName: ''}
      }
    } as any;

    const titlePrefix = service.getPrefixTitle(data.dynamicSnippetData);

    expect(titlePrefix).toEqual('PRIMARY');
  });

  it('should set dynamic snippet prefix title for secondary employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: false,
        current: true,
        address: {cityName: ''}
      }
    } as any;

    const titlePrefix = service.getPrefixTitle(data.dynamicSnippetData);

    expect(titlePrefix).toEqual('SECONDARY');
  });

  it('should set dynamic snippet prefix title for previous employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: false,
        current: false,
        address: {cityName: ''}
      }
    } as any;

    const titlePrefix = service.getPrefixTitle(data.dynamicSnippetData);

    expect(titlePrefix).toEqual('PREVIOUS');
  });

  it('should set custom titles', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: false,
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.firstDate.customTitle).toEqual('Date from');
    expect(dynamicSnippetCellsData.secondDate.customTitle).toEqual('Date to');
  });

  it('should not set custom titles', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        primary: true,
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.firstDate.customTitle).toEqual(null);
    expect(dynamicSnippetCellsData.secondDate.customTitle).toEqual(null);
  });

  it('should transform first date to specific date format', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        startDate: new Date('2019-02-01'),
        address: {cityName: ''}
      }
    } as any;

    const dynamicSnippetCellsData = service.getData(data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.firstDate.value).toEqual('02/01/2019');
  });

  it('should set highlighted cells for current employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {cityName: ''},
        snippetDefinition: [
          {marked: true, coordinates: {snippetKey: 'CurrentEmploymentDates'}}
        ]
      }
    } as any;
    const dynamicSnippetCellsData: Cells = service.getData(data.dynamicSnippetData);

    service.markHighlightedFields(dynamicSnippetCellsData, data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.secondDate.highlighted).toEqual(true);
  });

  it('should set highlighted cells for previous employment', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {cityName: ''},
        snippetDefinition: [
          {marked: true, coordinates: {snippetKey: 'PreviousEmploymentDates'}}
        ]
      }
    } as any;
    const dynamicSnippetCellsData: Cells = service.getData(data.dynamicSnippetData);

    service.markHighlightedFields(dynamicSnippetCellsData, data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.firstDate.highlighted).toEqual(true);
    expect(dynamicSnippetCellsData.secondDate.highlighted).toEqual(true);
  });

  it('should set highlighted cells for employment position description', () => {
    const data = {
      type: DocTypes.URLA,
      dynamicSnippetData: {
        address: {cityName: ''},
        snippetDefinition: [
          {marked: true, coordinates: {snippetKey: 'EmploymentPositionDescription'}}
        ]
      }
    } as any;
    const dynamicSnippetCellsData: Cells = service.getData(data.dynamicSnippetData);

    service.markHighlightedFields(dynamicSnippetCellsData, data.dynamicSnippetData);

    expect(dynamicSnippetCellsData.position.highlighted).toEqual(true);
  });
});
