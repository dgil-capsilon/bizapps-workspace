import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {ToDatePipe} from '@app/modules/to-date/pipe/to-date.pipe';
import {TitlePrefix} from '@models/data.enum';
import {Cells, DynamicSnippetData} from '@models/data.interface';
import {DynamicSnippetModule} from '../dynamic-snippet.module';

@Injectable({providedIn: DynamicSnippetModule})
export class DynamicSnippetHandler {

  constructor(@Inject(LOCALE_ID) private locale: string) {
  }

  getData(dynamicSnippetData: DynamicSnippetData) {
    const datePipe = new ToDatePipe(this.locale);

    const nameAndAddressValue = this.createAddressCellValue(dynamicSnippetData);
    const secondDateValue = this.createDateCellValue(dynamicSnippetData, datePipe);
    const {firstDateCustomTitle, secondDateCustomTitle} = this.createCustomTitles(dynamicSnippetData);

    return {
      nameAndAddress: {value: nameAndAddressValue},
      firstDate: {
        value: datePipe.transform(dynamicSnippetData.startDate, 'MM/dd/yyyy'),
        customTitle: firstDateCustomTitle
      },
      position: {value: dynamicSnippetData.positionDescription},
      secondDate: {
        value: secondDateValue,
        customTitle: secondDateCustomTitle
      },
      phone: {value: dynamicSnippetData.phoneNumber},
      yrsEmployedInProfession: {
        value: dynamicSnippetData.employmentTimeInLineOfWorkYearsCount
      }
    };
  }

  markHighlightedFields(data: Cells, dynamicSnippetData: DynamicSnippetData) {
    const {
      highlightCurOrPrevEmployment,
      highlightPreviousEmployment,
      highlightEmploymentPositionDescription,
      highlightYrsEmployedInProfession
    } = dynamicSnippetData.snippetDefinition.reduce((highlightFields, snippetDef) => {
      if (!snippetDef.marked) {
        return highlightFields;
      }

      if (snippetDef.coordinates.snippetKey === 'CurrentEmploymentDates'
        || snippetDef.coordinates.snippetKey === 'PreviousEmploymentDates') {
        highlightFields.highlightCurOrPrevEmployment = snippetDef;
      }
      if (snippetDef.coordinates.snippetKey === 'PreviousEmploymentDates') {
        highlightFields.highlightPreviousEmployment = snippetDef;
      }
      if (snippetDef.coordinates.snippetKey === 'EmploymentPositionDescription') {
        highlightFields.highlightEmploymentPositionDescription = snippetDef;
      }
      if (snippetDef.coordinates.snippetKey === 'EmploymentTimeInLineOfWorkYearsCount') {
        highlightFields.highlightYrsEmployedInProfession = snippetDef;
      }

      return highlightFields;
    }, {
      highlightCurOrPrevEmployment: null,
      highlightPreviousEmployment: null,
      highlightEmploymentPositionDescription: null,
      highlightYrsEmployedInProfession: null
    });

    if (highlightCurOrPrevEmployment) {
      data.secondDate.highlighted = true;
      data.secondDate.highlightColor = highlightCurOrPrevEmployment.color;
    }

    if (highlightPreviousEmployment) {
      data.firstDate.highlighted = true;
      data.firstDate.highlightColor = highlightPreviousEmployment.color;
    }

    if (highlightEmploymentPositionDescription) {
      data.position.highlighted = true;
      data.position.highlightColor = highlightEmploymentPositionDescription.color;
    }

    if (highlightYrsEmployedInProfession) {
      data.yrsEmployedInProfession.highlighted = true;
      data.yrsEmployedInProfession.highlightColor = highlightYrsEmployedInProfession.color;
    }
  }

  getPrefixTitle({primary, current}) {
    if (primary) {
      return TitlePrefix.Primary;
    }
    if (!primary && current) {
      return TitlePrefix.Secondary;
    }
    return TitlePrefix.Previous;
  }

  private createAddressCellValue({address: {addressLineText, cityName, stateCode, postalCode}, employerName}) {
    let address = `${employerName}\n`;
    let normalizedCityName = '';
    if (cityName) {
      normalizedCityName = cityName.replace(',', '');
    }
    address += [addressLineText, normalizedCityName, stateCode, postalCode].filter(Boolean).join(', ');

    return address;
  }

  private createDateCellValue({primary, current, yearsOnJob, monthsOnJob, endDate}, datePipe: ToDatePipe) {
    let date = '';
    if (primary && current) {
      if (yearsOnJob) {
        date = `${yearsOnJob} Years`;
      }
      if (monthsOnJob) {
        date += date ? `, ` : '';
        date += `${monthsOnJob} mnths`;
      }
    } else {
      date = datePipe.transform(endDate, 'MM/dd/yyyy');
    }

    return date;
  }

  private createCustomTitles({primary}) {
    return {
      firstDateCustomTitle: !primary ? 'Date from' : null,
      secondDateCustomTitle: !primary ? 'Date to' : null
    };
  }
}
