import {I18nPluralPipe} from '@angular/common';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'employerAliases'
})
export class EmployerAliasesPipe implements PipeTransform {
  private aliasesMapping:
    { [k: string]: string } = {'=1': '# other', 'other': '# others'};

  constructor(private i18nPluralPipe: I18nPluralPipe) {
  }

  transform(value: any, aliases: string[]): string {
    if (!aliases || !aliases.length) {
      return value;
    }

    if (aliases.length > 1) {
      const amountTextPart = this.i18nPluralPipe.transform(aliases.length - 1, this.aliasesMapping);

      return `${value} [${aliases[0]} & ${amountTextPart}]`;
    }

    return `${value} [${aliases[0]}]`;
  }

}
