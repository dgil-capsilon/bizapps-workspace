import {EmployerAliasesPipe} from './employer-aliases.pipe';

describe('EmployerAliasesPipe', () => {
  const i18nPluralPipeSpy: any = {transform: () => '1 other'};

  it('should create an instance', () => {
    const pipe = new EmployerAliasesPipe(null);
    expect(pipe).toBeTruthy();
  });

  it('should return original value (no aliases at all)', () => {
    const pipe = new EmployerAliasesPipe(null);

    const result = pipe.transform('Awesome Computers', undefined);

    expect(result).toEqual('Awesome Computers');
  });

  it('should return original value (aliases empty)', () => {
    const pipe = new EmployerAliasesPipe(null);

    const result = pipe.transform('Awesome Computers 2', []);

    expect(result).toEqual('Awesome Computers 2');
  });

  it('should return name with one alias', () => {
    const pipe = new EmployerAliasesPipe(null);

    const result = pipe.transform('Awesome Computers 3', ['Awesome Computer 4']);

    expect(result).toEqual('Awesome Computers 3 [Awesome Computer 4]');
  });

  it('should return name with two aliases', () => {
    const pipe = new EmployerAliasesPipe(i18nPluralPipeSpy);

    const result = pipe.transform('Awesome Computers 4', ['Awesome Computer 5', 'Awesome Computer 6']);

    expect(result).toEqual('Awesome Computers 4 [Awesome Computer 5 & 1 other]');
  });

});
