import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployerAliasesPipe} from './employer-aliases.pipe';

@NgModule({
  declarations: [EmployerAliasesPipe],
  imports: [
    CommonModule
  ],
  exports: [EmployerAliasesPipe]
})
export class EmployerAliasesModule { }
