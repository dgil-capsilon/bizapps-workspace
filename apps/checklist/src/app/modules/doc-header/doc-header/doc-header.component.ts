import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';
import {EmploymentTypes} from '@models/data.enum';
import {TimelineEmploymentDetails} from '@models/data.interface';

@Component({
  selector: 'chk-doc-header',
  templateUrl: './doc-header.component.html',
  styleUrls: ['./doc-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocHeaderComponent {
  @Input() set employer(employer: TimelineEmploymentDetails) {
    this.docTitle = this.getDocTitle(employer);
    this.isNonEmployment = employer.type !== EmploymentTypes.Employment;

    if (employer.type === EmploymentTypes.Employment) {
      this.employmentType = this.getEmploymentType(employer);
      this.isPrimary = employer.primary && employer.statusType === 'Current';
    }

    if (employer.employerOtherNames && employer.employerOtherNames.length) {
      this.aliases = employer.employerOtherNames;
      this.aliasesTooltipText = this.aliases.join('\n');
    }
  }

  @Input() set customTitle(title: string) {
    this.docTitle = title;
    this.employmentType = null;
    this.hasCustomTitle = true;
  }

  @HostBinding('class.non-employment-header')
  isNonEmployment: boolean;
  docTitle: string;
  employmentType: string;
  isPrimary: boolean;
  aliases: string[];
  aliasesTooltipText: string;
  hasCustomTitle = false;

  getDocTitle(employer: TimelineEmploymentDetails): string {
    if (employer.type === EmploymentTypes.Gap) {
      return employer.error ? 'Error' : '(Gap)';
    }

    return employer.employerName;
  }

  private getEmploymentType(employer): string {
    if (employer.primary && employer.statusType === 'Current') {
      return 'Primary';
    } else if (!employer.primary && employer.statusType === 'Current') {
      return 'Secondary';
    }
    return 'Previous';
  }

}
