import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EmploymentTypes} from '@models/enums';
import {FakeEmployerAliasesPipe} from '@app/test/employer-aliases.pipe';
import {FakeMatTooltipDirective} from '@app/test/mat-tooltip.directive';
import {DocHeaderComponent} from './doc-header.component';

describe('DocHeaderComponent', () => {
  let component: DocHeaderComponent;
  let fixture: ComponentFixture<DocHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DocHeaderComponent,
        FakeEmployerAliasesPipe,
        FakeMatTooltipDirective
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocHeaderComponent);
    component = fixture.componentInstance;
    component.employer = {type: 'GAP', employerName: 'Test'} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.docTitle).toBe('(Gap)');
  });

  it('check name for employment', () => {
    component.employer = {type: EmploymentTypes.Employment, employerName: 'Real employment'} as any;
    fixture.detectChanges();
    expect(component.docTitle).toBe('Real employment');
  });

  it('should get different name for error gap', () => {
    component.employer = {type: EmploymentTypes.Gap, error: 'Some error'} as any;
    fixture.detectChanges();
    expect(component.docTitle).toBe('Error');
  });
});
