import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DocHeaderComponent} from './doc-header/doc-header.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {EmployerAliasesModule} from '../employer-aliases/employer-aliases.module';

@NgModule({
  declarations: [DocHeaderComponent],
  imports: [
    CommonModule,
    MatTooltipModule,
    EmployerAliasesModule
  ],
  exports: [DocHeaderComponent]
})
export class DocHeaderModule { }
