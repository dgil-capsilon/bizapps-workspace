import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToDatePipe} from './pipe/to-date.pipe';

@NgModule({
  declarations: [ToDatePipe],
  imports: [
    CommonModule
  ],
  exports: [ToDatePipe]
})
export class ToDateModule { }
