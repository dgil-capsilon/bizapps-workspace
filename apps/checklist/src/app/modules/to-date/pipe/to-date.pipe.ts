import {formatDate} from '@angular/common';
import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import * as jstz from 'jstimezonedetect';

@Pipe({
  name: 'toDate'
})
export class ToDatePipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) public locale: string) {
  }

  transform(value: any, format: string, preserveTimezoneOffset = false, useUSATimeZoneName = false): string | null {
    if (!value) {
      return null;
    }

    let timezone = '';
    if (useUSATimeZoneName) {
      timezone = ' ' + jstz.determine().name();
      timezone = timezone.replace('_', ' ');
    }

    if (preserveTimezoneOffset) {
      return formatDate(value, format, this.locale) + timezone;
    }

    const date = new Date(value);
    const offset = date.getTimezoneOffset() / 60;

    date.setHours(date.getHours() + offset);

    return formatDate(date, format, this.locale) + timezone;
  }

}
