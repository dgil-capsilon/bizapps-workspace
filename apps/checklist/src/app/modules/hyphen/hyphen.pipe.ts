import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'hyphen'
})
export class HyphenPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      return value.replace('-', '\u2011');
    }
    return value;
  }

}
