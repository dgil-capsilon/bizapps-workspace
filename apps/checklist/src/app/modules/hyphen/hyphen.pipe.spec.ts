import {HyphenPipe} from './hyphen.pipe';

describe('HyphenPipe', () => {
  it('create an instance', () => {
    const pipe = new HyphenPipe();
    expect(pipe).toBeTruthy();
  });

  it('should be the same text', () => {
    const pipe = new HyphenPipe();
    const text = 'Sample\u2011text';
    expect(pipe.transform(text)).toBe(text);
  });

  it('should transform text', () => {
    const pipe = new HyphenPipe();
    const text = 'Sample-text';
    const transformed = pipe.transform(text);
    expect(transformed).toBe('Sample\u2011text');
    expect(transformed).not.toBe(text);
  });

  it('should return original value if it\'s falsy', () => {
    const pipe = new HyphenPipe();
    const text = null;
    const transformed = pipe.transform(text);
    expect(transformed).toBe(text);
  });
});
