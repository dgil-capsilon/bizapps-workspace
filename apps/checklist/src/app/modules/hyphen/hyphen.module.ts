import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HyphenPipe} from './hyphen.pipe';

@NgModule({
  declarations: [HyphenPipe],
  imports: [
    CommonModule
  ],
  exports: [HyphenPipe]
})
export class HyphenModule { }
