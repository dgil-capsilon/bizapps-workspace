import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DocNamePipe} from './doc-name.pipe';

@NgModule({
  declarations: [DocNamePipe],
  imports: [
    CommonModule
  ],
  exports: [DocNamePipe]
})
export class DocNameModule { }
