import {Pipe, PipeTransform} from '@angular/core';
import {DocNames} from '@models/data.enum';

@Pipe({
  name: 'docName'
})
export class DocNamePipe implements PipeTransform {

  transform(docKey: string, keyPrefix = ''): any {
    if (keyPrefix) {
      return DocNames[`${keyPrefix}_${docKey}`] || DocNames[docKey];
    }

    return DocNames[docKey];
  }

}
