import {DocNamePipe} from './doc-name.pipe';

describe('DocNamePipe', () => {
  let pipe: DocNamePipe;

  beforeEach(() => {
    pipe = new DocNamePipe();
  });

  it('should get document name', () => {
    const docName = pipe.transform('URLA');

    expect(docName).toEqual('Loan Application');
  });

  it('should get document name with key prefix', () => {
    const docName = pipe.transform('PAYSTUB', 'TL');

    expect(docName).toEqual('PS');
  });

  it('should get document name as fallback (no key with prefix)', () => {
    const docName = pipe.transform('W_2', 'TL');

    expect(docName).toEqual('W-2');
  });
});
