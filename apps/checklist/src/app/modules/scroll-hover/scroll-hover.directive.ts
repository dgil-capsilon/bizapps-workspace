import {Platform} from '@angular/cdk/platform';
import {Directive, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[chkScrollHover]'
})
export class ScrollHoverDirective implements OnInit {
  private id: number;

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private platform: Platform) {
  }

  ngOnInit(): void {
    if (!this.platform.TRIDENT) {
      this.renderer.setStyle(this.el.nativeElement, 'overflow', 'hidden');
    }
  }

  @HostListener('touchstart')
  @HostListener('mouseenter') onEnter(): void {
    if (this.disableScroll()) {
      return;
    }
    this.clearId();
    this.addScroll();
  }

  @HostListener('touchend')
  @HostListener('mouseleave') onLeave(): void {
    if (this.disableScroll()) {
      return;
    }
    this.removeScroll();
  }

  /**
   * Determination when show scrollbar is calculated at very beginning based on container width and height.
   * It won't work properly if container increase its dimension overtime (eg. snippets loading).
   */
  recalculateAndShow() {
    this.onEnter();
  }

  private disableScroll(): boolean {
    if (this.platform.TRIDENT) {
      return true;
    }

    const nativeElement: HTMLElement = this.el.nativeElement;
    return nativeElement.clientHeight === nativeElement.scrollHeight && nativeElement.clientWidth === nativeElement.scrollWidth;
  }

  private addScroll(): void {
    this.renderer.setStyle(this.el.nativeElement, 'overflow', 'auto');
    this.renderer.addClass(this.el.nativeElement, 'section-hover');
  }

  private removeScroll(): void {
    this.id = setTimeout(() => {
      this.renderer.setStyle(this.el.nativeElement, 'overflow', 'hidden');
      this.renderer.removeClass(this.el.nativeElement, 'section-hover');
    }, 500) as any;
  }

  private clearId(): void {
    if (this.id) {
      clearTimeout(this.id);
      this.id = null;
    }
  }
}
