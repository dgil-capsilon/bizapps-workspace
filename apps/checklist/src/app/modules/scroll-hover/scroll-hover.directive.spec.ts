import {ScrollHoverDirective} from './scroll-hover.directive';
import {Component, NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

@Component({
  selector: 'app-test-component',
  template: `
      <div id="host" chkScrollHover style="max-width: 300px; max-height: 300px">
          <div style="height: 1000px; width: 1000px;"></div>
      </div>`
})
class TestComponentMockComponent {
}

describe('ScrollHoverDirective', () => {
  let component: TestComponentMockComponent;
  let fixture: ComponentFixture<TestComponentMockComponent>;
  let elementDe;
  let element: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponentMockComponent, ScrollHoverDirective],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponentMockComponent);
    component = fixture.componentInstance;
    elementDe = fixture.debugElement.query(By.css('#host'));
    element = elementDe.nativeElement;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('styles on initialization', () => {
    expect(element.classList.length).toBe(0);
    expect(element.style.overflow).toBe('hidden');
  });

  it('when mouse enter div should get styles and scrolls', fakeAsync(() => {
    elementDe.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();
    expect(element.classList).toContain('section-hover');
    expect(element.style.overflow).toBe('auto');

    tick(3001);

    expect(element.classList).toContain('section-hover');
    expect(element.style.overflow).toBe('auto');
  }));

  it('when mouse enter and leave after 1 second', fakeAsync(() => {
    elementDe.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();

    tick(2999);

    expect(element.classList).toContain('section-hover');
    expect(element.style.overflow).toBe('auto');

    elementDe.triggerEventHandler('mouseleave', null);

    tick(1000);

    expect(element.classList).not.toContain('section-hover');
    expect(element.style.overflow).toBe('hidden');
  }));
});
