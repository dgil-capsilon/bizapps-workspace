import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScrollHoverDirective} from './scroll-hover.directive';

@NgModule({
  declarations: [ScrollHoverDirective],
  imports: [
    CommonModule
  ],
  exports: [ScrollHoverDirective]
})
export class ScrollHoverModule { }
