import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ValidationFieldTypes} from '@models/enums';
import {ValidationFieldComponent} from './validation-field.component';
import {ToDatePipeMock} from '@app/test/to-date.pipe';

describe('ValidationFieldComponent', () => {
  let component: ValidationFieldComponent;
  let fixture: ComponentFixture<ValidationFieldComponent>;
  let element;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ValidationFieldComponent,
        ToDatePipeMock
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationFieldComponent);
    component = fixture.componentInstance;
    element = fixture.elementRef.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return default value when only type is provided', () => {
    component.type = ValidationFieldTypes.Date;
    fixture.detectChanges();
    expect(element.textContent).toContain(component.defaultValue);
  });

  it('should return default value when no data is provided', () => {
    fixture.detectChanges();
    expect(element.textContent).toContain(component.defaultValue);
  });

  it('should return string', () => {
    component.type = ValidationFieldTypes.String;
    const exampleString = 'String example';
    component.value = exampleString;
    fixture.detectChanges();
    expect(element.textContent).toContain(exampleString);
  });

  it('should return currency value', () => {
    component.type = ValidationFieldTypes.Number;
    component.value = '102035';
    fixture.detectChanges();
    expect(element.textContent).toContain('$102,035.00');
  });

  it('should return "not present" for boolean false value', () => {
    component.type = ValidationFieldTypes.Boolean;
    component.value = 'false';
    fixture.detectChanges();
    expect(element.textContent).toContain('Not present');
  });

});
