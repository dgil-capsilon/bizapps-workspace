import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {ValidationFieldTypes} from '@models/data.enum';

@Component({
  selector: 'chk-validation-field',
  templateUrl: './validation-field.component.html',
  styleUrls: ['./validation-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationFieldComponent {
  @Input() type: ValidationFieldTypes;
  @Input() value: string;
  defaultValue = 'Not present';
}
