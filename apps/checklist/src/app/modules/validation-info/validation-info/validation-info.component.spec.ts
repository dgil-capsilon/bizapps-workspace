import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ValidationInfoComponent} from './validation-info.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { ValidationStatus } from 'src/app/models/enums';

describe('ValidationInfoComponent', () => {
  let component: ValidationInfoComponent;
  let fixture: ComponentFixture<ValidationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ValidationInfoComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationInfoComponent);
    component = fixture.componentInstance;
    component.dataValidation = [{status: null, fieldValue: null, fieldName: null}] as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true when expired or invalid', () => {
    expect(component.isExpired(ValidationStatus.Expired)).toBeTruthy();
  });

  it('should be true when invalid', () => {
    expect(component.isInvalid(ValidationStatus.Invalid)).toBeTruthy();
  });

  xit('should be true when is valid', () => {
    expect(component.isValid(ValidationStatus.Valid)).toBeTruthy();
  });

  it('should be false when is not expired', () => {
    expect(component.isExpired(ValidationStatus.Valid)).toBeFalsy();
    expect(component.isExpired(ValidationStatus.NotEvaluated)).toBeFalsy();
  });

  it('should be false when is not invalid', () => {
    expect(component.isInvalid(ValidationStatus.Valid)).toBeFalsy();
    expect(component.isInvalid(ValidationStatus.NotEvaluated)).toBeFalsy();
  });
});
