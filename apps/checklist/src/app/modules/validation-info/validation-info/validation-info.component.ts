import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {EmploymentDataValidation} from '@models/data/app';
import {IsNotEvaluated} from '@decorators/is-not-evaluated.decorator';
import {IsInvalid} from '@decorators/is-invalid.decorator';
import {BaseStatuses} from '@models/base-statuses';
import {IsValid} from '@decorators/is-valid.decorator';
import {IsExpired} from '@decorators/is-expired.decorator';
import {IsMissing} from '@decorators/is-missing.decorator';

@IsNotEvaluated()
@IsInvalid()
@IsValid()
@IsExpired()
@IsMissing()
@Component({
  selector: 'chk-validation-info',
  templateUrl: './validation-info.component.html',
  styleUrls: ['./validation-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationInfoComponent extends BaseStatuses {
  private _dataValidation: EmploymentDataValidation[];
  private referenceValue: EmploymentDataValidation;
  tableData = [];
  @Input() useTable = false;
  @Input() ignoreReferenceValue = false;

  @Input() set dataValidation(value: EmploymentDataValidation[]) {
    if (value == null) {
      return;
    }
    this._dataValidation = value;
    this.referenceValue = this.ignoreReferenceValue ? this.dataValidation.find(data => data.referenceFieldName != null)
      : this.dataValidation.find(data => data.referenceFieldName != null && data.referenceValue != null);
    this.generateTableData();
  }

  get dataValidation(): EmploymentDataValidation[] {
    return this._dataValidation;
  }

  get reference(): EmploymentDataValidation {
    return this.referenceValue;
  }

  private generateTableData(): void {
    if (!this.useTable) {
      return;
    }
    const numberOfColumns = 4;
    const iterations = Math.ceil(this._dataValidation.length / numberOfColumns);
    for (let i = 0; i < iterations; i++) {
      this.tableData.push(this.dataValidation.slice(i * numberOfColumns, i * numberOfColumns + numberOfColumns));
    }
  }
}
