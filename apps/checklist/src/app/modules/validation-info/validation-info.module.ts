import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TagModule} from '../tag/tag.module';
import {ValidationInfoComponent} from './validation-info/validation-info.component';
import {ValidationFieldComponent} from './validation-field/validation-field.component';
import {ToDateModule} from '../to-date/to-date.module';

@NgModule({
  declarations: [
    ValidationInfoComponent,
    ValidationFieldComponent,
  ],
  imports: [
    CommonModule,
    TagModule,
    ToDateModule
  ],
  exports: [ValidationInfoComponent]
})
export class ValidationInfoModule {
}
