import {ValidationStatus} from '@models/enums';

export function IsMissing() {
  return (target) => {
    target.prototype.isMissing = (status: ValidationStatus): boolean => {
      return status === ValidationStatus.Missing;
    };
  };
}
