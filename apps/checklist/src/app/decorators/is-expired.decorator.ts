import {ValidationStatus} from '@models/enums';

export function IsExpired() {
  return (target) => {
    target.prototype.isExpired = (status: ValidationStatus): boolean => {
      return status === ValidationStatus.Expired;
    };
  };
}
