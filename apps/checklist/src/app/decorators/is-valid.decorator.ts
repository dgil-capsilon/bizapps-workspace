import {ValidationStatus} from '@models/enums';

export function IsValid() {
  return (target) => {
    target.prototype.isValid = (status: ValidationStatus): boolean => {
      return status === ValidationStatus.Valid;
    };
  };
}
