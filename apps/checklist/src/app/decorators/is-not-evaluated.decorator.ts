import {ValidationStatus} from '@models/enums';

export function IsNotEvaluated() {
  return (target) => {
    target.prototype.isNotEvaluated = (status: ValidationStatus): boolean => {
      return status === ValidationStatus.NotEvaluated;
    };
  };
}
