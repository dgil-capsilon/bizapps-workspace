import {RuleStatus} from '@models/enums';

export function IsNotDefined() {
  return (target) => {
    target.prototype.isNotDefined = (status: RuleStatus): boolean => {
      return status === RuleStatus.NotEvaluated || status === RuleStatus.NotApplicable;
    };
  };
}
