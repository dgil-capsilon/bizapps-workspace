import {ValidationStatus} from '@models/enums';

export function IsInvalid() {
  return (target) => {
    target.prototype.isInvalid = (status: ValidationStatus): boolean => {
      return status === ValidationStatus.Invalid;
    };
  };
}
