import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FolderStatus} from '@models/enums/folder-status';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {InitializeAction} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';

@Component({
  selector: 'chk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  isStatusFine$ = this.store$.select(fromFolder.getSpecificStatus, {status: FolderStatus.Fine});
  isStatusNotInitialized$ = this.store$.select(fromFolder.getSpecificStatus, {status: FolderStatus.NotInitialized});
  isRuleSelected$ = this.store$.select(fromRuleDetails.getRuleDetails);

  constructor(private store$: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store$.dispatch(InitializeAction());
  }
}
