import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, ofType} from '@ngrx/effects';
import {FolderReloadAction} from '@redux/folder/folder.actions';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CacheInterceptor implements HttpInterceptor {
  private storedData = new Map<string, any>();

  constructor(private actions$: Actions) {
    this.actions$.pipe(ofType(FolderReloadAction)).subscribe(() => this.storedData.clear());
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.method !== 'GET') {
      return next.handle(request);
    }

    if (this.storedData.has(request.url)) {
      return of(this.storedData.get(request.url));
    }

    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse && this.hasRuleData(request.url)) {
          this.storedData.set(request.url, event);
        }
      })
    );
  }

  private hasRuleData(url: string): boolean {
    return url.includes('checklist/rules/');
  }

}
