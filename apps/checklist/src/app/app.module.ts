import {OverlayModule} from '@angular/cdk/overlay';
import {I18nPluralPipe} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouteReuseStrategy} from '@angular/router';
import {environment} from '@environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {reducerProvider, REDUCERS_TOKEN} from '@redux/app.reducer';
import {ApplicantsEffects} from '@redux/applicants/applicants.effects';
import {FolderEffects} from '@redux/folder/folder.effects';
import {RuleDetailsEffects} from '@redux/rule-details/rule-details.effects';
import {RulesEffects} from '@redux/rules/rules.effects';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {ConfirmDialogComponent, DialogsModule, DocReaderModule, ReasonDialogComponent} from '@bizapps-workspace/components';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ArrowsNavComponent} from './components/arrows-nav/arrows-nav.component';
import {EmptyRuleComponent} from './components/empty-rule/empty-rule.component';
import {HeaderComponent} from './components/header/header.component';
import {ListStatusComponent} from './components/list-status/list-status.component';
import {LoaderComponent} from './components/loader/loader.component';
import {NoRulesComponent} from './components/no-rules/no-rules.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {RuleStatusFilterComponent} from './components/rule-status-filter/rule-status-filter.component';
import {RuleComponent} from './components/rule/rule.component';
import {RulesListComponent} from './components/rules-list/rules-list.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {StatusIconComponent} from './components/status-icon/status-icon.component';
import {ViewPartitionComponent} from './components/view-partition/view-partition.component';
import {ClampDirective} from './directives/clamp.directive';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {CacheInterceptor} from './interceptors/cache.interceptor';
import {DocNamePipe} from './modules/doc-name/doc-name.pipe';
import {HyphenModule} from './modules/hyphen/hyphen.module';
import {ScrollHoverModule} from './modules/scroll-hover/scroll-hover.module';
import {TagModule} from './modules/tag/tag.module';
import {ToDateModule} from './modules/to-date/to-date.module';
import {ChecklistErrorHandler, initializeLoggerService} from './services/logger-utils';
import {LoggerService} from './services/logger.service';
import {RulesRouteReuseStrategy} from './services/rules-route-reuse-strategy';

const MATERIALS_MODULES = [
  MatSidenavModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatTooltipModule,
  MatDialogModule,
  MatRadioModule
];

const CDK_MODULES = [OverlayModule];
const PROVIDERS = [];

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ListStatusComponent,
    RulesListComponent,
    HeaderComponent,
    RuleComponent,
    NotFoundComponent,
    ClampDirective,
    EmptyRuleComponent,
    StatusIconComponent,
    ArrowsNavComponent,
    LoaderComponent,
    ViewPartitionComponent,
    RuleStatusFilterComponent,
    NoRulesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MATERIALS_MODULES,
    CDK_MODULES,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    DocReaderModule.forRoot(),
    TagModule,
    ToDateModule,
    HyphenModule,
    ScrollHoverModule,
    StoreModule.forRoot(REDUCERS_TOKEN),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production, name: 'Checklist'}),
    EffectsModule.forRoot([FolderEffects, ApplicantsEffects, RuleDetailsEffects, RulesEffects]),
    DialogsModule
  ],
  providers: [
    PROVIDERS,
    reducerProvider,
    InjectableRxStompConfig,
    I18nPluralPipe,
    DocNamePipe,
    {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: RouteReuseStrategy, useClass: RulesRouteReuseStrategy},
    {provide: RxStompService, useFactory: rxStompServiceFactory, deps: [InjectableRxStompConfig]},
    {provide: APP_INITIALIZER, useFactory: initializeLoggerService, deps: [LoggerService], multi: true},
    {provide: ErrorHandler, useClass: ChecklistErrorHandler}
  ],
  entryComponents: [
    ReasonDialogComponent,
    ConfirmDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'passed',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/rule-passed.svg'));
    iconRegistry.addSvgIcon(
      'failed',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/rule-failed.svg'));
    iconRegistry.addSvgIcon(
      'waived',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/rule-waived.svg'));
    iconRegistry.addSvgIcon(
      'reverse',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/reverse.svg'));
    iconRegistry.addSvgIcon(
      'arrow-left',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow-left.svg'));
    iconRegistry.addSvgIcon(
      'popout',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/popout.svg'));
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/edit.svg'));
    iconRegistry.addSvgIcon(
      'cross',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/cross.svg'));
  }
}
