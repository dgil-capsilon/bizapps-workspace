import {Dictionary} from '@ngrx/entity';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {RulesAdapter, RulesState} from '@redux/rules/rules.reducer';
import {ApplicantRule} from '@models/data/app';
import {RuleStatus} from '@models/enums';

export const getRulesState = createFeatureSelector<RulesState>('rules');

export const {
  selectIds: getRuleIds,
  selectEntities: getRuleEntities,
} = RulesAdapter.getSelectors(getRulesState);

export type RulesAdapterSelector<T> = MemoizedSelector<RulesState, T>;

export const getSelectedRuleId = createSelector(
  getRulesState,
  (state: RulesState) => state.selectedRuleInfoId
);

export const getSearchTerm = createSelector(
  getRulesState,
  (state: RulesState) => state.searchTerm
);

export const getSelectedRuleStatus = createSelector(
  getSelectedRuleId,
  getRuleEntities,
  (selectedRuleId: number, ruleEntities: Dictionary<ApplicantRule>) => {
    return selectedRuleId && ruleEntities[selectedRuleId] ? ruleEntities[selectedRuleId].status : null;
  }
);

export const getSelectedRuleDescription = createSelector(
  getSelectedRuleId,
  getRuleEntities,
  (selectedRuleId: number, ruleEntities: Dictionary<ApplicantRule>) => {
    return selectedRuleId && ruleEntities[selectedRuleId] ? ruleEntities[selectedRuleId].description : null;
  }
);

export const getSelectedRuleCode = createSelector(
  getSelectedRuleId,
  getRuleEntities,
  (selectedRuleId: number, ruleEntities: Dictionary<ApplicantRule>) => {
    return selectedRuleId && ruleEntities[selectedRuleId] ? ruleEntities[selectedRuleId].ruleId : null;
  }
);

export const getAllRulesNotApplicable = createSelector(
  getRuleIds,
  getRuleEntities,
  (ruleIds: number[], ruleEntities: Dictionary<ApplicantRule>) => {
    return ruleIds.every(id => ruleEntities[id].status === RuleStatus.NotApplicable);
  }
);

export const getRulesWithStatus = createSelector(
  getRuleIds,
  getRuleEntities,
  (ruleIds: number[], ruleEntities: Dictionary<ApplicantRule>, props) => {
    const rules = ruleIds.filter(id => ruleEntities[id].status === props.status);
    return rules.length;
  }
);

export const getNextFailedRule = createSelector(
  getRulesState,
  (state: RulesState) => state.nextFailedRuleId
);
