import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import * as RuleInfoActions from '@redux/rules/rules.actions';
import {ApplicantRule} from '@models/data.interface';

export interface RulesState extends EntityState<ApplicantRule> {
  selectedRuleInfoId: number;
  previousRuleId: number;
  nextRuleId: number;
  searchTerm: string;
  nextFailedRuleId: number;
}

export const RulesAdapter: EntityAdapter<ApplicantRule> = createEntityAdapter<ApplicantRule>({
  selectId: (instance) => instance.id
});

export const initialState: RulesState = RulesAdapter.getInitialState({
  selectedRuleInfoId: null,
  previousRuleId: null,
  nextRuleId: null,
  searchTerm: null,
  nextFailedRuleId: null
});

export const RulesReducer = createReducer(
  initialState,
  on(RuleInfoActions.SetRules, (state, action) => RulesAdapter.addAll(action.rules, state)),
  on(RuleInfoActions.SelectRule, (state, action) => ({...state, selectedRuleInfoId: action.id})),
  on(RuleInfoActions.SetPreviousRuleId, (state, action) => ({...state, previousRuleId: action.id})),
  on(RuleInfoActions.SetNextRuleId, (state, action) => ({...state, nextRuleId: action.id})),
  on(RuleInfoActions.SetSearchTerm, (state, action) => ({...state, searchTerm: action.searchTerm})),
  on(RuleInfoActions.SetNextFailedRule, (state, action) => ({...state, nextFailedRuleId: action.id}))
);
