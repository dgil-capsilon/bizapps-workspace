import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {provideMockActions} from '@ngrx/effects/testing';
import {Dictionary} from '@ngrx/entity';
import {Action, Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '@redux/app.reducer';
import {SetApplicant} from '@redux/applicants/applicants.actions';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {LoadRule} from '@redux/rule-details/rule-details.actions';
import {NoRules, SelectNextRule, SelectPreviousRule, SelectRule} from '@redux/rules/rules.actions';
import {RulesEffects} from '@redux/rules/rules.effects';
import * as fromRules from '@redux/rules/rules.selectors';
import {RulesAdapterSelector} from '@redux/rules/rules.selectors';
import {cold, hot, initTestScheduler} from 'jasmine-marbles';
import {Observable} from 'rxjs';
import {ApplicantRule} from '@models/data';
import {ROUTER_CONSTS} from '@models/router-consts';
import {DataService} from '@app/services/data.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('RulesEffects', () => {
  let actions$: Observable<Action>;
  let effects: RulesEffects;
  let store: MockStore<any>;
  const routerSpy: SpyObj<Router> = createSpyObj('Router', ['navigate']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RulesEffects,
        provideMockActions(() => actions$),
        provideMockStore(),
        {provide: DataService, useValue: jasmine.createSpyObj('DataService', ['waiveRule', 'unWaiveRule', 'editWaiveRule'])},
        {provide: MatDialog, useValue: jasmine.createSpyObj('MatDialog', ['open'])},
        {provide: Router, useValue: routerSpy}
      ]
    });

    effects = TestBed.get<RulesEffects>(RulesEffects);
    store = TestBed.get<Store<AppState>>(Store);
    initTestScheduler();
  });

  it('should navigate to "no rules" view', () => {
    actions$ = cold('a', {a: NoRules()});

    effects.noRulesNavigate$.subscribe(() => {
      expect(routerSpy.navigate).toHaveBeenCalledWith([ROUTER_CONSTS.noRules], {queryParamsHandling: 'merge'});
    });
  });

  it('should select next rule', () => {
    store.overrideSelector(fromApplicants.getNextRuleId, 23);
    store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
      1: {} as ApplicantRule,
      23: {applicant: 16} as ApplicantRule,
      13: {} as ApplicantRule
    });

    actions$ = hot('a', {a: SelectNextRule()});

    const expected = cold('(abc)', {
      a: SetApplicant({id: 16}),
      b: SelectRule({id: 23}),
      c: LoadRule()
    });

    expect(effects.selectNextRule$).toBeObservable(expected);
  });

  it('should select previous rule', () => {
    store.overrideSelector(fromApplicants.getPreviousRuleId, 53);
    store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
      11: {} as ApplicantRule,
      53: {applicant: 2} as ApplicantRule,
      99: {} as ApplicantRule
    });

    actions$ = hot('a', {a: SelectPreviousRule()});

    const expected = cold('(abc)', {
      a: SetApplicant({id: 2}),
      b: SelectRule({id: 53}),
      c: LoadRule()
    });

    expect(effects.selectPreviousRule$).toBeObservable(expected);
  });

});
