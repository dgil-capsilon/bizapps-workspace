import {createAction, props} from '@ngrx/store';
import {ApplicantRule} from '@models/data.interface';

export const SetRules = createAction(
  '[Rules] SET RULES',
  props<{ rules: ApplicantRule[] }>()
);

export const SetPreviousRuleId = createAction(
  '[Rules] SET_PREVIOUS_RULE_ID',
  props<{ id: number }>()
);

export const SetNextRuleId = createAction(
  '[Rules] SET_NEXT_RULE_ID',
  props<{ id: number }>()
);

export const SelectRule = createAction(
  '[Rules] SET_ID',
  props<{ id: number }>()
);

export const SelectNextRule = createAction(
  '[Rules] SELECT_NEXT_RULE'
);

export const SelectPreviousRule = createAction(
  '[Rules] SELECT_PREVIOUS_RULE'
);

export const SetSearchTerm = createAction(
  '[Rules] SET_SEARCH_TERM',
  props<{ searchTerm: string }>()
);

export const WaiveRule = createAction(
  '[Rules] WAIVE_RULE',
  props<{ reason: string }>()
);

export const EditWaiveRule = createAction(
  '[Rules] EDIT_WAIVE_RULE',
  props<{ reason: string }>()
);

export const UnWaiveRule = createAction(
  '[Rules] UNWAIVE_RULE'
);

export const NoRules = createAction(
  '[Rules] NO_RULES'
);

export const SetNextFailedRule = createAction(
  '[Rules] SET_FAILED_RULE',
  props<{ id: number }>()
);
