import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {SetApplicant} from '@redux/applicants/applicants.actions';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {FolderReloadAction} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import {LoadRule} from '@redux/rule-details/rule-details.actions';
import {EditWaiveRule, NoRules, SelectNextRule, SelectPreviousRule, SelectRule, UnWaiveRule, WaiveRule} from '@redux/rules/rules.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {map, mergeMap, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {ROUTER_CONSTS} from '@models/router-consts';
import {DataService} from '@app/services/data.service';

@Injectable()
export class RulesEffects {

  @Effect()
  selectNextRule$ = this.actions$
    .pipe(
      ofType(SelectNextRule),
      withLatestFrom(
        this.store$.select(fromApplicants.getNextRuleId),
        this.store$.select(fromRules.getRuleEntities)
      ),
      mergeMap(([, nextRuleId, ruleEntities]) => {
        return [
          SetApplicant({id: ruleEntities[nextRuleId].applicant}),
          SelectRule({id: nextRuleId}),
          LoadRule()
        ];
      })
    );

  @Effect()
  selectPreviousRule$ = this.actions$
    .pipe(
      ofType(SelectPreviousRule),
      withLatestFrom(
        this.store$.select(fromApplicants.getPreviousRuleId),
        this.store$.select(fromRules.getRuleEntities)
      ),
      mergeMap(([, previousRuleId, ruleEntities]) => {
        return [
          SetApplicant({id: ruleEntities[previousRuleId].applicant}),
          SelectRule({id: previousRuleId}),
          LoadRule()
        ];
      })
    );

  @Effect()
  waiveRule$ = this.actions$
    .pipe(
      ofType(WaiveRule),
      withLatestFrom(
        this.store$.select(fromFolder.getId),
        this.store$.select(fromApplicants.getSelectedApplicantId),
        this.store$.select(fromRules.getSelectedRuleId)
      ),
      switchMap(([action, folderId, applicantId, ruleId]) => {
        return this.dataService.waiveRule(action.reason, folderId, applicantId, ruleId)
          .pipe(
            map(() => {
              this.dialog.closeAll();
              return FolderReloadAction();
            })
          );
      })
    );

  @Effect()
  editWaiveRule$ = this.actions$
    .pipe(
      ofType(EditWaiveRule),
      withLatestFrom(
        this.store$.select(fromFolder.getId),
        this.store$.select(fromApplicants.getSelectedApplicantId),
        this.store$.select(fromRules.getSelectedRuleId)
      ),
      switchMap(([action, folderId, applicantId, ruleId]) => {
        return this.dataService.editWaiveRule(action.reason, folderId, applicantId, ruleId)
          .pipe(
            map(() => {
              this.dialog.closeAll();
              return FolderReloadAction();
            })
          );
      })
    );

  @Effect()
  unWaiveRule$ = this.actions$
    .pipe(
      ofType(UnWaiveRule),
      withLatestFrom(
        this.store$.select(fromFolder.getId),
        this.store$.select(fromApplicants.getSelectedApplicantId),
        this.store$.select(fromRules.getSelectedRuleId)
      ),
      switchMap(([, folderId, applicantId, ruleId]) => {
        return this.dataService.unWaiveRule(folderId, applicantId, ruleId)
          .pipe(
            map(() => {
              this.dialog.closeAll();
              return FolderReloadAction();
            })
          );
      })
    );

  @Effect({dispatch: false})
  noRulesNavigate$ = this.actions$
    .pipe(
      ofType(NoRules),
      tap(() => this.router.navigate([ROUTER_CONSTS.noRules], {queryParamsHandling: 'merge'}))
    );

  constructor(private actions$: Actions,
              private store$: Store<AppState>,
              private dataService: DataService,
              private dialog: MatDialog,
              private router: Router) {
  }
}
