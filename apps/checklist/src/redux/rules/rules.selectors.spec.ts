import * as fromRules from '@redux/rules/rules.selectors';
import {RuleStatus} from '@models/enums';

describe('Rules Selectors', () => {
  it('getSelectedRuleId should return selected rule id', () => {
    expect(
      fromRules.getSelectedRuleId.projector({selectedRuleInfoId: 111})
    ).toEqual(111);
  });

  it('getSearchTerm should return search term', () => {
    expect(
      fromRules.getSearchTerm.projector({searchTerm: 'INC-110'})
    ).toEqual('INC-110');
  });

  describe('getSelectedRuleStatus', () => {
    it('should return selected rule status', () => {
      const selectedRuleId = 6;
      const ruleEntities = {
        6: {status: RuleStatus.Failed}
      };

      expect(
        fromRules.getSelectedRuleStatus.projector(selectedRuleId, ruleEntities)
      ).toEqual(RuleStatus.Failed);
    });

    it('should return null when no rule is selected', () => {
      const ruleEntities = {
        6: {status: RuleStatus.Failed}
      };

      expect(
        fromRules.getSelectedRuleStatus.projector(null, ruleEntities)
      ).toEqual(null);
    });
  });

  describe('getSelectedRuleDescription', () => {
    it('should return selected rule description', () => {
      const selectedRuleId = 3;
      const ruleEntities = {
        3: {description: 'Is the SSN on the W‑2 in a valid format?'}
      };

      expect(
        fromRules.getSelectedRuleDescription.projector(selectedRuleId, ruleEntities)
      ).toEqual('Is the SSN on the W‑2 in a valid format?');
    });

    it('should return null when no rule is selected', () => {
      const ruleEntities = {
        3: {description: 'Is the SSN on the W‑2 in a valid format?'}
      };

      expect(
        fromRules.getSelectedRuleDescription.projector(null, ruleEntities)
      ).toEqual(null);
    });
  });

  describe('getSelectedRuleCode', () => {
    it('should return selected rule code', () => {
      const selectedRuleId = 3;
      const ruleEntities = {
        3: {ruleId: 'INC-050'}
      };

      expect(
        fromRules.getSelectedRuleCode.projector(selectedRuleId, ruleEntities)
      ).toEqual('INC-050');
    });

    it('should return null when no rule is selected', () => {
      const ruleEntities = {
        3: {ruleId: 'INC-050'}
      };

      expect(
        fromRules.getSelectedRuleCode.projector(null, ruleEntities)
      ).toEqual(null);
    });
  });

  describe('getAllRulesNotApplicable', () => {
    it('should return true when all rules are not applicable', () => {
      const ruleIds = [1, 2, 3, 4];
      const ruleEntities = {
        1: {status: RuleStatus.NotApplicable},
        2: {status: RuleStatus.NotApplicable},
        3: {status: RuleStatus.NotApplicable},
        4: {status: RuleStatus.NotApplicable}
      };

      expect(
        fromRules.getAllRulesNotApplicable.projector(ruleIds, ruleEntities)
      ).toEqual(true);
    });

    it('should return false when not all rules are not applicable', () => {
      const ruleIds = [1, 2, 3, 4];
      const ruleEntities = {
        1: {status: RuleStatus.Passed},
        2: {status: RuleStatus.Failed},
        3: {status: RuleStatus.NotEvaluated},
        4: {status: RuleStatus.NotApplicable}
      };

      expect(
        fromRules.getAllRulesNotApplicable.projector(ruleIds, ruleEntities)
      ).toEqual(false);
    });
  });
});
