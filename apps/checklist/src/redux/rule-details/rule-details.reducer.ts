import {createReducer, on} from '@ngrx/store';
import * as RuleActions from '@redux/rule-details/rule-details.actions';
import {Rule} from '@models/data.interface';

export interface RuleDetailsState {
  rule: Rule;
  loading: boolean;
}

const initialState: RuleDetailsState = {
  rule: null,
  loading: true
};

export const RuleDetailsReducer = createReducer(
  initialState,
  on(RuleActions.LoadRule, (state) => ({...state, loading: true})),
  on(RuleActions.LoadRuleSuccess, (state, action) => ({...state, rule: action.rule, loading: false})),
  on(RuleActions.LoadRuleFailure, (state) => ({...state, loading: false}))
);
