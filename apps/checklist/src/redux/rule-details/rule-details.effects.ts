import {Injectable} from '@angular/core';
import {ChecklistStompService} from '@app/services/checklist-stomp.service';
import {DataService} from '@app/services/data.service';
import {catchErrorWithLogging} from '@app/services/logger-utils';
import {StompEvent} from '@models/data.enum';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {FolderReloadAction, SetIdAction} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import {LoadRule, LoadRuleFailure, LoadRuleSuccess} from '@redux/rule-details/rule-details.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {of} from 'rxjs';
import {filter, map, switchMap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class RuleDetailsEffects {

  @Effect()
  loadRuleDetails$ = this.actions$
    .pipe(
      ofType(LoadRule),
      withLatestFrom(this.store$.select(fromRules.getSelectedRuleId)),
      switchMap(([, ruleId]) => {
        if (!ruleId) {
          return of(LoadRuleSuccess({rule: null}));
        }

        return this.dataService.getRuleData(ruleId)
          .pipe(
            map(data => LoadRuleSuccess({rule: data})),
            catchErrorWithLogging(of(LoadRuleFailure()))
          );
      })
    );

  @Effect()
  watchFolderChanges$ = this.actions$
    .pipe(
      ofType(SetIdAction),
      switchMap(action => this.checklistStompService.folderEventWatch(action.id)),
      withLatestFrom(this.store$.select(fromFolder.getLoanStatus)),
      filter(([folderEvent, loanStatus]) => {
        const whitelistedEventType = folderEvent.eventType === StompEvent.ChecklistUpdated
          || folderEvent.eventType === StompEvent.LoadStatusUpdate;
        const changedLoanStatus = loanStatus !== folderEvent.loanStatus;

        return whitelistedEventType && changedLoanStatus;
      }),
      map(() => FolderReloadAction())
    );

  constructor(private actions$: Actions,
              private store$: Store<AppState>,
              private dataService: DataService,
              private checklistStompService: ChecklistStompService) {
  }
}
