import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';

describe('Rule-Details Selectors', () => {
  it('getStatus should return status', () => {
    expect(
      fromRuleDetails.getRuleDetails.projector({rule: {id: 123}})
    ).toEqual(jasmine.objectContaining({id: 123}));
  });

  it('getLoading should return loading (whenever rule loaded)', () => {
    expect(
      fromRuleDetails.getLoading.projector({loading: true})
    ).toEqual(true);
  });
});
