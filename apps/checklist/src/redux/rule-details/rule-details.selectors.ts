import {createFeatureSelector, createSelector} from '@ngrx/store';
import {RuleDetailsState} from '@redux/rule-details/rule-details.reducer';

export const getRuleDetailsState = createFeatureSelector<RuleDetailsState>('ruleDetails');

export const getRuleDetails = createSelector(
  getRuleDetailsState,
  state => state.rule
);

export const getLoading = createSelector(
  getRuleDetailsState,
  state => state.loading
);

export const getRuleDetailsCode = createSelector(
  getRuleDetails,
  ruleDetails => ruleDetails ? ruleDetails.ruleCode : null
);

export const getRuleDetailsWaive = createSelector(
  getRuleDetails,
  ruleDetails => ruleDetails ? ruleDetails.waive : null
);

export const getRuleDetailsCanWaive = createSelector(
  getRuleDetails,
  ruleDetails => ruleDetails ? ruleDetails.canWaive : null
);
