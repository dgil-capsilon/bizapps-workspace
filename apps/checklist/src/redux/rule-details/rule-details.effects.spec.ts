import {TestBed} from '@angular/core/testing';
import {ChecklistStompService} from '@app/services/checklist-stomp.service';
import {DataService} from '@app/services/data.service';
import {LoanStatus, StompEvent} from '@models/enums';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action, Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '@redux/app.reducer';
import {FolderReloadAction, SetIdAction} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import {LoadRule, LoadRuleFailure, LoadRuleSuccess} from '@redux/rule-details/rule-details.actions';
import {RuleDetailsEffects} from '@redux/rule-details/rule-details.effects';
import * as fromRules from '@redux/rules/rules.selectors';
import {Observable, of, throwError} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('RuleDetailsEffects', () => {
  let actions$: Observable<Action>;
  let effects: RuleDetailsEffects;
  let dataServiceSpy: SpyObj<DataService>;
  let checklistStompServiceSpy: SpyObj<ChecklistStompService>;
  let store: MockStore<{ folder: { loanStatus: LoanStatus } }>;

  beforeEach(() => {
    dataServiceSpy = createSpyObj('DataService', ['getRuleData']);
    checklistStompServiceSpy = createSpyObj('ChecklistStompService', ['folderEventWatch']);

    TestBed.configureTestingModule({
      providers: [
        RuleDetailsEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          selectors: [
            {
              selector: fromRules.getSelectedRuleId,
              value: 756
            }
          ]
        }),
        {provide: DataService, useValue: dataServiceSpy},
        {provide: ChecklistStompService, useValue: checklistStompServiceSpy}
      ]
    });

    store = TestBed.get<Store<AppState>>(Store);
    effects = TestBed.get<RuleDetailsEffects>(RuleDetailsEffects);
  });

  describe('loadRuleDetails$', () => {
    it('should return rule data', () => {
      dataServiceSpy.getRuleData.and.returnValue(of({folderId: 12345} as any));
      actions$ = of(LoadRule);

      effects.loadRuleDetails$.subscribe(action => {
        expect(dataServiceSpy.getRuleData).toHaveBeenCalledWith(756);
        expect(action).toEqual(LoadRuleSuccess({rule: {folderId: 12345}}));
      });
    });

    it('should catch error when rule detail request throw', () => {
      spyOn(console, 'error');
      dataServiceSpy.getRuleData.and.returnValue(throwError('Some error on rule details request'));
      actions$ = of(LoadRule);

      effects.loadRuleDetails$.subscribe(action => {
        expect(dataServiceSpy.getRuleData).toHaveBeenCalledWith(756);
        expect(action).toEqual(LoadRuleFailure());
      });
    });
  });

  describe('watchFolderChanges$', () => {
    it('should reload', () => {
      store.overrideSelector(fromFolder.getLoanStatus, LoanStatus.Lock);
      checklistStompServiceSpy.folderEventWatch.and.returnValue(
        of({loanStatus: LoanStatus.Open, eventType: StompEvent.ChecklistUpdated} as any)
      );

      actions$ = of(SetIdAction({id: '123'}));

      effects.watchFolderChanges$.subscribe(action => {
        expect(checklistStompServiceSpy.folderEventWatch).toHaveBeenCalledWith('123');
        expect(action).toEqual(FolderReloadAction());
      });
    });

    it('should not reload for not whitelisted event type', () => {
      store.overrideSelector(fromFolder.getLoanStatus, LoanStatus.Lock);
      checklistStompServiceSpy.folderEventWatch.and.returnValue(
        of({loanStatus: LoanStatus.Open, eventType: StompEvent.EmployerMerge} as any)
      );

      actions$ = of(SetIdAction({id: '123'}));

      effects.watchFolderChanges$.subscribe(() => {
        fail('Should filter out not whitelisted event type');
      });
    });

    it('should not reload for not changed loan status', () => {
      store.overrideSelector(fromFolder.getLoanStatus, LoanStatus.Open);
      checklistStompServiceSpy.folderEventWatch.and.returnValue(
        of({loanStatus: LoanStatus.Open, eventType: StompEvent.ChecklistUpdated} as any)
      );

      actions$ = of(SetIdAction({id: '123'}));

      effects.watchFolderChanges$.subscribe(() => {
        fail('Should filter out same loan status');
      });
    });
  });
});
