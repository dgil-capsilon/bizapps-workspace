import {createAction, props} from '@ngrx/store';

export const LoadRule = createAction(
  '[Rule Details API] LOAD'
);

export const LoadRuleFailure = createAction(
  '[Rule Details] LOAD_FAILURE'
);

export const LoadRuleSuccess = createAction(
  '[Rule Details] LOAD_SUCCESS',
  props<{ rule: any }>()
);
