import { createAction, props } from '@ngrx/store';
import { LoanStatus, StatusFilter } from '@models/data.enum';
import { FolderStatus } from '@models/enums/folder-status';

export const SetIdAction = createAction(
  '[Folder] SET_ID',
  props<{ id: string }>()
);

export const SetContextAction = createAction(
  '[Folder] SET_CONTEXT',
  props<{context: string}>()
)

export const NoIdAction = createAction(
  '[Folder] NO_ID'
);

export const InitializeAction = createAction(
  '[Folder] INITIALIZE'
);

export const FolderReloadAction = createAction(
  '[Folder] RELOAD'
);

export const SetStatusFilter = createAction(
  '[Folder] RULES_STATUS_FILTER',
  props<{ statusFilter: StatusFilter }>()
);

export const SetStatusAction = createAction(
  '[Folder] SET_STATUS',
  props<{ status: FolderStatus }>()
);

export const SetLoanStatus = createAction(
  '[Folder] SET_LOAN_STATUS',
  props<{ loanStatus: LoanStatus }>()
);
