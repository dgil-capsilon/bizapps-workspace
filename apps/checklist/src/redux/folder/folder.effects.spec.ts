import {TestBed} from '@angular/core/testing';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action} from '@ngrx/store';
import {LoadApplicants} from '@redux/applicants/applicants.actions';
import {InitializeAction, NoIdAction, SetIdAction} from '@redux/folder/folder.actions';
import {FolderEffects} from '@redux/folder/folder.effects';
import {Observable, of} from 'rxjs';
import createSpy = jasmine.createSpy;

describe('FolderEffects', () => {
  let actions$: Observable<Action>;
  let effects: FolderEffects;
  const routerMock = {events: of()};
  const getSpy = createSpy();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FolderEffects,
        provideMockActions(() => actions$),
        {provide: ActivatedRoute, useValue: {queryParamMap: of({get: getSpy})}},
        {provide: Router, useValue: {events: of(new NavigationEnd(1, 'some/url', 'some/url/after/redirect'))}}
      ]
    });

    effects = TestBed.get<FolderEffects>(FolderEffects);
  });

  it('triggerApplicantsLoad$ should load applicants when folder id is set', () => {
    actions$ = of(SetIdAction);

    effects.triggerApplicantsLoad$.subscribe(action => {
      expect(action).toEqual(LoadApplicants());
    });
  });

  describe('setFolderId$', () => {
    it('should set folder id', () => {
      routerMock.events = of(new NavigationEnd(1, 'some/url', 'some/url/after/redirect'));
      getSpy.and.returnValue('321');
      actions$ = of(InitializeAction);

      effects.setFolderId$.subscribe(action => {
        expect(getSpy).toHaveBeenCalledWith('folder');
        expect(action).toEqual(SetIdAction({id: '321'}));
      });
    });

    it('should not set folder id (no param)', () => {
      routerMock.events = of(new NavigationEnd(1, 'some/url', 'some/url/after/redirect'));
      getSpy.and.returnValue(null);
      actions$ = of(InitializeAction);

      effects.setFolderId$.subscribe(action => {
        expect(getSpy).toHaveBeenCalledWith('folder');
        expect(action).toEqual(NoIdAction());
      });
    });
  });
});
