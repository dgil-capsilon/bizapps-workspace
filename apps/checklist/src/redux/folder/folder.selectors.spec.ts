import * as fromFolder from '@redux/folder/folder.selectors';
import {StatusFilter} from '@models/enums';
import {FolderStatus} from '@models/enums/folder-status';

describe('Folder Selectors', () => {
  it('getStatus should return status', () => {
    expect(
      fromFolder.getStatus.projector({status: 'FINE'})
    ).toEqual('FINE');
  });

  it('getId should return folder id', () => {
    expect(
      fromFolder.getId.projector({id: 23})
    ).toEqual(23 as any); // TODO: Check why number value can't be
  });

  it('getStatusFilter should return indicator to show all rules or only failed', () => {
    expect(
      fromFolder.getStatusFilter.projector({statusFilter: StatusFilter.Failed})
    ).toEqual(StatusFilter.Failed);
  });

  describe('getSpecificStatus', () => {
    it('should return true when status is "FINE"', () => {
      expect(
        fromFolder.getSpecificStatus.projector({status: FolderStatus.Fine}, {status: FolderStatus.Fine})
      ).toEqual(true);
    });

    it('should return false when status is not "FINE"', () => {
      expect(
        fromFolder.getSpecificStatus.projector({status: FolderStatus.NotInitialized}, {status: FolderStatus.Fine})
      ).toEqual(false);
    });
  });
});
