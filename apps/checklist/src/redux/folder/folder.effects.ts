import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, ParamMap, Router} from '@angular/router';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {LoadApplicants} from '@redux/applicants/applicants.actions';
import { InitializeAction, NoIdAction, SetContextAction, SetIdAction } from '@redux/folder/folder.actions';
import { filter, first, map, mergeMap, switchMapTo } from 'rxjs/operators';

@Injectable()
export class FolderEffects {
  private queryParamMapAfterNavigationEnd$ = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    first(),
    switchMapTo(this.route.queryParamMap)
  );

  @Effect()
  setFolderId$ = this.actions$
    .pipe(
      ofType(InitializeAction),
      switchMapTo(this.queryParamMapAfterNavigationEnd$),
      mergeMap((params: ParamMap) => {
        console.log(params);
        const folderIdParam = params.get('folder');
        const context = params.get('context');
        if (folderIdParam) {
          return [
            SetIdAction({id: folderIdParam}),
            SetContextAction({context: context})
          ];
        }

        return [NoIdAction()];
      })
    );

  @Effect()
  triggerApplicantsLoad$ = this.actions$.pipe(
    ofType(SetIdAction),
    map(LoadApplicants)
  );

  constructor(private actions$: Actions,
              private route: ActivatedRoute,
              private router: Router) {
  }
}
