import {createReducer, on} from '@ngrx/store';
import * as FolderActions from '@redux/folder/folder.actions';
import {LoanStatus, StatusFilter} from '@models/data.enum';
import {FolderStatus} from '@models/enums/folder-status';

export interface FolderState {
  id: string;
  status: FolderStatus;
  statusFilter: StatusFilter;
  loanStatus: LoanStatus;
  context: string;
}

const initialState: FolderState = {
  id: null,
  status: FolderStatus.NotInitialized,
  statusFilter: StatusFilter.Failed,
  loanStatus: LoanStatus.Open,
  context: ''
};

export const FolderReducer = createReducer(
  initialState,
  on(FolderActions.SetIdAction, (state, action) => ({...state, id: action.id})),
  on(FolderActions.SetContextAction, (state, action) => ({...state, context: action.context})),
  on(FolderActions.SetStatusAction, (state, action) => ({...state, status: action.status})),
  on(FolderActions.SetStatusFilter, (state, action) => ({...state, statusFilter: action.statusFilter})),
  on(FolderActions.SetLoanStatus, (state, action) => ({...state, loanStatus: action.loanStatus}))
);
