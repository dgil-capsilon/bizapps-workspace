import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FolderState } from '@redux/folder/folder.reducer';
import { LoanStatus } from '@models/enums';
import { FolderStatus } from '@models/enums/folder-status';

export const getFolderState = createFeatureSelector<FolderState>('folder');

export const getStatus = createSelector(
  getFolderState,
  (state: FolderState) => state.status
);

export const getId = createSelector(
  getFolderState,
  (state: FolderState) => state.id
);

export const getStatusFilter = createSelector(
  getFolderState,
  (state: FolderState) => state.statusFilter
);

export const getSpecificStatus = createSelector(
  getFolderState,
  (state: FolderState, props: { status: FolderStatus }) => state.status === props.status
);

export const getLoanStatus = createSelector(
  getFolderState,
  (state: FolderState) => state.loanStatus
);

export const getLoanStatusFolderOpen = createSelector(
  getFolderState,
  (state: FolderState) => state.loanStatus === LoanStatus.Open
);

export const getFolderContext = createSelector(
  getFolderState,
  (state: FolderState) => {
    console.log(state.context);
    return state.context === 'credit-analyzer';
  }
);
