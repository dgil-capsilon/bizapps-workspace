import {InjectionToken} from '@angular/core';
import {ActionReducerMap} from '@ngrx/store';
import {ApplicantsReducer, ApplicantsState} from '@redux/applicants/applicants.reducer';
import {FolderReducer, FolderState} from '@redux/folder/folder.reducer';
import {RuleDetailsReducer, RuleDetailsState} from '@redux/rule-details/rule-details.reducer';
import {RulesReducer, RulesState} from '@redux/rules/rules.reducer';

export interface AppState {
  folder: FolderState;
  applicants: ApplicantsState;
  ruleDetails: RuleDetailsState;
  rules: RulesState;
}

const rootReducer: ActionReducerMap<AppState> = {
  folder: FolderReducer,
  applicants: ApplicantsReducer,
  ruleDetails: RuleDetailsReducer,
  rules: RulesReducer
};

export const REDUCERS_TOKEN = new InjectionToken<ActionReducerMap<AppState>>('App Reducers');
export const reducerProvider = {provide: REDUCERS_TOKEN, useValue: rootReducer};
