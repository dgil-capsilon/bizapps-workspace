import {Dictionary} from '@ngrx/entity';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {ApplicantsAdapter, ApplicantsState} from '@redux/applicants/applicants.reducer';
import * as fromFolder from '@redux/folder/folder.selectors';
import * as fromRules from '@redux/rules/rules.selectors';
import {RuleStatus, StatusFilter} from '@models/enums';
import {ApplicantData, ApplicantRule} from '@models/data/app';

export const getApplicantsState = createFeatureSelector<ApplicantsState>('applicants');

export const {
  selectEntities: getApplicantEntities,
  selectAll: getAllApplicants,
  selectTotal: getTotalApplicants
} = ApplicantsAdapter.getSelectors(getApplicantsState);

export type ApplicantsAdapterSelector<T> = MemoizedSelector<ApplicantsState, T>;

export const getSelectedApplicantId = createSelector(
  getApplicantsState,
  (state: ApplicantsState) => state.selectedApplicantId
);

export const getAllRulesOfApplicant = createSelector(
  getApplicantEntities,
  fromRules.getRuleEntities,
  fromFolder.getStatusFilter,
  (applicantEntities: Dictionary<ApplicantData>, ruleEntities: Dictionary<ApplicantRule>, statusFilter: StatusFilter, props) => {
    const applicant = applicantEntities[props.id];

    if (!applicant) {
      return [];
    }

    return applicant.rules
      .reduce((rules, ruleId) => {
        const rule = ruleEntities[ruleId as any]; // TODO: Check casting

        if (!rule) {
          return rules;
        }

        if (statusFilter === StatusFilter.Waived && rule.status === RuleStatus.Waived) {
          rules.push(rule);
        } else if (statusFilter === StatusFilter.Failed && rule.status === RuleStatus.Failed) {
          rules.push(rule);
        } else if (statusFilter === StatusFilter.All) {
          rules.push(rule);
        }

        return rules;
      }, []);
  }
);

export const getAllFailedRules = createSelector(
  getAllApplicants,
  fromRules.getRuleEntities,
  (applicants: ApplicantData[], ruleEntities: Dictionary<ApplicantRule>) => {
    return applicants.reduce((rules, applicant) => {
      const failedRules = applicant.rules
        .filter(ruleId => ruleEntities[ruleId as any] ? ruleEntities[ruleId as any].status === RuleStatus.Failed : null);
      return [...rules, ...failedRules];
    }, []);
  }
);

export const getTotalFailedRules = createSelector(
  getAllFailedRules,
  (failedRules: number[]) => failedRules.length
);

export const getTotalRules = createSelector(
  getAllApplicants,
  (applicants: ApplicantData[]) => {
    return applicants.reduce((rules, applicant) => rules + applicant.rules.length, 0);
  }
);

export const getSelectedApplicantFullName = createSelector(
  getSelectedApplicantId,
  getApplicantEntities,
  (selectedApplicantId: number, applicantEntities: Dictionary<ApplicantData>) => {
    return selectedApplicantId && applicantEntities[selectedApplicantId] ? applicantEntities[selectedApplicantId].fullName : null;
  }
);

export const getAllNavigationRules = createSelector(
  getAllApplicants,
  fromRules.getRuleEntities,
  fromFolder.getStatusFilter,
  (applicants: ApplicantData[], ruleEntities: Dictionary<ApplicantRule>, statusFilter: StatusFilter) => {
    return applicants.reduce((rulesAcc, applicant) => {
      const rules = applicant.rules
        .filter(ruleId => {
          const rule = ruleEntities[ruleId as any];

          if (!rule || (rule && rule.id < 0)) {
            return false;
          }

          if (statusFilter === StatusFilter.All) {
            return rule.status === RuleStatus.Failed || rule.status === RuleStatus.Passed || rule.status === RuleStatus.Waived;
          } else if (statusFilter === StatusFilter.Waived) {
            return rule.status === RuleStatus.Waived;
          }

          return rule.status === RuleStatus.Failed;
        });
      return [...rulesAcc, ...rules];
    }, []);
  }
);

export const getPreviousRuleId = createSelector(
  fromRules.getSelectedRuleId,
  getAllNavigationRules,
  (id: number, rules: number[]) => {
    const selectedRuleArrayIndex = rules.indexOf(id);

    return rules[selectedRuleArrayIndex - 1];
  }
);

export const getNextRuleId = createSelector(
  fromRules.getSelectedRuleId,
  getAllNavigationRules,
  (id: number, rules: number[]) => {
    const selectedRuleArrayIndex = rules.indexOf(id);

    return rules[selectedRuleArrayIndex + 1];
  }
);
