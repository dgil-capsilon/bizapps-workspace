import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import {ApplicantData} from '@models/data.interface';
import * as ApplicantActions from '@redux/applicants/applicants.actions';

export interface ApplicantsState extends EntityState<ApplicantData> {
  selectedApplicantId: number;
}

export const ApplicantsAdapter: EntityAdapter<ApplicantData> = createEntityAdapter<ApplicantData>({
  selectId: selectApplicantId
});

function selectApplicantId(a: ApplicantData): number {
  return a.applicantId;
}

export const initialState: ApplicantsState = ApplicantsAdapter.getInitialState({
  selectedApplicantId: null
});

export const ApplicantsReducer = createReducer(
  initialState,
  on(ApplicantActions.LoadApplicantsSuccess, (state, action) => ApplicantsAdapter.addAll(action.applicants, state)),
  on(ApplicantActions.SetApplicant, (state, action) => ({...state, selectedApplicantId: action.id}))
);
