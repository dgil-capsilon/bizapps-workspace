import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {RuleStatus, StatusFilter} from '../../app/models/data.enum';

describe('Applicants Selectors', () => {
  it('getSelectedApplicantId should return selected applicant id', () => {
    expect(
      fromApplicants.getSelectedApplicantId.projector({selectedApplicantId: 61})
    ).toEqual(61);
  });

  describe('getAllRulesOfApplicant', () => {
    it('should return all rules (statusFilter - All)', () => {
      const applicantEntities = {
        213: {rules: [9, 11, 13, 15]}
      };
      const ruleEntities = {
        9: {id: 9, status: RuleStatus.Failed},
        11: {id: 11, status: RuleStatus.Passed},
        13: {id: 13, status: RuleStatus.NotApplicable},
        15: {id: 15, status: RuleStatus.NotEvaluated}
      };
      const statusFilter = StatusFilter.All;
      const props = {id: 213};

      expect(
        fromApplicants.getAllRulesOfApplicant.projector(applicantEntities, ruleEntities, statusFilter, props)
      ).toEqual([
        {id: 9, status: RuleStatus.Failed},
        {id: 11, status: RuleStatus.Passed},
        {id: 13, status: RuleStatus.NotApplicable},
        {id: 15, status: RuleStatus.NotEvaluated}
      ]);
    });

    it('should return failed rules (statusFilter - Failed)', () => {
      const applicantEntities = {
        213: {rules: [9, 11, 13, 15]}
      };
      const ruleEntities = {
        9: {id: 9, status: RuleStatus.Failed},
        11: {id: 11, status: RuleStatus.Passed},
        13: {id: 13, status: RuleStatus.NotApplicable},
        15: {id: 15, status: RuleStatus.NotEvaluated}
      };
      const statusFilter = StatusFilter.Failed;
      const props = {id: 213};

      expect(
        fromApplicants.getAllRulesOfApplicant.projector(applicantEntities, ruleEntities, statusFilter, props)
      ).toEqual([{id: 9, status: RuleStatus.Failed}]);
    });
  });

  it('getAllFailedRules should return all ids of failed rules', () => {
    const applicants = [
      {rules: [11, 13, 15]},
      {rules: [9, 14, 17]}
    ];
    const ruleEntities = {
      9: {id: 9, status: RuleStatus.Failed},
      11: {id: 11, status: RuleStatus.Passed},
      13: {id: 13, status: RuleStatus.NotApplicable},
      14: {id: 14, status: RuleStatus.Failed},
      15: {id: 15, status: RuleStatus.NotEvaluated},
      17: {id: 17, status: RuleStatus.Failed},
    };

    expect(
      fromApplicants.getAllFailedRules.projector(applicants, ruleEntities)
    ).toEqual([9, 14, 17]);
  });

  it('getTotalFailedRules should return number of all failed rules', () => {
    const failedRules = [1, 2, 3, 4, 5, 6];

    // TODO: Is this fine to provide value of "getAllFailedRules" selector?
    expect(
      fromApplicants.getTotalFailedRules.projector(failedRules)
    ).toEqual(6);
  });

  it('getTotalRules should return number of all rules', () => {
    const applicants = [
      {rules: [11, 13, 15]},
      {rules: [9, 14, 17]}
    ];

    expect(
      fromApplicants.getTotalRules.projector(applicants)
    ).toEqual(6);
  });

  describe('getSelectedApplicantFullName', () => {
    it('should return fullName of applicant', () => {
      const applicantEntities = {
        213: {fullName: 'John Homeowner'}
      };

      expect(
        fromApplicants.getSelectedApplicantFullName.projector(213, applicantEntities)
      ).toEqual('John Homeowner');
    });

    it('should return null if there is no applicant selected', () => {
      const applicantEntities = {
        213: {fullName: 'John Homeowner'}
      };

      // TODO: Is this fine to provide value of "getSelectedApplicantId" selector?
      expect(
        fromApplicants.getSelectedApplicantFullName.projector(null, applicantEntities)
      ).toEqual(null);
    });
  });

  describe('getAllNavigationRules', () => {
    it('should return all failed and passed rules (statusFilter - All)', () => {
      const applicants = [
        {rules: [1, 2, 3, 4]},
        {rules: [5, 6, 7, 8]}
      ];
      const ruleEntities = {
        1: {id: 1, status: RuleStatus.Passed},
        2: {id: 2, status: RuleStatus.Failed},
        3: {id: 3, status: RuleStatus.NotEvaluated},
        4: {id: 4, status: RuleStatus.NotApplicable},
        5: {id: 5, status: RuleStatus.Passed},
        6: {id: 6, status: RuleStatus.Failed},
        7: {id: 7, status: RuleStatus.Failed},
        8: {id: 8, status: RuleStatus.Failed}
      };
      const statusFilter = StatusFilter.All;

      expect(
        fromApplicants.getAllNavigationRules.projector(applicants, ruleEntities, statusFilter)
      ).toEqual([1, 2, 5, 6, 7, 8]);
    });

    it('should return all failed passed rules (statusFilter - Failed)', () => {
      const applicants = [
        {rules: [1, 2, 3, 4]},
        {rules: [5, 6, 7, 8]}
      ];
      const ruleEntities = {
        1: {id: 1, status: RuleStatus.Passed},
        2: {id: 2, status: RuleStatus.Failed},
        3: {id: 3, status: RuleStatus.NotEvaluated},
        4: {id: 4, status: RuleStatus.NotApplicable},
        5: {id: 5, status: RuleStatus.Passed},
        6: {id: 6, status: RuleStatus.Failed},
        7: {id: 7, status: RuleStatus.Failed},
        8: {id: 8, status: RuleStatus.Failed}
      };
      const statusFilter = StatusFilter.Failed;

      expect(
        fromApplicants.getAllNavigationRules.projector(applicants, ruleEntities, statusFilter)
      ).toEqual([2, 6, 7, 8]);
    });
  });
});
