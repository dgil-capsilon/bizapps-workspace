import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Dictionary} from '@ngrx/entity';
import {Store} from '@ngrx/store';
import {AppState} from '@redux/app.reducer';
import {LoadApplicants, LoadApplicantsFailure, LoadApplicantsSuccess, SetApplicant} from '@redux/applicants/applicants.actions';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {FolderReloadAction, SetLoanStatus, SetStatusAction, SetStatusFilter} from '@redux/folder/folder.actions';
import {LoadRule, LoadRuleSuccess} from '@redux/rule-details/rule-details.actions';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';
import {NoRules, SelectRule, SetNextFailedRule, SetRules} from '@redux/rules/rules.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {of} from 'rxjs';
import {map, mapTo, mergeMap, skip, withLatestFrom} from 'rxjs/operators';
import {ApplicantDataApi} from '@models/data-api.interface';
import {RuleCode, RuleStatus, StatusFilter} from '@models/data.enum';
import {ApplicantData, ApplicantRule, FolderData} from '@models/data.interface';
import {FolderStatus} from '@models/enums/folder-status';
import {DataService} from '@app/services/data.service';
import {catchErrorWithLogging} from '@app/services/logger-utils';

@Injectable()
export class ApplicantsEffects {

  @Effect()
  reloadApplicantsData$ = this.actions$
    .pipe(
      ofType(FolderReloadAction),
      mergeMap(() => this.dataService.loadInitialData()
        .pipe(
          mergeMap((data) => {
            const aggregatedRules = this.getAggregatedRules(data);

            data.applicants = this.mapApplicantRulesToIds(data.applicants);

            return [
              SetLoanStatus({loanStatus: data.loanStatus}),
              SetRules({rules: aggregatedRules}),
              LoadApplicantsSuccess({applicants: data.applicants as ApplicantData[]})
            ];
          }),
          catchErrorWithLogging(of(LoadApplicantsFailure()))
        ))
    );

  @Effect()
  loadApplicantsData$ = this.actions$
    .pipe(
      ofType(LoadApplicants),
      mergeMap(() => this.dataService.loadInitialData()
        .pipe(
          mergeMap((data) => {
            const hasAnyFailedRule = data.applicants.some(applicant => applicant.rules.some(rule => rule.status === RuleStatus.Failed));
            const statusFilter = hasAnyFailedRule ? StatusFilter.Failed : StatusFilter.All;
            const aggregatedRules = this.getAggregatedRules(data);

            data.applicants = this.mapApplicantRulesToIds(data.applicants);

            return [
              SetLoanStatus({loanStatus: data.loanStatus}),
              SetStatusFilter({statusFilter: statusFilter}),
              SetRules({rules: aggregatedRules}),
              LoadApplicantsSuccess({applicants: data.applicants as ApplicantData[]}) // TODO: Check if cast is needed
            ];
          }),
          catchErrorWithLogging(of(LoadApplicantsFailure()))
        ))
    );

  @Effect()
  onLoadSuccess$ = this.actions$
    .pipe(
      ofType(LoadApplicantsSuccess),
      withLatestFrom(
        this.store$.select(fromApplicants.getAllNavigationRules),
        this.store$.select(fromRules.getRuleEntities),
        this.store$.select(fromRuleDetails.getRuleDetailsCode),
        this.store$.select(fromApplicants.getSelectedApplicantId),
        this.store$.select(fromRules.getNextFailedRule)
      ),
      mergeMap(([, navigationRules, ruleEntities, ruleDetailsCode, currentApplicantId, failedRuleId]) => {
        let rule;
        if (ruleDetailsCode) {
          rule = this.getRuleByRuleCodeAndApplicant(navigationRules, ruleEntities, ruleDetailsCode, currentApplicantId);
        } else {
          rule = this.getFirstFailedRule(navigationRules, ruleEntities);
        }

        if (!rule && failedRuleId) {
          rule = ruleEntities[failedRuleId];
        }

        if (!rule) {
          rule = ruleEntities[navigationRules[0]];
        }

        const applicantId = rule ? rule.applicant : null;
        const ruleId = rule ? rule.id : null;

        return [
          SetApplicant({id: applicantId}),
          SelectRule({id: ruleId}),
          LoadRule()
        ];
      })
    );

  @Effect()
  updateApplicantAndRule$ = this.actions$
    .pipe(
      ofType(SetStatusFilter),
      skip(1),
      withLatestFrom(
        this.store$.select(fromApplicants.getSelectedApplicantId),
        this.store$.select(fromApplicants.getApplicantEntities),
        this.store$.select(fromRules.getSelectedRuleId),
        this.store$.select(fromRules.getRuleEntities),
        this.store$.select(fromApplicants.getAllNavigationRules)
      ),
      mergeMap(([action, selectedApplicantId, applicantEntities, selectedRuleId, ruleEntities, navRules]) => {
        if (!navRules.length) {
          return [
            LoadRuleSuccess({rule: null}),
            NoRules()
          ];
        }

        const {applicantId, ruleId} = this.getApplicantAndRule(
          action.statusFilter, selectedRuleId, selectedApplicantId, ruleEntities, applicantEntities, navRules
        );

        return [
          SetApplicant({id: applicantId}),
          SelectRule({id: ruleId as any}),
          LoadRule()
        ];
      })
    );

  @Effect()
  onLoadSuccessUpdateStatus$ = this.actions$.pipe(
    ofType(LoadApplicantsSuccess),
    withLatestFrom(
      this.store$.select(fromApplicants.getTotalApplicants),
      this.store$.select(fromRules.getAllRulesNotApplicable)
    ),
    map(([, applicantsAmount, allRulesNotApplicable]) => {
      if (!applicantsAmount) {
        return SetStatusAction({status: FolderStatus.NoBorrowerSelected});
      }

      if (allRulesNotApplicable) {
        return SetStatusAction({status: FolderStatus.NotApplicable});
      }

      return SetStatusAction({status: FolderStatus.Fine});
    })
  );

  @Effect()
  onLoadFailureUpdateStatus$ = this.actions$.pipe(
    ofType(LoadApplicantsFailure),
    mapTo(SetStatusAction({status: FolderStatus.NoIncomeAvailable})),
  );

  @Effect()
  setNextFailedRuleId$ = this.actions$
    .pipe(
      ofType(SelectRule),
      withLatestFrom(this.store$.select(fromApplicants.getAllNavigationRules)),
      map(([action, ruleIds]) => {
        const id = ruleIds[ruleIds.indexOf(action.id) - 1];

        return SetNextFailedRule({id: id});
      })
    );

  private getApplicantAndRule(statusFilter: StatusFilter,
                              selectedRuleId: number,
                              selectedApplicantId: number,
                              ruleEntities: Dictionary<ApplicantRule>,
                              applicantEntities: Dictionary<ApplicantData>,
                              navRules: number[]) {
    if (ruleEntities[selectedRuleId]) {
      const statusToCheck = statusFilter === StatusFilter.Waived ? RuleStatus.Waived : RuleStatus.Failed;
      const specifiedRuleAlreadySelected = ruleEntities[selectedRuleId].status === statusToCheck;

      if (specifiedRuleAlreadySelected) {
        return {applicantId: selectedApplicantId, ruleId: selectedRuleId};
      }

      const firstSpecifiedRuleId = applicantEntities[selectedApplicantId].rules
        .find(ruleId => ruleEntities[ruleId as any].status === statusToCheck);
      if (firstSpecifiedRuleId) {
        return {applicantId: selectedApplicantId, ruleId: firstSpecifiedRuleId};
      }

    }

    // Selected applicant doesn't have specified rules
    const firstAvailableSpecifiedRuleId = navRules[0];
    const {applicant: applicantId} = ruleEntities[firstAvailableSpecifiedRuleId];

    return {applicantId, ruleId: firstAvailableSpecifiedRuleId};
  }

  /**
   * Gather applicants rules into single array
   * Extend rule with applicant id
   */
  private getAggregatedRules(folderData: FolderData) {
    return folderData.applicants.reduce((rules: ApplicantRule[], applicant: ApplicantData) => [
      ...rules,
      ...applicant.rules.map(rule => ({...rule, applicant: applicant.applicantId}))
    ], []);
  }

  /**
   * Normalization - map applicant rules to id
   */
  private mapApplicantRulesToIds(applicants: ApplicantDataApi[]) {
    return applicants.map(applicant => ({...applicant, rules: applicant.rules.map(r => r.id) as any}));
  }

  private getFirstFailedRule(rules: number[], ruleEntities: Dictionary<ApplicantRule>): ApplicantRule | null {
    const ruleId = rules.find(id => ruleEntities[id].status === RuleStatus.Failed);
    return ruleEntities[ruleId] || null;
  }

  private getRuleByRuleCodeAndApplicant(rules: number[], ruleEntities: Dictionary<ApplicantRule>, ruleCode: RuleCode, applicant: number) {
    const ruleId = rules.find(id => ruleEntities[id].ruleId === ruleCode && ruleEntities[id].applicant === applicant);
    return ruleEntities[ruleId] || null;
  }

  constructor(private actions$: Actions,
              private store$: Store<AppState>,
              private dataService: DataService) {
  }
}
