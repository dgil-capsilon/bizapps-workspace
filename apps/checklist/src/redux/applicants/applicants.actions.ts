import {createAction, props} from '@ngrx/store';
import {ApplicantData} from '@models/data.interface';

export const LoadApplicants = createAction(
  '[Applicants API] LOAD'
);

export const LoadApplicantsFailure = createAction(
  '[Applicants] LOAD_FAILURE'
);

export const LoadApplicantsSuccess = createAction(
  '[Applicants] LOAD_SUCCESS',
  props<{ applicants: ApplicantData[] }>()
);

export const SetApplicant = createAction(
  '[Applicants] SET APPLICANT',
  props<{ id: number }>()
);
