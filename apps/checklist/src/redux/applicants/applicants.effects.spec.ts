import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Dictionary} from '@ngrx/entity';
import {Action, Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AppState} from '@redux/app.reducer';
import {LoadApplicants, LoadApplicantsFailure, LoadApplicantsSuccess, SetApplicant} from '@redux/applicants/applicants.actions';
import {ApplicantsEffects} from '@redux/applicants/applicants.effects';
import * as fromApplicants from '@redux/applicants/applicants.selectors';
import {ApplicantsAdapterSelector} from '@redux/applicants/applicants.selectors';
import {SetStatusAction, SetStatusFilter} from '@redux/folder/folder.actions';
import * as fromFolder from '@redux/folder/folder.selectors';
import {LoadRule} from '@redux/rule-details/rule-details.actions';
import * as fromRuleDetails from '@redux/rule-details/rule-details.selectors';
import {SelectRule, SetRules} from '@redux/rules/rules.actions';
import * as fromRules from '@redux/rules/rules.selectors';
import {RulesAdapterSelector} from '@redux/rules/rules.selectors';
import {cold, hot} from 'jasmine-marbles';
import {Observable, of, throwError} from 'rxjs';
import {RuleCode, RuleStatus, StatusFilter} from '../../app/models/data.enum';
import {ApplicantData, ApplicantRule} from '../../app/models/data.interface';
import {FolderStatus} from '@models/enums/folder-status';
import {DataService} from '@app/services/data.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('ApplicantsEffects', () => {
  let actions$: Observable<Action>;
  let effects: ApplicantsEffects;
  let dataServiceSpy: SpyObj<DataService>;
  let store: MockStore<any>;

  beforeEach(() => {
    dataServiceSpy = createSpyObj('DataService', ['loadInitialData']);

    TestBed.configureTestingModule({
      providers: [
        ApplicantsEffects,
        provideMockActions(() => actions$),
        provideMockStore({initialState: {rules: {}, ruleDetails: {rule: {}}}}),
        {provide: DataService, useValue: dataServiceSpy}
      ]
    });

    effects = TestBed.get<ApplicantsEffects>(ApplicantsEffects);
    store = TestBed.get<Store<AppState>>(Store);

    store.overrideSelector(fromApplicants.getAllApplicants as ApplicantsAdapterSelector<ApplicantData[]>, []);
    store.overrideSelector(fromApplicants.getAllNavigationRules, []);
    store.overrideSelector(fromApplicants.getApplicantEntities as ApplicantsAdapterSelector<Dictionary<ApplicantData>>, {});
    store.overrideSelector(fromApplicants.getAllFailedRules, []);
    store.overrideSelector(fromRules.getAllRulesNotApplicable, false);
    store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {});
    store.overrideSelector(fromFolder.getStatusFilter, StatusFilter.Failed);
    store.overrideSelector(fromApplicants.getSelectedApplicantId, 0);
  });

  it('onLoadFailureUpdateStatus$ should set status "no income available"', () => {
    actions$ = of(LoadApplicantsFailure);

    effects.onLoadFailureUpdateStatus$.subscribe(action => {
      expect(action).toEqual(SetStatusAction({status: FolderStatus.NoIncomeAvailable}));
    });
  });

  it('onLoadSuccessUpdateStatus$ should set status "no borrower selected"', () => {
    store.overrideSelector(fromApplicants.getTotalApplicants as ApplicantsAdapterSelector<number>, 0);

    actions$ = of(LoadApplicantsSuccess);

    effects.onLoadFailureUpdateStatus$.subscribe(action => {
      expect(action).toEqual(SetStatusAction({status: FolderStatus.NoBorrowerSelected}));
    });
  });

  it('onLoadSuccessUpdateStatus$ should set status "no borrower selected"', () => {
    store.overrideSelector(fromApplicants.getTotalApplicants as ApplicantsAdapterSelector<number>, 1);
    store.overrideSelector(fromRules.getAllRulesNotApplicable, true);

    actions$ = of(LoadApplicantsSuccess);

    effects.onLoadFailureUpdateStatus$.subscribe(action => {
      expect(action).toEqual(SetStatusAction({status: FolderStatus.NotApplicable}));
    });
  });


  it('onLoadSuccessUpdateStatus$ should set status "fine"', () => {
    store.overrideSelector(fromApplicants.getTotalApplicants as ApplicantsAdapterSelector<number>, 1);
    store.overrideSelector(fromRules.getAllRulesNotApplicable, false);

    actions$ = of(LoadApplicantsSuccess);

    effects.onLoadFailureUpdateStatus$.subscribe(action => {
      expect(action).toEqual(SetStatusAction({status: FolderStatus.Fine}));
    });
  });

  describe('loadApplicantsData$', () => {
    xit('should set rules and applicants', () => {
      dataServiceSpy.loadInitialData.and.returnValue(of({
        applicants: [
          {applicantId: 1, rules: [{id: 1, status: RuleStatus.Passed}, {status: RuleStatus.Failed}]},
          {applicantId: 2, rules: [{id: 2, status: RuleStatus.NotEvaluated}, {id: 3, status: RuleStatus.NotApplicable}]}
        ]
      } as any));
      actions$ = hot('a', {a: LoadApplicants});

      const expected = cold('(abc)', {
        a: jasmine.anything(),
        b: SetRules({
          rules: [
            {id: 1, status: RuleStatus.Passed, applicant: 1},
            {status: RuleStatus.Failed, applicant: 1},
            {id: 2, status: RuleStatus.NotEvaluated, applicant: 2},
            {id: 3, status: RuleStatus.NotApplicable, applicant: 2}
          ] as any
        }),
        c: LoadApplicantsSuccess({
          applicants: [
            {applicantId: 1, rules: [1]},
            {applicantId: 2, rules: [2, 3]}
          ] as any
        })
      });
      expect(effects.loadApplicantsData$).toBeObservable(expected);
    });

    it('should catch error when initial data request throw', () => {
      spyOn(console, 'error');
      dataServiceSpy.loadInitialData.and.returnValue(throwError('Some error on initial data request'));

      actions$ = of(LoadApplicants);

      effects.loadApplicantsData$.subscribe(action => {
        expect(action).toEqual(LoadApplicantsFailure());
      });
    });

  });

  describe('onLoadSuccess$', () => {
    it('should set status, applicant, rule, and load it (failed rule)', () => {
      store.overrideSelector(fromApplicants.getAllApplicants as ApplicantsAdapterSelector<ApplicantData[]>,
        [{} as ApplicantData]
      );
      store.overrideSelector(fromApplicants.getAllNavigationRules, [1, 2, 3]);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        1: {id: 1, status: RuleStatus.Passed} as ApplicantRule,
        2: {id: 2, status: RuleStatus.Failed, applicant: 333} as ApplicantRule,
        3: {id: 3, status: RuleStatus.Passed} as ApplicantRule
      });

      actions$ = hot('a', {a: LoadApplicantsSuccess});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 333}),
        b: SelectRule({id: 2}),
        c: LoadRule()
      });
      expect(effects.onLoadSuccess$).toBeObservable(expected);
    });

    it('should set status, applicant, rule, and load it (initially failed rule)', () => {
      store.overrideSelector(fromApplicants.getAllApplicants as ApplicantsAdapterSelector<ApplicantData[]>,
        [{} as ApplicantData]
      );
      store.overrideSelector(fromApplicants.getAllNavigationRules, [1, 2, 3]);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        1: {id: 1, status: RuleStatus.Failed, applicant: 333} as ApplicantRule,
        2: {id: 2, status: RuleStatus.Passed} as ApplicantRule,
        3: {id: 3, status: RuleStatus.Passed} as ApplicantRule
      });

      actions$ = hot('a', {a: LoadApplicantsSuccess});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 333}),
        b: SelectRule({id: 1}),
        c: LoadRule()
      });
      expect(effects.onLoadSuccess$).toBeObservable(expected);
    });

    it('should set status, applicant, rule, and load it (passed rule)', () => {
      store.overrideSelector(fromApplicants.getAllApplicants as ApplicantsAdapterSelector<ApplicantData[]>,
        [{} as ApplicantData]
      );
      store.overrideSelector(fromApplicants.getAllNavigationRules, [1, 2, 3]);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        1: {id: 1, status: RuleStatus.Passed, applicant: 111} as ApplicantRule,
        2: {id: 2, status: RuleStatus.Passed} as ApplicantRule,
        3: {id: 3, status: RuleStatus.Passed} as ApplicantRule
      });

      actions$ = hot('a', {a: LoadApplicantsSuccess});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 111}),
        b: SelectRule({id: 1}),
        c: LoadRule()
      });
      expect(effects.onLoadSuccess$).toBeObservable(expected);
    });

    it('should set status, applicant, rule, and load it (folder event refresh)', () => {
      store.overrideSelector(fromApplicants.getAllApplicants as ApplicantsAdapterSelector<ApplicantData[]>,
        [{} as ApplicantData]
      );
      store.overrideSelector(fromApplicants.getAllNavigationRules, [16]);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        16: {id: 16, status: RuleStatus.Passed, applicant: 676, ruleId: 'INC-100' as RuleCode} as ApplicantRule
      });
      store.overrideSelector(fromRules.getSelectedRuleId, 16);
      store.overrideSelector(fromRuleDetails.getRuleDetailsCode, 'INC-100' as RuleCode);

      actions$ = hot('a', {a: LoadApplicantsSuccess});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 676}),
        b: SelectRule({id: 16}),
        c: LoadRule()
      });
      expect(effects.onLoadSuccess$).toBeObservable(expected);
    });
  });

  xdescribe('updateApplicantAndRule$', () => {
    it('should set current applicant, current rule then load it', () => {
      store.overrideSelector(fromApplicants.getSelectedApplicantId, 46);
      store.overrideSelector(fromRules.getSelectedRuleId, 89);

      actions$ = hot('a', {a: SetStatusFilter({statusFilter: StatusFilter.All})});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 46}),
        b: SelectRule({id: 89}),
        c: LoadRule()
      });
      expect(effects.updateApplicantAndRule$).toBeObservable(expected);
    });

    it('should set current applicant, current rule then load it (current rule - failed)', () => {
      store.overrideSelector(fromApplicants.getSelectedApplicantId, 46);
      store.overrideSelector(fromRules.getSelectedRuleId, 89);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>,
        {89: {status: RuleStatus.Failed} as ApplicantRule}
      );

      actions$ = hot('a', {a: SetStatusFilter({statusFilter: StatusFilter.Failed})});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 46}),
        b: SelectRule({id: 89}),
        c: LoadRule()
      });
      expect(effects.updateApplicantAndRule$).toBeObservable(expected);
    });

    it('should set current applicant, find first failed rule then load it', () => {
      store.overrideSelector(fromApplicants.getSelectedApplicantId, 46);
      store.overrideSelector(fromApplicants.getApplicantEntities as ApplicantsAdapterSelector<Dictionary<ApplicantData>>,
        {46: {rules: [89, 16] as any} as ApplicantData}
      );
      store.overrideSelector(fromRules.getSelectedRuleId, 89);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        89: {status: RuleStatus.Passed} as ApplicantRule,
        16: {status: RuleStatus.Failed} as ApplicantRule
      });

      actions$ = hot('a', {a: SetStatusFilter({statusFilter: StatusFilter.Failed})});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 46}),
        b: SelectRule({id: 16}),
        c: LoadRule()
      });
      expect(effects.updateApplicantAndRule$).toBeObservable(expected);
    });

    it('should find first failed rule, get applicant then load it', () => {
      store.overrideSelector(fromApplicants.getSelectedApplicantId, 46);
      store.overrideSelector(fromApplicants.getApplicantEntities as ApplicantsAdapterSelector<Dictionary<ApplicantData>>,
        {46: {rules: [89] as any} as ApplicantData}
      );
      store.overrideSelector(fromRules.getSelectedRuleId, 89);
      store.overrideSelector(fromRules.getRuleEntities as RulesAdapterSelector<Dictionary<ApplicantRule>>, {
        89: {status: RuleStatus.Passed} as ApplicantRule,
        49: {status: RuleStatus.Failed, applicant: 12} as ApplicantRule
      });
      store.overrideSelector(fromApplicants.getAllFailedRules, [49]);

      actions$ = hot('a', {a: SetStatusFilter({statusFilter: StatusFilter.Failed})});

      const expected = cold('(abc)', {
        a: SetApplicant({id: 12}),
        b: SelectRule({id: 49}),
        c: LoadRule()
      });
      expect(effects.updateApplicantAndRule$).toBeObservable(expected);
    });
  });

});
